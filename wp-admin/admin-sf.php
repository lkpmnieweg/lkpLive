<?php
session_start();
ini_set('display_errors',1);
error_reporting(E_ALL);

/** Load WordPress Bootstrap */
require_once dirname( __DIR__ ) . '/wp-load.php';

/** Allow for cross-domain requests (from the front end). */
send_origin_headers();

header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
header( 'X-Robots-Tag: noindex' );


require_once( __DIR__ . '/../wp-content/themes/laadkompas/funnel.class.php');
require_once( __DIR__ . '/../wp-content/themes/laadkompas/Cryptor.class.php');
$funnel = new Funnel();

$keyItem = $_POST;

$current_funnel = $funnel->decryptSession();

var_dump($current_funnel);
if(isset($keyItem['lastname']) && 
        isset($keyItem['firstname']) && 
        isset($keyItem['phone']) &&
        isset($keyItem['emailadres']) &&
        isset($keyItem['name_title']))
    {
        $title = sanitize_text_field($keyItem['name_title']);
        $firstname = sanitize_text_field($keyItem['firstname']);
        $name = sanitize_text_field($keyItem['lastname']);
        $phone = sanitize_text_field($keyItem['phone']);
        $email = $keyItem['emailadres'];
        $comments = sanitize_text_field($keyItem['comments']);

        $agree_ev = false;
        if(isset($keyItem['akkoord_evmarkt']) && $keyItem['akkoord_evmarkt'] == "yes")
        {
            $agree_ev = true;
        }
        
        $useCars = "nee";
        $allowedCars = array(
            'Kona', 
            'I-Pace',
            'e-Niro',
            'e-Soul',
            'ZS EV',
            'Leaf',
            'Leaf e+',
            'Ampera-e'
        );

        //override for cars
        if($current_funnel->singleDouble == "enkel" &&
            $current_funnel->smart == "ja" && 
            in_array($current_funnel->car_type, $allowedCars))
        {
            $useCars = "ja";
        }

        //override double = always smart
        if($current_funnel->singleDouble == "dubbel")
            $current_funnel->smart = "ja";

        $smart = $current_funnel->smart;

        //override when single & smart = advice me, set to no so gives cheapest model (lkt316-0)
        if($current_funnel->singleDouble == "enkel" && $current_funnel->smart == "Weet niet / Adviseer mij")
        {
            $smart = "nee";
        }

        //find correct vergelijking-url
        $vergelijking = get_posts(array(
            'numberposts'   => -1,
            'post_type'     => 'quote',
            'meta_query'    => array(
                'relation'      => 'AND',
                array(
                    'key' => 'slim__nietslimme_laadpaal',
                    'value'     => $smart,//$current_funnel->smart,
                    'compare'   => '=',
                ),
                array(
                    'key'  => 'enkele__dubbele_laadpaal',
                    'value'     => $current_funnel->singleDouble,
                    'compare'   => '=',
                ),
                array(
                    'key'  => 'gebruik_voertuigen_tabel',
                    'value'     => $useCars,
                    'compare'   => '=',
                )
            ),
        ));

        $foundQuoteUrl = null;
        // foreach($vergelijking as $vergelijk)
        // {
           
        //     $foundQuoteUrl = get_the_permalink($vergelijk->ID);
        // }

       
        if(isset($vergelijking[0]->ID))
            $foundQuoteUrl = get_the_permalink($vergelijking[0]->ID);


        // if($_SERVER['REMOTE_ADDR'] == '217.121.83.181')
        // {
        //     var_dump($foundQuoteUrl);
        //     exit;
        // }


        if($foundQuoteUrl == null)
            $foundQuoteUrl = '';

        $found = null;
        $found = get_the_permalink(5882);



        $quoteUrl = $foundQuoteUrl;
        $funnel = new Funnel(
            $current_funnel->car_brand, 
            $current_funnel->car_type, 
            $current_funnel->location, 
            $current_funnel->install, 
            $current_funnel->smart, 
            $current_funnel->singleDouble, 
            $current_funnel->extra_info_laadtijden, 
            $current_funnel->extra_info_personalisatie, 
            $current_funnel->extra_info_smartcharging,
            $current_funnel->extra_info_facturatie,
            $title,
            $name,
            $phone,
            $email,
            $comments,
            $agree_ev,
            $quoteUrl,
            $firstname
        );
        $funnel->encryptSession();
        if($foundQuoteUrl){
            include_once('/home/dffbc54067/domains/laadkompas.nl/public_html/wp-content/themes/laadkompas/sforce/SalesForce.class.php');
            $sforce = new SalesForce();
            $postAPI = $sforce->postToAPI($funnel);
            if($postAPI)
            {
                print_r($current_funnel);
                $current_funnel->clearSession();
            }
        }
    }
/*header("Access-Control-Allow-Origin: *");

ini_set('memory_limit','512M');
ini_set('display_errors',1);
error_reporting(E_ALL);


if(isset($_POST['name']) && isset($_POST['mobile']))
{
	include_once("SForce/api.php");
	$soap = new SalesForceLaadKompas();

	//init api
	$loginData = $soap->_WSDLLogin();


	//add lead
	$nawData = array(
		'locatielaadpaal' => $_POST['charging_station_location'],
		'title' => $_POST['heer'],
		'auto' => $_POST['car_type'],
		'LaadstationInstallatie__c' => $_POST['installatie'],
		'GewensteLaadpaal__c' => $_POST['model'],
		'SlimmeLaadpaal__c' => $_POST['laadstation'],
		'firstname' => $_POST['firstname'],
		'lastname' => $_POST['name'],
		'tel' => $_POST['mobile'],
		'email' => $_POST['emailadres'],
		'extra' => $_POST['extra_info'],
		'vergelijk_url' => $_POST['vergelijk_url']
	);


	var_dump('lead:');
	$lead = $soap->_WSDLLead($loginData, $nawData);
	var_dump($lead);
	exit;
}*/

?>