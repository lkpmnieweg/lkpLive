<?php

/**
 * Executing Ajax process.
 *
 * @since 2.1.0
 */
define( 'DOING_AJAX', true );
if ( ! defined( 'WP_ADMIN' ) ) {
	define( 'WP_ADMIN', true );
}

/** Load WordPress Bootstrap */
require_once dirname( __DIR__ ) . '/wp-load.php';

/** Allow for cross-domain requests (from the front end). */
send_origin_headers();

// header( 'Content-Type: application/text; charset=' . get_option( 'blog_charset' ) );
header( 'X-Robots-Tag: noindex' );


global $wpdb;
$table_name = 'lkp_lead_counter';

$postId = (isset($_POST['item']))? (int)$_POST['item'] : 1;

$wpdb->query($wpdb->prepare("UPDATE $table_name SET counter=counter+1 WHERE id=".$postId));

$result = $wpdb->get_results( "SELECT id,counter FROM lkp_lead_counter WHERE id = ".$postId, OBJECT );

echo($result[0]->counter);
exit;
?>