<?php

class ZonFunnel {
	public $consumption;
	public $preference;
	public $gotchp;


	public $title;
	public $firstname;
	public $name;
	public $phone;
	public $email;
	public $comments;
	public $agree_ev;

	public $quoteUrl;

	public $optin_newsletter;
	public $optin_commercial;

	public $newsletter_txt;
	public $commercial_txt;

	public $zipcode;
	public $housenr;

	public $fullAddress;

	public function __construct(
		$consumption = null,
		$preference = null,
		$gotchp = null,
		$title = null,
		$name = null,
		$phone = null,
		$email = null,
		$comments = null,
		$agree_ev = null,
		$quoteUrl = null,
		$firstname = null,
		$zipcode = null,
		$housenr = null,
		$fullAddress = null
	)
	{

		if($consumption != null)
			$this->consumption = $consumption;
		if($preference != null)
			$this->preference = $preference;
		if($gotchp != null)
			$this->gotchp = $gotchp;

		$this->title = '';

		if($name != null)
			$this->name = $name;
		if($phone != null)
			$this->phone = $phone;
		if($email != null)
			$this->email = $email;
		if($comments != null)
			$this->comments = $comments;
		if($agree_ev != null)
			$this->agree_ev = $agree_ev;
		if($quoteUrl != null)
			$this->quoteUrl = $quoteUrl;


		if($firstname != null)
			$this->firstname = $firstname;

		if($zonnepanelen != null)
			$this->zonnepanelen = $zonnepanelen;

		if($optin_newsletter != null)
			$this->optin_newsletter = $optin_newsletter;
		if($optin_commercial != null)
			$this->optin_commercial = $optin_commercial;

		
		if($zipcode != null)
			$this->zipcode = $zipcode;
		if($housenr != null)
			$this->housenr = $housenr;

		if($fullAddress != null)
			$this->fullAddress = $fullAddress;
	}

	public function decryptSession()
	{
		@session_start();

		$return = unserialize(decryptData($_SESSION['lkp_current_funnel_zon']));

		return $return;
	}

	public function encryptSession()
	{
		@session_start();

		$encrypt = encryptData(serialize($this));

		$_SESSION['lkp_current_funnel_zon'] = $encrypt;
		return true;
	}

	public function clearSession()
	{
		unset($_SESSION['lkp_current_funnel_zon']);
	}

}

?>