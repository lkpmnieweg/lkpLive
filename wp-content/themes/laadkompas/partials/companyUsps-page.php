<section class="companyUsps">
	<div class="container">
		<h2>Waarom Laadkompas?</h2>
		<div class="uspContainer clearfix">
			<div class="uspContainerLeft">
				<div class="usp clearfix">
					<div class="usp-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/nummer-1-van-nl-illo-bold@2x.png" alt="Persoonlijk Advies">
					</div>
					<div class="usp-content">
                       
			<?php
			$leadCount = "11.000";
			global $wpdb;
			$result = $wpdb->get_results( "SELECT id,counter FROM lkp_lead_counter WHERE id = 1", OBJECT );
			if($result && $result[0]->counter != null) {
				$leadCount = number_format($result[0]->counter, 0, ',', '.');
			}
			?>
						<h3>De #1 van Nederland</h3>
                        <p>Sinds 2015 al meer dan <span class="request_count"><?php echo $leadCount; ?></span> geholpen met de aanschaf van een laadpaal. Daarmee zijn wij veruit de grootste van Nederland.</p>
					</div>
				</div>
				<div class="usp clearfix">
					<div class="usp-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/advies-illo.svg" alt="Hoge klanttevredenheid">
					</div>
					<div class="usp-content">
						<h3>Persoonlijk advies</h3>
						<p>Onze experts geven u graag persoonlijk advies over producten, installaties, subsidies en belastingvoordelen.</p>
					</div>
				</div>
			</div>
			<div class="uspContainerRight">
				<div class="usp clearfix">
					<div class="usp-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/klanttevredenheid-illo.svg" alt="Persoonlijk Advies">
					</div>
					<div class="usp-content">
                        <h3>Hoge klanttevredenheid</h3>
						<p>Onze klanten zijn tevreden met ons en dat laten ze zien door ons te beoordelen met een <span>9,2</span>! Een sprekend gemiddelde na <?php echo wp_count_posts('reviews')->publish; ?> beoordelingen.</p>
					</div>
				</div>
				<div class="usp clearfix">
					<div class="usp-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/group-72.svg" alt="Installatie door experts">
					</div>
					<div class="usp-content">
						<h3>Experts in opladen</h3>
						<p>Advies en installatie wordt door experts uitgevoerd. Zo zijn al onze monteurs gecertificeerd en op de hoogte van de laatste ontwikkelingen.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>	