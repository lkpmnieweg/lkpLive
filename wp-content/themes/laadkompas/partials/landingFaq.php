<?php

if(get_field('toon_veelgestelde_vragen_block') == "on"):

?>
<div class="container ---Style ---LandingFaq">
	<div class="title">
		<h2 class="first"><?php echo get_field('veelgestelde_vragen_titel'); ?></h2>
		<h2><?php echo get_field('veelgestelde_vragen_ondertitel'); ?></h2>
	</div>	
	<div class="content-items">
		<?php
		for($i=1; $i<=4; $i+=1):
			if(get_field('veelgestelde_vraag_'.$i) != ""):
		?>

		<div class="collapse" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
			<a class="txt_title" href="javascript:void(0);"><span itemprop="name"><?php echo get_field('veelgestelde_vraag_'.$i); ?></span> <img src="https://laadkompas.nl/wp-content/themes/laadkompas/images/arrow-down-icon.svg" alt="Pijl"></a>
			<div itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
				<div itemprop="text">
					<?php echo get_field('veelgestelde_vraag_antwoord_'.$i); ?>
				</div>
			</div>
		</div>

		<?php 
			endif;
		endfor;
		?>
	</div>
</div>
<?php
endif;
?>