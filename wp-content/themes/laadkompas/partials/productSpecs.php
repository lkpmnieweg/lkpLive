<?php
global $product;

$hardwareIds = array(
	'pa_mincap',
	'pa_maxcap',
	'pa_internetaansluiting',
	'pa_rfidreader',
	'pa_fase',
	'pa_ampere',
	'pa_contactdoos',
	'pa_ipwaarde',
	'pa_statusweergave',
	'pa_breedte',
	'pa_diepte',
	'pa_hoogte',
	'pa_materiaal',
	'pa_mintemp',
	'pa_maxtemp',
	'pa_luchtdrukbereik',
	'pa_vochtigheidsgraad'
);

$softwareIds = array(
	'pa_abonnement',
	'pa_gastverbruik',
	'pa_onlineinzicht',
	'pa_laadtarieven',
	'pa_emailnotific',
	'pa_dynpowerman',
	'pa_toegangsbeheer',
	'pa_exportlaad',
	'pa_remotecharg'
);

?>
<div class="column-2">
	<div class="subtitle">Hardware</div>
	<ul>
	<?php
		if($product->sku && $product->sku != ""):
	?>
		<li>
			<span class="attribute">
				<span class="text">Artikelnummer</span>
				<a class="questionPopup" href="javascript:void(0);" data-title="Artikelnummer" data-txt="<?php echo getAttributeDescription('sku');?>"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a>
			</span>
			<span class="value">
			<?php
				echo $product->sku;
			?>
			</span>
			<div class="clearfix"></div>
		</li>
		<?php
		endif;

		echo parseAttributesToTable($product->get_attributes(), $hardwareIds);
		?>
	</ul>
</div>
<div class="column-2">
	<div class="subtitle">Software</div>
	<ul>
		<?php
		echo parseAttributesToTable($product->get_attributes(), $softwareIds);
		?>
	</ul>
<?php
	$button = get_field('tech_specs_datasheet_knop');
	if($button):
?>
	<a class="btn btn-primary" href="<?php echo $button['url']; ?>" title="<?php echo $button['title']; ?>"><?php echo $button['title']; ?></a>
<?php
	endif;
?>
</div>
<div class="clearfix"></div>
