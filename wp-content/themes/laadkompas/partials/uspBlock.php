<section class="company-usps second">
	<div class="container">
		<div class="row">
			<div class="column-4">
				<div class="item">
					<img src="<?php echo get_template_directory_uri(); ?>/images/advies-illo.svg" alt="Persoonlijk advies"><div class="title">Persoonlijk advies</div>
				</div>
			</div>
			<div class="column-4">
				<div class="item">
					<img src="<?php echo get_template_directory_uri(); ?>/images/klanttevredenheid-illo.svg" alt="Blije klanten"><div class="title">Blije<br/> klanten</div>
				</div>
			</div>
			<div class="column-4">
				<div class="item">
					<img src="<?php echo get_template_directory_uri(); ?>/images/laagste-prijs-illo.svg" alt="Laagste prijs garantie"><div class="title">Laagste prijs<br/> garantie</div>
				</div>
			</div>
			<div class="column-4">
				<div class="item">
					<img src="<?php echo get_template_directory_uri(); ?>/images/group-72.svg" alt="Installatie experts"> <div class="title">Installatie experts</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>