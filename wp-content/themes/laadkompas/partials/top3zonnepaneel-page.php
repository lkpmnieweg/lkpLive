<div class="container ---Style --Products">
	<div class="form-group">
		<h1 class="text-center">Zonnepanelen vergelijken</h1>

		<div class="product-item row">
		<?php

		if(get_field('top_3_zonnepaneel_1_titel') != null && get_field('top_3_zonnepaneel_1_titel') != ""):
		?>
		<div class="column-3 product product1">
			<div class="picture">
				<img src="<?php echo get_field('top_3_zonnepaneel_1_foto'); ?>" alt="<?php echo get_field('top_3_zonnepaneel_1_titel'); ?>" />
				<div class="num"><img class="num_img" src="<?php echo get_template_directory_uri(); ?>/images/1@2x.png" alt="Nummer 1" />
                </div>
			</div>
			<div class="details">
				<div class="title"><?php echo get_field('top_3_zonnepaneel_1_titel'); ?></div>
				
				<?php
				if(get_field('top_3_zonnepaneel_1_usps'))
				{
					echo productCategoryPlusMin(get_field('top_3_zonnepaneel_1_usps'),5);
				} else {
					echo '<ul></ul>';
				}
				?>
				<div class="bottom-bar">
					<form action="" method="POST">
					<div class="left">
						<span class="price">&euro; <?php echo number_format(get_field('top_3_zonnepaneel_1_prijs'),2,',','.'); ?> <span class="grey">excl. BTW</span></span>
						<p>Per 10 panelen</p>
					</div>
					<div class="clearfix"></div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php
	endif;
		if(get_field('top_3_zonnepaneel_2_titel') != null && get_field('top_3_zonnepaneel_2_titel') != ""):
			?>
		<div class="column-3 product product2">
			<div class="picture">
				<img src="<?php echo get_field('top_3_zonnepaneel_2_foto'); ?>" alt="<?php echo get_field('top_3_zonnepaneel_2_titel'); ?>" />
				<div class="num"><img class="num_img" src="<?php echo get_template_directory_uri(); ?>/images/2@2x.png" alt="Nummer 2" />
                </div>
			</div>
			<div class="details">
				<div class="title"><?php echo get_field('top_3_zonnepaneel_2_titel'); ?></div>
				
				<?php
				if(get_field('top_3_zonnepaneel_2_usps'))
				{
					echo productCategoryPlusMin(get_field('top_3_zonnepaneel_2_usps'),5);
				} else {
					echo '<ul></ul>';
				}
				?>
				<div class="bottom-bar">
					<form action="" method="POST">
					<div class="left">
						<span class="price">&euro; <?php echo number_format(get_field('top_3_zonnepaneel_2_prijs'),2,',','.'); ?> <span class="grey">excl. BTW</span></span>
						<p>Per 10 panelen</p>
					</div>
					<div class="clearfix"></div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php
	endif;
		if(get_field('top_3_zonnepaneel_3_titel') != null && get_field('top_3_zonnepaneel_3_titel') != ""):
			?>
		<div class="column-3 product product2">
			<div class="picture">
				<img src="<?php echo get_field('top_3_zonnepaneel_3_foto'); ?>" alt="<?php echo get_field('top_3_zonnepaneel_3_titel'); ?>" />
				<div class="num"><img class="num_img" src="<?php echo get_template_directory_uri(); ?>/images/3@2x.png" alt="Nummer 3" />
                </div>
			</div>
			<div class="details">
				<div class="title"><?php echo get_field('top_3_zonnepaneel_3_titel'); ?></div>
				
				<?php
				if(get_field('top_3_zonnepaneel_2_usps'))
				{
					echo productCategoryPlusMin(get_field('top_3_zonnepaneel_3_usps'),5);
				} else {
					echo '<ul></ul>';
				}
				?>
				<div class="bottom-bar">
					<form action="" method="POST">
					<div class="left">
						<span class="price">&euro; <?php echo number_format(get_field('top_3_zonnepaneel_3_prijs'),2,',','.'); ?> <span class="grey">excl. BTW</span></span>
						<p>Per 10 panelen</p>
					</div>
					<div class="clearfix"></div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php
		endif;

		?>
			
			<div class="clearfix"></div>
		</div>
	</div>
</div>