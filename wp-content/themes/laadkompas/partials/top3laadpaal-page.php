<div class="container ---Style --Products">
	<div class="form-group">
		<h1 class="text-center">De top 3 laadpalen van dit moment</h1>

		<div class="product-item row">
		<?php
		//collect all products where show on homepage is on
		$posts = [];

    	$options = get_option( 'lkp_plugin_options' );
    	if(isset($options['toplaadpaal1']))
    	{
    		$posts[] = wc_get_product( (int)$options['toplaadpaal1'] );
    	}
    	if(isset($options['toplaadpaal2']))
    	{
    		$posts[] = wc_get_product( (int)$options['toplaadpaal2'] );
    	}
    	if(isset($options['toplaadpaal3']))
    	{
    		$posts[] = wc_get_product( (int)$options['toplaadpaal3'] );
    	}

		if(count($posts) > 0):
			$num = 1;
			foreach($posts as $product):
				if($product != null):
		?>
		<div class="column-3 product product1">
			<div class="picture">
				<a href="<?php echo get_the_permalink($product->get_ID()); ?>"><img src="<?php echo wp_get_attachment_url( $product->get_image_id() ); ?>" alt="<?php echo $product->get_name(); ?>" /></a>
				<div class="num"><img class="num_img" src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $num;?>@2x.png" alt="Nummer <?php echo $num;?>" />
                </div>
			</div>
			<div class="details">
				<a href="<?php echo get_the_permalink($product->get_ID()); ?>"><?php echo $product->get_name(); ?></a>
				
				<?php
				if(get_field('plus-_en_minpunten_overzichtpagina', $product->get_ID()))
				{
					echo productCategoryPlusMin(get_field('plus-_en_minpunten_overzichtpagina', $product->get_ID()));
				} else {
					echo '<ul></ul>';
				}
				?>
				<div class="bottom-bar">
					<form action="" method="POST">
					<div class="left">
						<span class="price">&euro; <?php echo number_format($product->get_price(),2,',','.'); ?></span>
							<input name="product" data-label="<?php echo $product->get_name(); ?>" type="checkbox" id="product<?php echo $product->get_ID(); ?>">
							<label class="custom-label-wrap" for="product<?php echo $product->get_ID(); ?>">
								<span class="custom-check"></span> <span class="custom-label">Vergelijk</span>
							</label>
					</div>
					<div class="right">
						<a class="btn" href="<?php echo get_the_permalink($product->get_ID()); ?>">Bestel</a>
					</div>
					<div class="clearfix"></div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php
			$num += 1;
				endif;
			endforeach;
		endif;

		?>
			
			<div class="clearfix"></div>
		</div>
	</div>
</div>