<?php

$reviews = new WP_Query( array(
    'post_type' => 'reviews'
  )
);

if(count($reviews->posts) > 0):
?>
<div id="reviews">
	<div class="container">
		<div class="title">
			<h3>Reviews van klanten</h3>
			<h4>Op basis van <?php echo wp_count_posts('reviews')->publish; ?> beoordelingen</h4>
			<img src="<?php echo get_template_directory_uri(); ?>/images/kiyoh@2x.png" alt="Kiyoh" />
		</div>
		<div class="review-slider">
			<ul id="reviewSlider">

			<?php while ( $reviews->have_posts() ) : $reviews->the_post(); global $post; ?>
				<li>
					<span class="stars">
						<?php
        				$stars = get_field('aantal_sterren',$post->ID);
        				if($stars)
        				{
        					$totalStars = (int)$stars;
        					for($i=1; $i<=$totalStars; $i+=1)
        					{
        						echo '<img src="'.get_template_directory_uri().'/images/star.svg" alt="Ster" />';
        					}
        				}
        				?>
					</span>
					<span class="title"><?php the_title(); ?></span>
					<span class="description">
						<?php 
						$reviewContent = str_replace(array('<div class="component rating-description">', '</div>', '<p class="text opinion">', '</p>'), "", get_the_content());
						$maxLength = 150;
						if(strlen($reviewContent) > $maxLength)
						{

							echo substr($reviewContent, 0, $maxLength).'<span class="dots">..</span><span class="expand">'.substr($reviewContent, $maxLength, strlen($reviewContent)).'</span> <a href="javascript:void(0);">Lees meer</a>';
						}
						else {
							echo $reviewContent;
						}
						?>

					</span>
						
					</span>
					<span class="bottom">
						<span class="name"><?php echo get_field('naam',$reviews->ID); ?></span>
						<span class="date"><?php echo get_the_date(); ?></span>
						<span class="clearfix"></span>
					</span>
				</li>
			<?php endwhile; wp_reset_query(); ?>
			</ul>
				<div class="clearfix"></div>
		</div>
		<a class="btn btn-primary" href="https://www.kiyoh.com/reviews/1044005/laadkompas" target="_blank">Lees alle klantbeoordelingen</a>
	</div>	
</div>
<?php
endif;
?>