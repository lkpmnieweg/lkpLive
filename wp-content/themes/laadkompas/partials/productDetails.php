<?php
global $product;

$brand = $product->get_attribute( 'merk' );
?>

<span class="product-title">
	<h1><?php the_title(); ?></h1>
	<?php
		if($brand) echo '<span class="brand">'.$brand.'</span>';
	?>
</span>

<span class="mobile noBottom">
	<strong>Keuze opties</strong>
</span>
<?php 

do_action( 'woocommerce_single_product_summary' );

/*
?>
<!-- 
<ul class="options">
	<li>
		<span class="label">Kleur <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></span>
		<span class="colors">
			<label class="color active"><input type="radio" name="color" value="color1" /><span class="color-inner color1"></span></label>
			<label class="color"><input type="radio" name="color" value="color2" /><span class="color-inner color2"></span></label>
			<label class="color"><input type="radio" name="color" value="color3" /><span class="color-inner color3"></span></label>
			<label class="color"><input type="radio" name="color" value="color4" /><span class="color-inner color4"></span></label>
			<label class="color"><input type="radio" name="color" value="color5" /><span class="color-inner color5"></span></label>
			<div class="clearfix"></div>
		</span>
		<span class="mobile">
			<input disabled="disabled" type="hidden" name="color" value="" />
			<a class="option" data-id="color" data-title="Kleur"><span class="name">Kleur</span> <span class="color"></span> <span class="price">&euro; 0,-</span><img src="<?php echo get_template_directory_uri(); ?>/images/link-zwart.svg" alt="" /></a>
			<span class="options">
				<span class="colors">
					<label> 
						<span class="color"><input type="radio" name="colorMobile" value="color1" /><span class="color-inner color1"></span></span>
						<span class="label">RAL 9010 | Wit</span>
						<span class="price">&euro; 0,-</span>
					</label>
					<label>
						<span class="color"><input type="radio" name="colorMobile" value="color2" /><span class="color-inner color2"></span></span>
						<span class="label">RAL 7016 | Grijs</span>
						<span class="price">&euro; 190,-</span>
					</label>
					<label>
						<span class="color"><input type="radio" name="colorMobile" value="color3" /><span class="color-inner color3"></span></span>
						<span class="label">RAL 9005 | Zwart</span>
						<span class="price">&euro; 190,-</span>
					</label>
					<label>
						<span class="color"><input type="radio" name="colorMobile" value="color4" /><span class="color-inner color4"></span></span>
						<span class="label">RAL 6007 | Groen</span>
						<span class="price">&euro; 190,-</span>
					</label>
					<label> 
						<span class="color"><input type="radio" name="colorMobile" value="color5" /><span class="color-inner color5"></span></span>
						<span class="label">Kleur op maat</span>
						<span class="price">&euro; 230,-</span>
					</label>
				<div class="clearfix"></div>
				</span>
			</span>
		</span>
	</li>
	<li>
		<span class="label">Montage  <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></span>
		<select name="montage">
			<option>Wandmontage &euro; 0,-</option>
		</select>
		<span class="mobile">
			<input disabled="disabled" type="hidden" name="montage" value="" />
			<a class="option" data-id="montage" data-title="Montage"><span class="name">Montage</span> <span class="price">&euro; 0,-</span><img src="<?php echo get_template_directory_uri(); ?>/images/link-zwart.svg" alt="" /></a>
			<span class="options">
				<span class="montage">
					<label> 
						<input type="radio" name="montageMobile" value="geen" />
						<span class="label">Geen</span>
						<span class="price">&euro; 0,-</span>
					</label>
					<label> 
						<input type="radio" name="montageMobile" value="wandmontage" />
						<span class="label">Wandmontage</span>
						<span class="price">&euro; 0,-</span>
					</label>
				</span>
			</span>
		</span>
	</li>
	<li>
		<span class="label">Kabellengte  <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></span>
		<select name="kabellengtte">
			<option>Geen vaste kabel &euro; 0,-</option>
		</select>
		<span class="mobile">
			<input disabled="disabled" type="hidden" name="kabellengte" value="" />
			<a class="option" data-id="kabellengte" data-title="Kabellengte"><span class="name">Kabellengte</span> <span class="price">&euro; 0,-</span><img src="<?php echo get_template_directory_uri(); ?>/images/link-zwart.svg" alt="" /></a>
			<span class="options">
				<span class="kabellengte">
					<label> 
						<input type="radio" name="kabelMobile" value="geen" />
						<span class="label">Geen</span>
						<span class="price">&euro; 0,-</span>
					</label>
					<label> 
						<input type="radio" name="kabelMobile" value="10 meter" />
						<span class="label">10 meter</span>
						<span class="price">&euro; 100,-</span>
					</label>
				</span>
			</span>
		</span>
	</li>
	<li>
		<span class="label">Load balancing  <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></span>
		<select name="loadbalancing">
			<option>Geen &euro; 0,-</option>
		</select>
		<span class="mobile">
			<input disabled="disabled" type="hidden" name="loadbalancing" value="" />
			<a class="option" data-id="loadbalancing" data-title="Load balancing"><span class="name">Load balancing</span> <span class="price">&euro; 0,-</span><img src="<?php echo get_template_directory_uri(); ?>/images/link-zwart.svg" alt="" /></a>
			<span class="options">
				<span class="loadbalancing">
					<label> 
						<input type="radio" name="loadMobile" value="geen" />
						<span class="label">Geen</span>
						<span class="price">&euro; 0,-</span>
					</label>
					<label> 
						<input type="radio" name="loadMobile" value="ja" />
						<span class="label">Load balancing</span>
						<span class="price">&euro; 999,-</span>
					</label>
				</span>
			</span>
		</span>
	</li>
	<li>
		<span class="label">Installatie  <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></span>
		<select name="installatie">
			<option>Geen installatie &euro; 0,-</option>
		</select>
		<span class="mobile">
			<input disabled="disabled" type="hidden" name="installatie" value="" />
			<a class="option" data-id="installatie" data-title="Installatie"><span class="name">Installatie</span> <span class="price">&euro; 0,-</span><img src="<?php echo get_template_directory_uri(); ?>/images/link-zwart.svg" alt="" /></a>
			<span class="options">
				<span class="installatie">
					<label> 
						<input type="radio" name="installMobile" value="geen" />
						<span class="label">Geen</span>
						<span class="price">&euro; 0,-</span>
					</label>
					<label> 
						<input type="radio" name="installMobile" value="ja" />
						<span class="label">Installatie</span>
						<span class="price">&euro; 150,-</span>
					</label>
				</span>
			</span>
		</span>
	</li>
</ul>
<div class="price">
	<span class="productPrice">&euro; 908,26</span>
	<span>excl. BTW per stuk</span>
	<span class="info-balloon grey">Op voorraad</span>
</div>
<input class="btn btn-primary" type="submit" name="addToCart" value="In winkelmand" /> -->
*/?>