<?php

$reviews = new WP_Query( array(
    'post_type' => 'reviews',
    'posts_per_page' => -1
  )
);

if(count($reviews->posts) > 0):
?>
<section class="customerReviews">
	<div class="swiper-container">
	    <div class="swiper-wrapper">

		<?php while ( $reviews->have_posts() ) : $reviews->the_post(); 
				if(get_field('review_type', $reviews->ID) == 'zonnepaneel'):
				?>
			<div class="swiper-slide">
	        	<div class="customerReview clearfix">
	        		<?php
	        		$thumb = get_the_post_thumbnail_url($reviews->ID, 'medium_large', array( 'alt' => get_the_title() )  );
	        		if($thumb != "" && $thumb != null):
	        		?>
	        		<div class="customerReview-image" style="background-image: url(<?php echo $thumb; ?>);"></div>
	        		<?php
	        		endif;
	        		?>
	        		
	        	</div>
	    	</div>
		<?php
			endif;
		 endwhile; wp_reset_query(); ?>
	    </div>
	    <div class="swiper-button-next"></div>
	</div>
	<div class="swiper-pagination"></div>
</section>
<?php
endif;
?>