<?php
global $product;


$related = $product->get_cross_sell_ids();
//
if(count($related) > 0):

$slides = '';

foreach($related as $id)
{
	$_product = wc_get_product( $id );
	if($_product)
	{
	$slides .= '
	<li>
		<a href="'.get_permalink($_product->get_id()).'">
			<span class="picture">
				'.$_product->get_image().'
			</span>
			<span class="title">
				'.$_product->get_name().'
			</span>
			<span class="price">&euro; '.number_format($_product->get_price(), 2, ',', '.').'</span>
		</a>
	</li>';
	}
}
if($slides != ''):
?>
<div class="container product-detail related">
	<h3>Andere laadpalen</h3>
	<div class="product-slider">
		<ul id="relatedSlider">
			<?php
			echo $slides;
			?>
		</ul>
	</div>
</div>
<?php
endif;
endif;
?>