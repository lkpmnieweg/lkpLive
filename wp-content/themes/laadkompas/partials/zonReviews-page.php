<?php

$reviews = new WP_Query( array(
    'post_type' => 'reviews',
    'posts_per_page' => -1
  )
);

if(count($reviews->posts) > 0):
?>
<section class="customerReviews">
	<div class="container">
		<div class="header-wrapper clearfix">
			<div class="customerReviewsHeader-left">
				<h1>Onze klanten<br>geven ons een </h1>
				<p>Op basis van <?php echo wp_count_posts('reviews')->publish; ?> beoordelingen </p>
			</div>
			<div class="customerReviewsHeader-right">
				<img class="kiyohImage" src="<?php echo get_template_directory_uri(); ?>/images/kiyoh-9-2-logo@2x.jpg" alt="Kiyoh" />
			</div>
		</div>
		<div class="swiper-container">
		    <div class="swiper-wrapper">

			<?php 
				while ( $reviews->have_posts() ) : $reviews->the_post(); 
					if(get_field('review_type', $reviews->ID) != 'laadpaal' && get_field('review_type', $reviews->ID) != ''):
				?>
				<div class="swiper-slide">
		        	<div class="customerReview clearfix">
		        		<div class="customerReview-content">
		        			<div class="reviewStars">
		        				<?php
		        				$stars = get_field('aantal_sterren',$reviews->ID);
		        				if($stars)
		        				{
		        					$totalStars = (int)$stars;
		        					for($i=1; $i<=$totalStars; $i+=1)
		        					{
		        						echo '<img src="'.get_template_directory_uri().'/images/star.svg" alt="Ster" />';
		        					}
		        				}
		        				?>
		        			</div>
		        			<h3><?php echo ucfirst(get_the_title()); ?></h3>
		        			<?php echo the_excerpt(); ?>
		        			<div class="reviewer clearfix">
			        			<span class="reviewerName"><?php echo get_field('naam',$reviews->ID); ?></span>
			        			<span class="reviewDate"><?php echo get_the_date(); ?></span>
		        			</div>
		        		</div>
		        		<?php
		        		$thumb = get_the_post_thumbnail_url($reviews->ID, 'medium_large', array( 'alt' => get_the_title() )  );
		        		if($thumb != "" && $thumb != null):
		        		?>
		        		<div class="customerReview-image" style="background-image: url(<?php echo $thumb; ?>);"></div>
		        		<?php
		        		endif;
		        		?>
		        		
		        	</div>
		    	</div>
			<?php 
			endif;
			endwhile; wp_reset_query(); ?>
		    </div>
		    <div class="swiper-button-prev"></div>
		    <div class="swiper-button-next"></div>
		</div>
		<div class="swiper-pagination"></div>
		<div class="moreCustomerReviews">
			<a class="btn" href="https://www.kiyoh.com/reviews/1044005/laadkompas" target="_blank">Lees alle klantbeoordelingen</a>
		</div>
	</div>	
</section>
<?php
endif;
?>