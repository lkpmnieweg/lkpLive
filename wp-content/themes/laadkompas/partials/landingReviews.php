<?php

$reviews = new WP_Query( array(
    'post_type' => 'reviews'
  )
);

if(count($reviews->posts) > 0):
?>
<section class="customerReviews">
	<div class="container">
		<div class="header-wrapper clearfix">
			<div class="customerReviewsHeader-left">
				<h2>Onze klanten<br> geven ons</h2>
				<p>Op basis van <?php echo wp_count_posts('reviews')->publish; ?> beoordelingen </p>
			</div>
			<div class="customerReviewsHeader-right">
				<img class="kiyohImage" src="<?php echo get_template_directory_uri(); ?>/images/kiyoh-9-2-logo@2x.png" alt="Kiyoh">
			</div>
		</div>
		<div class="moreCustomerReviews">
			<div class="stars">
				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
				<img src="<?php echo get_template_directory_uri(); ?>/images/star-half-v3.svg" alt="Ster leeg" />
			</div>
			<a href="https://www.kiyoh.com/reviews/1044005/laadkompas" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.png" alt="Lees alle beoordelingen" /> <span>Lees alle klantbeoordelingen</span></a>
		</div>
	</div>	
</section>
<?php
endif;
?>