<?php
$compare_products = get_query_var('compare_products');
?>
<section id="options" class="specifications options ">
	<div class="spec-title"><span class="arrow down"></span> Opties &amp; Toebehoren</div>
	<div class="spec-info">
		<ul>
			<li><span>Paal</span> <a class="questionPopup" data-title="Paal" data-txt="<?php echo getAttributeDescription('pa_paal'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li class="sub"><span>Hoogte</span></li>
			<li class="sub"><span>Inclusief</span></li>
			<li><span>Wandmontage</span>  <a class="questionPopup" data-title="Wandmontage" data-txt="<?php echo getAttributeDescription('pa_wandmontage'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li class="small"><span>Kabels</span>  <a class="questionPopup" data-title="Kabels" data-txt="<?php echo getAttributeDescription('pa_kabels'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li class="sub"><span>Zonder vaste kabel</span></li>
			<li class="sub"><span>5 meter</span></li>
			<li class="sub"><span>6 meter</span></li>
			<li class="sub"><span>8 meter</span></li>
		</ul>
	</div>
	<div class="product-cards">
		<ul class="products">
			<?php
			if(count($compare_products) > 0)
			{
				$i=0;
				foreach($compare_products as $product)
				{
					echo '
					<li'.(($i == 0)? ' class="active"' : '').'>
						<ul class="results">';
						$fields = array(
							'pa_paalprijs',
							'pa_paalhoogte',
							'pa_paalinclusief',
							'pa_wandmontage',
							'pa_kabelzonder',
							'pa_kabel5meter',
							'pa_kabel6meter',
							'pa_kabel8meter'
						);
						foreach($fields as $field)
						{
	        				$field_name = wc_attribute_label($field);
							echo '<li>
									<span>'.$field_name.'</span> 
								 <a class="questionPopup" href="javascript:void(0);" data-title="'.$field_name.'" data-txt="'.getAttributeDescription($field).'" href="javascript:void(0);"><img src="'.get_template_directory_uri().'/images/question-2.svg" alt="Informatie"></a>
									'.formatYesNo($product->get_attribute($field)).'
								</li>';	
						}
					echo '
						</ul>
					</li>';
				}
			}
			?>	
			
			<div class="clearfix"></div>
		</ul>
	</div>
	<div class="clearfix"></div>
</section>
<div class="clearfix"></div>