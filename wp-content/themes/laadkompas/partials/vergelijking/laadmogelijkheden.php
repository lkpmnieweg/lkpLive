<?php
$compare_products = get_query_var('compare_products');
?>

<section id="loading" class="specifications loading ">
	<div class="spec-title"><span class="arrow down"></span> Laad mogelijkheden</div>
	<div class="spec-info">
		<ul>
			<li><span>Minimum vermogen</span> <a class="questionPopup" data-title="Minimum vermogen" data-txt="<?php echo getAttributeDescription('pa_mincap'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Maximum vermogen</span> <a class="questionPopup" data-title="Maximum vermogen" data-txt="<?php echo getAttributeDescription('pa_maxcap'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Fase</span> <a class="questionPopup" data-title="Fase" data-txt="<?php echo getAttributeDescription('pa_fase'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Smart charging</span> <a class="questionPopup" data-title="Smart charging" data-txt="<?php echo getAttributeDescription('pa_smartcharging'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Load balancing</span> <a class="questionPopup" data-title="Load balancing" data-txt="<?php echo getAttributeDescription('pa_loadbalancing'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
		</ul>
	</div>
	<div class="product-cards">
		<ul class="products">
		<?php
		if(count($compare_products) > 0)
		{
			$i=0;
			foreach($compare_products as $product)
			{
				echo '
				<li'.(($i == 0)? ' class="active"' : '').'>
					<ul class="results">';
					$fields = array(
						'pa_mincap',
						'pa_maxcap',
						'pa_fase',
						'pa_smartcharging',
						'pa_loadbalancing'
					);
					foreach($fields as $field)
					{
        				$field_name = wc_attribute_label($field);
						echo '<li>
								<span>'.$field_name.'</span> 
								 <a class="questionPopup" href="javascript:void(0);" data-title="'.$field_name.'" data-txt="'.getAttributeDescription($field).'" href="javascript:void(0);"><img src="'.get_template_directory_uri().'/images/question-2.svg" alt="Informatie"></a>
								'.formatYesNo($product->get_attribute($field)).'
							</li>';	
					}
				echo '
					</ul>
				</li>';
			}
		}
		?>	
			<div class="clearfix"></div>
		</ul>
	</div>
	<div class="clearfix"></div>
</section>
<div class="clearfix"></div>