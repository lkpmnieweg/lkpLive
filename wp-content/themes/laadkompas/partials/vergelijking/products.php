<?php
$compare_products = get_query_var('compare_products');
global $post;
?>
<section class="product-cards top">
	<?php if($post->post_type == "quote"): //only funnel-result ?>
		<div class="switch-box">
			<label class="switch">
				<input type="checkbox" data-identifier="funnel">
				<span class="slider round"></span>
			</label>
			<p>Toon prijzen inclusief BTW</p>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	<?php endif; ?>

	<form action="" method="POST">
	<div class="clearfix"></div>
	<div class="spec-titles">
		<ul>
			<li>Vermogen</li>
			<li>Levering</li>
			<li class="price">Prijs vanaf</li>
			<li>Installatie <a href="https://laadkompas.nl/installatie-calculator/" target=_blank >(calculator)</a></li>
			<li>Abonnement</li>
		</ul>
	</div>
	<ul class="products">
		<?php
		if(count($compare_products) > 0)
		{
			global $post;
			$i=0;
			foreach($compare_products as $product)
			{
				$install_costs = '0,00';
				// $product->get_attribute('pa_installatiekosten');

            	$taxonomy = wc_attribute_taxonomy_name( 'installatiekosten' );
            	
                // If this is a term slug, get the term's nice name.
                
				$f1 = 'post_tag_41';
				$f2 = 'post_tag_315'; 

				//NewMotion Business Pro + Lite t/m 22kW & 
				//EVBox BusinessLine (dubbel) = dubbele installatiekosten

				$fieldname = ($product->get_ID() == 1079 || $product->get_ID() == 2571)? $f2 : $f1;
            	
            	$install_costs = get_field('product_optie_prijs',$fieldname,'product');

				$compare = '';
				if($post->post_type == "quote2"){
					$compare = '
					<div class="compare">
						<input name="product" data-label="'.$product->get_name().'" id="product'.$product->get_ID().'" type="checkbox" value="" />
						<label class="custom-label-wrap" for="product'.$product->get_ID().'">
							<span class="custom-check"></span> <span class="custom-label">Vergelijk</span>
						</label>
					</div>';
				}

				//
				$cashback_txt = get_field('cashback_vergelijking_banner_tekst', $product->get_ID());
				echo '
				<li'.(($i == 0)? ' class="active"' : '').'>
					'.$compare.'
					<div class="picture">
					'.(($cashback_txt != null && $cashback_txt != "")? '<div class="ribbon ribbon-top-right"><span>'.$cashback_txt.'</span></div>' : '').'
						<a href="'.get_permalink($product->get_ID()).'"><img src="'.wp_get_attachment_url( $product->get_image_id() ).'" alt="'.$product->get_name().'" /></a>

					</div>
					<h2>'.$product->get_name().'</h2>

					<ul class="specs">
						<li>'.$product->get_attribute('pa_vermogen').'</li>
						<li>'.$product->get_attribute('pa_levering').'</li>
						<li class="price" data-exc="&euro; '.wc_get_price_excluding_tax( $product ).'" data-inc="&euro; '.wc_get_price_including_tax( $product ).'">
							&euro; <span class="price_digit">'.@number_format(wc_get_price_excluding_tax( $product ),2,',','.').'</span>
						</li>
						<li>&euro; <span class="install_costs_tax" data-inc="'.number_format(($install_costs*1.21),2,',', '.').'" data-exc="'.number_format($install_costs,2,',', '.').'">'.number_format($install_costs,2,',', '.').'</span></li>
						<li>'.$product->get_attribute('pa_abonnement').'</li>
					</ul>
					<a class="btn" href="'.get_permalink($product->get_ID()).'">Opties &amp; bestel</a>
				</li>';
				$i+=1;
			}
		}
		?>
		<div class="clearfix"></div>
	</ul>
	</form>
</section>