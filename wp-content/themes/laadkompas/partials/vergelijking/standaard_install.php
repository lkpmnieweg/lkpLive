<section id="installation" class="specifications standard_install">
	<div class="spec-title"><span class="arrow down"></span> Standaard installatie</div>
	<div class="spec-info">
		<ul>
			<li><span>Prijs</span> <a class="questionPopup" data-title="Prijs" data-txt="<?php echo getAttributeDescription('pa_price'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Voorrijkosten</span> <a class="questionPopup" data-title="Voorrijkosten" data-txt="<?php echo getAttributeDescription('pa_voorrijkosten'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Arbeidskosten</span> <a class="questionPopup" data-title="Arbeidskosten" data-txt="<?php echo getAttributeDescription('pa_arbeidskosten'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Klein materiaal</span> <a class="questionPopup" data-title="Klein materiaal" data-txt="<?php echo getAttributeDescription('pa_kleinmateriaal'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Ingraven paal</span> <a class="questionPopup" data-title="Ingraven paal" data-txt="<?php echo getAttributeDescription('pa_ingegravenpaal'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>10m voedingskabel</span> <a class="questionPopup" data-title="10m voedingskabel" data-txt="<?php echo getAttributeDescription('pa_10mvoeding'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>5m graafwerk</span> <a class="questionPopup" data-title="5m graafwerk" data-txt="<?php echo getAttributeDescription('pa_5mgraafwerk'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Bestratingswerk</span> <a class="questionPopup" data-title="Bestratingswerk" data-txt="<?php echo getAttributeDescription('pa_bestratingswerk'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
		</ul>
	</div>
	<div class="product-cards">
		<ul class="products">
			<?php
			if(count($compare_products) > 0)
			{

				$i=0;
				foreach($compare_products as $product)
				{
					echo '
					<li'.(($i == 0)? ' class="active"' : '').'>
						<ul class="results">';

					echo '<li>
							<span>Prijs</span> 
							<a class="questionPopup" href="javascript:void(0);" data-title="Prijs" data-txt="'.getAttributeDescription('pa_price').'" href="javascript:void(0);"><img src="'.get_template_directory_uri().'/images/question-2.svg" alt="Informatie"></a>
							&euro; '.number_format($product->get_price(),2,',','.').'
						</li>';
						$fields = array(
							'pa_voorrijkosten',
							'pa_arbeidskosten',
							'pa_kleinmateriaal',
							'pa_ingegravenpaal',
							'pa_10mvoeding',
							'pa_5mgraafwerk',
							'pa_bestratingswerk'
						);
						foreach($fields as $field)
						{
	        				$field_name = wc_attribute_label($field);
							echo '<li>
									<span>'.$field_name.'</span> 
								 <a class="questionPopup" href="javascript:void(0);" data-title="'.$field_name.'" data-txt="'.getAttributeDescription($field).'" href="javascript:void(0);"><img src="'.get_template_directory_uri().'/images/question-2.svg" alt="Informatie"></a>
									'.formatYesNo($product->get_attribute($field)).'
								</li>';	
						}
					echo '
						<li class="more"><a href="'.get_the_permalink(3549).'" target="_blank">Meer over installatie</a></li>
						</ul>
					</li>';
				}
			}
			?>
			<div class="clearfix"></div>
		</ul>
		<div class="clearfix"></div>
		<ul class="buttons">
			<?php
			if(count($compare_products) > 0)
			{

				$i=0;
				foreach($compare_products as $product)
				{
					echo '<li><a class="btn" href="'.get_the_permalink($product->get_ID()).'" title="'.$product->get_name().'">Opties &amp; Bestel</a></li>';
				}
			}
			?>					
			<div class="clearfix"></div>
		</ul>
	</div>
	<div class="clearfix"></div>
</section> 
<div class="clearfix"></div>