<?php
$compare_products = get_query_var('compare_products');
?>
<section id="plus" class="specifications plus">
	<div class="spec-title"><span class="arrow down"></span> Belangrijkste kenmerken</div>
	<div class="spec-info">
		<ul>
			<li><span>Verbruik verrekenen</span> <a class="questionPopup" data-title="Verbruik verrekenen" data-txt="<?php echo getAttributeDescription('pa_verbruikverrekenen'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Online inzicht in verbruik</span> <a class="questionPopup" data-title="Online inzicht in verbruik" data-txt="<?php echo getAttributeDescription('pa_onlineinzicht'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Smart-charging</span> <a class="questionPopup" data-title="Smart-charging" data-txt="<?php echo getAttributeDescription('pa_smartcharging'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Updaten naar slimme functies mogelijk</span>  <a class="questionPopup" data-title="Updaten naar slimme functies mogelijk" data-txt="<?php echo getAttributeDescription('pa_updaten'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Onderhoudsarm</span> <a class="questionPopup" data-title="Onderhoudsarm" data-txt="<?php echo getAttributeDescription('pa_onderhoudsarm'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Vaste kabel</span> <a class="questionPopup" data-title="Vaste kabel" data-txt="<?php echo getAttributeDescription('pa_vastekabel'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
		</ul>
	</div>
	<div class="product-cards">
		<ul class="products">
		<?php
		if(count($compare_products) > 0)
		{
			$i=0;
			foreach($compare_products as $product)
			{
				echo '
				<li'.(($i == 0)? ' class="active"' : '').'>
					<ul class="results">';
					$fields = array(
						'pa_verbruikverrekenen',
						'pa_onlineinzicht',
						'pa_smartcharging',
						'pa_updaten',
						'pa_onderhoudsarm',
						'pa_vastekabel'
					);
					foreach($fields as $field)
					{
        				$field_name = wc_attribute_label($field);
						echo '<li>
								<span>'.$field_name.'</span> 
								 <a class="questionPopup" href="javascript:void(0);" data-title="'.$field_name.'" data-txt="'.getAttributeDescription($field).'" href="javascript:void(0);"><img src="'.get_template_directory_uri().'/images/question-2.svg" alt="Informatie"></a>
								'.formatYesNo($product->get_attribute($field)).'
							</li>';	
					}
				echo '
					</ul>
				</li>';
			}
		}
		?>	
			<div class="clearfix"></div>
		</ul>
	</div>
	<div class="clearfix"></div>
</section>
<div class="clearfix"></div>