<section id="minpoints" class="specifications min">
	<div class="spec-title"><span class="arrow down"></span> Minpunten</div>
	<div class="spec-info">
		<ul>
			<li><span>Hogere aanschafwaarde</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Prijs aan de hoge kant</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
			<li><span>Display maakt laadpaal minder robuust</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg" alt="Informatie"></a></li>
		</ul>
	</div>
	<div class="product-cards">
		<ul class="products">
			<?php
			if(count($compare_products) > 0)
			{
				$i=0;
				foreach($compare_products as $product)
				{
					echo '
					<li'.(($i == 0)? ' class="active"' : '').'>
						<ul class="results">';
						$fields = array(
							'pa_hogereaanschafwaarde',
							'pa_prijsaandehogekant',
							'pa_displaylaadpaal'
						);
						foreach($fields as $field)
						{
	        				$field_name = wc_attribute_label($field);
							echo '<li>
									<span>'.$field_name.'</span> 
								 <a class="questionPopup" href="javascript:void(0);" data-title="'.$field_name.'" data-txt="'.getAttributeDescription($field).'" href="javascript:void(0);"><img src="'.get_template_directory_uri().'/images/question-2.svg" alt="Informatie"></a>
									'.formatYesNo($product->get_attribute($field)).'
								</li>';	
						}
					echo '
						</ul>
					</li>';
				}
			}
			?>	
			<div class="clearfix"></div>
		</ul>
	</div>
	<div class="clearfix"></div>
</section>
<div class="clearfix"></div>