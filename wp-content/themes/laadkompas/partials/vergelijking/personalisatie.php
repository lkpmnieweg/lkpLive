<section id="personal" class="specifications personal ">
	<div class="spec-title"><span class="arrow down"></span> Personalisatie</div>
	<div class="spec-info">
		<ul>
			<li><span>Eigen logo</span> <a class="questionPopup" data-title="Eigen logo" data-txt="<?php echo getAttributeDescription('pa_eigenlogo'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Beschikbare standaard kleuren</span> <a class="questionPopup" data-title="Beschikbare standaard kleuren" data-txt="<?php echo getAttributeDescription('pa_beschikkleur'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Andere standaard kleuren</span> <a class="questionPopup" data-title="Andere standaard kleuren" data-txt="<?php echo getAttributeDescription('pa_andstandkleur'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Custom kleur mogelijk</span> <a class="questionPopup" data-title="Custom kleur mogelijk" data-txt="<?php echo getAttributeDescription('pa_customkleur'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
		</ul>
	</div>
	<div class="product-cards">
		<ul class="products">
			<?php
			if(count($compare_products) > 0)
			{
				$i=0;
				foreach($compare_products as $product)
				{
					echo '
					<li'.(($i == 0)? ' class="active"' : '').'>
						<ul class="results">';
						$fields = array(
							'pa_eigenlogo',
							'pa_beschikkleur',
							'pa_andstandkleur',
							'pa_customkleur'
						);
						foreach($fields as $field)
						{
	        				$field_name = wc_attribute_label($field);
							echo '<li>
									<span>'.$field_name.'</span> 
								 <a class="questionPopup" href="javascript:void(0);" data-title="'.$field_name.'" data-txt="'.getAttributeDescription($field).'" href="javascript:void(0);"><img src="'.get_template_directory_uri().'/images/question-2.svg" alt="Informatie"></a>
									'.formatYesNo($product->get_attribute($field)).'
								</li>';	
						}
					echo '
						</ul>
					</li>';
				}
			}
			?>	
			<!-- <li class="active">
				<ul class="results">
					<li><span>Eigen logo</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
					<li><span>Bes. standaard kleuren</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a><span class="color color-light-grey"></span></li>
					<li><span>Andere standaard kleuren</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a>Tegen meerprijs</li>
					<li><span>Custom kleur mogelijk</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a>Tegen meerprijs</li>
				</ul>
			</li>
			<li>
				<ul class="results">
					<li><span>Eigen logo</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
					<li>
						<span>Bes. standaard kleuren</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a>
						<span class="color color-light-grey"></span>
						<span class="color color-grey"></span>
						<span class="color color-dark-grey"></span>
					</li>
					<li><span>Andere standaard kleuren</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a>Tegen meerprijs</li>
					<li><span>Custom kleur mogelijk</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a>Tegen meerprijs</li>
				</ul>
			</li>
			<li>
				<ul class="results">
					<li><span>Eigen logo</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a>Tegen meerprijs</li>
					<li><span>Bes. standaard kleuren</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a><span class="color color-light"></li>
					<li><span>Andere standaard kleuren</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
					<li><span>Custom kleur mogelijk</span> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
				</ul>
			</li> -->
			<div class="clearfix"></div>
		</ul>
	</div>
	<div class="clearfix"></div>
</section>
<div class="clearfix"></div>