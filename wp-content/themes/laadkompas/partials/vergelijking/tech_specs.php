<section id="technical" class="specifications technical ">
	<div class="spec-title"><span class="arrow down"></span> Technische specificaties</div>
	<div class="spec-info">
		<ul>
			<li><span>Artikelnummer</span> <a class="questionPopup" data-title="Artikelnummer" data-txt="<?php echo getAttributeDescription('sku'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Fase</span> <a class="questionPopup" data-title="Fase" data-txt="<?php echo getAttributeDescription('pa_fase'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Ampere</span> <a class="questionPopup" data-title="Ampere" data-txt="<?php echo getAttributeDescription('pa_ampere'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Contactdoos</span> <a class="questionPopup" data-title="Contactdoos" data-txt="<?php echo getAttributeDescription('pa_contactdoos'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>IP-waarde</span> <a class="questionPopup" data-title="IP-waarde" data-txt="<?php echo getAttributeDescription('pa_ipwaarde'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Statusweergave</span> <a class="questionPopup" data-title="Statusweergave" data-txt="<?php echo getAttributeDescription('pa_statusweergave'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Hoogte (mm)</span> <a class="questionPopup" data-title="Hoogte (mm)" data-txt="<?php echo getAttributeDescription('pa_hoogte'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Breedte (mm)</span> <a class="questionPopup" data-title="Breedte (mm)" data-txt="<?php echo getAttributeDescription('pa_breedte'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Diepte (mm)</span> <a class="questionPopup" data-title="Diepte (mm)" data-txt="<?php echo getAttributeDescription('pa_diepte'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
			<li><span>Materiaal</span> <a class="questionPopup" data-title="Materiaal" data-txt="<?php echo getAttributeDescription('pa_materiaal'); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/question-2.svg"></a></li>
		</ul>
	</div>
	<div class="product-cards">
		<ul class="products">
			<?php
			if(count($compare_products) > 0)
			{

				$i=0;
				foreach($compare_products as $product)
				{
					echo '
					<li'.(($i == 0)? ' class="active"' : '').'>
						<ul class="results">';

					echo '<li>
							<span>Artikelnummer</span> 
							<a class="questionPopup" href="javascript:void(0);" data-title="Artikelnummer" data-txt="'.getAttributeDescription('sku').'" href="javascript:void(0);"><img src="'.get_template_directory_uri().'/images/question-2.svg" alt="Informatie"></a>
							<span class="sku">'.$product->sku.'</span>
						</li>';
						$fields = array(
							'pa_fase',
							'pa_ampere',
							'pa_contactdoos',
							'pa_ipwaarde',
							'pa_statusweergave',
							'pa_hoogte',
							'pa_breedte',
							'pa_diepte',
							'pa_materiaal'
						);
						foreach($fields as $field)
						{
	        				$field_name = wc_attribute_label($field);
							echo '<li>
									<span>'.$field_name.'</span> 
								 <a class="questionPopup" href="javascript:void(0);" data-title="'.$field_name.'" data-txt="'.getAttributeDescription($field).'" href="javascript:void(0);"><img src="'.get_template_directory_uri().'/images/question-2.svg" alt="Informatie"></a>
									'.formatYesNo($product->get_attribute($field)).'
								</li>';	
						}
					echo '
						</ul>
					</li>';
				}
			}
			?>
			<div class="clearfix"></div>
		</ul>
	</div>
	<div class="clearfix"></div>
</section>
<div class="clearfix"></div>