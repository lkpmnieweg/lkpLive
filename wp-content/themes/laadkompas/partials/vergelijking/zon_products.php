<?php
$compare_products = get_query_var('compare_products');
global $post;

?>
<section class="product-cards top">
	<?php if(strpos(get_page_template(), "single-zon-quote.php")): //only funnel-result ?>
		<div class="switch-box">
			<label class="switch">
				<input type="checkbox" data-identifier="funnel">
				<span class="slider round"></span>
			</label>
			<p>Toon prijzen inclusief BTW</p>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	<?php endif; ?>

	<form action="" method="POST">
	<div class="clearfix"></div>
	<div class="spec-titles">
		<ul>
			<li>Vermogen per paneel</li>
			<li>Aantal panelen</li>
			<li>kWh-opbrengst per jaar</li>
			<li>Opbrengst per jaar in &euro;</li>
			<li class="price">Prijs incl. installatie</li>
			<li>Garantie op panelen</li>
		</ul>
	</div>
	<ul class="products">
		<?php
		function findRightVariation($variations=false) {
			$found = false;
			$searchConsumption = get_field('stroomverbruik');
			if($variations) {
				foreach($variations as $variation) {
					if($searchConsumption == $variation['attributes']['attribute_zon_verbruik']) {
						$found = $variation;
					}
				}
			}
			return $found;
		}

		if(count($compare_products) > 0)
		{
			global $post;
			$i=0;
			foreach($compare_products as $product)
			{
				$foundVariation = findRightVariation($product->get_available_variations());
				if($foundVariation) {
					//custom_field


				$install_costs = '0,00';
				// $product->get_attribute('pa_installatiekosten');

            	$taxonomy = wc_attribute_taxonomy_name( 'installatiekosten' );
            	
                // If this is a term slug, get the term's nice name.
                
				$f1 = 'post_tag_41';
				$f2 = 'post_tag_315'; 


				$fieldname = ($product->get_ID() == 1079 || $product->get_ID() == 2571)? $f2 : $f1;
            	
				$compare = '';
				if($post->post_type == "quote2"){
					$compare = '
					<div class="compare">
						<input name="product" data-label="'.$product->get_name().'" id="product'.$product->get_ID().'" type="checkbox" value="" />
						<label class="custom-label-wrap" for="product'.$product->get_ID().'">
							<span class="custom-check"></span> <span class="custom-label">Vergelijk</span>
						</label>
					</div>';
				}

				$button = '';
				if($product->get_attribute('pa_button-landingspagina-url') != "" && $product->get_attribute('pa_button-landingspagina-url') != false) {
					$button = '<a class="btn" href="'.$product->get_attribute('pa_button-landingspagina-url').'">Meer informatie</a>';
					$productUrl = $product->get_attribute('pa_button-landingspagina-url');
				}

				//
				$cashback_txt = get_field('cashback_vergelijking_banner_tekst', $product->get_ID());
				echo '
				<li'.(($i == 0)? ' class="active"' : '').'>
					'.$compare.'
					<div class="picture">
					'.(($cashback_txt != null && $cashback_txt != "")? '<div class="ribbon ribbon-top-right"><span>'.$cashback_txt.'</span></div>' : '').'
						<a href="'.$productUrl.'"><img src="'.wp_get_attachment_url( $product->get_image_id() ).'" alt="'.$product->get_name().'" /></a>

					</div>
					<h2>'.$product->get_name().'</h2>

					<ul class="specs">
						<li><strong>'.$foundVariation['vermogen_paneel'].'</strong></li>
						<li><strong>'.$foundVariation['custom_field'].'</strong></li>
						<li>'.$foundVariation['stroomproductie'].'</li>
						<li>&euro; '.$foundVariation['opbrengst_jaar'].'</li>
						<li class="price" data-exc="&euro; '.$foundVariation['display_price'].'" data-inc="&euro; '.$foundVariation['display_price'].'">
							&euro; <span class="price_digit">'.@number_format($foundVariation['display_price'],2,',','.').'</span>
						</li>
						<li>'.$foundVariation['garantie'].'</li>
					</ul>
					'.$button.'
				</li>';
				//
				$i+=1;
				}
			}
		}
		?>
		<div class="clearfix"></div>
	</ul>
	</form>
</section>