<?php
	$compare_products = [];
	$raw_value = $_COOKIE['lkp_comparison_ids'];
	
	//decyper
	$decrypted = str_rot13($raw_value);

	$product_ids = json_decode(str_replace("\\", "", $decrypted));
	foreach($product_ids as $id)
	{
		$productId = (int)str_replace("product", "", $id);
		if($productId)
		{
			$_product = wc_get_product( $productId );
			if($_product)
			{
				//collect product objects into array for parsing
				$compare_products[] = $_product;
			}
		}
	}
	set_query_var('compare_products', $compare_products);
?>

<div class="route-labels">
	<span>Direct naar:</span>
	<ul>
		<li><a href="#plus">Belangrijkste kenmerken</a></li>
		<!-- <li><a href="#minpoints">Minpunten</a></li> -->
		<li><a href="#options">Opties &amp; Toebehoren</a></li>
		<li><a href="#loading">Laadmogelijkheden</a></li>
		<li><a href="#personal">Personalisatie</a></li>
		<li><a href="#technical">Technische specificaties</a></li>
		<li><a href="#installation">Installatie</a></li>
	</ul>
</div>
<div class="container first">
	<div class="switch-box">
		<label class="switch">
			<input type="checkbox" data-identifier="vergelijking">
			<span class="slider round"></span>
		</label>
		<p>Toon prijzen inclusief BTW</p>
		<div class="clearfix"></div>
	</div>
	<div class="arrow-right">
		<img src="<?php echo get_template_directory_uri(); ?>/images/icon-arrow-right.svg" alt="" />
	</div>
</div>
<div class="container scroll">
	<?php
	echo get_template_part('partials/vergelijking/products');
	?>
	<form action="" method="POST">
	<?php
	echo get_template_part('partials/vergelijking/belangrijkste_kenmerken');
	// echo get_template_part('partials/vergelijking/minpunten');
	echo get_template_part('partials/vergelijking/opties');
	echo get_template_part('partials/vergelijking/laadmogelijkheden');
	echo get_template_part('partials/vergelijking/personalisatie');
	echo get_template_part('partials/vergelijking/tech_specs');
	echo get_template_part('partials/vergelijking/standaard_install');

	?> 
	</form>
</div>
<section class="company-usps">
	<div class="container">
		<h2>Waarom Laadkompas?</h2>
		<div class="row">
			<div class="column-3">
				<div class="item">
					<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="" />100% onafhankelijk</div>
					<p>Laadkompas is de grootste onafhankelijke aanbieder van laadpalen in Nederland.</p>
				</div>
			</div>
			<div class="column-3">
				<div class="item">
					<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="" />Persoonlijk advies</div>
					<p>Wij staan klaar voor persoonlijk advies. Elke werkdag tussen 9:00 en 17:30</p>
				</div>
			</div>
			<div class="column-3">
				<div class="item">
					<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="" />Tevreden klanten</div>
					<div class="rating">
						<p>9,2 op basis van <?php echo wp_count_posts('reviews')->publish; ?> beoordelingen</p>
						<div class="reviewStars">
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg">
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg">
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg">
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg">
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/starninety.svg">
	        			</div>
					</div>
        			<div class="kiyoh">
        				<img src="<?php echo get_template_directory_uri(); ?>/images/kiyoh-9-2-logo@2x.jpg" alt="Kiyoh">
        			</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>