<h1>Bestellen</h1>

<?php

 if ( $checkout->get_checkout_fields() ) : ?>

	<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

	<?php do_action( 'woocommerce_checkout_billing' ); ?>
	<?php do_action( 'woocommerce_checkout_shipping' ); ?>

	<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

<?php endif; ?>

<div class="clearfix"></div>
<?php

?>
<div class="form-group">
	<label class="checkbox black">
		<input type="checkbox" name="newsletter" value="1" checked="checked" /> Ja, ik wil op de hoogte blijven van nieuws in de EV markt.
	</label>
</div>
<div class="submit">
	<a href="<?php echo get_page_link(); ?>?overview=summary" class="btn">Doorgaan naar betalen</a>
</div>
<div class="conditions">
	<p>Door op ‘Doorgaan naar betalen’ te klikken gaat u akkoord met de <a href="/algemene-voorwaarden/">Algemene Voorwaarden</a> van laadkompas.nl</p>
</div>