<?php
/*
Template name: Laadkompas - Checkout
*/
get_header();

if (have_posts()): while (have_posts()) : the_post();

$overview_slug = get_query_var('overview');
?>
<div class="container progress">
	<div class="progress-wrapper">
		<div class="line line1"></div>

		<ul>
			<li class="active">
				<span class="bullet"></span>
				<span class="title" data-target="<?php echo wc_get_cart_url(); ?>">Winkelwagen</span>
			</li>
			<li class="active">
				<span class="bullet_active"><span class="bullet_inner"></span></span>
				<span class="title" data-target="checkout">Gegevens</span>				
			</li>
			<li>
				<span class="bullet"><span class="bullet_inner"></span></span>
				<span class="title" data-target="review">Overzicht</span>				
			</li>
			<li>
				<span class="bullet"><span class="bullet_inner"></span></span>
				<span class="title" data-target="payment">Betalen</span>				
			</li>
		</ul>
	</div>
</div>
<div class="container checkout">
	<?php
		the_content();
	?>

</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<noscript>
	<style type="text/css">
		.place-order button[name=woocommerce_checkout_update_totals] {
			margin-top: 20px !important;
			background-color: #ffd866 !important;
			color: #14192e !important;
		}
		form a.btn,
		#block_review {
			display: none !important;
		}

		.page_block {
			display: block !important;
		}
	</style>
</noscript>
<?php
	endwhile;
endif;

get_footer('funnel');
