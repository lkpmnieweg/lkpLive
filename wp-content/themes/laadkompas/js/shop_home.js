$(document).ready(function() {
    if($(window).width() <= 550)
    {
        $('#responsive').lightSlider({
            item:1,
            loop:false,
            slideMove:2,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed:600,
            controls: false,
            pager: false,
            responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:6,
                      }
                },
                {
                    breakpoint:550,
                    settings: {
                        item:1.5,
                        slideMove:1,
                        slideMargin:10
                      }
                }
            ]
        });  

        $('#responsive-others').lightSlider({
            item:1,
            loop:false,
            slideMove:2,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed:600,
            controls: false,
            pager: false,
            responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:6,
                      }
                },
                {
                    breakpoint:550,
                    settings: {
                        item:2.25,
                        slideMove:1,
                        slideMargin:0
                      }
                },
                {
                    breakpoint:370,
                    settings: {
                        item:2,
                        slideMove:1,
                        slideMargin:0
                      }
                }
            ]
        }); 

        $('#responsive-products').lightSlider({
            item:1,
            loop:false,
            slideMove:2,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed:600,
            controls: true,
            pager: false,
            responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:6,
                      }
                },
                {
                    breakpoint:550,
                    settings: {
                        item:2.25,
                        slideMove:1,
                        slideMargin:0
                      }
                },
                {
                    breakpoint:370,
                    settings: {
                        item:2,
                        slideMove:1,
                        slideMargin:0
                      }
                }
            ]
        });  
    }
});