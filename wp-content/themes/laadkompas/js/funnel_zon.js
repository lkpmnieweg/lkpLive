let initPageToggles = function()
{
	$("input[name=consumption]").on('change', function(){
		//remove intro
		$(".---Funnel.first").hide();

		$("section.---Funnel.active").removeClass('active').hide();
		$("section.---Funnel.step2").addClass('active');
		$(".progress-bar .progress").addClass('progress1a');		
		mobileScrollTop();
	});

	$("input[name=preference]").on('change', function(){
		$("section.---Funnel.step2").removeClass('active').hide();
		$("section.---Funnel.step3").addClass('active');
		$(".progress-bar .progress").removeClass('progress1a').addClass('progress1b');
		mobileScrollTop();
	});

	// $("input[name=installation]").on('change', function(){
	// 	$("section.---Funnel.step3").removeClass('active').hide();
	// 	$("section.---Funnel.step4").addClass('active');
	// 	$(".progress-bar .progress").removeClass('progress1b').addClass('progress1c');
	// 	mobileScrollTop();
	// });

	// $("input[name=slimme_laadpaal]").on('change', function(){

	// 	$("section.---Funnel.step4").removeClass('active').hide();
	// 	$("section.---Funnel.step5").addClass('active');
	// 	$(".progress-bar .progress").removeClass('progress1c').addClass('progress1d');
	// 	mobileScrollTop();
	// });

	$("input[name=gotchp]").on('change', function(){

		$(".---Funnel-buttons .btn").click();
		console.log('click');
	});
};

let initFilter = function() {
	$(".btn-filter").on('click', function(){
		if($(this).hasClass('sort'))
		{
			$(".filter-products .filter-fields .form-group").hide();
			$(".filter-products .sort-fields").toggle();
		}
		else {
			$(".filter-products .sort-fields").hide();
			$(".filter-products .filter-fields .form-group").toggle();
		}
	});
};

jQuery.fn.capitalize = function() {
    $(this[0]).keyup(function(event) {
        var box = event.target;
        var txt = $(this).val();
        var stringStart = box.selectionStart;
        var stringEnd = box.selectionEnd;
        $(this).val(txt.replace(/^(.)|(\s|\-)(.)/g, function($word) {
            return $word.toUpperCase();
        }));
        box.setSelectionRange(stringStart , stringEnd);
    });

   return this;
}

let mobileScrollTop = function()
{
	if($(window).width() <= 768)
	{
		$([document.documentElement, document.body]).animate({
		    scrollTop: 0
		}, 100);
	}
};

let SForce = function()
{
	$.ajax({
		url: myAjax.ajaxurl,
		type: 'POST',
		data: {
			giveAdvice: true,
			emailadres: $("input[name=emailadres]").val(),
			firstname: $("input[name=firstname]").val(),
			lastname: $("input[name=lastname]").val(),
			phone: $("input[name=phone]").val(),
			comments: $("textarea[name=comments]").val(),
			name_title: ''
		},
		success: function(e)
		{
			console.log(e);

			$([document.documentElement, document.body]).animate({
			    scrollTop: 0
			}, 100);

			$('body').css('overflow', 'hidden');

			$('body').block({ 
				message: '<div><img src="'+templateUrl+'/images/logo-content.svg" alt="" /></div><h2>Uw aanvraag wordt verwerkt...</h2>', 
				css: { border: '2px solid #51b989' },
			    overlayCSS: {
			        background: '#fff',
			        opacity: 0.75
			    }
			}); 
			return true;
		},
		error: function()
		{

		}
	});

};

function updateOfferCounter() {
	var currentCount = "";
	var call = $.ajax({
		url: myAjax.updateCount,
		type: 'POST',
		data: {
			counter: 1,
			item: 2
		},
		success: function(data) {
			var result = number_format(data,0,',','.');
			$(".previous_orders .request_count").html(result);
		},
		error: function(e) {
			console.log(e);
		}
	});
};

$(document).ready(function(){
	if($("#funnel2").length == 1)
	{
		$.formUtils.addValidator({
		  name : 'phone_number',
		  validatorFunction : function(value, $el, config, language, $form) {
		  	var out = false;

		  	if(isNaN(value))
		  		return false;

		  	if(value.length >= 10 && value.length <= 14)
		  	{
		  		if(value.substring(0,8) == '06123456')
		  			return false;

		  		if(value.substring(0,1) == '0')
		  		out = true;
		  	}


		    return out;
		  },
		  errorMessage : 'Voer een geldig telefoonnummer in.',
		  errorMessageKey: 'badPhoneNumber'
		});
		$.validate({
			form: "#funnel2",
		    onSuccess : function($form) {
		      updateOfferCounter();
		      return true;
		    }
		});

	}
	initFilter();

	if($(".page-template-page-funnel-1").length > 0 || $(".page-template-page-zon-funnel-1").length > 0)
		initPageToggles();

	$("input[name=giveAdvice]").on('click', function(){
		if($("input[name=product]").prop('checked'))
		{
			//save call to post type
			jQuery.ajax({
		         type : "post",
		         dataType : "json",
		         url : 'https://laadkompas.nl/wp-admin/admin-ajax.php',
		         data : {action: "my_user_like", emailAddress: $("#funnel2 input[name=emailadres]").val()},
		         success: function(response) {
		            if(response.type == "success") {
		               
		            }
		            else {
		            }

					return true;
		         }
		      });
		}
	});

	if($(".swiper-container").length > 0) {
		//swipe images automatically
		setInterval(function(){
			$(".swiper-container .swiper-button-next").trigger('click');
		}, 3000);
	}
});

$(window).scroll(function(){
	if($(this).scrollTop() > 200)
	{
		$(".scrollDown").fadeOut();
	}
	else $(".scrollDown").fadeIn();
});

$('body').on({
    'touchmove': function(e) { 
		if($(this).scrollTop() > 200)
		{
			$(".scrollDown").fadeOut();
		}
		else $(".scrollDown").fadeIn();
        
    }
});