var validateFields = function()
{
	var zipcode = $(".action-form [name=sun_zipcode]"),
		house_nr = $(".action-form [name=sun_nr]"),
		zip_errors = 0,
		nr_errors = 0;

	if(zipcode.val() == "" || zipcode.val() == false)
	{
		zipcode.addClass('error');
		zip_errors += 1;
	}
	else if(!validateZipcode(zipcode.val()))
	{
		zipcode.addClass('error');
		zip_errors += 1;
	}
	else
	{ 
		zipcode.removeClass('error');
		zip_errors = 0;
	}

	if(house_nr.val() == "" || house_nr.val() == false)
	{
		house_nr.addClass('error');
		nr_errors += 1;
	}
	else if(!validateHouseNr(house_nr.val()))
	{
		house_nr.addClass('error');
		nr_errors += 1;
	}
	else
	{ 
		house_nr.removeClass('error');
		nr_errors = 0;
	}
	var errors = zip_errors + nr_errors;

	if(errors == 0)
		return true;
	else
		return false;
};


var validateZipcode = function(zipcode) {
    var regex = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
    return regex.test(zipcode);
}

var validateHouseNr = function(nr)
{
    var regex = /^[1-9][0-9]*(([-][1-9][0-9]*)|([\s]?[a-zA-Z]+))?$/i;
    return regex.test(nr);
}

$(document).ready(function(){
	// $(".action-form .btn").on('click', function(e){
	// 	e.preventDefault();
	// 	validateFields();
	// });
});
// var validateFields = function()
// {
// 	var zipcode = $(".action-form [name=sun_zipcode]"),
// 		house_nr = $(".action-form [name=sun_nr]"),
// 		zip_errors = 0,
// 		nr_errors = 0;

// 	if(zipcode.val() == "" || zipcode.val() == false)
// 	{
// 		zipcode.addClass('error');
// 		zip_errors += 1;
// 	}
// 	else if(!validateZipcode(zipcode.val()))
// 	{
// 		zipcode.addClass('error');
// 		zip_errors += 1;
// 	}
// 	else
// 	{ 
// 		zipcode.removeClass('error');
// 		zip_errors = 0;
// 	}

// 	if(house_nr.val() == "" || house_nr.val() == false)
// 	{
// 		house_nr.addClass('error');
// 		nr_errors += 1;
// 	}
// 	else if(!validateHouseNr(house_nr.val()))
// 	{
// 		house_nr.addClass('error');
// 		nr_errors += 1;
// 	}
// 	else
// 	{ 
// 		house_nr.removeClass('error');
// 		nr_errors = 0;
// 	}
// 	var errors = zip_errors + nr_errors;

// 	if(errors == 0)
// 		makeAPICall();
// };

// var closeOverlay = function()
// {
// 	$("#sun_overlay_form .wpcf7-response-output, .wpcf7-not-valid-tip").hide();
// 	$("#sun_overlay_form .form-control").removeClass('wpcf7-not-valid');
// 	// $("#sun_overlay_form #reset").trigger('click');
// 	$(".sun_overlay").hide();
// };

// var validateZipcode = function(zipcode) {
//     var regex = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
//     return regex.test(zipcode);
// }

// var validateHouseNr = function(nr)
// {
//     var regex = /^[1-9][0-9]*(([-][1-9][0-9]*)|([\s]?[a-zA-Z]+))?$/i;
//     return regex.test(nr);
// }

// var prepareAddressString = function()
// {
// 	var house_nr = $(".action-form [name=sun_nr]");

// 	return house_nr.val()+", ";
// }


// var prepareZipcodeString = function()
// {
// 	var zipcode = $(".action-form [name=sun_zipcode]");

// 	return "("+zipcode.val()+")";
// }

// var apiOut = false;
// var makeAPICall = function()
// {
// 	var house_nr = $(".action-form [name=sun_nr]"),
// 		zipcode = $(".action-form [name=sun_zipcode]");

// 	var url = "https://geodata.nationaalgeoregister.nl/locatieserver/free?fq=postcode:"+zipcode.val().replace(" ", "").trim()+"&fq=huisnummer~"+house_nr.val().trim()+"*";
		
// 	$.ajax({
//         type: 'GET', 
//         url: url, 
// 	    dataType: 'json',
//         data: {  }, 
//         success: function (data) {
//         	if(data.response)
//         	{
//         		if(data.response.docs.length > 0)
//         		{
//         			var address = data.response.docs[0].weergavenaam;
        			
//         			address = address.replace(",", " "+prepareAddressString().trim())
        			
// 					var address_string = address;	
//         		}
//         		else {
// 					var address_string = "Huisnummer: " + prepareAddressString() + " " + prepareZipcodeString();	
//         		}

//     			$overlay_form = $("#sun_overlay_form");
// 				$overlay = $("<div class='sun_overlay'></div>");

// 				//address
// 				$overlay_form.find('[name=sun_full_address]').val(address_string);
// 				$("#sun_overlay_form .address_label span").html(address_string);


// 				$overlay.append($overlay_form.show());
// 				$("body").addClass('sun_relative').prepend($overlay);

// 				$("#sun_overlay_form .close").on('click', function(){
// 					closeOverlay();
// 				});
//         	}
//         }
// 	});

// 	return apiOut;
// }

// $(document).ready(function(){
// 	$(".action-form .btn").on('click', function(e){
// 		e.preventDefault();
// 		validateFields();
// 	});
// });