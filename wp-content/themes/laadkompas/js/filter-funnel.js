let changeFilterUrl = function()
{
	var loc_string = '';
	$(".filter-fields select").each(function(){
		var selected = $(this).find('option:selected').val();
		var name = $(this).attr('name');
		if(selected != "" && selected != undefined)
		{
			loc_string += 'filter_vars['+name+']='+selected+'&';
			console.log(selected+':'+name);	
		}
	});
	$(".single_quote .timer_overlay").show();

	location.href = current_page_url+loc_string;
};


var closeOverlay = function()
{
	$("#sun_overlay_form .wpcf7-response-output, .wpcf7-not-valid-tip").hide();
	$("#sun_overlay_form .form-control").removeClass('wpcf7-not-valid');
	$(".sun_overlay").hide();
};

var showOverlay = function() {

	var brand = $("#car_brand option:selected").val();
	var car_type = $("#car_type option:selected").val();

	var yourCar = "Uw auto: " + brand + " " + car_type;
	

	$overlay_form = $("#sun_overlay_form");
	$overlay = $("<div class='sun_overlay'></div>");
	
	//car
	$overlay_form.find('[name=sun_full_car]').val(yourCar.replace("Uw auto: ", ""));
	$("#sun_overlay_form .car_label span").html(yourCar);

	$overlay.append($overlay_form.show());
	$("body").addClass('sun_relative').prepend($overlay);

	$("#car_type").removeClass('error');
}

var validateFields = function()
{
	var errors = 0;
	if(!$(".quote-action-form").hasClass('active'))
	{
	console.log('test');
		errors += 1;
		if( $("#car_brand option:selected").val() == "Universeel, verschillende elektrische auto's" || $("#car_brand option:selected").val() == "Verschillende elektrische auto's" || $("#car_brand option:selected").val() == "Overig, mijn auto staat er niet tussen")
			errors = 0;
	}
	else {

		console.log($("#car_type option:selected").val());
		if($("#car_type option:selected").val() == "")
		{
			$("#car_type").addClass('error');
			errors += 1;
		}
	}

	console.log(errors);

	if(errors == 0)
		showOverlay();
}

$(document).ready(function(){
	$(".filter-fields select").on('change', changeFilterUrl);

	$(".sort-fields a").on('click', function(){
		$(".single_quote .timer_overlay").show();
	});

	if($(".quote-action-form").length == 1)
	{
		$(".quote-action-form .btn").on('click', function(){
			validateFields();
		});
	}

	$("#car_brand").on('change', function(){

		if($(this).find("option:selected").val() == "Universeel, verschillende elektrische auto's" || $(this).find("option:selected").val() == "Verschillende elektrische auto's" || $(this).find("option:selected").val() == "Overig, mijn auto staat er niet tussen")
		{
			$("#car_type").removeClass('error');
		}
	})


	$("#sun_overlay_form .close").on('click', function(){
		closeOverlay();
	});
});