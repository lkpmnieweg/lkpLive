let updateTopPagerLinks = function(current="")
{	
	var par = $(".progress-wrapper ul");
	$(".progress ul li .title").on('click', function(e){
		e.preventDefault();
		var tgt = $(this).data('target');
		if(tgt !== null)
		{
			switch(current) {
				case "checkout":
				if(tgt.indexOf('laadkompas') !== -1)
				{
					location.href=tgt;
				}
				
				break;
				case "review":
				if(tgt == "")
				{
					$(".page_block").hide();
					$("#block_checkout").show();
					par.find('li').eq(2).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
					par.find('li').eq(3).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
					par.find('li').eq(1).addClass('active').find('.bullet').addClass('bullet_active').removeClass('bullet');
					par.closest('div').find('.line2,.line3').remove();
				}
				else if(tgt == "checkout")
				{
					$(".page_block").hide();
					$("#block_checkout").show();
					par.find('li').eq(2).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
					par.find('li').eq(3).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
					par.find('li').eq(1).addClass('active').find('.bullet').addClass('bullet_active').removeClass('bullet');
					par.closest('div').find('.line2,.line3').remove();
				}
				break;
				case "payment":
				if(tgt == "review")
				{
					$(".page_block").hide();
					$("#block_review").show();
					par.find('li').eq(2).addClass('active').find('.bullet').addClass('bullet_active').removeClass('bullet');
					par.find('li').eq(3).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
					par.closest('div').find('.line3').remove();

					if(par.closest('.progress-wrapper').find('.line2').length == 0){
						par.closest('.progress-wrapper').prepend('<div class="line line2"></div>');
						par.find('li').eq(1).find('.bullet_active').addClass('bullet').removeClass('bullet_active');

					}
				}
				break;
			}
		}
	})
}

let updateTop = function(target,current)
{
	var par = $(".progress-wrapper ul");
	var tgt = target;
	if(tgt !== null)
	{
		switch(current) {
			case "checkout":
			if(tgt.indexOf('laadkompas') !== -1)
			{
				location.href=tgt;
			}
			
			break;
			case "review":
			if(tgt == "")
			{
				$(".page_block").hide();
				$("#block_checkout").show();
				par.find('li').eq(2).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
				par.find('li').eq(3).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
				par.find('li').eq(1).addClass('active').find('.bullet').addClass('bullet_active').removeClass('bullet');
				par.closest('div').find('.line2,.line3').remove();
			}
			else if(tgt == "checkout")
			{
				$(".page_block").hide();
				$("#block_checkout").show();
				par.find('li').eq(2).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
				par.find('li').eq(3).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
				par.find('li').eq(1).addClass('active').find('.bullet').addClass('bullet_active').removeClass('bullet');
				par.closest('div').find('.line2,.line3').remove();
			}
			break;
			case "payment":
			if(tgt == "review")
			{
				$(".page_block").hide();
				$("#block_review").show();
				par.find('li').eq(2).find('.bullet').addClass('bullet_active').removeClass('bullet');
				par.find('li').eq(3).removeClass('active').find('.bullet_active').addClass('bullet').removeClass('bullet_active');
				par.closest('div').find('.line3').remove();
			}
			break;
		}
	}
};

let updateTopPager = function(target)
{
	var par = $(".progress-wrapper ul");
	switch(target) {
		case "review":
		par.find('li').eq(1).find('.bullet_active').addClass('bullet').removeClass('bullet_active');
		par.find('li').eq(2).addClass('active').find('.bullet').addClass('bullet_active').removeClass('bullet');
		if(par.closest('.progress-wrapper').find('.line2').length == 0)
			par.closest('.progress-wrapper').prepend('<div class="line line2"></div>');
		break;
		case "payment":
		par.find('li').eq(2).find('.bullet_active').addClass('bullet').removeClass('bullet_active');
		par.find('li').eq(3).addClass('active').find('.bullet').addClass('bullet_active').removeClass('bullet');
		if(par.closest('.progress-wrapper').find('.line3').length == 0)
			par.closest('.progress-wrapper').prepend('<div class="line line3"></div>');
		break;
	}
};

let initCheckout = function()
{
	$("form .submit .btn, .checkoutLink").on('click', function(){
		if($(this).data('target') !== null)
		{
			var conti = false;
			var target = $(this).data('target');

			var formElem = $("form[name=checkout]");
			switch(target) {
				case "checkout": //gegevens
				conti = true;
				$(".totalPrice").removeClass('active');
				updateTop("checkout", "review");
				break;
				case "review":
				//
				var valid = validateFormFields();

				if(valid)
				{
					$(".totalPrice").addClass('active');
					//first check fields
					var elem = $("#block_review");
					elem.find('.company').text(formElem.find('[name=billing_company]').val());
					elem.find('.firstname').text(formElem.find('[name=billing_first_name]').val());
					elem.find('.lastname').text(formElem.find('[name=billing_last_name]').val());
					elem.find('.address').text(formElem.find('[name=billing_address_1]').val());
					elem.find('.zipcode').text(formElem.find('[name=billing_postcode]').val());
					elem.find('.city').text(formElem.find('[name=billing_city]').val());
					elem.find('.country').text(formElem.find('[name=billing_country]').val());
					elem.find('.contact_email').text(formElem.find('[name=billing_email]').val());
					elem.find('.contact_phone').text(formElem.find('[name=billing_phone]').val());

					if(formElem.find('[name=newsletter]').prop('checked'))
					{
						var newsletter = 'Ja, ik wil op de hoogte blijven van nieuws in de EV markt.';
						//save call to post type
						jQuery.ajax({
					         type : "post",
					         dataType : "json",
					         url : 'https://laadkompas.nl/wp-admin/admin-ajax.php',
					         data : {action: "my_user_like", emailAddress: formElem.find('[name=billing_email]').val()},
					         success: function(response) {
					            if(response.type == "success") {
					               
					            }
					            else {
					               console.log("Newsletter added");
					            }
					         }
					      });

					} else var newsletter = '';
					elem.find('.newsletter').text(newsletter);

					var diff_address = formElem.find('[name=ship_to_different_address]').prop('checked');
					if(diff_address == true)
					{
						$("#block_review > h1").text('Factuuradres');
						formElem.find('.shipping_title').html('Bezorgadres').show();
						$(".shipping_items").show();
						$(".button-wrapper").addClass('more_top');
						elem.find('.shipping_company').text(formElem.find('[name=shipping_company]').val());
						elem.find('.shipping_firstname').text(formElem.find('[name=shipping_first_name]').val());
						elem.find('.shipping_lastname').text(formElem.find('[name=shipping_last_name]').val());
						elem.find('.shipping_address').text(formElem.find('[name=shipping_address_1]').val());
						elem.find('.shipping_zipcode').text(formElem.find('[name=shipping_postcode]').val());
						elem.find('.shipping_city').text(formElem.find('[name=shipping_city]').val());
						elem.find('.shipping_country').text(formElem.find('[name=shipping_country]').val());
					}
					else {
						$(".shipping_items, .shipping_title").hide();
						$(".button-wrapper").removeClass('more_top');
					}

					conti = true;
				}
				else return false;
				break;
				case "payment":
				$(".totalPrice").addClass('active');
				conti = true;
				break;
			}

			if(conti)
			{
				$(".page_block").hide();
				$("#block_"+target).fadeIn(100);
				updateTopPager(target);
				updateTopPagerLinks(target);
				$(".payment_options [name=payment_method]").on('change', togglePayment);
			}
		}
	});



};

let togglePayment = function(e)
{
	if(!$(this).closest('label').hasClass('active'))
	{
		$(this).closest('.payment_options').find('label').each(function(){
			$(this).removeClass('active');
			if($(this).find('select').length > 0)
			{
				$(this).find('select').attr('disabled', 'disabled');	
			}
		});

		$(this).closest('label').addClass('active');
		$(this).closest('label').find('select').attr('disabled', false);	
	}
};

function validatePhone(phone) {
    var vast_nummer = /^(((0)[1-9]{2}[0-9][-]?[1-9][0-9]{5})|((\\+31|0|0031)[1-9][0-9][-]?[1-9][0-9]{6}))$/;
    var mobiel_nummer = /^(((\\+31|0|0031)6){1}[1-9]{1}[0-9]{7})$/i;
    return (vast_nummer.test(phone) || mobiel_nummer.test(phone));
}

function validateEmail(email) {
    var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return regex.test(email);
}

let validateFormFields = function()
{
	var return_value = false;

	//billing fields
	var errors = 0;
	$("#block_checkout input:not(.btn,[type=checkbox],[type=hidden])").each(function(){
		var name = $(this).attr('name');
		if(name.substring(0,8) == 'billing_' && name != "billing_company")
		{
			if($(this).val().trim() == '' || $(this).closest('p').hasClass('woocommerce-invalid'))
			{
				errors += 1;
				$(this).closest('p').addClass('validate-required woocommerce-invalid').removeClass('woocommerce-validated');
			}

			if(name == "billing_phone")
			{
				if(!validatePhone($(this).val().trim())){
					errors += 1;
					$(this).closest('p').addClass('validate-required woocommerce-invalid').removeClass('woocommerce-validated');
				}
			}

			if(name == "billing_email")
			{
				if(!validateEmail($(this).val().trim()))
				{
					errors += 1;
					$(this).closest('p').addClass('validate-required woocommerce-invalid').removeClass('woocommerce-validated');
				}
			}
		}

		if($('[name=ship_to_different_address]').prop('checked'))
		{
			if(name.substring(0,9) == 'shipping_' && name != "shipping_company")
			{
				if($(this).val().trim() == '' || $(this).closest('p').hasClass('woocommerce-invalid'))
				{
					errors += 1;
				$(this).closest('p').addClass('validate-required woocommerce-invalid').removeClass('woocommerce-validated');
				}
			}
		}
	});

	if(errors == 0)
		return_value = true;

	
	return return_value;
};

$(document).ready(function(){

	initCheckout();
	updateTopPagerLinks("checkout");


	$('form.checkout.woocommerce-checkout').bind('DOMSubtreeModified',function(){
	  if ($('#block_payment ul.woocommerce-error').length == 0) {
	    $('ul.woocommerce-error').insertAfter('#block_payment > p')//where you want to place 
	  }
	});

	var clone = $("input[name=ship_to_different_address]");

	if($(".woocommerce-order-pay").length == 0) //not return screen
	{
		$(".woocommerce-form__label-for-checkbox").before(clone);
		$(".woocommerce-form__label-for-checkbox").attr('for', 'ship-to-different-address-checkbox');
		$(".woocommerce-form__label-for-checkbox").find('span').addClass('custom-label');
		$(".woocommerce-form__label-for-checkbox").addClass('custom-label-wrap').prepend('<span class="custom-check"></span>');
	}


	$(document.body).on('applied_coupon_in_checkout', function(i){
		location.reload();
	});
});
