var validateFields = function()
{
	var type_customer = $(".action-form [name=type_customer]"),
		nr_errors = 0;

	if(type_customer.val() == "" || type_customer.val() == false)
	{
		type_customer.addClass('error');
		nr_errors += 1;
	}
	else
	{ 
		type_customer.removeClass('error');
		nr_errors = 0;
	}

	
	if(nr_errors == 0)
		openOverlay();
};

var closeOverlay = function()
{
	$("#sun_overlay_form .wpcf7-response-output, .wpcf7-not-valid-tip").hide();
	$("#sun_overlay_form .form-control").removeClass('wpcf7-not-valid');
	// $("#sun_overlay_form #reset").trigger('click');
	$(".sun_overlay").hide();
};


var openOverlay = function()
{
	$overlay_form = $("#sun_overlay_form");
	$overlay = $("<div class='sun_overlay'></div>");

	var type_customer = 'Advies voor: ' + $(".action-form select[name=type_customer] option:selected").val();

	//address
	$overlay_form.find('[name=sun_full_address]').val($(".action-form select[name=type_customer] option:selected").val());
	$("#sun_overlay_form .address_label span").html(type_customer);


	$overlay.append($overlay_form.show());
	$("body").addClass('sun_relative').prepend($overlay);

	$("#sun_overlay_form .close").on('click', function(){
	closeOverlay();
	});
};

var apiOut = false;
var makeAPICall = function()
{
	var house_nr = $(".action-form [name=sun_nr]"),
		zipcode = $(".action-form [name=sun_zipcode]");

	var url = "https://geodata.nationaalgeoregister.nl/locatieserver/free?fq=postcode:"+zipcode.val().replace(" ", "").trim()+"&fq=huisnummer~"+house_nr.val().trim()+"*";
		
	$.ajax({
        type: 'GET', 
        url: url, 
	    dataType: 'json',
        data: {  }, 
        success: function (data) {
        	if(data.response)
        	{
        		if(data.response.docs.length > 0)
        		{
        			var address = data.response.docs[0].weergavenaam;
        			
        			address = address.replace(",", " "+prepareAddressString().trim())
        			
					var address_string = address;	
        		}
        		else {
					var address_string = "Huisnummer: " + prepareAddressString() + " " + prepareZipcodeString();	
        		}

    			$overlay_form = $("#sun_overlay_form");
				$overlay = $("<div class='sun_overlay'></div>");

				//address
				$overlay_form.find('[name=sun_full_address]').val(address_string);
				$("#sun_overlay_form .address_label span").html(address_string);


				$overlay.append($overlay_form.show());
				$("body").addClass('sun_relative').prepend($overlay);

				$("#sun_overlay_form .close").on('click', function(){
					closeOverlay();
				});
        	}
        }
	});

	return apiOut;
}

$(document).ready(function(){
	$(".action-form .btn").on('click', function(e){
		e.preventDefault();
		validateFields();
	});
});