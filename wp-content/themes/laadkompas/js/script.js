let toggleMenu = function() {
	$("nav .header_menu").toggleClass('active');
  $("#menu_overlay").toggleClass('active');
};

let toggleFooterMenu = function()
{
  $(this).closest('.column-4').find('.collapse').toggleClass('active');
  $(this).closest('.column-4').find('.title i').toggleClass('active');
};



let getCookieValues = function(name)
{
  var cookie = getCookie(name);
  return JSON.parse(cookie);
};


function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

const setCookie = (cname, cvalue, exdays) => {
    let d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    document.cookie = cname + "=" + cvalue + ";expires=" + d.toUTCString() + ";path=/";
};

const getCookie = (cname) => {
    let name = cname + "=";
    let ca = document.cookie.split(";");

    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];

        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }

        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }

    return "";
};

var t;
var showPopup = function(e,relX,relY,title,txt)
{
  var elem = $(".question-popup");

  if($(window).width() <= 989)
    relX = '30';

  elem.css('top', relY+'px');
  elem.css('left', relX+'px');

  elem.find('.title').text(title);  
  elem.find('.text').text(txt);
  
  elem.show();

  t = elem;
};

var hidePopup = function()
{
  t.hide();
  t = null;
};

$(document).ready(function () {

  //initialize swiper when document ready
  var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
      el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  if($(window).width() <= 768)
  {
   $('a.questionPopup').on('click',function(e){ // Hover event
    $("p.tooltip").remove();
    var titleText = $(this).attr('data-title');
    var txt = $(this).attr('data-txt');
    $(this).data('tiptext', titleText).removeAttr('title');
    var titleHTML = `<span class="title">`+titleText+`</span>`;
    var popupHTML = (titleText === "")? `<span class="text">`+txt+`</span>` : titleHTML+`<span class="text">`+txt+`</span>`;
    var leftVal = ($(window).width() >= 768)? (e.pageX - ($("p.tooltip").width()/2)) : 40;
    $('<p class="tooltip"></p>').html(popupHTML).appendTo('body').css('top', (e.pageY - 10) + 'px').css('left', leftVal + 'px').fadeIn('slow');
  });

   $(document).on('click', function(e){
    if(!$(e.target).closest('a').hasClass('questionPopup')) {
      $("p.tooltip").remove();
    }
   });
   /*
   , function(){ // Hover off event
    $(this).attr('title', $(this).data('tiptext'));
    $('.tooltip').remove();
  }).mousemove(function(e){ // Mouse move event
    $('.tooltip').css('top', (e.pageY - 10) + 'px').css('left', (e.pageX + 20) + 'px');
  }
  */
}
else {
  $('a.questionPopup').hover(function(e){ // Hover event
    var titleText = $(this).attr('data-title');
    var txt = $(this).attr('data-txt');
    $(this).data('tiptext', titleText).removeAttr('title');
    var titleHTML = `<span class="title">`+titleText+`</span>`;
    var popupHTML = (titleText === "")? `<span class="text">`+txt+`</span>` : titleHTML+`<span class="text">`+txt+`</span>`;
    $('<p class="tooltip"></p>').html(popupHTML).appendTo('body').css('top', (e.pageY - 10) + 'px').css('left', (e.pageX + 20) + 'px').fadeIn('slow');
  }, function(){ // Hover off event
    $(this).attr('title', $(this).data('tiptext'));
    $('.tooltip').remove();
  }).mousemove(function(e){ // Mouse move event
    $('.tooltip').css('top', (e.pageY - 10) + 'px').css('left', (e.pageX + 20) + 'px');
  });

}

    //close menu on body click
  $(window).on('click', function(e){
    if(!$(e.target).closest(".header_menu.active").length) //
    {
      if($(".header_menu").hasClass('active'))
        toggleMenu();
    }
  });
  $(".closeMenu").on('click', toggleMenu);

  if($(window).width() <= 768)
  {
    $("footer .column-4 .title").on('click', toggleFooterMenu);
  }

  if($(".action-form").length > 0)
  {
    var ignoredValues = ["Verschillende elektrische auto's", "Universeel, verschillende elektrische auto's", "Overig, mijn auto staat er niet tussen"];
    $("#car_brand").on('change', function(){
      $(this).removeClass('error');
      if($(this).val() != "" && $.inArray($(this).val().trim(), ignoredValues) === -1)
      {
        $(".action-form").addClass('active');
          $("#car_type").attr('disabled', false);
        if($(window).width() <= 768)
        {
          $("header .usp-bar").addClass('active');
        }
        var brandId = $(this).find('option:selected').data("brandid");
        if(brandId)
        {
          $("#car_type").prop('selectedIndex',0);
          $("#car_type option").attr('disabled', 'disabled').hide();
          $("#car_type option[data-id="+brandId+"]").attr('disabled', false).show();

        }
      }
      else {
        $(".action-form").removeClass('active'); 
          $("#car_type").prop('selectedIndex',0).attr('disabled', 'disabled');
        if($(window).width() <= 768)
        {
          $("header .usp-bar").removeClass('active');
        }
      }
    });

    $(".action-form .btn").on('click', function(){
        if($(".action-form #car_brand option:selected").val() == ""){
            $("#car_brand").addClass('error');
          return false;
        }
        else {
            $("#car_brand").removeClass('error');
        }
    });
  }

  if($("[name=car_type]").length > 0)
  {
    var items = $("[name=car_type] option[data-selected='yes']"); 
    if(items.length > 0)
    {
      var par_id = items.last().attr('data-id');
      $("[name=car_type] option[data-id!="+par_id+"]").hide();
    }
  }
});