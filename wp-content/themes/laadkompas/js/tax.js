var initCheck = false;

let initTax = function(identifier)
{
	var tax_state = checkTaxState();
	if(tax_state != null)
	{
		if(tax_state == "true") //set prices inc
		{
			$(".switch-box .switch").trigger('click');
			taxToggle(identifier,true);
			// $(".switch-box .switch input").attr('checked', 'checked').prop('checked', 'checked');
		}
		else if(tax_state == "false")  //set prices exc
		{	taxToggle(identifier,false);
		}
	}
};

let checkTaxState = function()
{
	var return_value = false;
	var item = localStorage.getItem("wc_tax_opt_attr_val");
	if(item == null)
	{
		localStorage.setItem("wc_tax_opt_attr_val", false);
		return_value = false;
	}
	else {
		return_value = item;
	}

	return return_value;
};

let setTaxState = function(value)
{
	if(value)
	{
		localStorage.setItem("wc_tax_opt_attr_val", value);
	}
}

let taxToggle = function(identifier, incl)
{
  var elem = null;
  switch(identifier){
      case "category":
        elem = $(".products-container .column-3.product .price");
      break;
      case "product":
        elem = $(".product_details .price .amount");
      break;
      case "funnel":
        elem = $(".product-cards .products li .specs li.price, .filter-products .products-boxes li .price");
        break;
      case "vergelijking":
      	elem = $(".product-cards .products li .specs li.price");
      break;
    }

    if(elem)
    {
      elem.each(function(){
        if(identifier == "product")
        {
          var inc = number_format($(this).find('.price_digit').data('inc'),2,',','.');
          var exc = number_format($(this).find('.price_digit').data('exc'),2,',','.');
        }
        else {
          var inc = number_format($(this).data('inc'),2,',','.');
          var exc = number_format($(this).data('exc'),2,',','.');
        }
        
        if(incl)
        {
          $(this).find('.price_digit').text(inc);
          setTaxState("true");
        }
        else {
          $(this).find('.price_digit').text(exc);
          setTaxState("false");
        }
      });
    }

    if(identifier == "funnel")
    {
      if(incl)
      {
        $(".filter-products .products-boxes li .price span:not(.price_digit)").hide();
      }
      else {
        $(".filter-products .products-boxes li .price span:not(.price_digit)").show();
      }
    }

        if($(".install_costs_tax").length > 0)
        {
          var elem = $(".install_costs_tax");
          elem.each(function(e){
            var inc = $(this).data('inc');
            var exc = $(this).data('exc');

            if(incl)
              $(this).text(inc);
            else
              $(this).text(exc);
            
          });
        }
};

$(document).ready(function () {
	var id = ($("body").hasClass('archive'))? 'category' : 'product';
  if($("body").hasClass('page-template-page-vergelijking'))
    id = 'vergelijking';
  if($("body").hasClass('page-template-page-funnel-3'))
    id = 'funnel';

	initTax(id);

  $(".switch-box input[type=checkbox]").on('change', function(){
    var check = $(this).prop('checked');
    var identifier = $(this).data('identifier');

    if(identifier)
    {
      taxToggle(identifier, check);
    }
  });


  if($("body").hasClass('single-product'))
  {
    //price_digit
    var target = $( ".variations_form" )[0];
    // Create an observer instance
    var observer = new MutationObserver(function( mutations ) {
      var found = false;
       mutations.forEach(function( mutation ) {
           var newNodes = mutation.addedNodes; // DOM NodeList
           if( newNodes !== null ) { // If there are new nodes added

            for(var i=0; i<newNodes.length; i+=1)
            {
              var n = newNodes[i];
              if($(n).hasClass('woocommerce-variation-price'))
              {
                setTimeout(function(){
                  var checked = $(".switch input").prop('checked');
                  var inc = number_format($('.variations_form .price_digit').data('inc'),2,',','.');
                  var exc = number_format($('.variations_form .price_digit').data('exc'),2,',','.');
                  if(checked)
                  {
                    $('.variations_form .price_digit').each(function(){
                      var val = number_format($(this).data('inc'),2,',','.');
                      $(this).text(val);
                    });
                    setTaxState("true");
                  }
                  else {
                    $('.variations_form .price_digit').each(function(){
                      var val = number_format($(this).data('exc'),2,',','.');
                      $(this).text(val);
                    });
                    setTaxState("false");
                  }
                }, 0);
              }
            }

          }
       });    
    });

    // Configuration of the observer:
    var config = { 
        attributes: true,
        subtree: true, 
        childList: true, 
        characterData: true 
    };

    // Pass in the target node, as well as the observer options
    observer.observe(target, config);
    // Later, you can stop observing
    // observer.disconnect();
  }
});