// let validateFields = function(identifier=false) {
// 	var returnvalue = false;
// 	if (identifier) {
// 		switch(identifier) {
// 			case 'bekabeling':
// 			if(installObj.present != null && installObj.adapt != null && installObj.spacing != null) {
// 				if(installObj.spacing == "ja" && installObj.groups == null)
// 					returnvalue = false
// 				else returnvalue = true;
// 			}
// 			break;
// 			case 'werkzaamheden':
// 			returnvalue = true;
// 			break;
// 		}
// 	}

// 	return returnvalue;
// };

let openCollapse = function(identifier=false) {
	if (identifier) {
		$(".right .result-box .collapse:not(.collapse-standard)").removeClass('active');
		$(".right .result-box .collapse-"+identifier).addClass('active');
		if(identifier == "meterkast") {
			$(".right .result-box .collapse-standard").addClass('active');
		}
	}
}

let updateProgress = function(identifier=false) {
	if (identifier) {
		switch(identifier) {
			case 'meterkast':
				$(".progress-bar .progress").removeClass('progress1c');
			break;
			case 'bekabeling':
				$(".progress-bar .progress").removeClass('progress3').addClass('progress1c');
			break;
			case 'werkzaamheden':
				$(".progress-bar .progress").removeClass('progress1c').addClass('progress3');
			break;
		}
	}
};

let updatePricesBox = function(element) {
	$(element).fadeIn(300);
};

let toStep = function(identifier=false, prev=false) {
	if (prev) {
		$(".item-section.active").removeClass('active');
		$(".item-section.final").last().removeClass('final').addClass('active');
		$('html,body').scrollTop(0);
			openCollapse(identifier);
		updateProgress(identifier);
	} else {
		// var valid = validateFields(identifier);
		if (identifier) {
			$(".item-section.active").addClass('final').removeClass('active');
			$(".section-"+identifier).removeClass('hidden').addClass('active');
			$('html,body').scrollTop(0);

			openCollapse(identifier);
			updateProgress(identifier);
		}
	}

	if(identifier == 'werkzaamheden') {
		$(".right .result-box .totalPrice").addClass('blink_me');
		$(".right .outro").show();
	} else {
		$(".right .result-box .totalPrice").removeClass('blink_me');
		$(".right .outro").hide();
	}
};

let convertNullToNvt = function(digit, nextElement=false) {
	var returnValue = "n.v.t.";
	if(parseInt(digit) == 0) {
		if(nextElement) {
			//toggle euro sign
			nextElement.closest('div').find('.label-center').hide();

			var check = nextElement.closest('div').find('.label-left').text();
			if(check) {
				var label = check.split(" ");
				if(label[0] == "Voedingskabel" || label[0] == "Graafwerk")
					returnValue = "inbegrepen";
			}
		}
		return returnValue;
	} else {
		if(nextElement) {
			nextElement.closest('div').find('.label-center').show();
		}
		return number_format(digit,2,',','.');
	}
};

let toggleIsOn = function() {
	if($(".switch-box .switch input").prop('checked') == true) {
		return true;
	} else return false;
};

let updateResults = function() {
	$(".right .result-box .output").html(JSON.stringify(installObj));

	var total = installPrices.standard;
	
	
	//3-fase aanpassing
	var presentOutcome;
	if(installObj.adapt == "ja") {
		presentOutcome = installPrices.adapt;
		var presentOutcomeLabel = presentOutcome;
		if(toggleIsOn()) {
			presentOutcomeLabel = (installPrices.adapt*1.21);
		}
		total += presentOutcome;
		$(".right .result-box .present-outcome").attr('data-exc', presentOutcome).attr('data-inc', (presentOutcome*1.21));
	} else {
		presentOutcome = 0;
		var presentOutcomeLabel = 0;
		total -= presentOutcome;
		$(".right .result-box .present-outcome").attr('data-exc', 0).attr('data-inc', 0);
	}
	$(".right .result-box .present-outcome .price_digit").html(convertNullToNvt(presentOutcomeLabel, $(".right .result-box .present-outcome")));

	var installOutcome;
	if(installObj.groups == "nee") {
		//question 1 yes
		if(installObj.present == "ja") {
			installOutcome = installPrices.aardlek;
			var installOutcomeLabel = installOutcome;
			if(toggleIsOn()) {
				installOutcomeLabel = installPrices.aardlek*1.21;
			}
			$(".right .result-box .adapt-outcome").attr('data-exc', installPrices.aardlek).attr('data-inc', (installPrices.aardlek*1.21));
			//
		} else { //question 1 no
			installOutcome = installPrices.aardlek_drie;
			var installOutcomeLabel = installOutcome;
			if(toggleIsOn()) {
				installOutcomeLabel = installPrices.aardlek_drie*1.21;
			}
			$(".right .result-box .adapt-outcome").attr('data-exc', installPrices.aardlek_drie).attr('data-inc', (installPrices.aardlek_drie*1.21));
		}
	} else {
		installOutcome = 0;
		var installOutcomeLabel = 0;
		$(".right .result-box .adapt-outcome").attr('data-exc', 0).attr('data-inc', 0);
	}
	total += installOutcome;
	$(".right .result-box .adapt-outcome .price_digit").html(convertNullToNvt(installOutcomeLabel, $(".right .result-box .adapt-outcome")));

	var spacingOutcome;
	if(installObj.spacing == "ja") {
		spacingOutcome = 0;
		var spacingOutcomeLabel = 0;
		$(".right .result-box .extend-outcome").attr('data-exc', 0).attr('data-inc', 0);
	} else if(installObj.spacing != null) {
		spacingOutcome = installPrices.extend_small;
		var spacingOutcomeLabel = spacingOutcome;
		if(toggleIsOn()) {
			spacingOutcomeLabel = installPrices.extend_small*1.21;
		}
		$(".right .result-box .extend-outcome").attr('data-exc', installPrices.extend_small).attr('data-inc', (installPrices.extend_small*1.21));
	} else {
		spacingOutcome = 0;
		var spacingOutcomeLabel = 0;
		$(".right .result-box .extend-outcome").attr('data-exc', 0).attr('data-inc', 0);
	}
	total += spacingOutcome;
	$(".right .result-box .extend-outcome .price_digit").html(convertNullToNvt(spacingOutcomeLabel, $(".right .result-box .extend-outcome")));


	//Voedingskabel
	var voedingMeters = installObj.powercable <= 10 ? 0 : installObj.powercable - 10;
	var voedingOutcome = voedingMeters * installPrices.powercable;
	var voedingOutcomeLabel = voedingOutcome;
	if(toggleIsOn()) {
		voedingOutcomeLabel = voedingMeters * (installPrices.powercable*1.21);
	}
	$(".right .result-box .voeding-outcome").attr('data-exc',voedingOutcome).attr('data-inc', (voedingOutcome*1.21));
	total += voedingOutcome;
	$(".right .result-box .voeding-outcome .price_digit").html(convertNullToNvt(voedingOutcomeLabel, $(".right .result-box .voeding-outcome")));
	$(".right .result-box .voeding-label").html(installObj.powercable+'m');

	//Datakabel
	var dataOutcome = installObj.datacable * installPrices.datacable;
	var dataOutcomeLabel = dataOutcome;
	if(toggleIsOn()) {
		dataOutcomeLabel = installObj.datacable * (installPrices.datacable*1.21);
	}
	$(".right .result-box .data-outcome").attr('data-exc',dataOutcome).attr('data-inc', (dataOutcome*1.21));
	total += dataOutcome;
	$(".right .result-box .data-outcome .price_digit").html(convertNullToNvt(dataOutcomeLabel, $(".right .result-box .data-outcome")));
	$(".right .result-box .data-label").html(installObj.datacable+'m');



	//graafwerk
	var diggingMeters = installObj.spadework <= 5 ? 0 : installObj.spadework - 5;
	var digOutcome = diggingMeters * installPrices.digging;
	var digOutcomeLabel = digOutcome;
	if(toggleIsOn()) {
		digOutcomeLabel = diggingMeters * (installPrices.digging*1.21);
	}
	$(".right .result-box .dig-outcome").attr('data-exc',digOutcome).attr('data-inc', (digOutcome*1.21));
	total += digOutcome;
	$(".right .result-box .dig-outcome .price_digit").html(convertNullToNvt(digOutcomeLabel, $(".right .result-box .dig-outcome")));
	$(".right .result-box .dig-label").html(installObj.spadework+'m');

	//bestrating
	var streetOutcome = installObj.pavingwork * installPrices.pavement;
	var streetOutcomeLabel = streetOutcome;
	if(toggleIsOn()) {
		streetOutcomeLabel = installObj.pavingwork * (installPrices.pavement*1.21);
	}
	$(".right .result-box .street-outcome").attr('data-exc',streetOutcome).attr('data-inc', (streetOutcome*1.21));
	total += streetOutcome;
	$(".right .result-box .street-outcome .price_digit").html(convertNullToNvt(streetOutcomeLabel, $(".right .result-box .street-outcome")));
	$(".right .result-box .street-label").html(installObj.pavingwork+'m');


	$(".right .result-box .total .totalPrice").attr('data-inc', (total*1.21)).attr('data-exc', total);//.html('&euro; '+total);

	var parseTotal = total;
	if(toggleIsOn()) {
		parseTotal = parseTotal *1.21;
	}
	$(".right .result-box .total .totalPrice .price_digit").text(number_format(parseTotal,2,',','.'));
};

let initCollapse = function()
{	
	$(".collapse .txt_title").on('click', function(){
		$(this).closest('.collapse').toggleClass('active');
	});

	$(".collapse a.title").on('click', function(){
		$(this).closest('.collapse').toggleClass('active');
	});
};

$(document).ready(function(){
	initCollapse();
	updateResults();

	//
	$("input[name=spacing]").on('change', function(){
		
		installObj.spacing = $(this).val();
		updateResults();

		updatePricesBox(".collapse-meterkast .prices > div:eq(2)");
	});

	$("input[name=owned]").on('change', function(){
		installObj.present = $(this).val();
		updateResults();

		if($(this).val() == 'ja') {
			$(".form-group.second").hide();
			if($("input[name=install]:checked").val() == 'ja')
				$(".form-group.second input[value=nee]").trigger('click');

			$(".collapse-meterkast .prices > div:eq(0)").hide();
			$(".right .result-box .adapt-label").html('3 fase');
			// installObj.adapt = "ja";
		} else {
			$(".form-group.second").show();
			$(".right .result-box .adapt-label").html('1 fase');
			$("input[name=install]:checked").prop('checked', false).attr('checked', false);
			// installObj.adapt = null;
		}
		// updatePricesBox(".collapse-meterkast .prices > div:eq(0)");
	});
	$("input[name=install]").on('change', function(){
		installObj.adapt = $(this).val();
		updateResults();
		if($(this).val() == 'ja') {
			$(".form-group.third").hide();
			$(".form-group.third input[value=ja]").trigger('click');
			$(".collapse-meterkast .prices > div:eq(1)").hide();

		} else {
			$(".form-group.third").show();
			$("input[name=group_avail]:checked").prop('checked', false).attr('checked', false);
			// $(".collapse-meterkast .prices > div:eq(1)").show();
		}
		updatePricesBox(".collapse-meterkast .prices > div:eq(0)");
	});
	$("input[name=group_avail]").on('change', function(){
		installObj.groups = $(this).val();
		updateResults();
		if($(this).val() == 'nee'){
			updatePricesBox(".collapse-meterkast .prices > div:eq(1)");
		} else {

		}

	});

	$("select[name=powercable]").on('change', function() {
		installObj.powercable = parseInt($(this).val());
		updateResults();
		updatePricesBox(".collapse-bekabeling .prices > div:eq(0)");
	});

	$("select[name=datacable]").on('change', function() {
		installObj.datacable = parseInt($(this).val());
		updateResults();
		updatePricesBox(".collapse-bekabeling .prices > div:eq(1)");
	});

	$("select[name=digging]").on('change', function() {
		installObj.spadework = parseInt($(this).val());
		updateResults();
		updatePricesBox(".collapse-werkzaamheden .prices > div:eq(0)");
	});

	$("select[name=pavement]").on('change', function() {
		installObj.pavingwork = parseInt($(this).val());
		updateResults();
		updatePricesBox(".collapse-werkzaamheden .prices > div:eq(1)");
	});
});

