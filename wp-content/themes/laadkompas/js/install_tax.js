var initCheck = false;

let initTax = function()
{
	//taxToggle(false);
	
	var tax_state = checkTaxState();
	if(tax_state != null)
	{
		if(tax_state == "true") //set prices inc
		{
			$(".switch-box .switch").trigger('click');
			taxToggle(true);
		}
		else if(tax_state == "false")  //set prices exc
		{	taxToggle(false);
		}
	}
};

let checkTaxState = function()
{
	var return_value = false;
	var item = localStorage.getItem("install_tax_opt_attr_val");
	if(item == null)
	{
		localStorage.setItem("install_tax_opt_attr_val", false);
		return_value = false;
	}
	else {
		return_value = item;
	}

	return return_value;
};

let setTaxState = function(value)
{
	if(value)
	{
		localStorage.setItem("install_tax_opt_attr_val", value);
	}
}


let taxToggle = function(incl)
{
	var elem = $(".right .result-box .price-span");
	if(elem)
    {
      elem.each(function(){
        var inc = number_format($(this).attr('data-inc'),2,',','.');
        var exc = number_format($(this).attr('data-exc'),2,',','.');
        if(incl)
        {
        	//on init
        	if(parseInt(inc) == 0){
        		inc = "n.v.t.";

            var check = $(this).closest('div').find('.label-left').text();
            if(check) {
              var label = check.split(" ");
              if(label[0] == "Voedingskabel" || label[0] == "Graafwerk")
                inc = "inbegrepen";
            }
          }
          
          $(this).find('.price_digit').text(inc);
          setTaxState("true");
        }
        else {
        	if(parseInt(exc) == 0){
        		exc = "n.v.t.";

            var check = $(this).closest('div').find('.label-left').text();
            if(check) {
              var label = check.split(" ");
              if(label[0] == "Voedingskabel" || label[0] == "Graafwerk")
                exc = "inbegrepen";
            }
          }

          
          $(this).find('.price_digit').text(exc);
          setTaxState("false");
        }
      });
    }
    //trigger total
    var total_inc = number_format($(".right .result-box .totalPrice").attr('data-inc'),2,',','.');
    var total_exc = number_format($(".right .result-box .totalPrice").attr('data-exc'),2,',','.');
    if(incl) {
    	$(".right .result-box .totalPrice .price_digit").text(total_inc);
    } else {
    	$(".right .result-box .totalPrice .price_digit").text(total_exc);
    }
};

$(document).ready(function(){
	initTax();

  $(".switch-box input[type=checkbox]").on('change', function(){
    var check = $(this).prop('checked');
    taxToggle(check);
  });
});