let scrollToCTA = function()
{
    var stop = $(".photo").offset();
    $([document.documentElement, document.body]).animate({
        scrollTop: stop.top
    }, 200);
};

$(document).ready(function(){
	$("#relatedSlider").lightSlider({
		pager: false,
		item: 4,
		slideMargin: 70,
		autoWidth: false,
	    loop: true,
        responsive : [
            {
                breakpoint:550,
                settings: {
					item: 2,
					slideMargin: 24
                }
            },
            {
                breakpoint:324,
                settings: {
					item: 1,
					slideMargin: 10
                }
            }
        ]
	});

    $(".stickyTop .btn").on('click', function(e){
        scrollToCTA();
    });
});

$(window).scroll(function(){
    if($(".stickyTop").length == 1)
    {
        // if($(window).width() >= 768) 
        // {
            if($(this).scrollTop() > 120)
            {
                if(!$("header").hasClass("fixed"))
                    $("header").eq(0).hide().addClass('fixed').slideDown(100);
            }
            else {            
                $("header").eq(0).removeClass('fixed');

            }

            if($(this).scrollTop() > 500)
            {
                if(!$(".stickyTop").hasClass("fixed"))
                    $(".stickyTop").hide().addClass('fixed').fadeIn();
            }
            else {

                if($(".stickyTop").hasClass("fixed"))
                    $(".stickyTop").fadeOut().removeClass('fixed');
            }
        }
    // }
});