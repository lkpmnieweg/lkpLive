let minusOne = function()
{
	var parent = $(this).closest('.amount-box');
	var amount = parent.find('.qty');
	console.log(amount.val());
	var amountInt = parseInt(amount.val());
	if(amountInt > 0)
		amount.val(amountInt-1).trigger('change');
};

let plusOne = function()
{
	var parent = $(this).closest('.amount-box');
	var amount = parent.find('.qty');
	var amountInt = parseInt(amount.val());
	amount.val(amountInt+1).trigger('change');
};

$(document).ready(function(){
	$(".amount-box .minus").on('click', minusOne);
	$(".amount-box .plus").on('click', plusOne);
});