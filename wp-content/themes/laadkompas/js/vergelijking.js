
let initOpenCloseSpecs = function(){
	$("section.specifications .spec-title").on('click', function(){
		$(this).closest('section').toggleClass('closed');
	});
};

let initSyncScroll = function(){
	$('.product-cards.top.fixed-top').scroll(function(){
	    $('body .container.scroll').scrollLeft($(this).scrollLeft());    
	})
};


let initTopScroll = function(){
	$('body .container.scroll').scroll(function(){
	    $('.product-cards.top.fixed-top').scrollLeft($(this).scrollLeft());    
	})
};

let anchorClick = function(e)
{
	e.preventDefault();
	var top = $($(e.currentTarget).attr('href')).offset().top;
	if($(window).width() <= 1024)
	{
		var headerHeight = $("nav").height();
		top -= headerHeight;

		var headerHeight2 =$(".product-cards .products").height();
		top -= headerHeight2;
	}


    $([document.documentElement, document.body]).animate({
        scrollTop: top
    }, 1200);
};

$(document).ready(function(){
	initOpenCloseSpecs();

	if($(window).width() <= 550) //mobile only
	{
		$('body').bind('touchmove', function(e) {
			initSyncScroll();
			initTopScroll();
		});
	}

	$(".route-labels ul li a").on('click', anchorClick);
});

$(window).scroll(function(){
	if($(window).width() <= 768) //mobile only
	{
		if($(this).scrollTop() > 100) {
			$(".product-cards.top").addClass('fixed-top');
		}
		else {
			$(".product-cards.top").removeClass('fixed-top');
		}

		initSyncScroll();
	}
});