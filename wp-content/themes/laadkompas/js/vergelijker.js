$(function(){
	var collectedIds = [];
	var maxVergelijkItems = 3;

	let setCookie = function() {
		var name = "lkp_comparison_ids";
		var value = formatForCookie();
		var days = 1;
	    var expires = "";
	    if (days) {
	        var date = new Date();
	        date.setTime(date.getTime() + (days*24*60*60*1000));
	        expires = "; expires=" + date.toUTCString();
	    }
	    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	};

	let getCookie = function(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	}
	function eraseCookie() { 
		var name = "lkp_comparison_ids";  
	    document.cookie = name+'=; Max-Age=-99999999;';  
	}

	let formatForCookie = function()
	{
		var out = null;
		out = JSON.stringify(collectedIds);

		outCypher = out.rot13();


		//encrypt 
		return outCypher;
	};

	String.prototype.rot13 = function(){
	    return this.replace(/[a-zA-Z]/g, function(c){
	        return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
	    });
	};

	let parseAlert = function(label='', state=true)
	{
		
		$(".woocommerce-notices-wrapper.vergelijking").hide();
		var $tpl = $(".woocommerce-notices-wrapper.vergelijking");
		var msgElem = $tpl.find(".woocommerce-message");

		if(state == false || state == "full")
		{
			msgElem.addClass('error');
		} else msgElem.removeClass('error');

		if(state == false) //removal
		{
			msgElem.find('span.count').text(collectedIds.length);
			msgElem.find('span:not(.count)').html('<strong>&quot;'+label+'&quot;</strong> is verwijderd uit uw vergelijking.');
		}
		else if(state == "full")
		{
			msgElem.find('span.count').text(collectedIds.length);
			msgElem.find('span:not(.count)').text('U heeft al het max. aantal items ('+maxVergelijkItems+') voor vergelijking.');
		}
		else {
			msgElem.find('span.count').text(collectedIds.length);
			msgElem.find('span:not(.count)').html('<strong>&quot;'+label+'&quot;</strong> is toegevoegd aan uw vergelijking.');	
		}

		if(collectedIds.length > 1)
		{
			$tpl.find('.vergelijk-link').addClass('active');
		} else $tpl.find('.vergelijk-link').removeClass('active');

		$tpl.fadeIn(100);
	};

	let removeCollectedItem = function(id)
	{
		const index = collectedIds.indexOf(id);
		if (index > -1) {
		  collectedIds.splice(index, 1);
		}
		return collectedIds;
	};

	// Needed to give max to checking boxes
	let toggleProductId = function(e)
	{
		var state = $(this).prop('checked');
		var id = $(this).attr('id');
		var label = $(this).data('label');

		if(collectedIds.length < maxVergelijkItems)
		{
			if(state == true)
			{
				collectedIds.push(id);
			}
			else if(state == false)
			{	
				removeCollectedItem(id);
			}
			parseAlert(label, state);
		}
		else {
			if(state == true)
			{
				//disable checking
				$(this).attr('checked', false).prop('checked', false);	
				parseAlert(label, "full");
			}
			else if(state == false)
			{

				removeCollectedItem(id);
				parseAlert(label, state);
			}
		}

		//set Cookie on change
		eraseCookie();
		setCookie();
	};

	let resetAllComparison = function()
	{
		$("input[name=product]").attr('checked',false).prop('checked',false);
	}

	$("input[name=product]").on('change', toggleProductId);
	resetAllComparison();
});
