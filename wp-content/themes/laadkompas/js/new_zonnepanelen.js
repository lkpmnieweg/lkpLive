var validateFields = function()
{
	var zipcode = $(".action-form [name=sun_zipcode]"),
		house_nr = $(".action-form [name=sun_nr]"),
		zip_errors = 0,
		nr_errors = 0;

	if(zipcode.val() == "" || zipcode.val() == false)
	{
		zipcode.addClass('error');
		zip_errors += 1;
	}
	else if(!validateZipcode(zipcode.val()))
	{
		zipcode.addClass('error');
		zip_errors += 1;
	}
	else
	{ 
		zipcode.removeClass('error');
		zip_errors = 0;
	}

	if(house_nr.val() == "" || house_nr.val() == false)
	{
		house_nr.addClass('error');
		nr_errors += 1;
	}
	else if(!validateHouseNr(house_nr.val()))
	{
		house_nr.addClass('error');
		nr_errors += 1;
	}
	else
	{ 
		house_nr.removeClass('error');
		nr_errors = 0;
	}
	var errors = zip_errors + nr_errors;

	if(errors == 0)
		return true;
	else
		return false;
};


var validateZipcode = function(zipcode) {
    var regex = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
    return regex.test(zipcode);
}

var validateHouseNr = function(nr)
{
    var regex = /^[1-9][0-9]*(([-][1-9][0-9]*)|([\s]?[a-zA-Z]+))?$/i;
    return regex.test(nr);
}

$(document).ready(function(){
	// $(".action-form .btn").on('click', function(e){
	// 	e.preventDefault();
	// 	validateFields();
	// });
});