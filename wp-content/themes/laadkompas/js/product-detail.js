let outlinePageLinks = function()
{
	if($(window).width() <= 768)
	{
		var elem = $(".page_links");
		var clone = elem.clone();
		
		$(".usps").after(clone);
		elem.remove();
		$(".page_links > a").on('click', anchorClick);
	}
}

let anchorClick = function(e)
{
	e.preventDefault();
	var top = $($(e.currentTarget).attr('href')).offset().top;
	if($(window).width() <= 1024)
	{
		var headerHeight = $("nav").height();
		top -= headerHeight;
	}


    $([document.documentElement, document.body]).animate({
        scrollTop: top
    }, 1200);
};

var oldHeight = null;
let toggleReviewContent = function(e)
{
	e.preventDefault();
	if(oldHeight == null)
		oldHeight = $("#reviewSlider").height();

	var parent = $(this).closest('.description');
	parent.find('.dots').toggle();
	parent.find('.expand').toggle();
	

	if($(this).text() == "Lees meer"){
		$(this).text("Lees minder");
		var currentHeight = $(this).closest('li').height();
		$("#reviewSlider").height(currentHeight);
	}
	else {
		$(this).text("Lees meer");
		$("#reviewSlider").height(oldHeight);
		
	}
};

var resetReviewHeight = function()
{	
	$("#reviewSlider .description a").text("Lees meer");
	$("#reviewSlider .dots").show();
	$("#reviewSlider .expand").hide();
	$("#reviewSlider").height(oldHeight);

};

let toggleColors = function()
{
	$(".color").removeClass('active');
	$(this).addClass('active');
};

let hideFilterMenu = function()
{
	$("#overlay").removeClass('active');
	$('#product_options').hide();
	$(".scrollContainer").removeClass('open');
};

let setValueMobile = function(e)
{
	e.preventDefault();
	$(this).addClass('active-label');

	
	var identifier = $(this).data('id');
	var value = $(this).find('input[type="radio"]').val();


	var label = $(this).find('.label').text();
	var price = $(this).find('.price').text();

	//clear all
	$(".variations select#"+identifier+" option").each(function(){
		$(this).attr('selected', false).prop('selected', false).removeClass('active-label');
	});

	// $(".variations select#"+identifier+" option").each(function(){
	// 	if($(this).val() == value)
	// 	{
	// 		$(this).attr('selected', 'selected').prop('selected', 'selected').addClass('active-label');
	// 	}
	// });
	
	$(".variations select#"+identifier).val(value.trim());
	$(".variations select#"+identifier).trigger('change');


	$(".option[data-id="+identifier+"] .name").text(label);
	$(".option[data-id="+identifier+"] .price").text(price).show();

/*
	//overrule colors to color
	identifier = (identifier == "colors")? "color" : identifier;

	//set value
	var label = $(this).find('.label').text();
	var price = $(this).find('.price').text();
	$("input[type=hidden][name="+identifier+"]").attr('disabled', false).val(value);

	if($(".option[data-id="+identifier+"] .color").length > 0)
	{
		var inner = $(this).closest('span').find('label.active').find('.color-inner');
		$(".option[data-id="+identifier+"] .color").html(inner.clone());
	}
	$(".option[data-id="+identifier+"] .name").text(label);
	$(".option[data-id="+identifier+"] .price").text(price);
	*/
	hideFilterMenu();
}

let preloadOptionData = function()
{
	if($(window).width() <= 768)
	{
		$(".variations_form .variations select").each(function(){
			var identifier = $(this).attr('id');
			var val = $(this).find('option:selected').val();
			var label = $(this).find('option:selected').text();
			var price = $(this).find('option:selected').data('optprice').replace(" +", "");

			//replace price
			var priceChar = label.indexOf("+€");
			label = label.substring(0,priceChar-1);

			$(".option[data-id="+identifier+"] .name").text(label);
			$(".option[data-id="+identifier+"] .price").text(price).show();
		});
	}	
};

function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

var slider;
$(document).ready(function(){
	slider = $('#lightSlider').lightSlider({
	    gallery: true,
	    galleryMargin: 5,
	    item: 1,
	    loop: false,
	    slideMargin: 0,
	    thumbItem: 5,
	    thumbMargin: 8,
	    pager: true,
        enableTouch:true,
        enableDrag:true,
        freeMove:true,
        controls: false,
        swipeThreshold: 40,
        responsive : [
            {
                breakpoint:550,
                settings: {
					thumbItem:3,
					slideMove:1,
				    // slideMargin:6,
                }
            },
            {
                breakpoint:425,
                settings: {
					thumbItem:4,
					slideMove:1,
				    slideMargin:4,
                }
            }
        ]
	});

	$(".color").on('click', toggleColors);

	if($("#reviewSlider li").length > 2)
	{
		$("#reviewSlider").lightSlider({
			pager: false,
			item: 3,
			slideMargin: 40,
			autoWidth: false,
		    loop: true,
		    enableDrag: true,
		    enableTouch: true,
	        responsive : [
	            {
	                breakpoint:550,
	                settings: {
						item: 1
	                }
	            },
	            {
	                breakpoint:324,
	                settings: {
						item: 1,
						slideMargin: 10
	                }
	            }
	        ],
	        onBeforeSlide: function()
	        {
	        	resetReviewHeight();
	        }
		});
	}

	$("#relatedSlider").lightSlider({
		pager: false,
		// item: 4,
		slideMargin: 80,
		autoWidth: false,
	    loop: true,
        responsive : [
            {
                breakpoint:550,
                settings: {
					item: 2,
					slideMargin: 24
                }
            },
            {
                breakpoint:324,
                settings: {
					item: 1,
					slideMargin: 10
                }
            }
        ]
	});

	let mobileOption = function(id)
	{
		var out = '<span class="options">';
		if(id)
		{
			//clone elem
			var elem = $(".variations select#"+id);
			if(elem.length > 0)
			{
				elem.find('option').each(function(){
					var price = '';
					var label = $(this).text().split('+€');


					if($(this).text() != "Een optie kiezen" && label[1])
					{
						price = '<span class="price">&euro; '+label[1]+'</span>';	
					}
					
					var activeState = '';
					if($(this).prop('selected') || $(this).attr('selected')){
						activeState = ' class="active"';
					}

					out  += `
					<label data-id="`+id+`"`+activeState+`>
						<input type="radio" name="`+id+`Mobile" value="`+$(this).val()+`">
						<span class="label">`+label[0]+`</span>
						`+price+`
					</label>`;
				})
			}
		}

		out += '</span>';

		return out;
	};
	$("#product_options span:not(.colors) label").on('click', setValueMobile);		

	$(".variations_form .mobile .option").on('click', function(){
		$("#overlay").addClass('active');
		var title = $(this).data('title');
		var mobile = mobileOption($(this).data('id'));

		$("#product_options").show().html(mobile);

		$(".scrollContainer .top .label span").html(title);
		$(".scrollContainer").toggleClass('open');

		$("#product_options span:not(.colors) label").on('click', setValueMobile);		
	});

	$("#reviewSlider .description a").on('click', toggleReviewContent);

	$(".page_links > a").on('click', anchorClick);

	outlinePageLinks();

	preloadOptionData();

	//close filter menu on body click
	$(window).on('click', function(e){

		if(!$(e.target).closest("#product_options").length && !$(e.target).closest('.mobile').length) //
		{
			if($(".scrollContainer").hasClass('open'))
				hideFilterMenu();
		}
	});
});