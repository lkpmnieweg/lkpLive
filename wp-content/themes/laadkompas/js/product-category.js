var slider = document.getElementById('price-slider');

let initFilterToggle = function()
{
	$("#filters ul li h2").on('click', function(e){
		e.preventDefault();
		var parent = $(this).closest('li');
		parent.find('ul,.price_slider_wrapper').toggleClass('active');
		parent.find('.filter-arrow').toggleClass('active');
	});
}


let initFilter = function() {
	$(".btn-filter").on('click', function(){
		if($(this).hasClass('sort'))
		{
			$(".filter-products .filter-fields .form-group").hide();
			$(".filter-buttons .sort-fields").toggle();
		}
		else {
			$(".filter-buttons .sort-fields").hide();
			$(".filter-products .filter-fields .form-group").toggle();
		}
	});
};

let initFilterMenu = function()
{
	$(".btn-filter:not(.sort)").on('click', function(){
		if(!$(".scrollContainer").hasClass('open'))
		{
			$('#filters').css('height', $(window).height()).toggle();
			$(".scrollContainer").toggleClass('open').css('height', $(document).height());
		}
	});
};

let hideFilterMenu = function()
{
		$('#filters').css('height', 'auto').hide();
		$(".scrollContainer").removeClass('open').css('height', 'auto');
};

$(document).ready(function(){
	initFilterToggle();

	initFilterMenu();
	initFilter();

	//close filter menu on body click
	$(window).on('click', function(e){

		if(!$(e.target).closest("#filters").length && !$(e.target).closest('.filter-buttons').length)
		{
			if($(".scrollContainer").hasClass('open'))
				hideFilterMenu();
		}
	})
});

$(window).resize(function(){
	hideFilterMenu();
	if($(window).width() >= 1024)
	{
		$(".scrollContainer").removeClass('open');
		$("#filters").css('display', 'inline-block');
	} else {
		$("#filters").hide();
	}
});