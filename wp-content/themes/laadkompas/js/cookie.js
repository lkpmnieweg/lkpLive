window.LKPCookie = (function () {
	// var cookieTypes = ['nessecary'];

	let setCookie = function(value) {
		var name = "lkp_cookie_settings";
		var days = 30;
	    var expires = "";
	    if (days) {
	        var date = new Date();
	        date.setTime(date.getTime() + (days*24*60*60*1000));
	        expires = "; expires=" + date.toUTCString();
	    }
	    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	};

	let getCookie = function(name) {
	    var nameEQ = name + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
	        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	    }
	    return null;
	};

	let isStatistics = function()
	{
		var out = false;
		var cookie = getCookie("lkp_cookie_settings");
		var parsed = JSON.parse(cookie);
		if(cookie != "" && parsed)
		{
			if(parsed.includes("statistics"))
				out = true;
		}

		return out;
	};

	let isMarketing = function()
	{
		var out = false;
		var cookie = getCookie("lkp_cookie_settings");
		var parsed = JSON.parse(cookie);

		if(cookie != "" && parsed)
		{
			if(parsed.includes("marketing"))
				out = true;
		}

		return out;
	};

	let init = function() {
		if(jQuery("#cookie").length > 0)
		{
			jQuery("#cookie .top a").on('click', function(){
				jQuery("#cookie .top").hide();
				jQuery("#cookie .bottom").addClass('active');
			});

			jQuery("#cookie .button_wrapper .btn").on('click', function(){
				var str = [];
				jQuery("#cookie input[name=type_cookie]").each(function(){
					if(jQuery(this).prop('checked'))
					{
						str.push($(this).val());
					}
				});
				if(str != ""){
					setCookie(JSON.stringify(str));
					jQuery("#cookie .bottom").removeClass('active');
					location.reload();
				}
			});

			//all cookies
			jQuery("#cookie .right a:eq(0)").on('click', function(){
				jQuery("#cookie .bottom input[name=type_cookie]").each(function(){
					jQuery(this).prop('checked', 'checked').attr('checked', 'checked');
				});
				jQuery("#cookie .button_wrapper .btn").trigger('click');
			});

			//only func. cookies
			jQuery("#cookie .right a:eq(1)").on('click', function(){
				jQuery("#cookie .button_wrapper .btn").trigger('click');
			});
		}
	}

	return {
		init,
		isStatistics,
		isMarketing		
	};
})();

window.LKPCookie.init();