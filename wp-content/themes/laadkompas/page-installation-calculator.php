<?php
/* Template name: Laadkompas - Installatie calculator */
get_header('calculator');
?>
<div class="container">
	<section class="---Funnel-form">
		<div class="left">
			<div class="item-section section-meterkast active">
				<h2>Meterkast</h2>
				<p><?php echo str_replace(array("<p>", "</p>"), "", get_field('meterkast_uitleg')); ?></p>
				<a class="read-more" href="<?php echo get_field('1_of_3_fase_link'); ?>">Meer informatie over 1 of 3-fase</a>
				<div class="form-group first">
					<label><?php echo get_field('vraag_1'); ?> <?php if(get_field('hulp_vraag_1') != null && get_field('hulp_vraag_1') != ""): ?><a class="questionPopup" data-title="" href="javascript:void(0);" data-txt="<?php echo strip_tags(get_field('hulp_vraag_1')); ?>"><img src="<?php echo get_template_directory_uri().'/images/question-2.svg';?>" alt="" /></a><?php endif; ?></label>
					<label class="label-container leftOne">
						<input type="radio" name="owned" value="ja" />
						<span class="checkmark"><span class="text">Ja</span></span>
					</label>
					<label class="label-container">
						<input type="radio" name="owned" value="nee" />
						<span class="checkmark"><span class="text">Nee</span></span>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="form-group second" style="display: none;">
					<label class="under-label"><?php echo get_field('vraag_2'); ?> <?php if(get_field('hulp_vraag_2') != null && get_field('hulp_vraag_2') != ""): ?><a class="questionPopup" href="javascript:void(0);" data-title="" data-txt="<?php echo strip_tags(get_field('hulp_vraag_2')); ?>"><img src="<?php echo get_template_directory_uri().'/images/question-2.svg';?>" alt="" /><?php endif; ?></a></label>
					<a class="small" href="<?php echo get_field('waarom_3_fase_link'); ?>">Waarom zou ik 3-fase willen installeren?</a>
					<label class="label-container leftOne">
						<input type="radio" name="install" value="ja" />
						<span class="checkmark"><span class="text">Ja</span></span>
					</label>
					<label class="label-container">
						<input type="radio" name="install" value="nee" />
						<span class="checkmark"><span class="text">Nee</span></span>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="form-group third">
					<label class="under-label"><?php echo get_field('vraag_3'); ?> <?php if(get_field('hulp_vraag_3') != null && get_field('hulp_vraag_3') != ""): ?><a class="questionPopup" href="javascript:void(0);" data-title="" data-txt="<?php echo strip_tags(get_field('hulp_vraag_3')); ?>"><img src="<?php echo get_template_directory_uri().'/images/question-2.svg';?>" alt="" /></a><?php endif; ?></label>
					<label class="label-container leftOne">
						<input type="radio" name="group_avail" value="ja" />
						<span class="checkmark"><span class="text">Ja</span></span>
					</label>
					<label class="label-container">
						<input type="radio" name="group_avail" value="nee" />
						<span class="checkmark"><span class="text">Nee</span></span>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<label><?php echo get_field('vraag_4'); ?> <?php if(get_field('hulp_vraag_4') != null && get_field('hulp_vraag_4') != ""): ?><a class="questionPopup" href="javascript:void(0);" data-title="" data-txt="<?php echo strip_tags(get_field('hulp_vraag_4')); ?>"><img src="<?php echo get_template_directory_uri().'/images/question-2.svg';?>" alt="" /></a><?php endif; ?></label>
					<label class="label-container leftOne">
						<input type="radio" name="spacing" value="ja" />
						<span class="checkmark"><span class="text">Ja</span></span>
					</label>
					<label class="label-container">
						<input type="radio" name="spacing" value="nee" />
						<span class="checkmark"><span class="text">Nee</span></span>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="---Funnel-buttons">
					<div class="container">
						<div class="button-right">
							<input class="btn" type="button" onclick="return toStep('bekabeling');" name="next" value="Naar stap 2/3" />
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="item-section section-bekabeling hidden">
				<h2>Bekabeling</h2>
				<div class="form-group first">
					<label><?php echo get_field('vraag_5'); ?> <?php if(get_field('hulp_vraag_5') != null && get_field('hulp_vraag_5') != ""): ?><a class="questionPopup" href="javascript:void(0);" data-title="" data-txt="<?php echo strip_tags(get_field('hulp_vraag_5')); ?>"><img src="<?php echo get_template_directory_uri().'/images/question-2.svg';?>" alt="" /><?php endif; ?></a></label>
					<label class="select-label">
						<span class="text-label"><span class="price">&euro; <?php echo get_field('voedingskabel'); ?></span> per meter</span>
						<select class="form-control" name="powercable">
							<?php
							for($i=0; $i<=25; $i+=1) {
								print '<option value="'.$i.'">'.$i.' meter</option>';
							}
							?>	
						</select>
						<span class="info-label"><?php echo get_field('voeding_extra_tekst'); ?></span>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<label><?php echo get_field('vraag_6'); ?> <?php if(get_field('hulp_vraag_6') != null && get_field('hulp_vraag_6') != ""): ?><a class="questionPopup" href="javascript:void(0);" data-title="" data-txt="<?php echo strip_tags(get_field('hulp_vraag_6')); ?>"><img src="<?php echo get_template_directory_uri().'/images/question-2.svg';?>" alt="" /><?php endif; ?></a></label>
					<label class="select-label">
						<span class="text-label"><span class="price">&euro; <?php echo get_field('datakabel'); ?></span> per meter</span>
						<select class="form-control" name="datacable">
							<?php
							for($i=0; $i<=25; $i+=1) {
								print '<option value="'.$i.'">'.$i.' meter</option>';
							}
							?>	
						</select>
						<span class="info-label"><?php echo get_field('datakabel_extra_tekst'); ?></span>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="---Funnel-buttons">
					<div class="container">
						<div class="button-right">
							<input class="btn" type="button" name="next" onclick="return toStep('werkzaamheden');" value="Naar stap 3/3" />
						</div>
						<div class="button-left">
							<input class="btn disabled" type="button" onclick="return toStep('meterkast', true);" name="next" value="Naar stap 1/3" />
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="item-section section-werkzaamheden hidden">
				<h2>Werkzaamheden route</h2>
				<div class="form-group first">
					<label class="under-label"><?php echo get_field('vraag_7'); ?> <?php if(get_field('hulp_vraag_7') != null && get_field('hulp_vraag_7') != ""): ?><a class="questionPopup" href="javascript:void(0);" data-title="" data-txt="<?php echo strip_tags(get_field('hulp_vraag_7')); ?>"><img src="<?php echo get_template_directory_uri().'/images/question-2.svg';?>" alt="" /></a><?php endif; ?></label>
					<span class="small-info">Bestrating uitgesloten</span>
					<label class="select-label">
						<span class="text-label"><span class="price">&euro; <?php echo get_field('graafwerk'); ?></span> per meter</span>
						<select class="form-control" name="digging">
							<?php
							for($i=0; $i<=25; $i+=1) {
								print '<option value="'.$i.'">'.$i.' meter</option>';
							}
							?>	
						</select>
						<span class="info-label"><?php echo get_field('graafwerk_extra_tekst'); ?></span>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="form-group">
					<label class="under-label"><?php echo get_field('vraag_8'); ?> <?php if(get_field('hulp_vraag_8') != null && get_field('hulp_vraag_8') != ""): ?><a class="questionPopup" href="javascript:void(0);" data-title="" data-txt="<?php echo strip_tags(get_field('hulp_vraag_8')); ?>"><img src="<?php echo get_template_directory_uri().'/images/question-2.svg';?>" alt="" /></a><?php endif; ?></label>
					<span class="small-info"><?php echo get_field('bestrating_extra_tekst'); ?></span>
					<label class="select-label">
						<span class="text-label"><span class="price">&euro; <?php echo get_field('bestratingswerk'); ?></span> per meter</span>
						<select class="form-control" name="pavement">
							<?php
							for($i=0; $i<=25; $i+=1) {
								print '<option value="'.$i.'">'.$i.' meter</option>';
							}
							?>	
						</select>
					</span>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="---Funnel-buttons">
					<div class="container">
						<div class="button-right">
							<input class="btn btn-final-step" type="button" disabled="disabled" name="next" onclick="return false;" value="Einde stap 3/3" />
						</div>
						<div class="button-left">
							<input class="btn disabled" type="button" name="next" onclick="return toStep('bekabeling', true);" value="Naar stap 2/3" />
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="right">
			<div class="switch-box">
				<label class="switch">
					<input type="checkbox">
					<span class="slider round"></span>
				</label>
				<p>Toon prijzen inclusief BTW</p>
				<div class="clearfix"></div>
			</div>
			<div class="result-box">
				<h1>Uw overzicht</h1>	
				<div class="collapse collapse-standard active">
					<a class="title" href="javascript:void(0);"><span class="pull-left"><img src="<?php echo get_template_directory_uri();?>/images/arrow-down-icon.svg" alt="Pijl"> Standaard installatie</span> <span class="pull-right price-span" data-inc="<?php echo get_field('standaard_installatie')*1.21; ?>" data-exc="<?php echo get_field('standaard_installatie'); ?>">&euro; <span class="price_digit"><?php echo get_field('standaard_installatie'); ?></span></span><span class="clearfix"></span></a>
					<div class="description">
						<?php echo get_field('standaard_installatie_label'); ?>
					</div>			
				</div>
				<div class="collapse collapse-meterkast active">
					<a class="title" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri();?>/images/arrow-down-icon.svg" alt="Pijl"> Meterkast</a>
					<div class="prices">
						<div>
							<span class="label-left">3-fase aanpassing</span>
							<span class="label-right present-outcome price-span" data-inc="<?php echo get_field('3_fase_aanpassing')*1.21; ?>" data-exc="<?php echo get_field('3_fase_aanpassing'); ?>"><span class="price_digit">0</span></span>
							<span class="label-center">&euro;</span>
							<div class="clearfix"></div>
						</div>
						<div>
							<span class="label-left">Aardlekautomaat <span class="adapt-label">1 fase</span></span>
							<span class="label-right adapt-outcome price-span" data-inc="<?php echo get_field('aardlekautomaat')*1.21; ?>" data-exc="<?php echo get_field('aardlekautomaat'); ?>"><span class="price_digit">0</span></span>
							<span class="label-center">&euro;</span>
							<div class="clearfix"></div>
						</div>
						<div>
							<span class="label-left">Uitbereiding meterkast <span class="extend-label"></span></span>
							<span class="label-right extend-outcome price-span" data-inc="<?php echo get_field('uitbereiding_6_fasen')*1.21; ?>" data-exc="<?php echo get_field('uitbereiding_6_fasen'); ?>"><span class="price_digit">0</span></span>
							<span class="label-center">&euro;</span>
							<div class="clearfix"></div>
						</div>
					</div>			
				</div>
				<?php
				/*

		"powercable": <?php echo number_format(get_field('voedingskabel'),2,'.',','); ?>,
		"datacable": <?php echo number_format(get_field('datakabel'),2,'.',','); ?>,
		"digging": <?php echo number_format(get_field('graafwerk'),2,'.',','); ?>,
		"pavement": <?php echo number_format(get_field('bestratingswerk'),2,'.',','); ?>,*/
		?>
				<div class="collapse collapse-bekabeling">
					<a class="title" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri();?>/images/arrow-down-icon.svg" alt="Pijl"> Bekabeling</a>
					<div class="prices">
						<div>
							<span class="label-left">Voedingskabel <span class="voeding-label"></span></span>
							<span class="label-right voeding-outcome price-span" data-inc="<?php echo get_field('voedingskabel')*1.21; ?>" data-exc="<?php echo get_field('voedingskabel'); ?>"><span class="price_digit">n.v.t.</span></span>
							<span class="label-center">&euro;</span>
							<div class="clearfix"></div>
						</div>
						<div>
							<span class="label-left">Datakabel <span class="data-label"></span></span>
							<span class="label-right data-outcome price-span" data-inc="<?php echo get_field('datakabel')*1.21; ?>" data-exc="<?php echo get_field('datakabel'); ?>"><span class="price_digit">n.v.t.</span></span>
							<span class="label-center">&euro;</span>
							<div class="clearfix"></div>
						</div>
					</div>			
				</div>
				<div class="collapse collapse-werkzaamheden">
					<a class="title" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri();?>/images/arrow-down-icon.svg" alt="Pijl"> Werkzaamheden route</a>
					<div class="prices">
						<div>
							<span class="label-left">Graafwerk <span class="dig-label"></span></span>
							<span class="label-right dig-outcome price-span" data-inc="<?php echo get_field('graafwerk')*1.21; ?>" data-exc="<?php echo get_field('graafwerk'); ?>"><span class="price_digit">0</span></span>
							<span class="label-center">&euro;</span>
							<div class="clearfix"></div>
						</div>
						<div>
							<span class="label-left">Bestratingswerk <span class="street-label"></span></span>
							<span class="label-right street-outcome price-span" data-inc="<?php echo get_field('bestratingswerk')*1.21; ?>" data-exc="<?php echo get_field('bestratingswerk'); ?>"><span class="price_digit">0</span></span>
							<span class="label-center">&euro;</span>
							<div class="clearfix"></div>
						</div>
						<div class="hidden">
							<span class="label-left">Opbouwwerk <span class="dig-label"></span></span>
							<span class="label-right buildup-outcome"><span class="price_digit">0</span></span>
							<span class="label-center">&euro;</span>
							<div class="clearfix"></div>
						</div>
						<div class="hidden">
							<span class="label-left">Kabelgoot <span class="dig-label"></span></span>
							<span class="label-right cables-outcome"><span class="price_digit">0</span></span>
							<span class="label-center">&euro;</span>
							<div class="clearfix"></div>
						</div>
					</div>			
				</div>
				<div class="total">
					<span class="totalLabel">Totaal:</span><span class="totalPrice ">&euro; <span class="price_digit">0</span></span>
				</div>
			</div>
			<p class="outro" style="display: none;">
				<?php echo str_replace(array("<p>", "</p>"), "", get_field('intro')); ?>
			</p>
		</div>
		<div class="clearfix"></div>
	</section>
</div>
<script type="text/javascript">
	var installLabels = {
		"standard": "<?php echo str_replace(" ", "", nl2br(get_field('standaard_installatie_label'))); ?>",
		"adapt": "3-fase aanpassing",
		"aardlek": "Aardlekautomaat",
		"extend": "Uitbereiding meterkast"
	};

	var installPrices = {
		"standard": <?php echo number_format(get_field('standaard_installatie'),2,'.',','); ?>,
		"adapt": <?php echo number_format(get_field('3_fase_aanpassing'),2,'.',','); ?>,
		"aardlek": <?php echo number_format(get_field('aardlekautomaat'),2,'.',','); ?>,
		"aardlek_drie": <?php echo number_format(get_field('aardlekautomaat_drie_fase'),2,'.',','); ?>,
		"extend_small": <?php echo number_format(get_field('uitbereiding_6_fasen'),2,'.',','); ?>,
		"extend_big": <?php echo number_format(get_field('uitbereiding_8_fasen'),2,'.',','); ?>,
		"powercable": <?php echo number_format(get_field('voedingskabel'),2,'.',','); ?>,
		"datacable": <?php echo number_format(get_field('datakabel'),2,'.',','); ?>,
		"digging": <?php echo number_format(get_field('graafwerk'),2,'.',','); ?>,
		"pavement": <?php echo number_format(get_field('bestratingswerk'),2,'.',','); ?>,
		"buildup": <?php echo number_format(get_field('opbouwwerk'),2,'.',','); ?>,
		"cables": <?php echo number_format(get_field('kabelgoot'),2,'.',','); ?>
	};
</script>
<?php
get_footer('funnel');
?>