<?php

$GLOBALS['INFO_QUESTIONS'] = array(
	//product detail product-opties
	'pa_kleur' => 'Kies hier uw kleur',
	'pa_montage' => 'Geef hier aan op welke manier op u het laadstation wilt monteren. Bij een laadstation op de oprit is altijd een laadpaal benodigd.',
	'pa_installatiekosten' => 'Geef hier aan of u gebruik wil maken van de basisinstallatie. Op onze installatiepagina kunt u vinden wat hieronder valt.',
	'pa_vermogen' => 'Geef hier aan welk laadvermogen u wenst. Check van tevoren goed of dit in lijn is met het vermogen van uw meterkast. ',
		'pa_load-balancing-elvi' => 'Load balancing is de techniek die de laadsnelheid aanpast op het overige verbruik in huis. Alle andere elektrische apparaten krijgen voorrang, en de resterende capaciteit wordt gebruikt om de auto op te laden. Kies dus voor een laadpaal met load balancing om optimaal gebruik te maken van jouw stroomaansluiting, en op de snelst mogelijke manier te laden.<br></br><strong>Belangrijk:</strong> Laat u zich door ons adviseren of load balancing nodig is. <strong>Load balancing is verplicht door ons te laten installeren.</strong>',
	'pa_kabellengte' => 'Geef hier of u een vaste kabel wilt bestellen en in welke lengte.',
    'pa_installatiemateriaal' => 'De verbruiksmeter in de groepenkast kan worden ingebouwd. Hiermee kunt u zien hoeveel elektriciteit er is verbruikt door de laadpaal. De meters zijn MID geijkt, dat betekent dat het verbruik ook door de fiscus wordt geaccepteerd.',

	//product detail specs
	'sku' => 'Het unieke nummer behorende bij dit product.',
	'pa_fase' => 'Geeft het laadvermogen aan (1-fase of 3-fase).',
	'pa_ampere' => 'Geeft de stroomsterkte aan van de laadpaal.',
	'pa_contactdoos' => 'Bij uw auto ontvangt u een laadkabel. Het is mogelijk om een extra kabel te bestellen. Er zijn 2 verschillende soorten stekkers. Binnen Europa wordt de type 2 contactdoos gebruikt. De laadkabel die in de laadpaal gaat is altijd type 2. Het ligt aan het type auto welke contactdoos de auto heeft.',
	'pa_ipwaarde' => 'Het eerste cijfer geeft de mate van bescherming aan tegen vaste stoffen en het tweede cijfer de bescherming tegen vocht en water.',
	'pa_statusweergave' => 'Manier waarop gecommuniceerd wordt op de laadpaal.',
	'pa_breedte' => 'Breedte van het laadstation in millimeters. ',
	'pa_diepte' => 'Diepte van het laadstation in millimeters.',
	'pa_materiaal' => 'Materiaal van het laadstation. ',
	'pa_internetaansluiting' => 'Manier waarop het station met internet verbindt (GPRS, Ethernet, WiFi)',
	'pa_rfidreader' => 'Laadpas authenticatie d.m.v. radiosignaal (Radio Frequency Identification).',
	'pa_mintemp' => 'Minimale temperatuur voor het laadstation.',
	'pa_maxtemp' => 'Maximale temperatuur voor het laadstation.',
	'pa_luchtdrukbereik' => '',
	'pa_vochtigheidsgraad' => '',
	'pa_mincap' => 'Minimum capaciteit in kW waarop het station kan laden. ',
	'pa_maxcap' => 'Maximum capaciteit in kW waarop het station kan laden.',
	'pa_abonnement' => 'Abonnement op service en slimme diensten. ',
	'pa_onlineinzicht' => '',
	'pa_gastverbruik' => 'Mogelijkheid voor gasten om de laadpaal te gebruiken. ',
	'pa_laadtarieven' => 'Zelf bepalen tegen welke kosten anderen kunnen laden op uw station.',
	'pa_emailnotific' => 'Berichtgeving per e-mail over verbruik en informatie. ',
	'pa_dynpowerman' => '	Communicatie tussen het laadstation en de meterkast die overbelasting voorkomt. ',
	'pa_toegangsbeheer' => '',
	'pa_exportlaad' => 'Mogelijkheid tot downloaden van laadinformatie.',
	'pa_remotecharg' => 'Op afstand controle en beheer van uw laadstations',

	//belangrijkste kenmerken
	'pa_verbruikverrekenen' => 'Het laadstation beschikt over een kW-meter die uw verbruik bijhoudt.',
	'pa_onlineinzicht' => 'Uw verbruik in kW wordt online weergegeven in een app.',
	'pa_smartcharging' => 'Het laadstation is aangesloten op internet en communiceert met andere elektrische apparatuur om het netwerk zo veilig mogelijk te benutten.',
	'pa_updaten' => 'Het laadstation is geschikt voor updates naar ‘slimme’ online functies.',
	'pa_onderhoudsarm' => 'Het laadstation gaat lang mee en vergt geen regelmatig onderhoud of vervanging van componenten. ',
	'pa_vastekabel' => 'Er zit een vaste kabel aan het laadstation bevestigd. Deze hoeft u dus niet uit de auto te halen en naar de laadpaal te brengen. ',

	//opties
	'pa_paal' => 'Laadstations op de oprit worden op een paal geplaatst. ',
	'pa_paalhoogte' => '',
	'pa_wandmontage' => 'Het laadstation wordt aan de muur bevestigd, zonder laadpaal.',
	'pa_kabels' => 'Voedingskabels bij het laadstation zitten in de standaardinstallatie inbegrepen (tot 10m).',

	//laadmogelijkheden
	'pa_mincap' => 'Minimum capaciteit in kW waarop het station kan laden. ',
	'pa_maxcap' => 'Maximum capaciteit in kW waarop het station kan laden. ',
	'pa_fase' => 'Geeft het laadvermogen aan (1-fase of 3-fase).',
	'pa_loadbalancing' => 'Load balancing is de techniek die de laadsnelheid aanpast op het overige verbruik in huis. Alle andere elektrische apparaten krijgen voorrang, en de resterende capaciteit wordt gebruikt om de auto op te laden. Kies dus voor een laadpaal met load balancing om optimaal gebruik te maken van jouw stroomaansluiting, en op de snelst mogelijke manier te laden.<br></br><strong>Belangrijk:</strong> Laat u zich door ons adviseren of load balancing nodig is. <strong>Load balancing is verplicht door ons te laten installeren.</strong>',

	//personalisatie
	'pa_eigenlogo' => 'Mogelijk om eigen logo of andere afbeelding op het laadstation te plaatsen.',
	'pa_beschikkleur' => 'Kleuren waarin de behuizing van het laadstation standaard beschikbaar is.',
	'pa_andstandkleur' => 'Andere standaardkleuren',
	'pa_customkleur' => 'Mogelijk om zelf een kleur op te geven in RAL-waarde.',

	//technische specs
	'pa_ampere' => 'Geeft de stroomsterkte aan van de laadpaal.	',
	'pa_contactdoos' => 'Bij uw auto ontvangt u een laadkabel. Het is mogelijk om een extra kabel te bestellen. Er zijn 2 verschillende soorten stekkers. Binnen Europa wordt de type 2 contactdoos gebruikt. De laadkabel die in de laadpaal gaat is altijd type 2. Het ligt aan het type auto welke contactdoos de auto heeft.',
	'pa_ipwaarde' => 'Het eerste cijfer geeft de mate van bescherming aan tegen vaste stoffen en het tweede cijfer de bescherming tegen vocht en water.',
	'pa_statusweergave' => 'Manier waarop gecommuniceerd wordt op de laadpaal.',
	'pa_hoogte' => 'Hoogte van het laadstation in millimeters',
	'pa_breedte' => 'Breedte van het laadstation in millimeters',
	'pa_diepte' => 'Diepte van het laadstation in millimeters.',
	'pa_materiaal' => 'Materiaal van het laadstation',

	//installatie
	'pa_price' => 'Geef hier aan of u gebruik wil maken van de standaardinstallatie. Op onze installatiepagina kunt u vinden wat hieronder valt.',
	'pa_voorrijkosten' => 'De voorrijdkosten zijn in Nederland inbegrepen in de installatieprijs. ',
	'pa_arbeidskosten' => 'De arbeidskosten zijn inbegrepen in de installatieprijs, zeer complexe gevallen uitgezonderd. In die gevallen wordt er een schouwing gepland met een op maat gemaakte offerte.',
	'pa_kleinmateriaal' => 'Klein materiaal is inbegrepen in de installatieprijs. ',
	'pa_ingegravenpaal' => 'Klein materiaal is inbegrepen in de installatieprijs. ',
	'pa_10mvoeding' => 'Tot 10m zijn de kosten voor de voedingskabel inbegrepen in de installatieprijs. Daarboven geldt een meerprijs per additionele meter (u betaalt alleen voor wat u nodig heeft).',
	'pa_5mgraafwerk' => 'De kosten voor graafwerk zijn tot 5m inbegrepen in de installatieprijs. Daarboven geldt een meerprijs per additionele meter (u betaalt alleen voor wat u nodig heeft). ',
	'pa_bestratingswerk' => 'De kosten voor bestrating zijn niet inbegrepen in de installatieprijs. Hiervoor geldt een meerprijs per additionele meter (u betaalt alleen voor wat u nodig heeft).'
);
?>