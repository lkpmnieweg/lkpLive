<?php
/* Template name: Laadkompas - Zonnepanelen Klantenservice */

get_header();
?>
<div class="container page-container">
	<div class="title">
		<h1>Welkom bij de zonnepanelen klantenservice</h1>
		<p>Waar kunnen we u bij helpen?</p>
	</div>
	<div class="boxes">
		<div class="row">
			<div class="column-3">
				<div class="box">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/faq_1.svg" alt="Faq 1" />
					</div>
					<h2><?php echo get_field('titel_1'); ?></h2>
					<?php
					wp_nav_menu(
					  array(
					    'theme_location' => 'faq-menu-7'
					  )
					);
					?>
				</div>
			</div>
			<div class="column-3">
				<div class="box">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/faq_2.svg" alt="Faq 2" />
					</div>
					<h2><?php echo get_field('titel_2'); ?></h2>
					<?php
					wp_nav_menu(
					  array(
					    'theme_location' => 'faq-menu-8'
					  )
					);
					?>
				</div>
			</div>
			<div class="column-3">
				<div class="box">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/faq_3.svg" alt="Faq 3" />
					</div>
					<h2><?php echo get_field('titel_3'); ?></h2>
					<?php
					wp_nav_menu(
					  array(
					    'theme_location' => 'faq-menu-9'
					  )
					);
					?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="row">
			<div class="column-3">
				<div class="box">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/faq_4.svg" alt="Faq 4" />
					</div>
					<h2><?php echo get_field('titel_4'); ?></h2>
					<?php
					wp_nav_menu(
					  array(
					    'theme_location' => 'faq-menu-10'
					  )
					);
					?>
				</div>
			</div>
			<div class="column-3">
				<div class="box">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/faq_5.svg" alt="Faq 5" />
					</div>
					<h2><?php echo get_field('titel_5'); ?></h2>
					<?php
					wp_nav_menu(
					  array(
					    'theme_location' => 'faq-menu-11'
					  )
					);
					?>
				</div>
			</div>
			<div class="column-3">
				<div class="box">
					<div class="icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/faq_6.svg" alt="Faq 6" />
					</div>
					<h2><?php echo get_field('titel_6'); ?></h2>
					<?php
					wp_nav_menu(
					  array(
					    'theme_location' => 'faq-menu-12'
					  )
					);
					?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="questions">
		<h3>Toch nog vragen?</h3>
		<p>Neem dan even contact met ons op, wij helpen u graag verder.<br/>
Wij zijn bereikbaar van maandag t/m Vrijdag<br/>van 09:00 tot 17:30.</p>
			<div class="row">
				<div class="column-2">
					<a href="tel:<?php echo str_replace(" ", "", get_option('woocommerce_store_phone'));?>" title="Bel ons">
						<img src="<?php echo get_template_directory_uri(); ?>/images/contact_phone.svg" alt="Telefoonnummer" />
						<?php echo get_option('woocommerce_store_phone');?>
					</a>
				</div>
				<div class="column-2">
					<a href="mailto:info@laadkompas.nl">
						<img src="<?php echo get_template_directory_uri(); ?>/images/contact_mail.svg" alt="Emailadres" /> info@laadkompas.nl
					</a>
				</div>
			</div>
	</div>
</div>
<?php
get_footer();
?>