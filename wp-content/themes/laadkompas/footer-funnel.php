<footer>
	<div class="footer-content">
		<div class="container">
			<div class="row">
				<div class="footer-column-1">
					<div class="column-content">
						<img src="<?php echo get_template_directory_uri(); ?>/images/icon-lkp.svg" alt="" />
						<h4>Advies nodig?</h4>
						<p>Krijg direct advies van &eacute;&eacute;n van onze experts.</p>
					</div>
				</div>
				<div class="footer-column-2">
					<div class="column-content">
						<img src="<?php echo get_template_directory_uri(); ?>/images/icon-phone-white.svg" alt="" />
						<h4><a href="tel:0889990600">088 999 0600</a></h4>
						<p>Maandag t/m vrijdag : 09.00 - 17.30</p>
					</div>
				</div>
				<div class="footer-column-3">
					<div class="column-content">
						<img src="<?php echo get_template_directory_uri(); ?>/images/mail-icon.svg" alt="" />
						<h4><a href="mailto:info@laadkompas.nl">info@laadkompas.nl</a></h4>
						<p>Op werkdagen binnen 24u reactie.</p>
					</div>
				</div>
				<div class="clearfix">
					
				</div>
			</div>
		</div>	
	</div>
	<div class="footer-bar">
		<div class="container">
			<div class="social">
				<a href="https://www.facebook.com/laadkompas/" target="_blank" title="Facebook"><img src="<?php echo get_template_directory_uri(); ?>/images/f-logo-rgb-white-1024@2x.png" alt="Facebook" /></a>
				<a href="https://twitter.com/laadkompas" target="_blank" title="Twitter"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter-social-icon-circle-white@2x.png" alt="Twitter" /></a>
				<a href="https://www.instagram.com/laadkompas/" target="_blank" title="Instagram"><img src="<?php echo get_template_directory_uri(); ?>/images/glyph-logo-may-2016-copy@2x.png" alt="Instagram" /></a>
				<a href="https://www.linkedin.com/company/laadkompas" target="_blank" title="LinkedIn"><img src="<?php echo get_template_directory_uri(); ?>/images/in-white-128-px-r@2x.png" alt="LinkedIn" /></a>
				<div class="clearfix"></div>
			</div>
			<div class="links">
				<a href="<?php echo get_the_permalink(3014); ?>" title="Algemene voorwaarden">Algemene voorwaarden</a>
				<a href="<?php echo get_the_permalink(3); ?>" title="Privacybeleid">Privacybeleid</a>
			</div>
			<div class="copyright">&copy; <?php echo date("Y"); ?> <a href="http://www.laadkompas.nl">Laadkompas.nl</a></div>
			<div class="clearfix"></div>
		</div>
	</div>
</footer>

<?php
if(!isset($_COOKIE['lkp_cookie_settings'])):
?>
<div id="cookie">
	<div class="top">
		<div class="container">
			<div class="left">
				<p>Laadkompas.nl maakt gebruik van cookies ter verbetering van de website-ervaring en overige marketingdoeleinden.<br/> Door op ‘Alle Cookies’ te klikken gaat u akkoord met ons privacybeleid waaronder valt dat we op vertrouwelijke wijze met uw gegevens omgaan.</p>
				<a href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/link-wit.svg" alt="Meer opties" /> Meer opties</a>
			</div>
			<div class="right">
				<a href="javascript:void(0);">Ga naar website</a>
				<a href="javascript:void(0);">Personaliseren</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="bottom">
		<div class="container">
			<p>Wilt u een gepersonaliseerde ervaring? Vink dan alles aan.<br/> Wilt u geen gepersonaliseerde ervaring? Laat dan alles uit staan.</p>
			<div class="options">
				<div class="disabled">
					<input id="nessecary" disabled="disabled" checked="checked" type="checkbox" name="type_cookie" value="nessecary" />
					<label for="nessecary">
						<span class="custom-check"></span>
						<span class="custom-label text">
							<span>Noodzakelijk</span>
							<span>Helpen de website bij basisfuncties als navigatie en toegang tot beveiligde onderdelen, deze zijn noodzakelijk.</span>
						</span>
					</label>
				</div>
				<div>
					<input id="statistics" type="checkbox" name="type_cookie" value="statistics" />
					<label for="statistics">
						<span class="custom-check"></span>
						<span class="custom-label text">
							<span>Statistieken</span>
							<span>Volgen de bezoekers van de website anoniem en geven informatie waarmee we de website kunnen verbeteren.</span>
						</span>
					</label>
				</div>
				<div>
					<input id="marketing" type="checkbox" name="type_cookie" value="marketing" />
					<label for="marketing">
						<span class="custom-check"></span>
						<span class="custom-label text">
							<span>Marketing</span>
							<span>Volgen bezoekers via overige websites en geven informatie waarmee wij advertenties kunnen personaliseren.</span>
						</span>
					</label>
				</div>
			</div>
			<div class="button_wrapper">
				<a class="btn" href="javascript:void(0);">Voorkeuren opslaan</a>
			</div>
		</div>
	</div>
</div>
<?php
endif;
?>
	<?php wp_styles(); ?>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/lib/swiper.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<?php wp_footer(); 

if(isset($_COOKIE['lkp_cookie_settings']) && $_COOKIE['lkp_cookie_settings'] != "")
{
	$cookie = str_replace(array('[\"', '\"]'), '', $_COOKIE['lkp_cookie_settings']);
	$items = explode('\",', $cookie);
	$newItems = [];
	foreach($items as $item)
	{
		$itemname = str_replace('\"', '', $item);
		$newItems[] = $itemname;
	}

	if(in_array('nessecary', $newItems))
	{
		$functioneel = get_field('functioneel', 78);
		if($functioneel)
			print $functioneel;
	}
	if(in_array('statistics', $newItems))
	{
		$statistieken = get_field('statistieken', 78);
		if($statistieken)
			print $statistieken;
	}
	if(in_array('marketing', $newItems))
	{
		$marketing = get_field('marketing', 78);
		if($marketing)
			print $marketing;
	}
}
?>

	<noscript>
		<style type="text/css">
			body * {
				display: none;
			}
			.noJS { 
				border: 1px solid #000;
				max-width: 400px;
				margin: 100px auto;
				display: block !important;
				padding: 20px;
				text-align: center;
				line-height: 24px;
			}
		</style>
	</noscript>
	<div class="noJS">Deze website kan niet functioneren zonder JavaScript.</div>
</body>
</html>