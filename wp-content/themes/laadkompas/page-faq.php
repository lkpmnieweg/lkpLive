<?php
/* Template name: Laadkompas - Veelgestelde vragen */


get_header();
$klantenservice_page_ID = 768;
?>
<div class="container page-container back-link">
	<a href="<?php echo get_the_permalink(768); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="Terug naar klantenservice" /> <span>Terug naar de klantenservice</span></a>
</div>
<div class="container page-container">

	<?php
	//fetch parent
	global $post;
	$parent_id = wp_get_post_parent_id($post->ID);
	if($parent_id):
		$parent_post = get_post($parent_id);
	?>
	<div class="page-content">
		<div class="right first">
			<div class="title">
				<h1><?php echo $parent_post->post_title; ?></h1>
				<h2><?php echo strip_tags($parent_post->post_content); ?></h2>
			</div>
		</div>
	</div>
	<?php
	endif;

	?>	
	<div class="page-content">
		<div class="left">
			<div class="collapse <?php echo (cms_is_in_menu( 180, $page->ID ))? 'active' : ''; ?>">
				<a class="title" href="javascript:void(0);"><?php echo get_field('titel_1', $klantenservice_page_ID); ?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon-green.svg" alt="Pijl groen" /></a>
				<?php
				wp_nav_menu(
				  array(
				    'theme_location' => 'faq-menu-1'
				  )
				);
				?>
			</div>
			<div class="collapse <?php echo (cms_is_in_menu( 181, $page->ID ))? 'active' : ''; ?>">
				<a class="title" href="javascript:void(0);"><?php echo get_field('titel_2', $klantenservice_page_ID); ?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon-green.svg" alt="Pijl groen" /></a>
				<?php
				wp_nav_menu(
				  array(
				    'theme_location' => 'faq-menu-2'
				  )
				);
				?>
			</div>
			<div class="collapse <?php echo (cms_is_in_menu( 182, $page->ID ))? 'active' : ''; ?>">
				<a class="title" href="javascript:void(0);"><?php echo get_field('titel_3', $klantenservice_page_ID); ?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon-green.svg" alt="Pijl groen" /></a>
				<?php
				wp_nav_menu(
				  array(
				    'theme_location' => 'faq-menu-3'
				  )
				);
				?>
			</div>
			<div class="collapse <?php echo (cms_is_in_menu( 183, $page->ID ))? 'active' : ''; ?>">
				<a class="title" href="javascript:void(0);"><?php echo get_field('titel_4', $klantenservice_page_ID); ?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon-green.svg" alt="Pijl groen" /></a>
				<?php
				wp_nav_menu(
				  array(
				    'theme_location' => 'faq-menu-4'
				  )
				);
				?>
			</div>
			<div class="collapse <?php echo (cms_is_in_menu( 184, $page->ID ))? 'active' : ''; ?>">
				<a class="title" href="javascript:void(0);"><?php echo get_field('titel_5', $klantenservice_page_ID); ?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon-green.svg" alt="Pijl groen" /></a>
				<?php
				wp_nav_menu(
				  array(
				    'theme_location' => 'faq-menu-5'
				  )
				);
				?>
			</div>
			<div class="collapse <?php echo (cms_is_in_menu( 185, $page->ID ))? 'active' : ''; ?>">
				<a class="title" href="javascript:void(0);"><?php echo get_field('titel_6', $klantenservice_page_ID); ?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon-green.svg" alt="Pijl groen" /></a>
				<?php
				wp_nav_menu(
				  array(
				    'theme_location' => 'faq-menu-6'
				  )
				);
				?>
			</div>
		</div>
		<div class="right">
			<?php 
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post(); 
			?>
			<div class="faq_title"><?php echo get_the_title(); ?></div>
			<div class="content-items">
				<?php 
				if(get_field('vraag_1') && get_field('antwoord_1')):
				?>
				<div class="collapse active">
					<a class="txt_title" href="javascript:void(0);"><?php echo get_field('vraag_1');?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon.svg" alt="Pijl" /></a>
					<?php echo get_field('antwoord_1'); ?>
				</div>
				<?php
				endif;

				if(get_field('vraag_2') && get_field('antwoord_2')):
				?>
				<div class="collapse">
					<a class="txt_title" href="javascript:void(0);"><?php echo get_field('vraag_2');?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon.svg" alt="Pijl" /></a>
					<?php echo get_field('antwoord_2'); ?>
				</div>
				<?php
				endif;

				if(get_field('vraag_3') && get_field('antwoord_3')):
				?>
				<div class="collapse">
					<a class="txt_title" href="javascript:void(0);"><?php echo get_field('vraag_3');?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon.svg" alt="Pijl" /></a>
					<?php echo get_field('antwoord_3'); ?>
				</div>
				<?php
				endif;

				if(get_field('vraag_4') && get_field('antwoord_4')):
				?>
				<div class="collapse">
					<a class="txt_title" href="javascript:void(0);"><?php echo get_field('vraag_4');?> <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-icon.svg" alt="Pijl" /></a>
					<?php echo get_field('antwoord_4'); ?>
				</div>
				<?php
				endif;


				?>
			</div>
			<?php
				endwhile;
			endif;
			?>
			
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php
get_footer();
?>