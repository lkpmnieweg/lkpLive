<?php
@session_start();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Muli:400,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Google Tag Manager -->
	<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M9P25B6');
	</script>
	<!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-98392756-1"></script>
	<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-98392756-1');
	</script>

	<!-- Hotjar Tracking Code for www.laadkompas.nl -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1358613,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
</head>
<body <?php body_class(); ?>>
<div class="woocommerce-notices-wrapper vergelijking">
	<div class="woocommerce-message" role="alert">
		<span></span>
		<a class="vergelijk-link" href="<?php the_permalink(223); ?>">Vergelijk <span class="count">2</span> laadpalen</a>
	</div>
</div>
	<header>
		<nav>
		<div id="menu_overlay"></div>
			<div class="container">
				<a class="menu_btn" href="javascript:toggleMenu();" title="Menu"><img src="<?php echo get_template_directory_uri(); ?>/images/hamburger-menu.svg" alt="Menu" /></a>
				<a class="logo" href="<?php echo get_site_url(); ?>">laadkompas<span>|</span><span>Experts in opladen</span></a>
				<div class="float-right">
					<ul class="top-menu">
						<li class="first">
							<?php /* <a class="btn" href="<?php echo get_site_url(); ?>">Offerte aanvragen</a> */ ?>
						</li>
						<li class="last">
							<a href="tel:<?php echo str_replace(" ", "", get_option('woocommerce_store_phone'));?>" title="Bel ons">
								<img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon-white.svg" alt="Bel ons" /> 
								<?php echo get_option('woocommerce_store_phone');?>
							</a>
						</li>
						<li class="cart">
							<a href="<?php echo get_the_permalink(7); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/cart.svg" alt="winkelwagen" /><?php if(WC()->cart->get_cart_contents_count() > 0): ?><span class="count"><?php echo WC()->cart->get_cart_contents_count(); ?></span><?php endif; ?></a>
						</li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="header_menu">
				<div class="hidden mobile-title">
					Menu
					<a class="closeMenu"><i class="fa fa-times"></i></a>
					<div class="clearfix"></div>
				</div>
				<div class="container">
					<?php
					wp_nav_menu(
					  array(
					    'theme_location' => 'header-menu'
					  )
					);
					?>
					<div class="clearfix"></div>
				</div>
			</div>
		</nav>
		<?php
		if(is_front_page()):

		if(isset($_SESSION['lkp_current_funnel']))
		{
			$current_funnel = unserialize(decryptData($_SESSION['lkp_current_funnel']));

		}
		?>
		<div class="hero">
			<div class="container">
				<h1><?php echo get_field('banner_titel'); ?></h1>
				<h2><?php echo get_field('banner_ondertitel'); ?></h2>
				<div class="action-form <?php echo (isset($current_funnel->car_type))? 'active' : ''; ?>">
					<h3>Gratis persoonlijk laadadvies?<br/>Wij helpen u graag.</h3>
					<form action="" method="POST">
						<div class="form-group">
							<label for="car">Uw auto</label>
							<select name="car_brand" class="form-control" id="car_brand">
								<option value="Verschillende elektrische auto's" selected="selected">Verschillende elektrische auto's</option>
								<option value="Overig, mijn auto staat er niet tussen">Overig, mijn auto staat er niet tussen</option>
								<?php
									
									$brands = fetchCarBrands();
									foreach($brands as $brand)
									{
										echo '<option data-brandid="'.$brand->id.'" '.((isset($current_funnel->car_brand) && $current_funnel->car_brand == $brand->brand_name)? 'selected="selected"' : '').' value="'.$brand->brand_name.'">'.$brand->brand_name.'</option>';
									}
								?>
							</select>
						</div>
						<div class="form-group car_type">
							<label for="car">Auto type:</label>
							<select name="car_type" class="form-control" id="car_type">
								<option value="" selected="selected" disabled="disabled">Selecteer Type</option>
								<?php
									$types = fetchCarTypes(@$current_funnel->car_type);
									echo $types;
								?>
							</select>
						</div>
						<input class="btn" type="submit" name="advice" value="Advies aanvragen" />
					</form>
				</div>
			</div>
		</div>
		<div class="usp-bar <?php echo (isset($current_funnel->car_type))? 'active' : ''; ?>">
			<div class="container">
				<ul>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="100% afhankelijk" /> <strong>De </strong> #1 <strong>van Nederland</strong></li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Installatie" /> <strong>Installatie binnen </strong> 10 dagen</li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Besparen tot 40%" /> 100% <strong>onafhankelijk</strong></li>
				</ul>
			</div>
		</div>
		<?php
		endif;
		?>
	</header>
<?php

/*
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
			<header class="header clear" role="banner">

					<!-- logo -->
					<div class="logo">
						<a href="<?php echo home_url(); ?>">
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo" class="logo-img">
						</a>
					</div>
					<!-- /logo -->

					<!-- nav -->
					<nav class="nav" role="navigation">
						<?php html5blank_nav(); ?>
					</nav>
					<!-- /nav -->

			</header>
			<!-- /header -->
*/