<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
<?php
	if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<div class="row">
				<div class="column-2 column-offset-2">
					<h1>Bestelling afgerond</h1>
					<p>Dankuwel voor het plaatsen van uw bestelling bij Laadkompas. Wij gaan direct voor u aan de slag! Uw ordernummer is : <span>#<?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></span></p>
					<p>Wat kunt u nu van ons verwachten?</p>
					<ul>
						<li>
							<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/mail.svg" alt=""></span>
							<span class="description">
								<span class="title">Email</span>
								<p class="description">
									U ontvangt de factuur en de bevestiging spoedig per email op <span><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></span>
								</p>
							</span>
						</li>
						<li>
							<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/bezorging.svg" alt=""></span>
							<span class="description">
								<span class="title">Bezorging</span>
								<p class="description">
									Wij hebben inmiddels opdracht gegeven tot assemblage en uitlevering. De bestelling zal in ca. 3-5 werkdagen worden afgeleverd. 
								</p>
							</span>
						</li>
						<li>
							<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/calendar.svg" alt=""></span>
							<span class="description">
								<span class="title">Afspraak</span>
								<p class="description">
									Indien u ook heeft gekozen voor een installatie via Laadkompas zal er binnen 2 werkdagen contact met u opgenomen worden om de installatie verder door te nemen en in te plannen.
								</p>
							</span>
						</li>
					</ul>
					<p>Mocht u nog vragen hebben over uw bestelling dan zijn wij uiteraard op alle werkdagen bereikbaar per telefoon of email.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
    		<?php //do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
		<?php endif; ?>

		<?php //do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php //do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else :
		wp_redirect(wc_get_cart_url()); ?>
		<!-- <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p> -->

	<?php endif; ?>