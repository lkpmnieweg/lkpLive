<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
<div class="row">
	<div class="custom-column-6">
	<?php

	$checkout_slug = null;
	if($found_slug = get_query_var('overview'))
	{
		$checkout_slug = $found_slug;
	}
	?>
	<div class="page_block" id="block_checkout">
		<h1>Bestellen</h1>
		<?php
		 if ( $checkout->get_checkout_fields() ) : ?>

			<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

			<?php do_action( 'woocommerce_checkout_billing' ); ?>
			<?php do_action( 'woocommerce_checkout_shipping' ); ?>

			<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

		<?php endif; 
?>

		<div class="clearfix"></div>
		<?php /* <div class="form-group">
			<input type="checkbox" name="newsletter" id="newsletter" value="1" checked="checked" />
			<label class="custom-label-wrap" for="newsletter">
				<span class="custom-check"></span> <span class="custom-label">Ja, ik wil op de hoogte blijven van nieuws in de EV markt.</span>
			</label>
		</div> */ ?>
		<div class="submit">
			<a href="#block_review" data-target="review" class="btn">Doorgaan naar betalen</a>
		</div>
	</div>
		
	<div class="page_block" id="block_review">
		<h1>Bezorg- en factuuradres</h1>
		<ul>
			<li class="company"></li>
			<li class="name"><span class="firstname"></span> <span class="lastname"></span></li>
			<li class="address"></li>
			<li><span class="zipcode"></span> <span class="city"></span></li>
			<li class="country"></li>
		</ul>
		<h2 class="shipping_title" style="display: none;"></h2>
		<ul class="shipping_items">
			<li class="shipping_company"></li>
			<li class="shipping_name"><span class="shipping_firstname"></span> <span class="shipping_lastname"></span></li>
			<li class="shipping_address"></li>
			<li><span class="shipping_zipcode"></span> <span class="shipping_city"></span></li>
			<li class="shipping_country"></li>
		</ul>
		<h2>Contactgegevens</h2>
		<ul>
			<li class="contact_email"></li>
			<li class="contact_phone"></li>
			<li class="newsletter"></li>
		</ul>
		<a class="checkoutLink" data-target="checkout" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg"> Wijzig gegevens</a>
		<div class="button-wrapper">
			<div class="submit">
				<a href="#block_payment" data-target="payment" class="btn">Doorgaan naar betalen</a>
			</div>
		</div>
	</div>

	<div class="page_block" id="block_payment">
		<h1>Betaalwijze</h1>
		<p>Hoe wil je je bestelling betalen?</p>
		<?php

		do_action('woocommerce_checkout_shipping_custom');
		?>
	</div>

</div>
<div class="custom-column-4">
	<div class="result-box">
		<h3>Winkelmandje</h3>
		
		<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

		<div id="order_review" class="woocommerce-checkout-review-order">
			<?php do_action( 'woocommerce_checkout_order_review' ); ?>
		</div>

		<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
		<a class="checkoutLink" href="<?php echo wc_get_cart_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg"> Wijzig winkelmandje</a>
	</div>
	<div class="clearfix"></div>
	<div class="totalPrice" style="display: block !important;">
		<ul>
			<li class="line">
				<span class="label">Subtotaal</span>
				<span class="price"><?php wc_cart_totals_subtotal_html(); ?></span>
			</li>
			<li>
				<?php
				if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) {
					foreach ( WC()->cart->get_tax_totals() as $code => $tax ) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
						?>
						<span class="label">BTW</span>
						<span class="price" data-title="<?php echo esc_attr( $tax->label ); ?>"><?php echo wp_kses_post( $tax->formatted_amount ); ?></span>
						
						<?php
					}
				} else {
					?>
					<span class="label">BTW</span>
					<span class="price" data-title="<?php echo esc_attr( WC()->countries->tax_or_vat() ); ?>"><?php wc_cart_totals_taxes_total_html(); ?></span>
					
					
					<?php

				}

				?>
				
			</li>
			<li class="line">
				<span class="label">Verzending</span>
				<span class="price">&euro;<?php echo number_format(WC()->cart->get_shipping_total(),2,',','.') ?></span>
			</li>
			<?php
			if (!empty(WC()->cart->applied_coupons))
			{
				global $woocommerce;
					
				// print_r(WC()->cart->applied_coupons);
				foreach(WC()->cart->applied_coupons as $coupon) {
					$c = new WC_Coupon($coupon);
					echo '
					<li class="line">
						<span class="label">Kortingscode<br/><strong>'.$coupon.'</strong></span>
						<span class="price">- '.(($c->discount_type == "percent")? $c->amount.'%' : '&euro; '.$c->amount).'</span>
					</li>';
				}
			}
			?>
			<li class="total">
				<span class="label">Totaalbedrag</span>
				<span class="price" data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>"><?php wc_cart_totals_order_total_html(); ?></span>
			</li>
		</ul>
	</div>
</div>
<div class="clearfix"></div>
</div>
</form>
<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
<?php
/*

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
	
	<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
	
	<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>
	
	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
*/