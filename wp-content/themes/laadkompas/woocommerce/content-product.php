<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div class="column-3 product" <?php wc_product_class( '', $product ); ?>>
		
		<?php
		/**
		 * Hook: woocommerce_before_shop_loop_item.
		 *
		 * @hooked woocommerce_template_loop_product_link_open - 10
		 */
		// do_action( 'woocommerce_before_shop_loop_item' );

		/**
		 * Hook: woocommerce_before_shop_loop_item_title.
		 *
		 * @hooked woocommerce_show_product_loop_sale_flash - 10
		 * @hooked woocommerce_template_loop_product_thumbnail - 10
		 */
		do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
		<div class="details">
		<?php
		/**
		 * Hook: woocommerce_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_product_title - 10
		 */
		//do_action( 'woocommerce_shop_loop_item_title' );
		?>
		<h3><?php echo apply_filters( 'change_title_to_link_category', array(get_the_title(),get_the_permalink()) ); ?></h3>
		<?php

		global $product;

		if(get_field('plus-_en_minpunten_overzichtpagina'))
		{
			echo productCategoryPlusMin(get_field('plus-_en_minpunten_overzichtpagina'));
		} else {
			echo '<ul></ul>';
		}
		
		?>
		<div class="bottom-bar">
			<form action="" method="POST">
			<div class="left">
				<?php

					/**
					 * Hook: woocommerce_after_shop_loop_item_title.
					 *
					 * @hooked woocommerce_template_loop_rating - 5
					 * @hooked woocommerce_template_loop_price - 10
					 */
					do_action( 'woocommerce_after_shop_loop_item_title' );

				?>
				<input name="product" data-label="<?php echo $product->get_name(); ?>" type="checkbox" id="product<?php echo $product->get_id();?>">
				<label class="custom-label-wrap" for="product<?php echo $product->get_id();?>">
					<span class="custom-check"></span> <span class="custom-label">Vergelijk</span>
				</label>
			</div>
			<div class="right">
				<?php
				/**
				 * Hook: woocommerce_after_shop_loop_item.
				 *
				 * @hooked woocommerce_template_loop_product_link_close - 5
				 * @hooked woocommerce_template_loop_add_to_cart - 10
				 */
				do_action( 'woocommerce_after_shop_loop_item' );
				?>
			</div>
			<div class="clearfix"></div>
			</form>
		</div>
		</div>
		
	<div class="clearfix"></div>
</div>
