<?php
/**
 * Cart item data (when outputting non-flat)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-item-data.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     2.4.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<ul>
	<?php
	if(is_cart()):
	?>
	<?php foreach ( $item_data as $data ) : ?>
		<li>
			<span class="label <?php echo sanitize_html_class( 'variation-' . $data['key'] ); ?>"><?php echo wp_kses_post( $data['key'] ); ?>:</span>
			<span class="value <?php echo sanitize_html_class( 'variation-' . $data['key'] ); ?>"><?php echo strip_tags(wp_kses_post( wpautop( $data['display'] ) ) ); ?>
			</span>
			<a class="questionPopup" data-title="<?php echo wc_attribute_label( $data['key'] ); // WPCS: XSS ok. ?>" data-txt="<?php echo getAttributeDescription($data['origin_key']); ?>" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri();?>/images/question-2.svg"></a>
			<?php
			$price = number_format($data['price'],2,',','.');
			if($price > 0)
			{
				echo '<span class="price">&euro; '.$price.'</span>';
			}
			else {
				echo '<span class="price free">&euro; 0,00</span>';
			}
			?>
		</li>
	<?php endforeach; ?>
	<?php
	endif;
	if(is_checkout()):
	?>
	
	<?php foreach ( $item_data as $data ) : 
		?>
		<li>
			<span class="item <?php echo sanitize_html_class( 'variation-' . $data['key'] ); ?>"><?php echo strip_tags(wp_kses_post( wpautop( $data['display'] ) ) ); ?>
			</span>
			
			<?php
			$price = number_format($data['price'],2,',','.');
			if($price > 0)
			{
				echo '<span class="price">&euro; '.$price.'</span>';
			}
			else {
				echo '<span class="price free">&euro; 0,00</span>';
			}
			?>
		</li>
	<?php endforeach; ?>


	<?php
	endif;
	?>
</ul>