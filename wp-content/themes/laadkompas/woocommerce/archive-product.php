<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;


if (is_shop()) {
get_header('shop');
$shop_page_id = 6;
?>
<div class="container shop-home first">
	<ul class="row" id="responsive">
		<?php
		$top3 = get_field('top_3_categorieen',$shop_page_id);
		if(count($top3) > 0)
		{
			$i=1;
			foreach($top3 as $topItem)
			{
			?>
			<li class="column-3">
				<a href="<?php echo get_site_url().'/'; echo $topItem->slug; ?>/" class="banner">
					<?php
					$thumbnail_id = get_woocommerce_term_meta( $topItem->term_id, 'thumbnail_id', true ); 
				    $image = wp_get_attachment_url( $thumbnail_id ); 

				    // print the IMG HTML
				    echo "<img src='".$image."' alt='".$topItem->name."' />";
					?>
					<div class="caption">
						<?php echo $topItem->name; ?>
					</div>
					<span class="btn">Shop nu</span>
				</a>		
			</li>
			<?php
				$i+=1;

				if($i > 3)
					break;
			}
		}
		?>
		<!-- <div class="clearfix"></div> -->
	</ul>
</div>
<div class="container shop-home second">
	<ul id="responsive-others" class="row other_categories">
		<?php
		if(fetchFeaturedCategories(false,$shop_page_id))
		{
			$blocks = fetchFeaturedCategories(false,$shop_page_id);
			foreach($blocks as $block)
			{
				if($block)
				{
					if(isset($block->ID) && $block->ID != null)
					{
						$image = get_the_post_thumbnail_url( $block->ID);
						$link = get_the_permalink($block->ID);
					}
					else {
						$thumbnail_id = get_woocommerce_term_meta( $block->term_id, 'thumbnail_id', true );
						$image        = wp_get_attachment_url( $thumbnail_id );
						$link = get_term_link($block->term_id);
					}
					echo '
					<li class="column-4">
						<a href="'.$link.'" class="small_banner">
							<span class="picture">
								<img src="'.$image.'" alt="'.((isset($block->name) && $block->name != "")? $block->name : $block->post_title).'" alt="'.((isset($block->name) && $block->name != "")? $block->name : $block->post_title).'" />
								<span class="caption">
									'.((isset($block->name) && $block->name != "")? $block->name : $block->post_title).'
								</span>
							</span>
						</a>
						<a class="more" href="'.$link.'"><img src="'.get_template_directory_uri().'/images/link-groen.svg" alt="Lees meer" /> <span>'.((isset($block->name) && $block->name != "")? 'Shop '.strtolower($block->name) : 'Lees meer over '.strtolower($block->post_title)).'</span></a>
					</li>';	
				}
			}
		}
		?>
		<!-- <div class="clearfix"></div> -->
	</div>
</div>
<div class="container shop-home">
	<?php
	if(get_field('advies_banner_tonen', $shop_page_id) == "ja"):

	$options = get_option( 'lkp_plugin_options' );
	if($options['banner_title'] != "" && $options['banner_text'] != ""):

	?>
	<div class="product-category-banner-container">
		<div class="product-category-banner">
		<div class="logo_content">
			<img src="<?php echo get_template_directory_uri(); ?>/images/logo-content.svg" alt="Laadkompas" />
		</div>
		<h3><?php echo $options['banner_title']; ?></h3>
		<p><?php echo $options['banner_text']; ?></p>
		<?php
		if(isset($options['banner_btn']) && $options['banner_btn'] != ""):
		?>
		<a class="btn text-right btn-primary" href="<?php echo get_the_permalink($options['banner_btn']); ?>">Advies aanvragen</a>
		<?php
		endif;
		?>
		</div>
	</div>
	<?php
	endif;
	endif;
	?>
</div>
<div class="container shop-home last">
	<div class="populair">
		<h3>Populaire laadpalen</h3>
		<ul id="responsive-products" class="row">
			<?php
			if(get_field('top_4_populaire_laadpalen', $shop_page_id))
			{
				$products = get_field('top_4_populaire_laadpalen', $shop_page_id);
				foreach($products as $product_id)
				{
					$_product = wc_get_product($product_id->ID);
				?>
				<li class="column-4">
					<a class="product" href="<?php echo get_the_permalink($product_id->ID); ?>">
						<span class="picture">
							<?php echo $_product->get_image(); ?>
						</span>
						<span class="title">
							<?php print $_product->get_name(); ?>
						</span>
						<span class="price">&euro; <?php print number_format(wc_get_price_excluding_tax( $_product ),2,',','.'); ?></span>
					</a>
				</li>
				<?php
				}	
			}
			?>
		</ul>
	</div>
</div>

<section class="company-usps">
	<div class="container">
		<h2>Waarom Laadkompas?</h2>
		<div class="row">
			<div class="column-3">
				<div class="item">
					<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="100% onafhankelijk" />100% onafhankelijk</div>
					<p>Laadkompas is de grootste onafhankelijke aanbieder van laadpalen in Nederland.</p>
				</div>
			</div>
			<div class="column-3">
				<div class="item">
					<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Persoonlijk advies" />Persoonlijk advies</div>
					<p>Wij staan klaar voor persoonlijk advies. Elke werkdag tussen 09:00 en 17:30</p>
				</div>
			</div>
			<div class="column-3">
				<div class="item">
					<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Tevreden klanten" />Tevreden klanten</div>
					<div class="rating">
						<p>9,2 op basis van <?php echo wp_count_posts('reviews')->publish; ?> beoordelingen</p>
						<div class="reviewStars">
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/starninety.svg" alt="Ster" />
	        			</div>
					</div>
        			<div class="kiyoh">
        				<img src="<?php echo get_template_directory_uri(); ?>/images/kiyoh-9-2-logo@2x.png" alt="Kiyoh">
        			</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<?php	
}
else {
get_header();
?>
<div class="container category-page">
	<div id="breadcrumb">
		<?php
		global $wp_query;
		?>
		<ul>
			<li><a href="<?php echo get_the_permalink(6); ?>">Home</a></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="<?php echo $wp_query->get_queried_object()->name; ?>" /></li>
			<?php
			if($slug = $wp_query->get_queried_object()->slug)
			{
				echo '<li><a href="'.get_site_url().'/product-categorie/'.$slug.'/">'.$wp_query->get_queried_object()->name.'</a></li>';
			}
			?>
			<div class="clearfix"></div>
		</ul>
	</div>
</div>
<div class="container category-page">
	<div class="row">
		<div class="scrollContainer">
			<div class="column-filters" id="filters">
				<a class="closeFilters" href="javascript:hideFilterMenu();"><i class="fa fa-times"></i></a>
				<ul>
				<?php
				dynamic_sidebar( 'wc-filters' );
				?>
				</ul>
			</div>
		</div>
		<div class="column-content">
			<h1><?php woocommerce_page_title(); ?></h1>

			<?php
			do_action( 'woocommerce_archive_description' );
				

				if ( woocommerce_product_loop() ) {

					/**
					 * Hook: woocommerce_before_shop_loop.
					 *
					 * @hooked woocommerce_output_all_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );



					print '<div class="products-container">';
					woocommerce_product_loop_start();
					echo '<div class="product-item row">';
					$productI = 0;
					$rowCounter = 1;
					$lkp_options = get_option( 'lkp_plugin_options' );
					if ( wc_get_loop_prop( 'total' ) ) {
						while ( have_posts() ) {

							the_post();

							/**
							 * Hook: woocommerce_shop_loop.
							 */
							do_action( 'woocommerce_shop_loop' );

							if($productI == 2 && $lkp_options['image_attachment_id'] != "")
							{
								print '
								<div class="column-3 product product3">
									<div class="banner-container">
										<div class="banner">
											'.(($lkp_options['cat_banner_btn'] != "")? '<a href="'.get_the_permalink($lkp_options['cat_banner_btn']).'" title="'.$lkp_options['cat_banner_title'].'">' : '').'
												
												<img data-img="'.wp_get_attachment_url( $lkp_options['image_attachment_id'] ).'" src="'.wp_get_attachment_url( $lkp_options['image_attachment_id'] ).'" alt="'.$lkp_options['cat_banner_title'].'">
												

												<span class="caption">
													'.$lkp_options['cat_banner_title'].'
												</span>
											'.(($lkp_options['cat_banner_btn'] != "")? '</a>': '').'
										</div>
									</div>
								</div>';
								$productI += 1;

								$rowCounter += 1;
							}

							if ($rowCounter%3===1){
								echo '<div class="clearfix"></div></div><div class="product-item row">';
							}

							wc_get_template_part( 'content', 'product' );


							if($productI == 5) //CTA bar
							{
								print '<div class="clearfix"></div></div>';
								$options = get_option( 'lkp_plugin_options' );
								if($options['banner_title'] != "" && $options['banner_text'] != ""){
								print '<div class="product-category-banner-container">
									<div class="product-category-banner">
									<div class="logo_content">
										<img src="'.get_template_directory_uri().'/images/logo-content.svg" alt="Laadkompas" />
									</div>
									<h3>'.$options['banner_title'].'</h3>
									<p>'.$options['banner_text'].'</p>';
									if(isset($options['banner_btn_product']) && $options['banner_btn_product'] != ""){
										echo '<a class="btn text-right btn-primary" href="'.get_the_permalink($options['banner_btn_product']).'">Advies aanvragen</a>';
									}
								print '
									</div>
								</div>
								<div class="product-item row">';
								}
								else print '<div class="product-item row">';
							}

							if($productI == 8) //checks banner
							{
								echo '<div class="clearfix"></div></div>
								<div class="usp-bar">
									<ul class="usps">
										<li class="first"><img src="'.get_template_directory_uri().'/images/icon-checkmark.svg" alt="100% onafhankelijk"> 100% onafhankelijk</li>
										<li><img src="'.get_template_directory_uri().'/images/icon-checkmark.svg" alt="Besparen tot 40%"> Besparen tot 40%</li>
										<li><img src="'.get_template_directory_uri().'/images/icon-checkmark.svg" alt="5.000+ laadpalen ge&iuml;nstalleerd"> 5.000+ laadpalen ge&iuml;nstalleerd</li>
									</ul>
								</div>
								<div class="product-item row">';
							}

							$productI += 1;
							$rowCounter += 1;
						}
					}
					echo '</div>';
					woocommerce_product_loop_end();

					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				}

				wpm_product_cat_display_details_meta();
				?>

			</div>
		</div>
	<div class="clearfix"></div>
	</div>
</div>
<section class="company-usps">
		<div class="container">
			<h2>Waarom Laadkompas?</h2>
			<div class="row">
				<div class="column-3">
					<div class="item">
						<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="100% onafhankelijk" />100% onafhankelijk</div>
						<p>Laadkompas is de grootste onafhankelijke aanbieder van laadpalen in Nederland.</p>
					</div>
				</div>
				<div class="column-3">
					<div class="item">
						<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Persoonlijk advies" />Persoonlijk advies</div>
						<p>Wij staan klaar voor persoonlijk advies. Elke werkdag tussen 09:00 en 17:30</p>
					</div>
				</div>
				<div class="column-3">
					<div class="item">
						<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Tevreden klanten" />Tevreden klanten</div>
						<div class="rating">
							<p>9,2 op basis van <?php echo wp_count_posts('reviews')->publish; ?> beoordelingen</p>
							<div class="reviewStars">
		        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
		        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
		        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
		        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
		        				<img src="<?php echo get_template_directory_uri(); ?>/images/starninety.svg" alt="Ster" />
		        			</div>
						</div>
	        			<div class="kiyoh">
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/kiyoh@2x.png" alt="Kiyoh" />
	        			</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</section>
<?php
}

get_footer();
