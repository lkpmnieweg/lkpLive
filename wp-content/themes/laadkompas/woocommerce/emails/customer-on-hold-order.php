<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
//Template when order comes in -> send to customer
?>
<html lang="nl">
   <head>
      <!--[if gte mso 9]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>

      <![endif]-->
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <base target="_blank">
      <title>Laadkompas</title>
      <meta charset="utf-8">
      <!--[if !mso]><!-->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!--<![endif]-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="format-detection" content="telephone=no">
      <style type="text/css">
         div[data-type=object-instance-collection] {
         float:none !important;
         display: block;
         }
         div[data-type=container-instance-collection], div[data-type=container-instance-variation], div[data-type=container-instance-collection].no-children {
         height:auto!important;
         }
      </style>
      <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
      <!--[if (gte mso 9)|(IE)]>
      <style>
         .block__nice-button td {padding: 10px 10px 5px 10px !important;}
         .block__nice-button td a {padding: 0 !important;}
         table {border-collapse: collapse !important;}
         .rightyleft{ direction:ltr !important};
      </style>
      <![endif]-->
      <!--[if (mso)|(mso 16)]>
         <style type="text/css">
         
         a {text-decoration: none;}
         </style>
         <![endif]-->
      
   </head>
   <body style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; height: 100% !important; margin: 0; padding: 0; width: 100% !important;">
      <style type="text/css">
         body {
         padding: 0 !important;
         margin: 0 auto !important;
         -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;
         height: 100% !important;
         width: 100% !important;        
         }
         img {
         -ms-interpolation-mode: bicubic; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; max-width: 100%;      
         }
         
         @media only screen and (max-width: 499px) {
         td {
         font-size: 14px !important;
         }
         .template__container {
         padding: 0 0px;
         }
         .block .block__image {
         font-size: 0 !important;
         }
         .block h2 {
         font-size: 24px !important; line-height: 28px !important;
         }
         .block p {
         font-size: 14px !important;
         }
         .block__nice-button {
         float: none !important; margin-bottom: 10px; margin-right: 0 !important; width: 100%;
         }
         .block__nice-button tbody {
         display: block; width: 100%;
         }
         .block__nice-button tr {
         display: block; width: 100%;
         }
         }
         @media only screen and (max-width: 550px) {
         .footer-text {
         font-size: 12px !important;
         }
         
         
      </style>
      <!--[if mso]>
      <style type=”text/css”>
         .mso-fallback {
         font-family: Arial, sans-serif;
         }
      </style>
      <![endif]-->
      <div class="template__container" style="background: #51b989; padding-top: 20px; padding-bottom: 20px; font-family: Arial, sans-serif; font-size: 16px; " align="center" >
         <!--[if (gte mso 9)|(IE)]> 
         <table style="background: #51b989; padding-top: 20px; padding-bottom: 20px; role="presentation" width="600" align="center" valign="top" cellpadding="0" cellspacing="0">
            <tr>
               <td width="600" align="center" valign="top" >
                  <![endif]-->
                  <table role="presentation" class="template__table--max" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; margin: auto; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4D4D4D; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 150%; mso-line-height-rule: exactly; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 8px 0 0;" align="left" valign="top">
                              <!--[if !mso 9]><!-->
                              <div style="display:none;font-size:1px;color:#F3F3F3;line-height:1px;font-family: Helvetica, Arial, sans-serif;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;">
                                 Uw bevestiging
                              </div>
                              <!--<![endif]-->
                              <table role="presentation" class="template__table" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                                 <tbody>
                                    <tr>
                                       <td width="175" style="padding-right: 5px; padding-left: 20px; padding-bottom: 5px;">
                                          <a valign="middle" href="https://www.laadkompas.nl" title="Logo" alt="Schiphol - Logo" target="_blank" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #fff; font-family: Arial;
  font-size: 30px; font-weight: bold; text-decoration: none;" >
                                            
                                             laadkompas
                                          </a>
                                       </td>
                                       <td style=" color: #fff; font-size: 15px !important; color: #fff; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #FFFFFF; font-family: Arial, sans-serif; font-weight: normal; line-height: 150%;">
                                             |
                                          </td>
                                          <td style=" color: #fff; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #FFFFFF; font-family: Arial, sans-serif; font-size: 14px; font-weight: normal;">
                                              Experts in opladen
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <!--[if (gte mso 9)|(IE)]> 
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>
      
      <div class="template__container" style="background: #FFFFFF; padding-top: 40px; padding-bottom: 60px;" align="center" >
         <!--[if (gte mso 9)|(IE)]> 
         <table style="background: #FFFFFF; padding-top: 40px; padding-bottom: 60px;" role="presentation" width="600" align="center" valign="top" cellpadding="0" cellspacing="0">
            <tr>
               <td width="600" align="center" valign="top">
                  <![endif]-->
                  <table role="presentation" class="template__table--max" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; margin: auto; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4D4D4D; font-family: Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 150%; mso-line-height-rule: exactly; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 8px 20px;" align="left" valign="top">
                              <table role="presentation" class="template__table" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                                 <tbody>
                                    <tr>
                                       <td style="font-family: Arial, sans-serif; font-size: 24px !important; color: #040a07; font-weight: bold; padding-bottom: 20px;">
                                          Bestelling ontvangen!
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="font-family: Arial, sans-serif; font-size: 16px; color: #6e706f; padding-bottom: 20px; line-height: 1.38;">
                                          Geachte heer/mevrouw <?php echo ucfirst(esc_html( $order->get_billing_last_name() )) ?>,<br><br>

                                          Wij hebben inmiddels opdracht gegeven tot assemblage en uitlevering van uw bestelling met ordernummer&nbsp;<b style="color: #000;">
                                          	<?php
											/* translators: %s: Order ID. */
											echo wp_kses_post( sprintf( __( ' #%s', 'woocommerce' )  , $order->get_order_number(), $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ) );
											?>
                                          </b>.
                                       </td>
                                    </tr>
                                     <tr>
                                       <td style="font-family: Arial, sans-serif; font-size: 16px; color: #6e706f; padding-bottom: 20px; line-height: 1.38;">
                                          Wij controleren uw bestelling en zetten deze zo spoedig mogelijk door. Hierna ontvangt u van ons een definitieve bevestiging, de factuur en informatie over de levering en eventuele installatie.
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style=" text-decoration: underline; font-family: Arial, sans-serif; font-weight:bold; font-size: 16px; color: #000000; padding-top: 20px;padding-bottom: 20px; line-height: 1.38;">
                                          Uw gegevens:
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="font-family: Arial, sans-serif; font-weight:bold; font-size: 16px; color: #000000; padding-bottom: 10px; line-height: 1.38;">
                                          Betaalmethode: <span style="color: #6e706f; font-weight: normal"><?php echo $order->get_payment_method_title(); ?></span>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td style="font-family: Arial, sans-serif; font-weight:bold; font-size: 16px; color: #000000; padding-bottom: 10px; line-height: 1.38;">
                                          Adres: <span style="color: #6e706f; font-weight: normal"><?php echo formatted_billing_address($order); ?></span>
                                       </td>
                                    </tr>
                                    <!-- if alternative adres is not empty -->

                                    <?php
                                    if($order->billing_address_1 != $order->shipping_address_1):
                                    ?>
                                    <tr>
                                       <td style="font-family: Arial, sans-serif; font-weight:bold; font-size: 16px; color: #000000; padding-bottom: 10px; line-height: 1.38;">
                                          Alternatief&nbsp;adres: <span style="color: #6e706f; font-weight: normal"><?php echo formatted_shipping_address($order); ?></span>
                                       </td>
                                    </tr>
                                 <?php endif; ?>

                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <!--[if (gte mso 9)|(IE)]> 
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>
      <!-- template__container -->


      <div class="template__container" style="background: #f9f9f9; padding-top: 40px; padding-bottom: 60px;" align="center" >
         <!--[if (gte mso 9)|(IE)]> 
         <table style="background: #f9f9f9; padding-top: 40px; padding-bottom: 60px;" role="presentation" width="600" align="center" valign="top" cellpadding="0" cellspacing="0">
            <tr>
               <td width="600" align="center" valign="top">
                  <![endif]-->
                  <table role="presentation" class="template__table--max" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; margin: auto; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4D4D4D; font-family: Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 150%; mso-line-height-rule: exactly; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 8px 20px;" align="left" valign="top">
                              <table role="presentation" class="template__table" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                                 <tbody>
                                 	<?php

									do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );
									do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );
                                 	?>
                                    <!-- start product 
                                    <tr>
                                       <td style="padding-bottom: 10px;">
                                          <tr>
                                            <td style="background: #fff; width: 100%; max-width: 440px; padding: 40px;" bgcolor="#ffffff">
                                                <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
                                                   <tr>
                                                      <td style="font-family: Arial, sans-serif; font-size: 24px !important; color: #040a07; font-weight: bold; padding-bottom: 16px;">
                                                         Uw bestelling
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td style="padding-bottom: 25px;">
                                                         <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tr>
                                                               <td width="52" valign="top" style="padding-right: 16px;">
                                                                  <img src="https://bmdigital.nl/laadkompas/wp-content/uploads/2020/04/evbox-homeline-74-kw-thuislader-voor-iedereen.png" style="display: block;">
                                                               </td>
                                                               <td valign="top">
                                                                  <table cellspacing="0" cellpadding="0" width="100%">
                                                                     <tr>
                                                                        <td style="padding-right: 25px;">
                                                                           <table cellspacing="0" cellpadding="0" width="100%">
                                                                              <tr>
                                                                                 <td valign="top" style="font-family: Arial, sans-serif; font-size: 16px !important; color: #040a07; font-weight: bold; padding-bottom: 3px;">
                                                                                    Newmotion Home Advanced
                                                                                 </td>
                                                                              </tr>
                                                                              <tr>
                                                                                 <td style="font-family: Arial, sans-serif; font-size: 14px; color: #9faba5;">
                                                                                    1x
                                                                                 </td>
                                                                              </tr>
                                                                           </table>      
                                                                        </td>
                                                                        <td align="right">
                                                                           <table cellspacing="0" cellpadding="0" width="100%">
                                                                              <tr>
                                                                                 <td style="font-family: Arial, sans-serif; font-size: 16px; color: #000000; padding-bottom: 3px;" align="right">
                                                                                    <b>&euro;&nbsp;345,00</b>
                                                                                 </td>
                                                                              </tr>
                                                                              <tr>
                                                                                 <td style="font-family: Arial, sans-serif; font-size: 10px; color: #9faba5;" align="right">
                                                                                    ex. BTW
                                                                                 </td>
                                                                              </tr>
                                                                           </table>      
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  RAL 9010 | Wit
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #9faba5;" align="right">
                                                                  Gratis
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  Wandmontage
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #9faba5;" align="right">
                                                                  Gratis
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  Geen
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #9faba5;" align="right">
                                                                  Gratis
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                    <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  3-fase
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;" align="right">
                                                                  <b>&euro;&nbsp;81.82</b>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                    <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  Standaard installatie
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;" align="right">
                                                                  <b>&euro;&nbsp;345,00</b>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </td>
                                    </tr>
                                    <!-- end product -->

                                    <!-- start product 
                                    <tr>
                                       <td style="padding-bottom: 10px; padding-top: 10px;">
                                          <tr>
                                            <td style="width: 100%; max-width: 440px; padding: 40px;" bgcolor="#ffffff">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                   <tr>
                                                      <td style="font-family: Arial, sans-serif; font-size: 24px !important; color: #040a07; font-weight: bold; padding-bottom: 16px;">
                                                         Uw bestelling
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td style="padding-bottom: 25px;">
                                                         <table cellspacing="0" cellpadding="0" width="100%">
                                                            <tr>
                                                               <td width="52" valign="top" style="padding-right: 16px;">
                                                                  <img src="https://bmdigital.nl/laadkompas/wp-content/uploads/2020/04/evbox-homeline-74-kw-thuislader-voor-iedereen.png" style="display: block;">
                                                               </td>
                                                               <td valign="top">
                                                                  <table cellspacing="0" cellpadding="0" width="100%">
                                                                     <tr>
                                                                        <td style="padding-right: 25px;">
                                                                           <table cellspacing="0" cellpadding="0" width="100%">
                                                                              <tr>
                                                                                 <td valign="top" style="font-family: Arial, sans-serif; font-size: 16px !important; color: #040a07; font-weight: bold; padding-bottom: 3px;">
                                                                                    Newmotion Home Advanced
                                                                                 </td>
                                                                              </tr>
                                                                              <tr>
                                                                                 <td style="font-family: Arial, sans-serif; font-size: 14px; color: #9faba5;">
                                                                                    1x
                                                                                 </td>
                                                                              </tr>
                                                                           </table>      
                                                                        </td>
                                                                        <td align="right">
                                                                           <table cellspacing="0" cellpadding="0" width="100%">
                                                                              <tr>
                                                                                 <td style="font-family: Arial, sans-serif; font-size: 16px; color: #000000; padding-bottom: 3px;" align="right">
                                                                                    <b>&euro;&nbsp;345,00</b>
                                                                                 </td>
                                                                              </tr>
                                                                              <tr>
                                                                                 <td style="font-family: Arial, sans-serif; font-size: 10px; color: #9faba5;" align="right">
                                                                                    ex. BTW
                                                                                 </td>
                                                                              </tr>
                                                                           </table>      
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  RAL 9010 | Wit
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #9faba5;" align="right">
                                                                  Gratis
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  Wandmontage
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #9faba5;" align="right">
                                                                  Gratis
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  Geen
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #9faba5;" align="right">
                                                                  Gratis
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                    <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  3-fase
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;" align="right">
                                                                  <b>&euro;&nbsp;81.82</b>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                    <tr>
                                                      <td style="padding-bottom: 6px;">
                                                         <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                                                                  Standaard installatie
                                                               </td>
                                                               <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000;" align="right">
                                                                  <b>&euro;&nbsp;345,00</b>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </td>
                                    </tr>
                                    <!-- end product -->


                                    
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <!--[if (gte mso 9)|(IE)]> 
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>
      <!-- template__container -->



      <!-- start footer blok 1 -->
      <div class="template__container" style="background: #51b989; padding-top: 20px; padding-bottom: 20px;" align="center" >
         <!--[if (gte mso 9)|(IE)]> 
         <table style="background: #51b989; padding-top: 20px; padding-bottom: 20px;" role="presentation" width="600" align="center" valign="top" cellpadding="0" cellspacing="0">
            <tr>
               <td bgcolor="#51b989" style="background: #51b989;" width="600" align="center" valign="top" >
                  <![endif]-->
                  <table role="presentation" class="template__table--max" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; margin: auto; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                     <tbody>
                        <tr>
                           <td style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4D4D4D; font-family: Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 150%; mso-line-height-rule: exactly; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 8px 0 0;" align="left" valign="top">
                              <table role="presentation" class="template__table" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                                 <tbody>
                                    <tr>
                                       <table role="presentation" cellpadding="0" border="0" cellspacing="0" width="100%">
                              <tr>
                                 <td style="padding-top: 20px;">
                                     <!--[if (gte mso 9)|(IE)]>
                                         <table role="presentation" width="600" align="center" style="border-spacing:0;font-family: Helvetica, Arial, sans-serif;  color: #4D4D4D;" >
                                         <tr>
                                         <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                                         <![endif]-->
                                     <table role="presentation" style="border-spacing:0; font-family: Arial, sans-serif; font-size: 16px; color: #4D4D4D; Margin:0 auto;width:100%;max-width:600px;">
                                       <tr>
                                          <td style="padding-top:0px;padding-bottom:0px; padding-right:0px;padding-left:0px;text-align:center;font-size:0;">
                                                <!--[if (gte mso 9)|(IE)]>
                                                <table role="presentation" width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                                                   <tr>
                                                         <td width="50%" align="center" valign="top" style="padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;" >
                                                 <![endif]-->
                                                 
                                                 <div class="responsive-img" style="width:100%;max-width:300px;display:inline-block;vertical-align:top; overflow:visible;">
                                                     <table role="presentation" style="border-spacing:0;font-family:Arial, sans-serif; color: #4D4D4D;" width="100%">
                                                      <tbody>
                                                            <tr>
                                                                 <td class="inner" style="padding-top:0px;padding-bottom: 20px;padding-right:0px;padding-left:0px;">
                                                                    <table role="presentation" style="border-spacing:0;font-family:Arial, sans-serif; color:#333333;width:100%;font-size:14px; ">
                                                                         <tbody>
                                                                             <tr>
                                                                              <td valign="top" width="34" style="padding-left: 20px; padding-right: 10px; width: 34px!important;">
                                                                                 <img width="34" src="https://laadkompas.nl/wp-content/themes/laadkompas/images/phone_2.png" alt="laadkompas telefoon" style=" width: 34px !important; font-size: 16px; border:0;" border="0">
                                                                              </td>
                                                                                 <td>
                                                                                     <h1 class="mso-fallback" align="left" style="font-family: Arial, sans-serif; color: #141251; font-size: 22px; font-weight: bold; margin: 0 0 5px; padding: 0; line-height: 32px;"><a style="text-decoration: none; color: #fff;" href="tel:088 999 0600">088 999 0600</a></h1>
                                                                                     <p class="paragraph-text" align="left" style="margin: 0 0 0; padding: 0; color: #ffffff; font-size: 16px; line-height: 1.43;">Maandag t/m vrijdag<br>09:00 - 17:30</p>
                                                                              </td>
                                                                        </tr>
                                                                         </tbody>
                                                                     </table>
                                                                 </td>
                                                             </tr>
                                                         </tbody>
                                                     </table>
                                                 </div>
                                                 <!--[if (gte mso 9)|(IE)]>
                                                   </td>
                                                   <td width="50%" align="left" valign="top" style="padding-top:0;padding-bottom:0;padding-right:17px;padding-left:0;" >
                                             <![endif]-->
                                                 <div class="full-width-text" style="width:100%;max-width:300px;display:inline-block;vertical-align:top; overflow:visible;">
                                                     
                                                     <table role="presentation" dir="ltr" style="border-spacing:0;font-family: Arial, sans-serif; color:#4D4D4D;" width="100%">
                                                         <tbody>
                                                             <tr>
                                                                 <td align="left" class="inner" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #4D4D4D; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 150%; mso-line-height-rule: exactly; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding-bottom: 20px;">
                                                                     <table role="presentation" style="border-spacing:0;font-family:Arial, sans-serif; color:#333333;width:100%;font-size:14px; ">
                                                                         <tbody>
                                                                             <tr>
                                                                                 <tr>
                                                                              <td valign="top" width="34" style="padding-left: 20px; padding-right: 10px">
                                                                                 <img width="34" src="https://laadkompas.nl/wp-content/themes/laadkompas/images/email_2.png" alt="mail laadkompas" style=" width: 34px; font-size: 16px; border:0;" border="0">
                                                                              </td>
                                                                                 <td>

                                                                                     <h1 class="mso-fallback" align="left" style="font-family: Arial, sans-serif; color: #141251; font-size: 22px; font-weight: bold; margin: 0 0 5px; padding: 0; line-height: 32px;"><a style="text-decoration: none; color: #fff;" href="mailto:info@laadkompas.nl">info@laadkompas.nl</a></h1>
                                                                                     <p class="paragraph-text" align="left" style="margin: 0 0 0; padding: 0; color: #ffffff; font-size: 16px; line-height: 1.43;">Op werkdagen binnen<br>24u reactie.</p>
                                                                              </td>
                                                                        </tr>
                                                                        </tr>
                                                                         </tbody>
                                                                     </table>
                                                                 </td>
                                                             </tr>
                                                         </tbody>
                                                     </table>
                                                 </div>
                                                 <!--[if (gte mso 9)|(IE)]>
                                                   </td>
                                                </tr>
                                             </table>
                                             <![endif]-->
                                             </td>
                                         </tr>
                                     </table>
                                     <!--[if (gte mso 9)|(IE)]>
                                          </td>
                                       </tr>
                                    </table>
                                    <![endif]-->    
                                 </td>
                                 </tr>
                              </table>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <!--[if (gte mso 9)|(IE)]> 
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>
      <!-- start footer blok 2 -->
      <div class="template__container" bgcolor="#45a378" style="background: #45a378; padding-top: 25px; padding-bottom: 25px;" align="center" >
         <!--[if (gte mso 9)|(IE)]> 
         <table style="padding-top: 25px; padding-bottom: 25px;" bgcolor="#45a378" role="presentation" width="600" align="center" valign="top" cellpadding="0" cellspacing="0">
            <tr>
               <td bgcolor="#45a378" width="600" align="center" valign="top" >
                  <![endif]-->
                  <table role="presentation" class="template__table--max" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; margin: auto; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                     <tbody>
                       <div>
                         <table align="center" role="presentation" style="border-collapse: collapse; border-spacing: 0px;" cellpadding="0" cellspacing="0">
                            <tbody>
                               <tr>
                                  <td style="padding-right: 8px;"><a href="#" style="display: block; width: 30px; height: 30px; max-height: 30px; border: 0px;color:#FFFFFF" target="_blank"> <img width="30" src="https://cdn.myclang.com/public/Laadkompas/e827522b56e1c64040e0f6b033a0f0e3/facebook_2x_3.png" alt="facebook link" border="0" style="display: block;"> </a> </td>
                                  <td style="padding-right: 8px;"><a href="#" style="display: block; width: 30px; height: 30px; max-height: 30px; border: 0px;color:#FFFFFF" target="_blank"> <img width="30" src="https://cdn.myclang.com/public/Laadkompas/e827522b56e1c64040e0f6b033a0f0e3/twitter_2x.png" alt="twitter link" border="0" style="display: block;"> </a> </td>
                                  <td style="padding-right: 8px;"><a href="#" style="display: block; width: 30px; height: 30px; max-height: 30px; border: 0px;color:#FFFFFF" target="_blank"> <img width="30" src="https://cdn.myclang.com/public/Laadkompas/e827522b56e1c64040e0f6b033a0f0e3/instagram_2x.png" alt="instagram link" border="0" style="display: block;"> </a> </td>
                                  <td><a href="#" style="display: block; width: 30px; height: 30px; max-height: 30px; border: 0px;color:#FFFFFF" target="_blank"> <img width="30" src="https://cdn.myclang.com/public/Laadkompas/e827522b56e1c64040e0f6b033a0f0e3/linkedin_2x.png" alt="linkedin link" border="0" style="display: block;"> </a> </td>
                               </tr>
                            </tbody>
                         </table>
                      </div>
                     </tbody>
                  </table>
                  <!--[if (gte mso 9)|(IE)]> 
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>

      <!-- start footer blok 3 -->
      <div class="template__container" style="background: #ffffff; padding-top: 15px; padding-bottom: 15px;" align="center" >
         <!--[if (gte mso 9)|(IE)]> 
         <table style="background: #ffffff; padding-top: 15px; padding-bottom: 15px;" role="presentation" width="600" align="center" valign="top" cellpadding="0" cellspacing="0">
            <tr>
               <td width="600" align="center" valign="top" >
                  <![endif]-->
                  <table role="presentation" class="template__table--max" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; border-collapse: collapse !important; margin: auto; max-width: 600px; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0;" width="100%" cellpadding="0" cellspacing="0">
                     <tbody>
                       <div>
                         <table align="center" role="presentation" style="border-collapse: collapse; border-spacing: 0px;" cellpadding="0" cellspacing="0">
                            <tbody>
                               <tr>
                                  <td style="padding-right: 8px; font-family: Arial, sans-serif; font-size: 14px; color: color:#51b989;"><a href="https://www.laadkompas.nl/algemene-voorwaarden/" style="color:#51b989" target="_blank">Algemene voorwaarden</a>&nbsp;&nbsp;</td>
                                  <td style="padding-right: 8px; color: grey; font-family: Arial, sans-serif; font-size: 20px;">|</td>
                                  <td style=" font-family: Arial, sans-serif; font-size: 14px; color: color:#51b989;"><a href="https://www.laadkompas.nl/privacybeleid/" style="color:#51b989;" target="_blank">Privacybeleid</a> </td>
                               </tr>
                            </tbody>
                         </table>
                      </div>
                     </tbody>
                  </table>
                  <!--[if (gte mso 9)|(IE)]> 
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>
   </body>
</html>
<?php
/**
 * Customer on-hold order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-on-hold-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 *

defined( 'ABSPATH' ) || exit;

/*
 * @hooked WC_Emails::email_header() Output the email header
 
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name  ?>
<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
<p><?php esc_html_e( 'Thanks for your order. It’s on-hold until we confirm that payment has been received. In the meantime, here’s a reminder of what you ordered:', 'woocommerce' ); ?></p>

<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );
/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Show user-defined additional content - this is set in each email's settings.
 
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 
do_action( 'woocommerce_email_footer', $email );
*/