<?php
/**
 * Email Order Items
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-items.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$text_align  = is_rtl() ? 'right' : 'left';
$margin_side = is_rtl() ? 'left' : 'right';

foreach ( $items as $item_id => $item ) :
	$product       = $item->get_product();
	$sku           = '';
	$purchase_note = '';
	$image         = '';

	if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
		continue;
	}

	if ( is_object( $product ) ) {
		$sku           = $product->get_sku();
		$purchase_note = $product->get_purchase_note();
		$image         = $product->get_image( $image_size );
	}
?>
<!-- start product -->
<tr>
   <td style="padding-bottom: 10px;">
      <tr>
        <td style="background: #fff; width: 100%; max-width: 440px; padding: 40px;" bgcolor="#ffffff">
            <table bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="100%">
               <tr>
                  <td style="font-family: Arial, sans-serif; font-size: 24px !important; color: #040a07; font-weight: bold; padding-bottom: 16px;">
                     Uw bestelling
                  </td>
               </tr>
               <tr>
                  <td style="padding-bottom: 25px;">
                     <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                           <td width="52" valign="top" style="padding-right: 16px;">
                              <?php
                              $image_size[0] = '100%';
                              $image_size[1] = null;
                              echo apply_filters( 'woocommerce_order_item_thumbnail', '<div style="margin-bottom: 5px"><img src="' . ( $product->get_image_id() ? current( wp_get_attachment_image_src( $product->get_image_id(), 'thumbnail' ) ) : wc_placeholder_img_src() ) . '" alt="' . esc_attr__( 'Product image', 'woocommerce' ) . '" height="' . esc_attr( $image_size[1] ) . '" width="' . esc_attr( $image_size[0] ) . '" style="max-width: 52px; vertical-align:middle; margin-' . ( is_rtl() ? 'left' : 'right' ) . ': 10px;" /></div>', $item ); 
                              ?>
                           </td>
                           <td valign="top">
                              <table cellspacing="0" cellpadding="0" width="100%">
                                 <tr>
                                    <td style="padding-right: 25px;">
                                       <table cellspacing="0" cellpadding="0" width="100%">
                                          <tr>
                                             <td valign="top" style="font-family: Arial, sans-serif; font-size: 16px !important; color: #040a07; font-weight: bold; padding-bottom: 3px;">
                                                <?php
                                                echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', $item->get_name(), $item, false ) );
                                                ?>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td style="font-family: Arial, sans-serif; font-size: 14px; color: #9faba5;">
                                               <?php
												$qty          = $item->get_quantity();
												$refunded_qty = $order->get_qty_refunded_for_item( $item_id );

												if ( $refunded_qty ) {
													$qty_display = '<del>' . esc_html( $qty ) . '</del> <ins>' . esc_html( $qty - ( $refunded_qty * -1 ) ) . '</ins>';
												} else {
													$qty_display = esc_html( $qty );
												}
												echo wp_kses_post( apply_filters( 'woocommerce_email_order_item_quantity', $qty_display, $item ) );
												?>x
                                             </td>
                                          </tr>
                                       </table>      
                                    </td>
                                    <td align="right">
                                       <table cellspacing="0" cellpadding="0" width="100%">
                                          <tr>
                                             <td style="font-family: Arial, sans-serif; font-size: 16px; color: #000000; padding-bottom: 3px;" align="right">
                                                <b>
                                                	<?php

                                                		$prod_sub = wp_kses_post( $order->get_formatted_line_subtotal( $item ) ); 
                                                		echo str_replace("(excl. btw)", "", $prod_sub);
                                                	?>
                                                </b>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td style="font-family: Arial, sans-serif; font-size: 10px; color: #9faba5;" align="right">
                                                ex. BTW
                                             </td>
                                          </tr>
                                       </table>      
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <?php
               		// allow other plugins to add additional product information here.
				do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, $plain_text );

				wc_display_item_meta_custom_email(
					$item,
					array(
						// 'label_before' => '<strong class="wc-item-meta-label" style="float: ' . esc_attr( $text_align ) . '; margin-' . esc_attr( $margin_side ) . ': .25em; clear: both">',
					)
				);

				// allow other plugins to add additional product information here.
				do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, $plain_text );

				?>
          
            </table>
         </td>
      </tr>
   </td>
</tr>
<!-- end product -->
<?php endforeach; ?>
