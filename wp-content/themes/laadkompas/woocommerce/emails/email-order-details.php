<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$text_align = is_rtl() ? 'right' : 'left';

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
<?php
echo wc_get_email_order_items( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	$order,
	array(
		'show_sku'      => $sent_to_admin,
		'show_image'    => false,
		'image_size'    => array( 32, 32 ),
		'plain_text'    => $plain_text,
		'sent_to_admin' => $sent_to_admin,
	)
);
?>
<!-- start total  -->
<tr>
   <table cellpadding="0" cellspacing="0" width="100%">
	<?php
	$item_totals = $order->get_order_item_totals();
	unset($item_totals['payment_method']);

	if(isset($item_totals['belasting-1']))
	{
		$item_totals['belasting-1']['label'] = 'BTW: ';
	}

	if ( $item_totals ) :
		$i = 0;
		foreach ( $item_totals as $total ) :
			$i++;
			?>
      <tr>
         <td style="padding: 15px 40px 0px 40px;">
            <table cellpadding="0" cellspacing="0" width="100%">
				<tr>
                  <td style="<?php echo($i != count($item_totals))? 'border-bottom: 1px solid #d3d3d3;':''; ?>">
                     <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                           <td style="font-family: Arial, sans-serif; font-size: <?php echo ($i != count($item_totals))? '14px' : '16px'; ?> !important; color: #040a07; <?php echo ($i == 1 || $i == count($item_totals))? 'font-weight: bold;' : ''; ?> padding-bottom: 8px;">
                              <?php echo wp_kses_post( $total['label'] ); ?>
                           </td>
                           <td style="font-family: Arial, sans-serif; font-size: <?php echo ($i != count($item_totals))? '14px' : '16px'; ?>; color: #000000; <?php echo ($i == 1 || $i == count($item_totals))? 'font-weight: bold;' : ''; ?> padding-bottom: 8px;" align="right">
                              <?php echo wp_kses_post( $total['value'] ); ?>
                           </td>
                        </tr>
                    </table>
                  </td>
               </tr>
          </table>
         </td>
        </tr>    
		<?php
		endforeach;
	endif;
	?>
               <!-- 
               <tr>
                  <td style="padding-top: 10px; border-bottom: 1px solid #d3d3d3">
                     <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                           <td style="font-family: Arial, sans-serif; font-size: 14px !important; color: #040a07; padding-bottom: 8px;">
                              BTW
                           </td>
                           <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; padding-bottom: 8px;" align="right">
                              &euro;&nbsp;345,00
                           </td>
                        </tr>
                        <tr>
                           <td style="font-family: Arial, sans-serif; font-size: 14px !important; color: #040a07; padding-bottom: 8px;">
                              Verzending
                           </td>
                           <td style="font-family: Arial, sans-serif; font-size: 14px; color: #000000; padding-bottom: 8px;" align="right">
                              &euro;&nbsp;345,00
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td style="padding-top: 10px;">
                     <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                           <td style="font-family: Arial, sans-serif; font-size: 16px !important; color: #040a07; font-weight: bold;">
                              Totaal
                           </td>
                           <td style="font-family: Arial, sans-serif; font-size: 16px; color: #000000;" align="right">
                              <b>&euro;&nbsp;1659,24</b>
                           </td>
                        </tr>
                    </table>
                  </td>
               </tr> -->
            </table>
         </td>
      </tr>
   </table>
   
</tr>
<!-- end total -->
<?php

if ( $order->get_customer_note() ) {
	?>
	<tr>
		<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
		<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
	</tr>
	<?php
}
?>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
