<?php
/*
Template name: Laadkompas - Vergelijking
*/

if(isset($_COOKIE['lkp_comparison_ids']))
{
	$raw_value = $_COOKIE['lkp_comparison_ids'];
	$product_ids = json_decode(str_replace("\\", "", $raw_value));
	if(count($product_ids) > 1 && count($product_ids) <= 3)
	{
		get_header();

		echo get_template_part('partials/vergelijkingPage');

		get_footer();
	}
	else {
		wp_redirect(get_site_url());		
	}

} else {
	wp_redirect(get_site_url());
}
