<?php
session_start();

global $post;
global $wp;
if(isset($_SESSION['lkp_current_funnel']))
{
	$current_funnel = $funnel->decryptSession();

	$current_funnel->clearSession();
}

get_header('funnel');

$linkProducten = get_field('producten',$post->ID);
if(count($linkProducten) > 0)
{
	$compare_products = [];
	foreach($linkProducten as $prod)
	{
		$compare_products[] = wc_get_product($prod->ID);
	}
	set_query_var('compare_products', $compare_products);
}
else set_query_var('compare_products', []);


$filter_vars = null;
//FILTER LOGIC
if(isset($_GET['filter_vars']))
{
	$filter_vars = $_GET['filter_vars'];
}
?>
<div class="container">
	<!-- <h1 class="intro">Ons advies voor u: de <span>goedkoopste</span>, <span>snelste</span> of meest <span>premium</span> laadpaal op basis van uw wensen.</h1> -->
	<h1 class="intro"><?php echo ($post->ID == 7487)? get_field('koptekst') : 'Hierbij ontvangt u de <span>top 3</span> geschikte laadpalen op basis van uw wensen en persoonlijke situatie.'; ?></h1>
	<?php

	echo get_template_part('partials/vergelijking/products');
	?>
	<section class="usps">
		<h3>Welke laadpaal u ook kiest, bij ons krijgt u:</h3>
		<ul>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="100% onafhankelijk advies" /> <strong>100%</strong> onafhankelijk advies</li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Gecertificeerde experts voor uw installatie" /> <strong>Gecertificeerde</strong> experts voor uw installatie</li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Tot 40% besparing op installatie" /> Tot <strong>40%</strong> besparing op installatie</li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="98% wordt binnen 1 week ge&iuml;nstalleerd" /> Snelle levering (<strong>98%</strong> binnen 1 week)</li>
            <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Een laagsteprijsgarantie" />Een <strong>laagsteprijsgarantie</strong></li>
			<li class="black"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Nu tijdelijk: Dagje Wellness cadeau t.w.v. &euro; 60,-" /> Bomen planten: Tiny Forest Haarlem</li>
			<div class="clearfix"></div>
		</ul>
	</section>
	<?php
	if($post->ID == 7487)
	{
		$car_brand_prefill = get_field('auto_merk_funnel_header');
		print '
		<div class="quote-action-form action-form '.(($car_brand_prefill != "" && $car_brand_prefill != false)? ' active' : '').'">
			<div class="quote-action-form-title">'.get_field('advies_titel').'</div>
			<p class="slogan">'.get_field('advies_tekst').'</p>
			<div class="form-group">
				<div class="row">
					<div class="column-2">
						<div class="form-group">
							<label for="car">Uw auto</label>
							<select name="car_brand" class="form-control" id="car_brand">
								<option value="" selected="selected">Selecteer Merk</option>
								<option value="Universeel, verschillende elektrische auto\'s">Universeel, verschillende elektrische auto\'s</option>
								<option value="Overig, mijn auto staat er niet tussen">Overig, mijn auto staat er niet tussen</option>	';

									$brands = fetchCarBrands();
									foreach($brands as $brand)
									{
										echo '<option data-brandid="'.$brand->id.'" '.((isset($current_funnel->car_brand) && $current_funnel->car_brand == $brand->brand_name)? 'selected="selected"' : '').' '.(($car_brand_prefill == $brand->id)? 'selected="selected"' : '').' value="'.$brand->brand_name.'">'.$brand->brand_name.'</option>';
									}
								
								print '
							</select>
						</div>
					</div>
					<div class="column-2">
						<div class="form-group car_type '.(($car_brand_prefill != "0")? ' car_type_active' : '').'">
							<label for="car">Auto type</label>
							<select name="car_type" class="form-control" id="car_type">
								<option value="" selected="selected" disabled="disabled">Selecteer Type</option>';

									$selected = null;
									$types = fetchCarTypes(@$current_funnel->car_type, (int)$car_brand_prefill, (int)$car_type_prefill);
									echo $types;
								print '
							</select>
						</div>
					</div>
					<div class="clear"></div>
					<input class="btn" type="button" name="openPopup" value="Advies aanvragen" />
				</div>
			</div>
		</div>
		<div id="sun_overlay_form">
			<a class="close" href="javascript:void(0);"><i class="fa fa-times"></i></a>
			<div class="form_title">'.get_field('popup_titel').'</div>
			'.do_shortcode('[contact-form-7 id="11859" title="Laadkompas - EvBox formulier"]').'
		</div>';
	}
	?>
	<section class="filter-products">
		<h3>Filter, sorteer en vergelijk verschillende laadpalen</h3>
		<form class="single_quote" action="" method="POST">
			<div class="timer_overlay"></div>
			<a class="btn-filter" href="javascript:void(0);">Filter <img src="<?php echo get_template_directory_uri(); ?>/images/filter-icon.svg" alt="Filteren" /></a>
			<a class="btn-filter sort" href="javascript:void(0);">Sorteren <img src="<?php echo get_template_directory_uri(); ?>/images/filter-sort-icon.svg" alt="Sorteren" /></a>
			<div class="filter-fields">
				<script type="text/javascript">
					var current_page_url = '<?php echo home_url( $wp->request ); ?>/?';
					<?php
					$filter_url = '';
					if(isset($_GET['filter_vars'])){

						$allowed = array('price', 'month', 'power');
						foreach($_GET['filter_vars'] as $id => $var)
						{
							if(in_array($id, $allowed)){
								echo 'current_page_url+=\'filter_vars['.$id.']='.$var.'&\';';
							}
							else 
								$filter_url .= 'filter_vars['.$id.']='.$var.'&';
						}

						//jump to
						echo '
							if(jQuery(window).width() <= 768){
								var offtop = jQuery("form.single_quote").offset().top - 80;
							} else {
								var offtop = jQuery("form.single_quote").offset().top;
							}

							console.log(offtop);
							jQuery([document.documentElement, document.body]).animate({
						        scrollTop: offtop
						    }, 1000);';
					}
					?>
				</script>
				<div class="form-group">
					<select name="singledouble" class="form-control">
						<?php
						echo loadFiltersSingleDouble(@$filter_vars['singledouble']);
						?>
					</select>
				</div>
				<div class="form-group">
					<select name="slimme_laadpaal" class="form-control">
						<?php
						echo loadFiltersSmartCharg(@$filter_vars['slimme_laadpaal']);
						?>
					</select>
				</div>
				<div class="form-group">
					<select name="vermogen" class="form-control">
						<?php
						echo loadFiltersPower(@$filter_vars['vermogen']);
						?>
					</select>
				</div>
				<div class="form-group">
					<select name="merk" class="form-control">
						<?php
						echo loadFiltersBrand(@$filter_vars['merk']);
						?>
					</select>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="sort-fields">
				<?php
				if(!isset($filter_vars))
				{
					echo '
					<a class="active" href="'.home_url( $wp->request ).'/?filter_vars[price]=asc">Prijs <span class="down"></span></a>
					<a href="'.home_url( $wp->request ).'/?filter_vars[month]=asc">Per maand</a>
					<a href="'.home_url( $wp->request ).'/?filter_vars[power]=asc">Vermogen</a>';	
				}
				else {
					$links = array(
						'price' => 'Prijs',
						'month' => 'Per maand',
						'power' => 'Vermogen',
					);
					foreach($links as $id => $link)
					{
						echo '
						<a '.((isset($filter_vars[$id]))? 'class="active"' : '').' href="'.home_url( $wp->request ).'/?'.$filter_url.'filter_vars['.$id.']='.((isset($filter_vars[$id]) && $filter_vars[$id] == 'desc')? 'asc' : 'desc').'">
							'.$link.' 
							<span class="'.((isset($filter_vars[$id]) && $filter_vars[$id] == 'desc')? 'down' : 'up').'"></span>
						</a>';
					}
				}
				?>
			</div>
			<ul class="products-boxes">
				<?php

				if(!isset($filter_vars))
				{
					$args = array(
				        'post_type'      => 'product',
					    'orderby' => 'meta_value_num',
					    'meta_key' => '_price',
					    'order' => 'desc'
				    );
			    } else {
			    	$meta_key = '_price';
			    	$order = 'desc';
			 

			    	if(isset($filter_vars['merk']) || 
			    		isset($filter_vars['singledouble']) ||
			    		isset($filter_vars['slimme_laadpaal']) ||
			    		isset($filter_vars['vermogen']))
			    	{
			
			    		$meta_query = [];

			    		if(isset($filter_vars['merk']))
			    		{
				    		$meta_query['merk_clause'] = array(
				    			'key' => 'pa_merk',
				    			'value' => $filter_vars['merk']
				    		);
			    		}
			    		if(isset($filter_vars['singledouble']))
			    		{
				    		$meta_query['singledouble_clause'] = array(
				    			'key' => 'pa_enkeldubbel',
				    			'value' => $filter_vars['singledouble']
				    		);
			    		}
			    		if(isset($filter_vars['slimme_laadpaal']))
			    		{
				    		$meta_query['smartcharging_clause'] = array(
				    			'key' => 'pa_smartcharging',
				    			'value' => (int)$filter_vars['slimme_laadpaal']
				    		);
			    		}
			    		if(isset($filter_vars['vermogen']))
			    		{
				    		$meta_query['vermogen_clause'] = array(
				    			'key' => 'pa_vermogen',
				    			'value' => $filter_vars['vermogen']
				    		);
			    		}


			    		if(isset($filter_vars['month']))
				    	{
				    		$meta_key = 'pa_abonnement';
				    		$order = ($filter_vars['month'] == 'asc')? 'asc' : 'desc';
							
				    		$meta_query['abonnement_clause'] = array(
				    			'key' => 'pa_abonnement',
            					'compare' => 'EXISTS'
				    			// 'value' => $filter_vars['pa_abonnement']
				    		);							
				    	}

			    		$orderBy = [];
			    		if(isset($filter_vars['price']))
				    	{
				    		$order = ($filter_vars['price'] == 'asc')? 'asc' : 'desc';
							
				    		$meta_query['price_clause'] = array(
				    			'key' => '_price',
            					'compare' => 'EXISTS'
				    		);						
				    		$orderBy = array(
						    	'price_clause' => $filter_vars['price']
						    );
				    	}
			    		if(isset($filter_vars['month']))
				    	{
				    		$meta_key = 'pa_abonnement';
				    		$order = ($filter_vars['month'] == 'asc')? 'asc' : 'desc';
							
				    		$meta_query['abonnement_clause'] = array(
				    			'key' => 'pa_abonnement',
            					'compare' => 'EXISTS'
				    		);						
				    		$orderBy = array(
						    	'abonnement_clause' => $filter_vars['month']
						    );
				    	}
			    		if(isset($filter_vars['power']))
				    	{
				    		$meta_key = 'pa_vermogen';
				    		$order = ($filter_vars['power'] == 'asc')? 'asc' : 'desc';
							
				    		$meta_query['power_clause'] = array(
				    			'key' => 'pa_vermogen',
            					'compare' => 'EXISTS'
				    		);						
				    		$orderBy = array(
						    	'power_clause' => $filter_vars['power']
						    );
				    	}

			    		$args = array(
						    'post_type'  => 'product',
					        'posts_per_page'	=> -1,
        					'relation' => 'AND',
						    'meta_query' => $meta_query,
						    // 'meta_key' => 'pa_abonnement',
						    // 'order_by'       => 'pa_abonnement',
						    'orderby'          => $orderBy
						);
			    	}
			    	else {
			    		$orderBy = [];
			    		if(isset($filter_vars['price']))
				    	{
				    		$order = ($filter_vars['price'] == 'asc')? 'asc' : 'desc';
							
				    		$meta_query['price_clause'] = array(
				    			'key' => '_price',
            					'compare' => 'EXISTS'
				    		);						
				    		$orderBy = array(
						    	'price_clause' => $filter_vars['price']
						    );
				    	}
			    		if(isset($filter_vars['month']))
				    	{
				    		$meta_key = 'pa_abonnement';
				    		$order = ($filter_vars['month'] == 'asc')? 'asc' : 'desc';
							
				    		$meta_query['abonnement_clause'] = array(
				    			'key' => 'pa_abonnement',
            					'compare' => 'EXISTS'
				    		);						
				    		$orderBy = array(
						    	'abonnement_clause' => $filter_vars['month']
						    );
				    	}
			    		if(isset($filter_vars['power']))
				    	{
				    		$meta_key = 'pa_vermogen';
				    		$order = ($filter_vars['power'] == 'asc')? 'asc' : 'desc';
							
				    		$meta_query['power_clause'] = array(
				    			'key' => 'pa_vermogen',
            					'compare' => 'EXISTS'
				    		);						
				    		$orderBy = array(
						    	'power_clause' => $filter_vars['power']
						    );
				    	}

			    		$args = array(
						    'post_type'  => 'product',
					        'posts_per_page'	=> -1,
        					'relation' => 'AND',
						    'meta_query' => $meta_query,
						    // 'meta_key' => 'pa_abonnement',
						    // 'order_by'       => 'pa_abonnement',
						    'orderby'          => $orderBy
						);
			    	}
			    }


			    $loop = new WP_Query( $args );
			    if($loop->have_posts()):
			    while ( $loop->have_posts() ) : $loop->the_post();
			        global $product;
			        $terms = get_the_terms( $product->get_id(), 'product_cat' );

		        	if(isset($terms[0]->term_id) && $terms[0]->term_id != 422): //Zonnepanelen category
			    ?>
			    <li>
					<div class="picture">
						<?php echo $product->get_image(); ?>
					</div>
					<div class="title">
						<h3><?php echo $product->get_name(); ?></h3>
					</div>
						<?php
						$price = 0;
						echo '<div class="price" data-inc="'.wc_get_price_including_tax( $product ).'" data-exc="'.wc_get_price_excluding_tax( $product ).'">&euro; <span class="price_digit">'.@number_format(wc_get_price_excluding_tax( $product ),2,',','.').'</span><span>ex. BTW</span></div>';
						?>
						
					<div class="price month">
						<?php
						if($product->get_attribute('pa_abonnement')):
						
						$abbo = $product->get_attribute('pa_abonnement');
						echo str_replace(array(" p.m.", "(optioneel)"), array("<span>p.m.</span>", "<span class='small'>(optioneel)</span>"), $abbo);

						endif;
						?>
					</div>
					<div class="power">
						<?php echo $product->get_attribute('pa_vermogen'); ?>
						<?php
						if($product->get_attribute('pa_fase')):
						?>
						<span><?php echo $product->get_attribute('pa_fase'); ?> fase</span>
						<?php
						endif;
						?>
					</div>
					<div class="button">
						<a class="btn" href="<?php echo get_the_permalink($product->get_ID()); ?>">Opties &amp; Bestel</a>
						<div class="compare">
							<input name="product" data-label="<?php echo $product->get_name(); ?>" id="product<?php echo $product->get_ID(); ?>" type="checkbox" value="" />
							<label class="custom-label-wrap" for="product<?php echo $product->get_ID(); ?>">
								<span class="custom-check"></span> <span class="custom-label">Vergelijk</span>
							</label>
						</div>
						<div class="clearfix"></div>
					</div>
				</li>
			    <?php
				endif;
			    endwhile;

			    wp_reset_query();
				else:
				?>
				<li class="no_result">Geen resultaten gevonden.</li>
				<?php
				endif;
				?>
				
				
			</ul>
		</form>
	</section>
</div>
<section class="company-usps">
	<div class="container">
		<h2>Waarom Laadkompas?</h2>
		<div class="row">
			<div class="column-3">
				<div class="item">
					<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="100% afhankelijk" />100% onafhankelijk</div>
					<p>Laadkompas is de grootste onafhankelijke aanbieder van laadpalen in Nederland.</p>
				</div>
			</div>
			<div class="column-3">
				<div class="item">
					<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Persoonlijk advies" />Persoonlijk advies</div>
					<p>Wij staan klaar voor persoonlijk advies. Elke werkdag tussen 09:00 en 17:30</p>
				</div>
			</div>
			<div class="column-3">
				<div class="item">
					<div class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Tevreden klanten" />Tevreden klanten</div>
					<div class="rating">
						<p>9,2 op basis van <?php echo wp_count_posts('reviews')->publish; ?> beoordelingen</p>
						<div class="reviewStars">
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
	        				<img src="<?php echo get_template_directory_uri(); ?>/images/starninety.svg" alt="Ster" />
	        			</div>
					</div>
        			<div class="kiyoh">
        				<img src="<?php echo get_template_directory_uri(); ?>/images/kiyoh-9-2-logo@2x.png" alt="Kiyoh">
        			</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<?php
get_footer();