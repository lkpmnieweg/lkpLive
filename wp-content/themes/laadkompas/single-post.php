<?php
get_header();

if ( have_posts() ) :
	while ( have_posts() ):
		the_post(); 
?>
	<div class="container back">
		<a href="<?php echo get_the_permalink(10008); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="" /> Terug naar kennisbank</a>
	</div>
	<div class="container big">
		<div class="article_image"><?php echo get_the_post_thumbnail(); ?></div>
	</div>
	<div class="container big">
		<div class="title_block">
			<h1><?php echo get_the_title(); ?></h1>
			<?php

			$introductie = get_field('introductie_tekst');
			if($introductie != null && $introductie != ""){
				echo '<h2>'.$introductie.'</h2>';
			}
			?>
			<div class="category">
				<a href="<?php echo get_the_permalink(10008).get_the_category()[0]->slug;?>/"><?php echo get_the_category()[0]->name; ?></a>
			</div>
		</div>
		<div class="social_bar">
			<?php
			global $wp;
			?>
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo home_url($wp->request); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-facebook.svg" alt="Facebook" /></a>
			<a href="http://twitter.com/share?text=<?php echo get_the_title(); ?>&url=<?php echo home_url($wp->request); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-twitter.svg" alt="Twitter" /></a>
			<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo home_url($wp->request); ?>&title=<?php echo get_the_title(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-linkedin.svg" alt="LinkedIn" /></a>
			<a href="https://www.instagram.com/laadkompas/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-instagram.svg" alt="Instagram" /></a>
		</div>
	</div>
	<div class="container content">
		<?php the_content(); ?>
		<?php
		$afb = get_field('afbeelding_in_tekst');
		if($afb != null && $afb != ""):
		?>
		<div class="image">
			<img src="<?php echo $afb; ?>" alt="" />
			<?php
			$undertitle = get_field('afbeelding_ondertitel');
			if($undertitle != null && $undertitle != ""):
			?>
			<div class="caption">
				<?php echo $undertitle; ?>
			</div>
			<?php
			endif;
			?>
		</div>
		<?php
		endif;

		$text2 = get_field('tekst_2');
		if($text2 != null && $text2 != "")
		{
			echo $text2;
		}
		?>
		<div class="tag_bar">
			<div class="row">
				<?php
				$tags = get_the_tags();
				if($tags):
				?>
				<div class="column-2">
					<strong>Tags</strong>
					<ul class="tags">
						<?php
						foreach($tags as $tag):
						?>
						<li><a >#<?php echo $tag->name; ?></a></li> <?php /* href="<?php echo get_tag_link($tag->term_id); ?>" */ ?>
						<?php
						endforeach;
						?>
					</ul>
				</div>
				<?php
				endif;
				?>
				<div class="column-2">
					<div class="social">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo home_url($wp->request); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-facebook.svg" alt="Facebook" /></a>
						<a href="http://twitter.com/share?text=<?php echo get_the_title(); ?>&url=<?php echo home_url($wp->request); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-twitter.svg" alt="Twitter" /></a>
						<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo home_url($wp->request); ?>&title=<?php echo get_the_title(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-linkedin.svg" alt="LinkedIn" /></a>
						<a href="https://www.instagram.com/laadkompas/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-instagram.svg" alt="Instagram" /></a>
					</div>	
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<?php
	    $args = array( 'category' => get_the_category()[0]->term_id, 'post_type' =>  'post', 'post__not_in' => array(get_the_ID()) ); 
	    $postslist = get_posts( $args ); 
		$related = null;
		if($postslist):
		?>
		<div class="related">
			<div class="title_bar">Meer zoals dit</div>
			<div class="items">
				<?php   
			    foreach ($postslist as $post) :  setup_postdata($post); 

			    ?>
			    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<span class="image"><?php echo get_the_post_thumbnail(); ?></span>
					<span class="title"><?php the_title(); ?></span>
					<span class="txt"><?php echo strip_tags(get_the_excerpt()); ?></span>
					<span class="more"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="" /> <span>Lees meer</span></span>
				</a>  
			    <?php endforeach; ?> 
			</div>
		</div>
		<?php
		endif;
		?>
		<div class="contact_block">
			<div class="title">Gratis persoonlijk advies</div>
			<p>Wij helpen u graag verder. Bel of mail ons, of vraag gratis advies. Wij zijn bereikbaar van maandag t/m Vrijdag<br/> van 9:00 tot 21:00.</p>
			<a class="btn" href="<?php echo get_site_url(); ?>">Gratis advies aanvragen</a>
			<ul>
				<li>
					<a href="tel:<?php echo get_option('woocommerce_store_phone');?>"><img src="<?php echo get_template_directory_uri(); ?>/images/contact_phone.svg" alt="" /> <?php echo get_option('woocommerce_store_phone');?></a>
				</li>
				<li>
					<a href="mailto: info@laadkompas.nl"><img src="<?php echo get_template_directory_uri(); ?>/images/contact_mail.svg" alt="" /> info@laadkompas.nl</a>
				</li>
			</ul>
		</div>
	</div>
<?php
	endwhile;
endif;

get_footer();
?>