<?php
/* Template name: Laadkompas - Kennisbank Home */
get_header();
?>
<div class="container">
	<div class="title_bar">
		<h1>Laadkompas Kennisbank</h1>
		<div class="categories">
			<?php
			foreach(get_categories() as $category)
			{
				print '<a href="'.get_the_permalink(10008).''.$category->slug.'/">'.$category->name.'</a>';
			}
			?>
			
		</div>
	</div>
	<div class="items">
		<div class="row">
			<div class="column-9">
				<?php
				$first_item = kennisbank_news(1);
				?>
				<a class="bigBlock" href="<?php echo $first_item['url']; ?>" title="<?php echo $first_item['title']; ?>">
					<span class="image" style="background-image: url(<?php echo $first_item['image']; ?>);"></span>
					<span class="caption">
						<span class="category"><?php echo $first_item['cat']; ?></span>
						<span class="title"><?php echo $first_item['title']; ?></span>
					</span>
					<span class="btn">Lees meer</span>
				</a>
			</div>
			<div class="column-3">
				<h2 class="populair">Populair</h2>
				<div class="populair_items">
				<?php
				$populair = kennisbank_populair();
				foreach($populair as $pop):
					?>
					<a href="<?php echo $pop['url']; ?>" title="<?php echo $pop['title']; ?>">
						<span class="image" style="background-image: url(<?php echo $pop['image']; ?>);"></span>
						<span class="caption">
							<span class="category"><?php echo $pop['cat']; ?></span>
							<span class="title"><?php echo $pop['title']; ?></span>
						</span>
					</a>
				<?php
				endforeach;
				?>
				</div>
			</div>
		</div>
		<div class="row news">
			<div class="column-3">
				<h2>Nieuws</h2>
				<?php
				$second_item = kennisbank_news(2);
				?>
				<a href="<?php echo $second_item['url']; ?>" title="<?php echo $second_item['title']; ?>">
					<span class="image" style="background-image: url(<?php echo $second_item['image']; ?>);"></span>
					<span class="category"><?php echo $second_item['cat']; ?></span>
					<span class="title"><?php echo $second_item['title']; ?></span>
					<span class="text">
						<?php echo substr($second_item['txt'],0,100).'...'; ?>
					</span>
				</a>
			</div>
			<div class="column-3">
				<div class="empty"></div>
				<?php
				$third_item = kennisbank_news(3);
				?>
				<a href="<?php echo $third_item['url']; ?>" title="<?php echo $third_item['title']; ?>">
					<span class="image" style="background-image: url(<?php echo $third_item['image']; ?>);"></span>
					<span class="category"><?php echo $third_item['cat']; ?></span>
					<span class="title"><?php echo $third_item['title']; ?></span>
					<span class="text">
						<?php echo substr($third_item['txt'],0,100).'...'; ?>
					</span>
				</a>
				<div class="button_bar">
					<a class="btn" href="<?php echo get_the_permalink(10008); ?>nieuws/">Bekijk meer nieuws</a>	
				</div>
			</div>
			<div class="column-3">
				<h2 class="social_title">Volg ons</h2>
				<ul class="social">
					<li>
						<a href="https://www.facebook.com/laadkompas/" target="_blank">
							<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-facebook.svg" alt="" /></span>
							<span class="title">Facebook</span>
						</a>
					</li>
					<li>
						<a href="https://twitter.com/laadkompas" target="_blank">
							<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-twitter.svg" alt="" /></span>
							<span class="title">Twitter</span>
						</a>
					</li>
					<li>
						<a href="https://www.linkedin.com/company/laadkompas" target="_blank">
							<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-linkedin.svg" alt="" /></span>
							<span class="title">LinkedIn</span>
						</a>
					</li>
					<li>
						<a href="https://www.instagram.com/laadkompas/" target="_blank">
							<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-green-instagram.svg" alt="" /></span>
							<span class="title">Instagram</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="toptips">
		<h2>Onze top tips</h2>
		<div class="row">
			<?php
			$tips = kennisbank_tips();
			foreach($tips as $tip):
				?>
			<div class="column-3">
				<a href="<?php echo $tip['url']; ?>">
					<span class="image" style="background-image: url(<?php echo $tip['image']; ?>);"></span>
					<span class="details">
						<span class="category"><?php echo $tip['cat']; ?></span>
						<span class="title"><?php echo $tip['title']; ?></span>
						<span class="text">
							<?php echo substr($tip['txt'],0,100).'...'; ?>
						</span>
					</span>
				</a>
			</div>
			<?php
			endforeach;
			?>
			
		</div>
		<div class="button_bar">
			<a class="btn" href="<?php echo get_the_permalink(10008); ?>tips/">Bekijk meer tips</a>	
		</div>
	</div>
	<div class="technical">
		<h2>Supercharge je kennis</h2>
		<div class="row">
			<?php
			$technical = kennisbank_technical();
			foreach($technical as $tech):
				?>
			<div class="column-2">
				<a href="<?php echo $tech['url']; ?>">
					<span class="image" style="background-image: url(<?php echo $tech['image']; ?>);"></span>
					<span class="details">
						<span class="category"><?php echo $tech['cat']; ?></span>
						<span class="title"><?php echo $tech['title']; ?></span>
						<span class="text">
							<?php echo substr($tech['txt'],0,100).'...'; ?>
						</span>
					</span>
				</a>
			</div>
			<?php
			endforeach;
			?>
		</div>
		<div class="button_bar">
			<a class="btn" href="<?php echo get_the_permalink(10008); ?>techniek/">Bekijk meer technisch</a>	
		</div>
	</div>
</div>
<div class="usp-bar">
	<div class="container">
	  <ul class="usps">
		  <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Check"> 100% onafhankelijk</li>
		  <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Check"> Besparen tot 40%</li>
		  <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Check"> 5.000+ laadpalen geïnstalleerd</li>
		</ul>
		<a class="btn" href="/shop/" title="Bezoek onze webshop">Bezoek onze webshop</a>
	</div>
</div>
<div class="other_news">
	<div class="container">
		<h2>Alle artikelen</h2>
		<div class="row">
			<?php
			$allKennis = kennisbank_all();
			foreach($allKennis as $kennis_item):
			?>
			<div class="column-3">
				<a href="<?php echo $kennis_item['url']; ?>" title="<?php echo $kennis_item['title']; ?>">
					<span class="image" style="background-image: url(<?php echo $kennis_item['image']; ?>);"></span>
					<span class="details">
						<span class="category"><?php echo $kennis_item['cat']; ?></span>
						<span class="title"><?php echo $kennis_item['title']; ?></span>
						<span class="text">
							<?php echo substr($kennis_item['txt'],0,100).'...'; ?>
						</span>
					</span>
				</a>
			</div>
			<?php
			endforeach;
			?>
			
		</div>
		<!-- <div class="button_bar">
			<a class="btn" href="#">Bekijk alle artikelen</a>	
		</div> -->
	</div>
</div>
<?php
get_footer();
?>