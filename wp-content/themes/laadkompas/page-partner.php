<?php
/* Template name: Laadkompas - Partnerpagina */


get_header();

// if($_SERVER['REMOTE_ADDR'] != '217.121.83.181'){
// 	wp_redirect(get_home()); exit;
// }

if (have_posts()): while (have_posts()) : the_post();

if(get_field('toon_sticky_menu') == "aan"):
?>
<div class="stickyTop">
	<div class="container">
		<?php
		if(get_field('sticky_menu_usps') != false && get_field('sticky_menu_usps') != "")
		{
			$usps = explode("\n", get_field('sticky_menu_usps'));
			if(count($usps) > 0)
			{
				print '<ul>';
				$i=0;
				foreach($usps as $usp)
				{
					if($i <= 2)
						print '<li><img src="'.get_template_directory_uri().'/images/icon-checkmark.svg" alt="'.$usp.'">'.$usp.'</li>';

					$i+=1;
				}
				print '</ul>';
			}
		}
		?>
		<button class="btn" type="button" id="goToCTA"><?php echo (get_field('sticky_menu_button') != "")? get_field('sticky_menu_button') : 'Top 3 laadpalen'; ?></button>
		<div class="clearfix"></div>
	</div>
</div>
<?php
endif;
?>
<header>
<div class="hero landing-hero">
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<p class="under"><?php echo strip_tags(get_field('slogan')); ?></p>
		<div class="photo-block">
			<?php
			$header_img = get_field('header_foto');
			if($header_img):
			?>
			<div class="photo"><img src="<?php echo $header_img; ?>" alt="<?php echo get_field('titel_funnel_header'); ?>" /></div>
			<?php
			endif;

			$want_form = get_field("toon_funnel_in_header");
			
			if($want_form == "on"):
			?>
			
			<div class="action-form">
				<div class="title"><?php echo get_field('titel_funnel_header'); ?></div>
				<form action="" method="POST">
					<label for="type_cust"><?php echo (get_field('label_boven_header_funnel_dropdown') != "")? get_field('label_boven_header_funnel_dropdown') : 'U bent een:'; ?></label>
					<div class="form-group">
						<select class="form-control" id="type_cust" name="type_customer">
							<?php
							$optionsValues = get_field("opties_voor_in_funnel_header");
							$optionsVals = ($optionsValues !== null && $optionsValues !== "")? $optionsValues : null;

							// if($_SERVER['REMOTE_ADDR'] == '217.121.79.115') {
							// 	var_dump($formatTextFieldToOptions($optionsVals)); exit;
							// }

							print formatTextFieldToOptions($optionsVals);
							?>
						</select>
					</div>
					
					<input class="btn" type="submit" name="advice" value="Advies aanvragen">
				</form>
			</div>
			<?php
			endif;
			?>
		</div>
	</div>
</div>
</header>
<?php

?>
	<?php
	//Text block 1
	$img1 = get_field('afbeelding_1');
	$titel1 = get_field('titel_1');
	$txt1 = get_field('tekst_1');

	if($titel1 || $txt1 || $img1):
	?>
<div class="container ---Style">
	<div class="info-block top">
		<?php
			if($img1):
		?>
			<div class="images">
				<?php
					if($img1) print formatImgObjToLink($img1,$titel1);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<h2><?php echo $titel1; ?></h2>
			<?php echo get_field('tekst_1'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php
	endif;
	?>

	<?php
	//Text block 2
	$img2 = get_field('afbeelding_2');
	$img3 = get_field('afbeelding_3');
	$titel2 = get_field('titel_2');
	$txt2 = get_field('tekst_2');
	$link = get_field('link');

	if($titel2 || $txt2 || $img2):
	?>
	<div class="info-block right first">
		<?php
			if($img2 || $img3):
		?>
			<div class="images">
				<?php
					if($img2) print formatImgObjToLink($img2,$titel2);
					if($img3) print formatImgObjToLink($img3,$titel2);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<h2><?php echo $titel2; ?></h2>
			<?php echo get_field('tekst_2'); ?>
			<ul class="links">
				<?php
				if($link && get_field('link_titel')):
				?>
				<li>
					<a href="<?php echo get_the_permalink($link->ID); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.png" alt="<?php echo get_field('link_titel'); ?>"><?php echo get_field('link_titel'); ?></a>
				</li>
				<?php
				endif;
				?>
			</ul>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
	<?php 
	endif;
	?>
<?php
$show_usp = get_field('usp_blok_1_tonen');

if($show_usp == "on" || $show_usp == null)
	echo get_template_part('partials/uspBlock', 'page');
?>
<?php
//Text block 1
$img4 = get_field('afbeelding_4');
$img5 = get_field('afbeelding_5');
$titel3 = get_field('titel_3');
$txt3 = get_field('tekst_3');
$button = get_field('button');

if($titel3 || $titel4 || $titel5 || $titel6 || $titel7):
?>
<div class="container ---Style">
<?php
if($titel3 || $txt3 || $img4):
?>
	<div class="info-block">
		<?php
			if($img4 || $img5):
		?>
			<div class="images">
				<?php
					if($img4) print formatImgObjToLink($img4,$titel3);
					if($img5) print formatImgObjToLink($img5,$titel3);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<h2><?php echo $titel3; ?></h2>
			<?php echo get_field('tekst_3'); ?>
			<?php
			if($button):
			?>
			<a class="btn" href="<?php echo get_the_permalink($button->ID); ?>"><?php echo get_field("button_tekst"); ?></a>
			<?php
			endif;
			?>
		</div>
		<div class="clearfix"></div>
	</div>
		<?php
	endif;
	//Text block 2
	$img6 = get_field('afbeelding_6');
	$img7 = get_field('afbeelding_7');
	$titel4 = get_field('titel_4');
	$txt4 = get_field('tekst_4');

	if($titel4 || $txt4 || $img6 || $img7):
	?>
	<div class="info-block right second">
		<?php
			if($img6 || $img7):
		?>
			<div class="images">
				<?php
					if($img6) print formatImgObjToLink($img6,$titel4);
					if($img7) print formatImgObjToLink($img7,$titel4);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<h2><?php echo $titel4; ?></h2>
			<?php echo get_field('tekst_4'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php 
	endif;
	?>
	<?php
	//Text block 5
	$img8 = get_field('afbeelding_8');
	$img9 = get_field('afbeelding_9');
	$titel5 = get_field('titel_5');
	$txt5 = get_field('tekst_5');

	if($titel5 || $txt5 || $img8 || $img9):
	?>
	<div class="info-block middle">
		<?php
			if($img8 || $img9):
		?>
			<div class="images">
				<?php
					if($img8) print formatImgObjToLink($img8,$titel1);
					if($img9) print formatImgObjToLink($img9,$titel1);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<h2><?php echo $titel5; ?></h2>
			<?php echo get_field('tekst_5'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php
	endif;
	?>
		<?php
	//Text block 2
	$img10 = get_field('afbeelding_10');
	$img11 = get_field('afbeelding_11');
	$titel6 = get_field('titel_6');
	$txt6 = get_field('tekst_6');

	if($titel6 || $txt6 || $img10 || $img11):
	?>
	<div class="info-block right second">
		<?php
			if($img10 || $img11):
		?>
			<div class="images">
				<?php
					if($img10) print formatImgObjToLink($img10,$titel6);
					if($img11) print formatImgObjToLink($img11,$titel6);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<h2><?php echo $titel6; ?></h2>
			<?php echo get_field('tekst_6'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php 
	endif;
	?>
	<?php
	//Text block 5
	$img12 = get_field('afbeelding_12');
	$img13 = get_field('afbeelding_13');
	$titel7 = get_field('titel_7');
	$txt7 = get_field('tekst_7');

	if($titel7 || $txt7 || $img12 || $img13):
	?>
	<div class="info-block middle last_item">
		<?php
			if($img12 | $img13):
		?>
			<div class="images">
				<?php
					if($img12) print formatImgObjToLink($img12,$titel7);
					if($img13) print formatImgObjToLink($img13,$titel7);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<h2><?php echo $titel7; ?></h2>
			<?php echo get_field('tekst_7'); ?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php
	endif;
	?>
</div>
<?php
endif;

$show_usp2 = get_field('usp_blok_2_tonen');
if($show_usp2 == "on" || $show_usp2 == null):
?>
<div class="usp-bar">
	<div class="container">
	  <ul class="usps">
		  <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Check" /> 100% onafhankelijk</li>
		  <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Check" /> Besparen tot 40%</li>
		  <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Check" /> 5.000+ laadpalen ge&iuml;nstalleerd</li>
		</ul>
	</div>
</div>
<?php
endif;

$show_usp_blocks = get_field('3_blokken_tonen');
if($show_usp_blocks == null || $show_usp_blocks == "on"):
?>
<div class="container ---Style usp-blocks">
	<?php
	if(get_field('3_blokken_titel')):
	?>	
	<h2><?php echo get_field('3_blokken_titel'); ?></h2>
	<?php
	endif;
	?>
	<div class="row">
		<?php
		//Text block 1
		$icon_titel_1 = get_field('icon_titel_1');
		$icon_tekst_1 = get_field('icon_tekst_1');

		if($icon_titel_1 || $icon_tekst_1):
		?>
		<div class="column-3">
			<div class="block_item">
				<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/verrekenen.svg" alt="Verrekenen" /></span>
				<span class="details">
					<span class="title"><?php echo $icon_titel_1; ?></span>
					<span class="text">
						<?php echo $icon_tekst_1; ?>
					</span>
				</span>
			</div>
		</div>
		<?php
		endif;
		?>
		<?php
		//Text block 1
		$icon_titel_2 = get_field('icon_titel_2');
		$icon_tekst_2 = get_field('icon_tekst_2');

		if($icon_titel_2 || $icon_tekst_2):
		?>
		<div class="column-3">
			<div class="block_item">
				<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/balancing.svg" alt="Balancing" /></span>
				<span class="details">
					<span class="title"><?php echo $icon_titel_2; ?></span>
					<span class="text">
						<?php echo $icon_tekst_2; ?>
					</span>
				</span>
			</div>
		</div>
		<?php
		endif;
		//Text block 1
		$icon_titel_3 = get_field('icon_titel_3');
		$icon_tekst_3 = get_field('icon_tekst_3');

		if($icon_titel_3 || $icon_tekst_3):
		?>
		<div class="column-3">
			<div class="block_item">
				<span class="icon"><img src="<?php echo get_template_directory_uri(); ?>/images/thuis_prive.svg" alt="Thuis / prive" /></span>
				<span class="details">
					<span class="title"><?php echo $icon_titel_3; ?></span>
					<span class="text">
						<?php echo $icon_tekst_3; ?>
					</span>
				</span>
			</div>
		</div>
		<?php
		endif;
		?>
		<div class="clearfix"></div>
	</div>
	<?php
	$button2 = get_field('button_2');
	if($button2):
	?>
	<div class="button-bar">
		<a class="btn" href="<?php echo get_the_permalink($button2->ID);?>"><?php echo get_field("button_2_tekst"); ?></a>
	</div>
	<?php
	endif;
	?>


</div>
<?php
endif;

echo get_template_part('partials/landingReviews', 'page');

echo get_template_part('partials/landingBest', 'page');

endwhile;
endif;
?>

<div id="sun_overlay_form">
	<a class="close" href="javascript:void(0);"><i class="fa fa-times"></i></a>
	<div class="form_title"><?php echo get_field("titel_in_popup"); ?></div>
	<p class="slogan"><?php echo get_field("ondertitel_in_popup"); ?></p>
	<?php echo do_shortcode('[contact-form-7 id="12955" title="Laadkompas - Partnerformulier"]'); ?>
</div>
<?php

get_footer();
?>