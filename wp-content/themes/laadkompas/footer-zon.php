<footer>
	<div class="footer-content">
		<div class="container">
			<div class="company-info">
				<a class="footer-logo" href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-logo-laadkompas-white.svg" alt="Laadkompas" /> <span>laadkompas</span></a>
				<p>Laadkompas is de #1 van Nederland in slimme oplossingen op het gebied van elektrische mobiliteit en duurzaamheid. Sinds 2015 voorzien wij consumenten en bedrijven van een passende laad- en energieoplossing.</p>
			</div>
		</div>
		<div class="line"></div>
		<div class="container last">
			<div class="row">
				<div class="column-4">
					<div class="title">Zonnepanelen <i class="fa fa-chevron-down"></i></div>
					<div class="collapse">
						<?php
						wp_nav_menu(
						  array(
						    'theme_location' => 'footer-menu-zon1'
						  )
						);
						?>
					</div>
				</div>
				<div class="column-4">
					<div class="title">Klantenservice <i class="fa fa-chevron-down"></i></div>
					<div class="collapse">
						<?php
						wp_nav_menu(
						  array(
						    'theme_location' => 'footer-menu-zon2'
						  )
						);
						?>
					</div>
				</div>
				<div class="column-4">
					<div class="title">Over ons <i class="fa fa-chevron-down"></i></div>
					<div class="collapse">						
						<?php
						wp_nav_menu(
						  array(
						    'theme_location' => 'footer-menu-3'
						  )
						);
						?>
					</div>
				</div>
				<div class="column-4">
					<div class="title">Contact & Advies <i class="fa fa-chevron-down"></i></div>
					<div class="collapse">
						<p>Microfoonstraat 5<br/>1322 BN Almere</p>
						<a class="bold" href="tel:<?php echo str_replace(" ", "", get_option('woocommerce_store_phone'));?>" title="Bel ons">
							<?php echo get_option('woocommerce_store_phone');?>
						</a>
						<p>Maandag t/m vrijdag :<br/> 09:00 - 17:30</p>
						<a class="bold" href="mailto:info@laadkompas.nl">info@laadkompas.nl</a>
					</div>
				</div>	
				<div class="clearfix"></div>
			</div>
		</div>	
	</div>
	<div class="footer-bar">
		<div class="container">
			<div class="social">
				<a href="https://www.facebook.com/laadkompas/" target="_blank" title="Facebook"><img src="<?php echo get_template_directory_uri(); ?>/images/f-logo-rgb-white-1024@2x.png" alt="Facebook" /></a>
				<a href="https://twitter.com/laadkompas" target="_blank" title="Twitter"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter-social-icon-circle-white@2x.png" alt="Twitter" /></a>
				<a href="https://www.instagram.com/laadkompas/" target="_blank" title="Instagram"><img src="<?php echo get_template_directory_uri(); ?>/images/glyph-logo-may-2016-copy@2x.png" alt="Instagram" /></a>
				<a href="https://www.linkedin.com/company/laadkompas" target="_blank" title="LinkedIn"><img src="<?php echo get_template_directory_uri(); ?>/images/in-white-128-px-r@2x.png" alt="LinkedIn" /></a>
				<div class="clearfix"></div>
			</div>
			<div class="links">
				<a href="<?php echo get_the_permalink(3014); ?>" title="Algemene voorwaarden">Algemene voorwaarden</a>
				<a href="<?php echo get_the_permalink(3); ?>" title="Privacybeleid">Privacybeleid</a>
			</div>
			<div class="copyright">&copy; <?php echo date("Y"); ?> <a href="http://www.laadkompas.nl">Laadkompas.nl</a></div>
			<div class="clearfix"></div>
		</div>
	</div>
</footer>
<?php
if(!isset($_COOKIE['lkp_cookie_settings'])):
?>
<div id="cookie">
	<div class="top">
		<div class="container">
			<div class="left">
				<p>Laadkompas.nl maakt gebruik van cookies ter verbetering van de website-ervaring en overige marketingdoeleinden.<br/> Door op ‘Ga naar website’ te klikken gaat u akkoord met ons privacybeleid waaronder valt dat we op vertrouwelijke wijze met uw gegevens omgaan.</p>
				<a href="javascript:void(0);"><img src="<?php echo get_template_directory_uri(); ?>/images/link-wit.svg" alt="Meer opties" /> Meer opties</a>
			</div>
			<div class="right">
				<a href="javascript:void(0);">Ga naar website</a>
				<a href="javascript:void(0);">Personaliseren</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="bottom">
		<div class="container">
			<p>Wilt u een gepersonaliseerde ervaring? Vink dan alles aan.<br/> Wilt u geen gepersonaliseerde ervaring? Laat dan alles uit staan.</p>
			<div class="options">
				<div class="disabled">
					<input id="nessecary" disabled="disabled" checked="checked" type="checkbox" name="type_cookie" value="nessecary" />
					<label for="nessecary">
						<span class="custom-check"></span>
						<span class="custom-label text">
							<span>Noodzakelijk</span>
							<span>Helpen de website bij basisfuncties als navigatie en toegang tot beveiligde onderdelen, deze zijn noodzakelijk.</span>
						</span>
					</label>
				</div>
				<div>
					<input id="statistics" type="checkbox" name="type_cookie" value="statistics" />
					<label for="statistics">
						<span class="custom-check"></span>
						<span class="custom-label text">
							<span>Statistieken</span>
							<span>Volgen de bezoekers van de website anoniem en geven informatie waarmee we de website kunnen verbeteren.</span>
						</span>
					</label>
				</div>
				<div>
					<input id="marketing" type="checkbox" name="type_cookie" value="marketing" />
					<label for="marketing">
						<span class="custom-check"></span>
						<span class="custom-label text">
							<span>Marketing</span>
							<span>Volgen bezoekers via overige websites en geven informatie waarmee wij advertenties kunnen personaliseren.</span>
						</span>
					</label>
				</div>
			</div>
			<div class="button_wrapper">
				<a class="btn" href="javascript:void(0);">Voorkeuren opslaan</a>
			</div>
		</div>
	</div>
</div>
<?php
endif;
?>
	<?php wp_styles(); ?>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/lib/swiper.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<?php wp_footer(); 

$functioneel = get_field('functioneel', 78);
if($functioneel)
	print $functioneel;
if(isset($_COOKIE['lkp_cookie_settings']) && $_COOKIE['lkp_cookie_settings'] != "")
	{
		$cookie = str_replace(array('[\"', '\"]'), '', $_COOKIE['lkp_cookie_settings']);
		$items = explode('\",', $cookie);
		$newItems = [];
		foreach($items as $item)
		{
			$itemname = str_replace('\"', '', $item);
			$newItems[] = $itemname;
		}

		//if(in_array('nessecary', $newItems))
		//{
		//}
		if(in_array('statistics', $newItems))
		{
			$statistieken = get_field('statistieken', 78);
			if($statistieken)
				print $statistieken;
		}
		if(in_array('marketing', $newItems))
		{
			$marketing = get_field('marketing', 78);
			if($marketing)
				print $marketing;
		}
	}

?>

	<noscript>
		<style type="text/css">
			body * {
				display: none;
			}
			.noJS { 
				border: 1px solid #000;
				max-width: 400px;
				margin: 100px auto;
				display: block !important;
				padding: 20px;
				text-align: center;
				line-height: 24px;
			}
		</style>
	</noscript>
	<div class="noJS">Deze website kan niet functioneren zonder JavaScript.</div>
</body>
</html>