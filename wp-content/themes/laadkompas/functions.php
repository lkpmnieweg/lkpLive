<?php
/*
 *  Author: Mick Nieweg
 *  URL: bmdigital.nl (BMdigital)
 *  Custom functions
 */

// ini_set('display_errors', 1);
// error_reporting(E_ALL);

require_once(__DIR__.'/QUESTIONS.php');
require_once( __DIR__ . '/funnel.class.php');
require_once( __DIR__ . '/zonfunnel.class.php');
require_once( __DIR__ . '/Cryptor.class.php');
$funnel = new Funnel();
$zonfunnel = new ZonFunnel();

//Disable Auto Update for WordPress
define( 'WP_AUTO_UPDATE_CORE', false );



//fetch post car for funnel
if(isset($_POST['advice'])) //step 1 -> 2
{
    if(isset($_POST['car_brand']))
    {
        $brand = str_replace("\'", "", sanitize_text_field($_POST['car_brand']));
        $type = sanitize_text_field($_POST['car_type']);

        //overrule type
        if($brand == "Verschillende elektrische autos" || $brand == "Universeel, verschillende elektrische autos" || $brand == "Overig, mijn auto staat er niet tussen")
        {
            $type = "";
        }

        $funnel = new Funnel($brand, $type);
        $funnel->encryptSession();
        wp_redirect(get_the_permalink(229));
        exit;
    }
}

if(isset($_POST['calculateSun'])) {
        $zipcode = sanitize_text_field($_POST['sun_zipcode']);
        $housenr = sanitize_text_field($_POST['sun_nr']);

/*
$consumption = null,
        $preference = null,
        $gotchp = null,
        $title = null,
        $name = null,
        $phone = null,
        $email = null,
        $comments = null,
        $agree_ev = null,
        $quoteUrl = null,
        $firstname = null,
        $zipcode = null,
        $housenr = null
        */

        $zonfunnel = new ZonFunnel(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        $zipcode,
        $housenr);
        $zonfunnel->encryptSession();

        wp_redirect(get_the_permalink(14204));
        exit;
}

//Post to SForce
if(isset($_POST['giveAdviceSolarPanels'])) {
    @session_start();
    giveAdviceSolarPanels();
}

if(isset($_POST['giveAdvice'])) { //step 2 -> 3
    if(isset($_POST['lastname']) && 
        isset($_POST['firstname']) && 
        isset($_POST['phone']) &&
        isset($_POST['emailadres']))
    {
        $firstname = sanitize_text_field($_POST['firstname']);
        $name = sanitize_text_field($_POST['lastname']);
        $phone = sanitize_text_field($_POST['phone']);
        $email = $_POST['emailadres'];
        $comments = sanitize_text_field($_POST['comments']);

        $agree_ev = false;
        if(isset($_POST['akkoord_evmarkt']) && $_POST['akkoord_evmarkt'] == "yes")
        {
            $agree_ev = true;
        }


        $current_funnel = $funnel->decryptSession();
        
        $useCars = "nee";
        $allowedCars = array(
            'Kona', 
            'I-Pace',
            'e-Niro',
            'e-Soul',
            'ZS EV',
            'Leaf',
            'Leaf e+',
            'Ampera-e'
        );

        //override for cars
        if($current_funnel->singleDouble == "enkel" &&
            $current_funnel->smart == "ja" && 
            in_array($current_funnel->car_type, $allowedCars))
        {
            $useCars = "ja";
        }

        //override double = always smart
        if($current_funnel->singleDouble == "dubbel")
            $current_funnel->smart = "ja";

        $smart = $current_funnel->smart;

        //override when single & smart = advice me, set to no so gives cheapest model (lkt316-0)
        if($current_funnel->singleDouble == "enkel" && $current_funnel->smart == "Weet niet / Adviseer mij")
        {
            $smart = "nee";
        }

        //find correct vergelijking-url
        $vergelijking = get_posts(array(
            'numberposts'   => -1,
            'post_type'     => 'quote',
            'meta_query'    => array(
                'relation'      => 'AND',
                array(
                    'key' => 'slim__nietslimme_laadpaal',
                    'value'     => $smart,//$current_funnel->smart,
                    'compare'   => '=',
                ),
                array(
                    'key'  => 'enkele__dubbele_laadpaal',
                    'value'     => $current_funnel->singleDouble,
                    'compare'   => '=',
                ),
                array(
                    'key'  => 'gebruik_voertuigen_tabel',
                    'value'     => $useCars,
                    'compare'   => '=',
                )
            ),
        ));

        $foundQuoteUrl = '';

        if(isset($vergelijking[0]->ID)){
            
            $foundQuoteUrl = get_the_permalink($vergelijking[0]->ID, false);

        }
        // if($_SERVER['REMOTE_ADDR'] == '84.80.118.72')
        // {
            $qq = new WP_Query(array('post_type' => 'quote', 'p' => $vergelijking[0]->ID));
            if($qq) {
                if($qq->posts[0]->post_name != null) {
                    $foundQuoteUrl = get_site_url().'/quote/'.$qq->posts[0]->post_name.'/';
                }
            }

        //     var_dump($foundQuoteUrl);
        // }

        $found = null;
        $found = get_the_permalink(5882);


        

        $newsletter_txt = 'Ja, ik wil op de hoogte blijven van nieuws in de EV markt.';
        $commercial_txt = 'Ja, ik wil informatie ontvangen omtrent aanbiedingen, kortingen en winacties.';

        //$optin_news = (isset($_POST['optin_newsletter']) && $_POST['optin_newsletter'] == 'yes')? 'true' : 'false';
        //$optin_commerc = (isset($_POST['optin_commercial']) && $_POST['optin_commercial'] == 'yes')? 'true' : 'false';

        $optin_news = 'false';
        $optin_commerc = 'false';

        $zonnepanelen = '';

        if(isset($current_funnel->zonnepanelen) && $current_funnel->zonnepanelen != '' ) 
            $zonnepanelen = $current_funnel->zonnepanelen;
        

        $quoteUrl = $foundQuoteUrl;
        $funnel = new Funnel(
            $current_funnel->car_brand, 
            $current_funnel->car_type, 
            $current_funnel->location, 
            $current_funnel->install, 
            $current_funnel->smart, 
            $current_funnel->singleDouble, 
            $current_funnel->extra_info_laadtijden, 
            $current_funnel->extra_info_personalisatie, 
            $current_funnel->extra_info_smartcharging,
            $current_funnel->extra_info_facturatie,
            '',
            $name,
            $phone,
            $email,
            $comments,
            $agree_ev,
            $quoteUrl,
            $firstname,
            $zonnepanelen,
            $optin_news,
            $optin_commerc,
            $newsletter_txt,
            $commercial_txt
        );
        /*
        $optin_newsletter = null,
        $newsletter_txt = null,
        $optin_commercial = null,
        $commercial_txt = null
        */

        $funnel->encryptSession();


        if($foundQuoteUrl){

            include_once(__DIR__.'/sforce/SalesForce.class.php');
            $sforce = new SalesForce();
            $postAPI = $sforce->postToAPI($funnel);
            if($postAPI)
            {
                var_dump('found');
                $current_funnel->clearSession();
                //redirect
                // wp_redirect(add_query_arg('pi_list_email', $email, $found));
                wp_redirect($found);
                exit;
            }
            else  {
                wp_redirect(get_the_permalink(231));
            }
            
        } else {
            // var_dump('notfound'); exit;
            wp_redirect(get_the_permalink(231));
        }
    }
    exit;
}

function getAddressByZipHouseNr($zipcode, $housenr) {
    $out = 'Adres niet gevonden';
    $url = "https://geodata.nationaalgeoregister.nl/locatieserver/free?fq=postcode:".trim(str_replace(" ", "", $zipcode))."&fq=huisnummer~".trim($housenr)."*";
    $response = file_get_contents($url);
    $json_response = json_decode($response, true);
    if(isset($json_response['response']['docs'][0])) {
        if(isset($json_response['response']['docs'][0]['weergavenaam'])) {
            $out = str_replace(",", " ".$housenr.",", $json_response['response']['docs'][0]['weergavenaam']);
        }
    }
    return $out;    
}

function findSolarPanelsQuoteUrl($consumption=false) {
    $foundQuoteUrl = false;
    $returnFoundQuoteUrl = '';
    if($consumption) {
        $newConsumption = explode(" ", $consumption);
        $vergelijking = get_posts(array(
            'numberposts'   => -1,
            'post_type'     => 'zonnepanelen-quote',
            'meta_query'    => array(
                'relation'      => 'AND',
                array(
                    'key' => 'stroomverbruik',
                    'value'     => $consumption,
                    'compare'   => '=',
                ),
            ),
        ));

        $foundQuoteUrl = '';

        if(isset($vergelijking[0]->ID)){
            // $foundQuoteUrl = get_the_permalink($vergelijking[0]->ID);
            // $returnFoundQuoteUrl = str_replace("geen-categorie/", "zonnepanelen-quote/", $foundQuoteUrl);
            $qq = new WP_Query(array('post_type' => 'zonnepanelen-quote', 'p' => $vergelijking[0]->ID));
            if($qq) {
                if($qq->posts[0]->post_name != null) {
                    $returnFoundQuoteUrl = get_site_url().'/zonnepanelen-quote/'.$qq->posts[0]->post_name.'/';
                }
            }
        }

    }
    return $returnFoundQuoteUrl;
}

function giveAdviceSolarPanels() {
    $firstname = sanitize_text_field($_POST['firstname']);
    $name = sanitize_text_field($_POST['lastname']);
    $phone = sanitize_text_field($_POST['phone']);
    $email = $_POST['emailadres'];
    $comments = sanitize_text_field($_POST['comments']);
    $current_funnel = unserialize(decryptData($_SESSION['lkp_current_funnel_zon']));

    //Find QuoteUrl
    $quoteUrl = findSolarPanelsQuoteUrl($current_funnel->consumption); //"https://laadkompas.nl/zonnepanelen-quote/";
    $fullAddress = sanitize_text_field($_POST['full_address']);

    $zonfunnel = new ZonFunnel(
        $current_funnel->consumption,
        $current_funnel->preference,
        $current_funnel->gotchp,
        null,
        $name,
        $phone,
        $email,
        $comments,
        null,
        $quoteUrl,
        $firstname,
        $current_funnel->zipcode,
        $current_funnel->housenr,
        $fullAddress);
    $zonfunnel->encryptSession();

    include_once(__DIR__.'/sforce/SalesForce.class.php');
    $sforce = new SalesForce();
    $postAPI = $sforce->postSForceSolarPanel($zonfunnel);

    if($postAPI)
    {
        // var_dump('found');
        @session_start();
        unset($_SESSION['lkp_current_funnel_zon']);
        //redirect
        // wp_redirect(add_query_arg('pi_list_email', $email, $found));
        wp_redirect(get_the_permalink(14275));
        exit;
    }
    else  {
        wp_redirect(get_the_permalink(14209));
    }
}


function curl_req_Clang($jsonString=false) {
    if($jsonString && $jsonString != '') {
        $ch4 = curl_init('https://webhook.myclang.com/app/api/rest/public/v2/project/datahook/data-receiver/2-orders');
        curl_setopt($ch4, CURLOPT_CUSTOMREQUEST, "POST");  
        curl_setopt($ch4, CURLOPT_POST, 1);
        curl_setopt($ch4, CURLOPT_POSTFIELDS, json_encode($jsonString));
        curl_setopt($ch4, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, 1);                                                                 
        curl_setopt($ch4, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen(json_encode($jsonString)),
            'Authorization: Bearer 7243b597-ed2b-476e-b55d-42fd787a67b1'
        ),              
        );                                                                                                                   
                                                                                                                             
        $result4 = curl_exec($ch4);
        curl_close($ch4);
    }
}

function formatTextFieldToOptions($options=null) {
    $out = '';
    if($options == null) {
        $default_options = array(
            "leasemaatschappij" => "Leasemaatschappij",
            "autodealer" => "Autodealer",
            "vastgoedbedrijf-projectontwikkelaar" => "Vastgoedbedrijf / Projectontwikkelaar",
            "vereniging-van-eigenaars-vve" => "Vereniging van eigenaars (VvE)",
            "installatiebedrijf-elektricien" => "Installatiebedrijf / Elektricien",
            "gemeente" => "Gemeente",
            "semi-publieke-instelling" => "(Semi-) publieke instelling",
            "Overig" => "Overig"
        );
        foreach($default_options as $key => $val) {
            $out .= '<option value="'.$val.'">'.$val.'</option>';
        }
    } else {
        $exp = explode("\n", $options);
        foreach($exp as $opt) {
            $out .= '<option value="'.$opt.'">'.$opt.'</option>';
        }

    }
    return $out;
}

//mollie_wc_gateway_ideal

//send to Clang (Mollie)
add_action('woocommerce_thankyou_mollie_wc_gateway_ideal', 'ps_thankyou_mollie', 10, 1);
function ps_thankyou_mollie( $order_id ) {

    if ( ! $order_id )
        return;

        // Getting an instance of the order object
        $order = wc_get_order( $order_id );

        if($order->is_paid())
            $paid = 'yes';
        else
            $paid = 'no';

        // $productString = '';
        $productsArray = array();
        // iterating through each order items (getting product ID and the product object) 
        // (work for simple and variable products)
        foreach ( $order->get_items() as $item_id => $item ) {

            if( $item['variation_id'] > 0 ){
                $product_id = $item['variation_id']; // variable product
            } else {
                $product_id = $item['product_id']; // simple product
            }

            $item_data = $item->get_data();

            // Get the product object
            $product = wc_get_product( $product_id );

            $productsArray[] = array(
                "product_name" => $product->get_title(),
                "product_total_price" => $item_data['total'],
                "product_price" => $product->get_price(),
                "product_amount" => $item_data['quantity'],
                "product_tax" => $item_data['subtotal_tax'],
                "order_nr" => $order_id
            );

        }
        $order_total = is_callable(array($order, 'get_total')) ? $order->get_total() : $order->order_total;
        $payedString = 'payed_'.$paid;

        $productString = substr($productString, 0, strlen($productString)-1);
        $rawArray = array(
            "company_name" => $order->get_billing_company(),
            "firstname" => $order->get_billing_first_name(),
            "lastname" => $order->get_billing_last_name(),
            "address" => $order->get_billing_address_1(),
            "zipcode" => $order->get_billing_postcode(),
            "city" => $order->get_billing_city(),
            "emailaddress" => $order->get_billing_email(),
            "phone_nr" => $order->get_billing_phone(),
            "order_nr" => $order_id,
            "order_date" => $order->order_date,
            "order_subtotal" => $order->get_subtotal(),
            "order_shipping" => $order->get_total_shipping(),
            "order_tax" => $order->get_total_tax(),
            "order_total" => $order_total,
            "status" => $payedString,
            "products" => $productsArray
        );

        curl_req_Clang($rawArray); exit;
}

//send to Clang (op factuur)
add_action('woocommerce_thankyou_bacs', 'ps_thankyou', 10, 1);
function ps_thankyou( $order_id ) {

    if ( ! $order_id )
        return;

        // Getting an instance of the order object
        $order = wc_get_order( $order_id );

        if($order->is_paid())
            $paid = 'yes';
        else
            $paid = 'no';

        // $productString = '';
        $productsArray = array();
        // iterating through each order items (getting product ID and the product object) 
        // (work for simple and variable products)
        foreach ( $order->get_items() as $item_id => $item ) {

            if( $item['variation_id'] > 0 ){
                $product_id = $item['variation_id']; // variable product
            } else {
                $product_id = $item['product_id']; // simple product
            }

            $item_data = $item->get_data();

            // Get the product object
            $product = wc_get_product( $product_id );

            $productsArray[] = array(
                "product_name" => $product->get_title(),
                "product_total_price" => $item_data['total'],
                "product_price" => $product->get_price(),
                "product_amount" => $item_data['quantity'],
                "product_tax" => $item_data['subtotal_tax'],
                "order_nr" => $order_id
            );

        }
        $order_total = is_callable(array($order, 'get_total')) ? $order->get_total() : $order->order_total;
        $payedString = 'payed_no';

        $productString = substr($productString, 0, strlen($productString)-1);
        $rawArray = array(
            "company_name" => $order->get_billing_company(),
            "firstname" => $order->get_billing_first_name(),
            "lastname" => $order->get_billing_last_name(),
            "address" => $order->get_billing_address_1(),
            "zipcode" => $order->get_billing_postcode(),
            "city" => $order->get_billing_city(),
            "emailaddress" => $order->get_billing_email(),
            "phone_nr" => $order->get_billing_phone(),
            "order_nr" => $order_id,
            "order_date" => $order->order_date,
            "order_subtotal" => $order->get_subtotal(),
            "order_shipping" => $order->get_total_shipping(),
            "order_tax" => $order->get_total_tax(),
            "order_total" => $order_total,
            "status" => $payedString,
            "products" => $productsArray
        );

        curl_req_Clang($rawArray); exit;
}

function cms_is_in_menu( $menu = null, $object_id = null ) {

    // get menu object
    $menu_object = wp_get_nav_menu_items( esc_attr( $menu ) );

    // stop if there isn't a menu
    if( ! $menu_object )
        return false;

    // get the object_id field out of the menu object
    $menu_items = wp_list_pluck( $menu_object, 'object_id' );


    // use the current post if object_id is not specified
    if( !$object_id ) {
        global $post;
        $object_id = get_queried_object_id();
    }
    // test if the specified page is in the menu or not. return true or false.
    return in_array( (int) $object_id, $menu_items );

}

add_filter( 'formatted_woocommerce_price', 'span_custom_prc', 10, 5 );
function span_custom_prc( $number_format, $price, $decimals, $decimal_separator, $thousand_separator){
    $inc = round($price * 1.21,2);
    return '<span class="price_digit" data-inc="'.$inc.'" data-exc="'.$price.'">'.$number_format.'</span>';
}

function loadFiltersBrand($selected = null)
{
    global $wpdb;
    $out = null;
    $field = 'pa_merk';
    //collect all meta values for pa_merk
    $q = "SELECT pm.meta_id,
    pm.meta_key,
    pm.meta_value,
    t.name,
    t.slug 
    FROM `lkp_postmeta` as pm
    INNER JOIN lkp_terms as t 
    ON pm.meta_value=t.term_id
    WHERE pm.`meta_key` = 'pa_merk'
    GROUP BY t.name";
    $results = $wpdb->get_results($q);
    if(count($results) > 0)
    {
        $out = '<option value="" selected="selected">Merk</option>';
        foreach($results as $result)
        {
            $out .= '<option '.((isset($selected) && $selected == $result->meta_value)? 'selected="selected"' : '').' value="'.$result->meta_value.'">'.$result->name.'</option>';
        }
    }
    return $out;
}
/*
function loadFiltersLoadBalancing($selected = null)
{
    global $wpdb;
    $out = null;
    $field = 'pa_merk';
    //collect all meta values for pa_merk
    $q = "SELECT pm.meta_id,
    pm.meta_key,
    pm.meta_value,
    t.name,
    t.slug 
    FROM `lkp_postmeta` as pm
    INNER JOIN lkp_terms as t 
    ON pm.meta_value=t.term_id
    WHERE pm.`meta_key` = 'pa_loadbalancing'
    GROUP BY t.name";
    $results = $wpdb->get_results($q);
    if(count($results) > 0)
    {
        $out = '<option value="" selected="selected">Loadbalancing</option>';
        foreach($results as $result)
        {
            $out .= '<option '.((isset($selected) && $selected == $result->meta_value)? 'selected="selected"' : '').' value="'.$result->meta_value.'">'.$result->name.'</option>';
        }
    }
    return $out;
}
*/

function loadFiltersSmartCharg($selected = null)
{
    global $wpdb;
    $out = null;
    $field = 'pa_merk';
    //collect all meta values for pa_merk
    $q = "SELECT pm.meta_id,
    pm.meta_key,
    pm.meta_value,
    t.name,
    t.slug 
    FROM `lkp_postmeta` as pm
    INNER JOIN lkp_terms as t 
    ON pm.meta_value=t.term_id
    WHERE pm.`meta_key` = 'pa_smartcharging'
    GROUP BY t.name";
    $results = $wpdb->get_results($q);
    // if(count($results) > 0)
    // {
    //     $out = '<option value="" selected="selected">Slimme laadpaal</option>';
    //     foreach($results as $result)
    //     {
    //         $out .= '<option '.((isset($selected) && $selected == $result->meta_value)? 'selected="selected"' : '').' value="'.$result->meta_value.'">'.$result->name.'</option>';
    //     }
    // }

        $out = '<option value="" selected="selected">Slimme laadpaal</option>
    <option value="34"'.(($selected == 34)? ' selected="selected"' : '').'>Ja</option>
    <option value="35"'.(($selected == 35)? ' selected="selected"' : '').'>Nee</option>';


    return $out;
}


function loadFiltersPower($selected = null)
{
    global $wpdb;
    $out = null;
    $field = 'pa_merk';
    //collect all meta values for pa_merk
    $q = "SELECT pm.meta_id,
    pm.meta_key,
    pm.meta_value,
    t.name,
    t.slug 
    FROM `lkp_postmeta` as pm
    INNER JOIN lkp_terms as t 
    ON pm.meta_value=t.term_id
    WHERE pm.`meta_key` = 'pa_vermogen'
    GROUP BY t.name
    ORDER BY t.name DESC";
    $results = $wpdb->get_results($q);
    
    /*if(count($results) > 0)
    {
        $out = '<option value="" selected="selected">Vermogen</option>';
        foreach($results as $result)
        {
            $out .= '<option '.((isset($selected) && $selected == $result->meta_value)? 'selected="selected"' : '').' value="'.$result->meta_value.'">'.$result->name.'</option>';
        }
    }*/
    $out = '
    <option value="" selected="selected">Vermogen</option>
    <option value="243"'.(($selected == 243)? ' selected="selected"' : '').'>3,7 kW</option>
    <option value="244"'.(($selected == 244)? ' selected="selected"' : '').'>7,4 kW</option>
    <option value="38"'.(($selected == 38)? ' selected="selected"' : '').'>11 kW</option>
    <option value="39"'.(($selected == 39)? ' selected="selected"' : '').'>22 kW</option>';

    return $out;
}

function loadFiltersSingleDouble($selected = null)
{
    global $wpdb;
    $out = null;
    $field = 'pa_merk';
    //collect all meta values for pa_merk
    $q = "SELECT pm.meta_id,
    pm.meta_key,
    pm.meta_value,
    t.name,
    t.slug 
    FROM `lkp_postmeta` as pm
    INNER JOIN lkp_terms as t 
    ON pm.meta_value=t.term_id
    WHERE pm.`meta_key` = 'pa_enkeldubbel'
    GROUP BY t.name";
    $results = $wpdb->get_results($q);
    // var_dump($results); exit;

    //252 & 253

    /*if(count($results) > 0)
    {
        $out = '<option value="" selected="selected">Enkele / Dubbele connector</option>';
        foreach($results as $result)
        {
            $out .= '<option '.((isset($selected) && $selected == $result->meta_value)? 'selected="selected"' : '').' value="'.$result->meta_value.'">'.$result->name.'</option>';
        }
    }*/

    $out = '<option value="" selected="selected">Enkele / Dubbele connector</option>
    <option value="253"'.(($selected == 253)? ' selected="selected"' : '').'>Dubbel</option>
    <option value="252"'.(($selected == 252)? ' selected="selected"' : '').'>Enkel</option>';

    return $out;
}

//custom function to override default sort by category
function custom_default_catalog_orderby() {

    $options = get_option( 'lkp_plugin_options' );
    $sort = $options['lkp_cat_sort'];

    if($sort)
        return $sort;
    else
        return 'menu_order';
} 
add_filter( 'woocommerce_default_catalog_orderby', 'custom_default_catalog_orderby' ); //add the filter

function encryptData($token="")
{  
    $crypted_token = null;
    if($token)
    {
        $encryption_key = 'CKXH2U9RPY3EFD70TLS1ZG4N8WQBOVI6AMJ5';
        $cryptor = new Cryptor($encryption_key);
        $crypted_token = $cryptor->encrypt($token);
        unset($token);
    }
    return $crypted_token;
}

function decryptData($crypted_token="")
{
    $token = null;
    if($crypted_token)
    {

      $encryption_key = 'CKXH2U9RPY3EFD70TLS1ZG4N8WQBOVI6AMJ5';
      $cryptor = new Cryptor($encryption_key);
      $token = $cryptor->decrypt($crypted_token);

    }
  return $token;
}



function my_wpcf7_form_elements($html) {
    $text = 'Aanhef';
    $html = str_replace('---',  $text , $html);
    return $html;
}
add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');

function wh_save_product_custom_meta($post_id, $post, $update) {
    $post_type = get_post_type($post_id);
    // If this isn't a 'product' post, don't update it.
    if ($post_type != 'product')
        return;

    if (!empty($_POST['attribute_names']) && !empty($_POST['attribute_values'])) {
        $attribute_names = $_POST['attribute_names'];
        $attribute_values = $_POST['attribute_values'];
        foreach ($attribute_names as $key => $attribute_name) {
            switch ($attribute_name) {
                case 'pa_abonnement':
                    if (!empty($attribute_values[$key][0])) {
                        update_post_meta($post_id, 'pa_abonnement', $attribute_values[$key][0]);
                    }
                    break;
                case 'pa_vermogen':
                    if (!empty($attribute_values[$key][0])) {
                        update_post_meta($post_id, 'pa_vermogen', $attribute_values[$key][0]);
                    }
                    break;
                case 'pa_merk':
                    if (!empty($attribute_values[$key][0])) {
                        update_post_meta($post_id, 'pa_merk', $attribute_values[$key][0]);
                    }
                    break;
                case 'pa_smartcharging':
                    if (!empty($attribute_values[$key][0])) {
                        update_post_meta($post_id, 'pa_smartcharging', $attribute_values[$key][0]);
                    }
                    break;
                case 'pa_enkeldubbel':
                    if (!empty($attribute_values[$key][0])) {
                        update_post_meta($post_id, 'pa_enkeldubbel', $attribute_values[$key][0]);
                    }
                    break;
                case 'pa_loadbalancing':
                    if (!empty($attribute_values[$key][0])) {
                        update_post_meta($post_id, 'pa_loadbalancing', $attribute_values[$key][0]);
                    }
                    break;

                default:
                    break;
            }
        }
    }
}

add_action( 'save_post', 'wh_save_product_custom_meta', 10, 3);


function wpdocs_custom_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


function formatted_billing_address($order)
{
    return
        $order->billing_address_1 . ' ' . 
        $order->billing_address_2 . ' ' .
        $order->billing_postcode  . ' ' .
        $order->billing_city;
}

function formatted_shipping_address($order)
{
    return
        $order->shipping_address_1 . ' ' . 
        $order->shipping_address_2 . ' ' .
        $order->shipping_postcode  . ' ' .
        $order->shipping_city;
}

function formatted_google_maps_url($address_url)
{
    return str_replace(" ", "+", str_replace("  ", ", ", $address_url));
}

function laadkompas_create_posttype_reviews() {
$supports = array(
    'title', // post title
    'editor', // post content
    'author', // post author
    'thumbnail', // featured images
    'excerpt', // post excerpt
    'custom-fields', // custom fields
    // 'comments', // post comments
    // 'revisions', // post revisions
    // 'post-formats', // post formats
);
$labels = array(
    'name' => _x('Reviews', 'plural'),
    'singular_name' => _x('review', 'singular'),
    'menu_name' => _x('Reviews', 'admin menu'),
    'name_admin_bar' => _x('reviews', 'admin bar'),
    'add_new' => _x('Nieuwe review toevoegen', 'add new'),
    'add_new_item' => __('Nieuw review item toevoegen'),
    'new_item' => __('Nieuwe review'),
    'edit_item' => __('Review aanpassen'),
    'view_item' => __('Bekijk reviews'),
    'all_items' => __('Alle reviews'),
    'search_items' => __('Reviews zoeken'),
    'not_found' => __('Geen reviews gevonden.'),
);
$args = array(
    'supports' => $supports,
    'labels' => $labels,
    'public' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'reviews'),
    'has_archive' => false,
    'hierarchical' => false,
    'menu_icon'           => 'dashicons-universal-access'
);

register_post_type( 'reviews', $args);

add_filter( 'manage_reviews_posts_columns', 'smashing_filter_posts_columns' );
function smashing_filter_posts_columns( $columns ) {
  $columns['reviewType'] = __( 'Review Type' );
  return $columns;
}
add_action( 'manage_reviews_posts_custom_column', 'smashing_realestate_column', 10, 2);
function smashing_realestate_column( $column, $post_id ) {
  // Image column
  if ( 'reviewType' === $column ) {
    echo get_field('review_type', $post_id);
    // echo get_the_post_thumbnail( $post_id, array(80, 80) );
  }
}


//Opt-ins
$supports2 = array(
    'title', // post title
    // 'editor', // post content
    // 'author', // post author
    // 'thumbnail', // featured images
    // 'excerpt', // post excerpt
    // 'custom-fields', // custom fields
    // 'comments', // post comments
    // 'revisions', // post revisions
    // 'post-formats', // post formats
);
$labels2 = array(
    'name' => _x('Opt-ins', 'plural'),
    'singular_name' => _x('opt-in', 'singular'),
    'menu_name' => _x('Opt-ins', 'admin menu'),
    'name_admin_bar' => _x('optins', 'admin bar'),
    'add_new' => _x('Nieuwe opt-in toevoegen', 'add new'),
    'add_new_item' => __('Nieuw opt-in item toevoegen'),
    'new_item' => __('Nieuwe opt-in'),
    'edit_item' => __('Opt-in aanpassen'),
    'view_item' => __('Bekijk opt-ins'),
    'all_items' => __('Alle opt-ins'),
    'search_items' => __('Opt-ins zoeken'),
    'not_found' => __('Geen opt-ins gevonden.'),
);
$args2 = array(
    'supports' => $supports2,
    'labels' => $labels2,
    'public' => true,
    'query_var' => false,
    'rewrite' => array('slug' => 'optins'),
    'has_archive' => false,
    'hierarchical' => false,
    'menu_icon'           => 'dashicons-email-alt2'
);

register_post_type( 'optins', $args2);
}
// Hooking up our function to theme setup
add_action( 'init', 'laadkompas_create_posttype_reviews' );


add_action("wp_ajax_my_user_like", "my_user_like");

function my_user_like() {
   
   // nonce check for an extra layer of security, the function will exit if it fails
   // if ( !wp_verify_nonce( $_REQUEST['nonce'], "my_user_like_nonce")) {
   //    exit("Woof Woof Woof");
   // }   
  
  $post_title = $_POST['emailAddress'];
  // $post_custom_field = $_POST['fieldValues'];
  $post = array(
    'post_title'    => $post_title,
    'post_status'   => 'publish',
    'post_type'   => 'optins'
  );
  
  if ( get_page_by_title( $post_title ) === null ) {
    // the title is unique, we can add the new page
    $post_id = wp_insert_post( $post );
    // add_post_meta($post_id, 'customField', $post_custom_field], true);

    update_field('type_opt_in', 'newsletter', $post_id);

    $link = get_permalink( get_page_by_title( $post_title ) );
    $title = get_the_title($post_id);
    $newPostAttributes[] = array("link"=>$link, "title"=>$title);
    echo json_encode($newPostAttributes);
  } else {
    // that title already exists, tell the user to change it
    echo json_encode("exists");
  }
  wp_die();


   // don't forget to end your scripts with a die() function - very important
   die();
}



// function data_Publish_Optin() {
//   $post_title = $_POST['emailAddress'];
//   // $post_custom_field = $_POST['fieldValues'];
//   $post = array(
//     'post_title'    => $post_title,
//     'post_status'   => 'publish',
//     'post_type'   => 'optins'
//   );
//   var_dump('test');
//   if ( get_page_by_title( $post_title ) === null ) {
//     // the title is unique, we can add the new page
//     $post_id = wp_insert_post( $post );
//     // add_post_meta($post_id, 'customField', $post_custom_field], true);
//     $link = get_permalink( get_page_by_title( $post_title ) );
//     $title = get_the_title($post_id);
//     $newPostAttributes[] = array("link"=>$link, "title"=>$title);
//     echo json_encode($newPostAttributes);
//   } else {
//     // that title already exists, tell the user to change it
//     echo json_encode("exists");
//   }
//   wp_die();
// }
// add_action('wp_ajax_data_Publish', 'data_Publish');

function fetchFeaturedCategories($show=false, $pageId)
{
    $out = null;
    if(get_field('uitgelichte_categorieen_weergeven',$pageId))
    {
        $show = get_field('uitgelichte_categorieen_weergeven',$pageId);
        $block1 = null;
        $block2 = null;
        $block3 = null;
        $block4 = null;
        if($show)
        {
            if($show == "none")
                return false;
            else
            {
                switch($show)
                {
                    case '1cat3page':
                        $block1 = get_field('categorie_1',$pageId);
                        $block2 = get_field('pagina_1',$pageId);
                        $block3 = get_field('pagina_2',$pageId);
                        $block4 = get_field('pagina_3',$pageId);
                    break;
                    case '2cat2page':
                        $block1 = get_field('categorie_1',$pageId);
                        $block2 = get_field('categorie_2',$pageId);
                        $block3 = get_field('pagina_1',$pageId);
                        $block4 = get_field('pagina_2',$pageId);
                    break;
                    default:
                    case '3cat1page':
                        $block1 = get_field('categorie_1',$pageId);
                        $block2 = get_field('categorie_2',$pageId);
                        $block3 = get_field('categorie_3',$pageId);
                        $block4 = get_field('pagina_1',$pageId);
                    break;
                    case '4catpage':
                        $block1 = get_field('categorie_1',$pageId);
                        $block2 = get_field('categorie_2',$pageId);
                        $block3 = get_field('categorie_3',$pageId);
                        $block4 = get_field('categorie_4',$pageId);
                    break;
                }

                $out = array($block1, $block2, $block3, $block4);
            }
        }
    }

    return $out;
}

function laadkompas_create_posttype_vergelijkingUrls() {
$supports = array(
    'title', // post title
    // 'editor', // post content
    // 'author', // post author
    // 'thumbnail', // featured images
    // 'excerpt', // post excerpt
    'custom-fields', // custom fields
    // 'comments', // post comments
    // 'revisions', // post revisions
    // 'post-formats', // post formats
);
$labels = array(
    'name' => _x('Vergelijking urls', 'plural'),
    'singular_name' => _x('Vergelijking url', 'singular'),
    'menu_name' => _x('VergelijkingUrls', 'admin menu'),
    'name_admin_bar' => _x('quote', 'admin bar'),
    'add_new' => _x('Nieuwe vergelijking url toevoegen', 'add new'),
    'add_new_item' => __('Nieuw vergelijking url item toevoegen'),
    'new_item' => __('Nieuwe vergelijking url'),
    'edit_item' => __('Vergelijking url aanpassen'),
    'view_item' => __('Bekijk vergelijking urls'),
    'all_items' => __('Alle vergelijking urls'),
    'search_items' => __('Vergelijking url zoeken'),
    'not_found' => __('Geen reviews gevonden.'),
);
$args = array(
    'supports' => $supports,
    'labels' => $labels,
    'public' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'quote'),
    'has_archive' => false,
    'hierarchical' => false,
    'menu_icon'           => 'dashicons-tickets-alt'
);

register_post_type( 'quote', $args);
}
// Hooking up our function to theme setup
add_action( 'init', 'laadkompas_create_posttype_vergelijkingUrls' );



function laadkompas_create_posttype_zonVergelijkingUrls() {
$supports = array(
    'title', // post title
    // 'editor', // post content
    // 'author', // post author
    // 'thumbnail', // featured images
    // 'excerpt', // post excerpt
    'custom-fields', // custom fields
    // 'comments', // post comments
    // 'revisions', // post revisions
    // 'post-formats', // post formats
);
$labels = array(
    'name' => _x('Zonnepaneel Vergelijking', 'plural'),
    'singular_name' => _x('Vergelijking url', 'singular'),
    'menu_name' => _x('Zonnepaneel vergel.', 'admin menu'),
    'name_admin_bar' => _x('Zonnepaneel vergel.', 'admin bar'),
    'add_new' => _x('Nieuwe vergelijking url toevoegen', 'add new'),
    'add_new_item' => __('Nieuw vergelijking url item toevoegen'),
    'new_item' => __('Nieuwe vergelijking url'),
    'edit_item' => __('Vergelijking url aanpassen'),
    'view_item' => __('Bekijk vergelijking urls'),
    'all_items' => __('Alle vergelijking urls'),
    'search_items' => __('Vergelijking url zoeken'),
    'not_found' => __('Geen reviews gevonden.'),
);
$args = array(
    'supports' => $supports,
    'labels' => $labels,
    'public' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'zonnepanelen-quote'),
    'has_archive' => false,
    'hierarchical' => false,
    'menu_icon'           => 'dashicons-tickets-alt'
);

register_post_type( 'zonnepanelen-quote', $args);
}
// Hooking up our function to theme setup
add_action( 'init', 'laadkompas_create_posttype_zonVergelijkingUrls' );





function laadkompas_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'laadkompas_add_woocommerce_support' );

function formatLinkObjToListLink($link)
{
    $out = null;
    if($link)
    {
        if(isset($link['title']) && $link['url'])
        {
            $out = '
            <li>
                <a href="'.$link['url'].'"><img src="'.get_template_directory_uri().'/images/link-groen.png" alt="'.$link['title'].'" /> '.$link['title'].'</a>
            </li>';
        }
    }
    return $out;
}

function formatColor($colorString)
{
    $out = '';
    $exp = explode(", ",$colorString);
    if(count($exp) > 1)
    {
        $i=0;
        foreach($exp as $item)
        {
            switch($item)
            {
                case 'wit':
                $out .= '<span class="color color-light-grey"></span>';
                break;
                case 'antraciet':
                $out .= '<span class="color color-grey"></span>';
                break;
                case 'zwart':
                $out .= '<span class="color color-darkgrey"></span>';
                break;
                default:
                $val = $item.(($i != count($exp)-1)? ', ' : '');
                $out .= $val;
                break;
            }
            $i+=1;
        }
    } else $out = $colorString;

    return $out;
}

function formatYesNo($answer)
{
    $out = null;
    $origin_answer = $answer;
    $answer = strtolower($answer);
    if($answer)
    {
        //override on found a "ja"
        if(strpos($answer, 'ja') !== -1)
            $answer = 'ja';

        switch($answer){
            case 'ja':
            $out = '<img src="'.get_template_directory_uri().'/images/icon-checkmark.svg" alt="Check" />';
            break;
            case 'nee':
            $out = '<i class="fa fa-times"></i>';
            break;
            case 'leeg':
            $out = '';
            break;
            default:
            $out = formatColor($origin_answer);
            break;
        }
    }
    return trim($out);
}

function formatImgObjToLink($img, $alt='')
{
    $out = null;
    if($img)
    {
        $out = '<div class="image"><img src="'.$img.'" alt="'.$alt.'" /></div>';
    }
    return $out;
}

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'header-menu-zon' => __( 'Header Menu Zonnepanelen' ),
      'shop-menu' => __( 'Shop Menu' ),
      'footer-menu' => __( 'Footer Menu' ),
      'footer-menu-2' => __( 'Footer Menu 2' ),
      'footer-menu-3' => __( 'Footer Menu 3' ),


      'footer-menu-zon1' => __( 'Footer Menu Zonnepaneel' ),
      'footer-menu-zon2' => __( 'Footer Menu Zonnepaneel Klantenservice' ),

      'faq-menu-1' => __( 'Klantenservice Menu 1' ),
      'faq-menu-2' => __( 'Klantenservice Menu 2' ),
      'faq-menu-3' => __( 'Klantenservice Menu 3' ),
      'faq-menu-4' => __( 'Klantenservice Menu 4' ),
      'faq-menu-5' => __( 'Klantenservice Menu 5' ),
      'faq-menu-6' => __( 'Klantenservice Menu 6' ),

      'faq-menu-7' => __( 'Zonnepanelen faq Menu 1' ),
      'faq-menu-8' => __( 'Zonnepanelen faq Menu 2' ),
      'faq-menu-9' => __( 'Zonnepanelen faq Menu 3' ),
      'faq-menu-10' => __( 'Zonnepanelen faq Menu 4' ),
      'faq-menu-11' => __( 'Zonnepanelen faq Menu 5' ),
      'faq-menu-12' => __( 'Zonnepanelen faq Menu 6' ),

     )
   );
 }
 add_action( 'init', 'register_my_menus' );


//add phone nr to woocommerce settings
add_filter('woocommerce_general_settings', 'general_settings_shop_phone');
function general_settings_shop_phone($settings) {
$key = 0;

foreach( $settings as $values ){
    $new_settings[$key] = $values;
    $key++;

    // Inserting array just after the post code in "Store Address" section
    if($values['id'] == 'woocommerce_store_postcode'){
        $new_settings[$key] = array(
            'title'    => __('Telefoonummer'),
            'desc'     => __('Optioneel telefoonnummer'),
            'id'       => 'woocommerce_store_phone', // <= The field ID (important)
            'default'  => '',
            'type'     => 'text',
            'desc_tip' => true, // or false
        );
        $key++;
        $new_settings[$key] = array(
            'title'    => __('Telefoonnummer zonnepanelen'),
            'desc'     => __('Tel. nr zonnepanelen'),
            'id'       => 'woocommerce_store_phone_zon', // <= The field ID (important)
            'default'  => '',
            'type'     => 'text',
            'desc_tip' => true, // or false
        );
        $key++;
        $new_settings[$key] = array(
            'title'    => __('E-mailadres'),
            'desc'     => __('Optioneel emailadres'),
            'id'       => 'woocommerce_store_email', // <= The field ID (important)
            'default'  => '',
            'type'     => 'text',
            'desc_tip' => true, // or false
        );
        $key++;
    }
}
return $new_settings;
}


/**
 * Gets and formats a list of cart item data + variations for display on the frontend.
 *
 * @since 3.3.0
 * @param array $cart_item Cart item object.
 * @param bool  $flat Should the data be returned flat or in a list.
 * @return string
 */
function wc_get_formatted_cart_item_data_Checkout( $cart_item, $flat = false ) {
    $item_data = array();

    // Variation values are shown only if they are not found in the title as of 3.0.
    // This is because variation titles display the attributes.
    if ( $cart_item['data']->is_type( 'variation' ) && is_array( $cart_item['variation'] ) ) {
        foreach ( $cart_item['variation'] as $name => $value ) {
            $taxonomy = wc_attribute_taxonomy_name( str_replace( 'attribute_pa_', '', urldecode( $name ) ) );

            if ( taxonomy_exists( $taxonomy ) ) {
                // If this is a term slug, get the term's nice name.
                $term = get_term_by( 'slug', $value, $taxonomy );
                if ( ! is_wp_error( $term ) && $term && $term->name ) {
                    $value = $term->name;
                }
                $label = wc_attribute_label( $taxonomy );
            } else {
                // If this is a custom option slug, get the options name.
                $value = apply_filters( 'woocommerce_variation_option_name', $value, null, $taxonomy, $cart_item['data'] );
                $label = wc_attribute_label( str_replace( 'attribute_', '', $name ), $cart_item['data'] );
            }

            // Check the nicename against the title.
            if ( '' === $value || wc_is_attribute_in_product_name( $value, $cart_item['data']->get_name() ) ) {
                continue;
            }

            $price = 0;
            $fieldname = 'post_tag_'.$term->term_id;
            $tax_data = get_term_by('slug', $taxonomy);
            if($newPrice = get_field('product_optie_prijs',$fieldname))
            {
                $price = $newPrice;
            }


            $item_data[] = array(
                'key'   => $label,
                'value' => $value,
                'price' => $price,
                'origin_key' => $taxonomy
            );
        }
    }

    // Filter item data to allow 3rd parties to add more to the array.
    $item_data = apply_filters( 'woocommerce_get_item_data', $item_data, $cart_item );

    // Format item data ready to display.
    foreach ( $item_data as $key => $data ) {
        // Set hidden to true to not display meta on cart.
        if ( ! empty( $data['hidden'] ) ) {
            unset( $item_data[ $key ] );
            continue;
        }
        $item_data[ $key ]['key']     = ! empty( $data['key'] ) ? $data['key'] : $data['name'];
        $item_data[ $key ]['display'] = ! empty( $data['display'] ) ? $data['display'] : $data['value'];
    }

    // Output flat or in list format.
    if ( count( $item_data ) > 0 ) {
        ob_start();

        if ( $flat ) {
            foreach ( $item_data as $data ) {
                echo esc_html( $data['key'] ) . ': ' . wp_kses_post( $data['display'] ) . "\n";
            }
        } else {
            wc_get_template( 'cart/cart-item-data.php', array( 'item_data' => $item_data ) );
        }

        return ob_get_clean();
    }

    return '';
}

function wpse_enqueue_page_template_styles() {
        global $post;
        wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );


        if(is_front_page() || strpos(get_page_template(), "page-zon-homepage.php"))
        {
            wp_enqueue_style( 'swiper', get_template_directory_uri() . '/css/swiper.min.css' );
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'companyUsps', get_template_directory_uri() . '/css/companyUsps.css' );
            wp_enqueue_style( 'customerReview', get_template_directory_uri() . '/css/customerReview.css' );
            wp_enqueue_style( 'products', get_template_directory_uri() . '/css/products.css' );
            wp_enqueue_script( 'vergelijkerjs', get_template_directory_uri() . '/js/vergelijker.js', array(), '1.0.0', true);

            if(strpos(get_page_template(), "page-zon-homepage.php")) {
                wp_enqueue_script( 'zonjs', get_template_directory_uri() . '/js/new_zonnepanelen.js', array(), '1.0.0', true);
            }

        }
        elseif(is_product())
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'lightslider', get_template_directory_uri() . '/css/lightslider.css' );
            wp_enqueue_style( 'company-usps', get_template_directory_uri() . '/css/company-usps-funnel.css' );
            wp_enqueue_style( 'landing', get_template_directory_uri() . '/css/uspBlock.css' );
                wp_enqueue_style( 'product-detail', get_template_directory_uri() . '/css/product-detail.css' );

            wp_enqueue_script( 'lightslider', get_template_directory_uri() . '/js/lib/lightslider.min.js', array(), '1.0.0', true);

                wp_enqueue_script( 'product-detail', get_template_directory_uri() . '/js/product-detail.js', array(), '1.0.0', true);
            wp_enqueue_script( 'taxjs', get_template_directory_uri() . '/js/tax.js', array(), '1.0.0', true);
        }
        elseif(is_cart())
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'cart', get_template_directory_uri() . '/css/cart.css' );
            wp_enqueue_script( 'cartjs', get_template_directory_uri() . '/js/cart_script.js', array(), '1.0.0', true);
        }
        elseif(is_product_category())
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'company-usps-funnel', get_template_directory_uri() . '/css/company-usps-funnel.css' );
            wp_enqueue_style( 'nouicss', get_template_directory_uri() . '/js/lib/nouislider.min.css' );


            wp_enqueue_style( 'products', get_template_directory_uri() . '/css/products.css' );
            wp_enqueue_style( 'filter-products', get_template_directory_uri() . '/css/filter-products.css' );
            wp_enqueue_style( 'product-category', get_template_directory_uri() . '/css/product-category.css' );
            wp_enqueue_script( 'nouijs', get_template_directory_uri() . '/js/lib/nouislider.min.js', array(), '1.0.0', true);
            wp_enqueue_script( 'product-categoryjs', get_template_directory_uri() . '/js/product-category.js', array(), '1.0.0', true);
            wp_enqueue_script( 'taxjs', get_template_directory_uri() . '/js/tax.js', array(), '1.0.0', true);
            wp_enqueue_script( 'vergelijkerjs', get_template_directory_uri() . '/js/vergelijker.js', array(), '1.0.0', true);

        }
        elseif ( is_checkout() && !empty( is_wc_endpoint_url('order-received') ) ) {

            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'bestelling_afgerond', get_template_directory_uri() . '/css/bestelling_afgerond.css' );
        }
        elseif(is_checkout())
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'checkout', get_template_directory_uri() . '/css/checkout.css' );
            wp_enqueue_style( 'review', get_template_directory_uri() . '/css/review.css' );
            wp_enqueue_style( 'payment', get_template_directory_uri() . '/css/payment.css' );
            
            wp_enqueue_script( 'nouijs', get_template_directory_uri() . '/js/checkout.js', array(), '1.0.0', true); 
        }
        elseif($post->ID == 223) //vergelijken
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'vergelijking', get_template_directory_uri() . '/css/vergelijking.css' );
            wp_enqueue_style( 'company-usps-funnel', get_template_directory_uri() . '/css/company-usps-funnel.css' );
            wp_enqueue_style( 'product-cards', get_template_directory_uri() . '/css/product-cards.css' );
            wp_enqueue_script( 'vergelijkingjs', get_template_directory_uri() . '/js/vergelijking.js', array(), '1.0.0', true);
            wp_enqueue_script( 'taxjs', get_template_directory_uri() . '/js/tax.js', array(), '1.0.0', true);
        }
        elseif($post->ID == 229) //Funnel stap 1
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'funnel', get_template_directory_uri() . '/css/funnel.css' );
            wp_enqueue_script( 'vergelijkingjs', get_template_directory_uri() . '/js/funnel.js', array(), '1.0.0', true);

        }
        elseif($post->ID == 14204) //zonnepaneel stap 1
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'funnel', get_template_directory_uri() . '/css/funnel.css' );
            wp_enqueue_script( 'vergelijkingjs', get_template_directory_uri() . '/js/funnel_zon.js', array(), '1.0.0', true);

        }
        elseif($post->ID == 231) //Funnel stap 2
        {
            wp_enqueue_style( 'swiper', get_template_directory_uri() . '/css/swiper.min.css' );
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'customerReview', get_template_directory_uri() . '/css/customerReview.css' );
            wp_enqueue_style( 'funnel-2', get_template_directory_uri() . '/css/funnel-2.css' );

            wp_enqueue_script( 'formValid', '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js', array(), null, true);
            wp_enqueue_script( 'funneljs', get_template_directory_uri() . '/js/funnel.js', array(), '1.0.0', true);
               wp_localize_script( 'funneljs', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-sf.php' ),  'updateCount' => admin_url( 'admin-update-counter.php' ))); 

        }
        elseif($post->ID == 14209) //Zonnepaneel Funnel stap 2
        {
            wp_enqueue_style( 'swiper', get_template_directory_uri() . '/css/swiper.min.css' );
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'customerReview', get_template_directory_uri() . '/css/customerReview.css' );
            wp_enqueue_style( 'funnel-2', get_template_directory_uri() . '/css/funnel-2.css' );

            wp_enqueue_script( 'formValid', '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js', array(), null, true);
            wp_enqueue_script( 'funneljs', get_template_directory_uri() . '/js/funnel_zon.js', array(), '1.0.0', true);
               wp_localize_script( 'funneljs', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-sf.php' ),  'updateCount' => admin_url( 'admin-update-counter.php' ))); 

        }
        elseif($post->post_type == "quote" || $post->post_type == "zonnepanelen-quote") //Funnel stap 3
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'funnel-result', get_template_directory_uri() . '/css/funnel-result.css' );
            wp_enqueue_style( 'company-usps-funnel', get_template_directory_uri() . '/css/company-usps-funnel.css' );
            wp_enqueue_style( 'product-cards', get_template_directory_uri() . '/css/product-cards.css' );
            wp_enqueue_style( 'filter-products', get_template_directory_uri() . '/css/filter-products.css' );
            wp_enqueue_script( 'funneljs', get_template_directory_uri() . '/js/funnel.js', array(), '1.0.0', true);
            wp_enqueue_script( 'filter-funneljs', get_template_directory_uri() . '/js/filter-funnel.js', array(), '1.0.0', true);
            wp_enqueue_script( 'taxjs', get_template_directory_uri() . '/js/tax.js', array(), '1.0.0', true);
            wp_enqueue_script( 'vergelijkerjs', get_template_directory_uri() . '/js/vergelijker.js', array(), '1.0.0', true);
        }
        elseif($post->ID == 712)
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'contact', get_template_directory_uri() . '/css/contact.css' );

            
            wp_enqueue_script( 'mapsjs', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBLV9bVHslhH2aI18Tdza3yZLtXW7JrzTY', array(), false, true);   
            wp_enqueue_script( 'contactjs', get_template_directory_uri() . '/js/contact.js', array(), '1.0.0', true);   
        }
        elseif($post->ID == 768)
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'klantenservice', get_template_directory_uri() . '/css/klantenservice.css' );
        }
        elseif($post->ID == 13326) //zonnepanelen klantenservice
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'klantenservice', get_template_directory_uri() . '/css/klantenservice.css' );
        }
        elseif($post->ID == 10008) //kennisbank home
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'kennisbank', get_template_directory_uri() . '/css/kennisbank.css' );
        }
        elseif($post->ID == 13176) //installatie calculator
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'funnel', get_template_directory_uri() . '/css/funnel-2.css' );
            wp_enqueue_style( 'calculator', get_template_directory_uri() . '/css/calculator.css' );
            wp_enqueue_script( 'install_objjs', get_template_directory_uri() . '/js/install_obj.js', array(), '1.0.0', true);
            wp_enqueue_script( 'install_calculatorjs', get_template_directory_uri() . '/js/install_calculator.js', array(), '1.0.0', true);
            wp_enqueue_script( 'install_taxjs', get_template_directory_uri() . '/js/install_tax.js', array(), '1.0.0', true);
        }
        elseif($post->ID == 13324) { //FAQ tussenpage

            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'faq2', get_template_directory_uri() . '/css/faq-2.css' );
        }
        elseif(is_single())
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'kennisbank_artikel', get_template_directory_uri() . '/css/kennisbank_artikel.css' );
        }
        elseif(is_category())
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'kennisbank', get_template_directory_uri() . '/css/kennisbank.css' );
            wp_enqueue_style( 'kennisbank_category', get_template_directory_uri() . '/css/kennisbank_category.css' );
        }
        elseif(strpos(get_page_template(), "page-faq.php") || strpos(get_page_template(), "page-faq-zonnepaneel.php"))
        {
            wp_enqueue_style( 'styleguide', get_template_directory_uri() . '/css/styleguide.css' );
            wp_enqueue_style( 'faq', get_template_directory_uri() . '/css/faq.css' );
            wp_enqueue_script( 'faqjs', get_template_directory_uri() . '/js/faq.js', array(), '1.0.0', true);  
        }
        elseif(is_shop())
        {
            wp_enqueue_style( 'company-usps-funnel', get_template_directory_uri() . '/css/company-usps-funnel.css' );
            wp_enqueue_style( 'lightslider', get_template_directory_uri() . '/css/lightslider.css' );
            wp_enqueue_style( 'shop-home', get_template_directory_uri() . '/css/shop-home.css' );

            wp_enqueue_script( 'lightslider', get_template_directory_uri() . '/js/lib/lightslider.min.js', array(), '1.0.0', true);
            wp_enqueue_script( 'shop_homejs', get_template_directory_uri() . '/js/shop_home.js', array(), '1.0.0', true);
        }
        elseif(strpos(get_page_template(), "page-landingspage.php") || strpos(get_page_template(), "page-zonnepanelen.php") || strpos(get_page_template(), "page-partner.php"))
        {
            wp_enqueue_style( 'lightslider', get_template_directory_uri() . '/css/lightslider.css' );
            wp_enqueue_style( 'customerReviews', get_template_directory_uri() . '/css/customerReview.css' );


            wp_enqueue_style( 'company-usps', get_template_directory_uri() . '/css/company-usps-funnel.css' );
            wp_enqueue_style( 'uspBlock', get_template_directory_uri() . '/css/uspBlock.css' );

            if(strpos(get_page_template(), "page-landingspage.php")) {
                wp_enqueue_style( 'products', get_template_directory_uri() . '/css/products.css' );
                // wp_enqueue_style( 'faq', get_template_directory_uri() . '/css/faq.css' );
                wp_enqueue_script( 'faqjs', get_template_directory_uri() . '/js/faq.js', array(), '1.0.0', true);  
                wp_enqueue_style( 'landingFaq', get_template_directory_uri() . '/css/landingFaq.css' );
            }
            wp_enqueue_style( 'landing', get_template_directory_uri() . '/css/landing.css' );


            if(strpos(get_page_template(), "page-zonnepanelen.php")){
                wp_enqueue_style( 'products', get_template_directory_uri() . '/css/products.css' );
                wp_enqueue_style( 'zonnepanelen', get_template_directory_uri() . '/css/zonnepanelen.css' );
                wp_enqueue_script( 'zonnepanelenjs', get_template_directory_uri() . '/js/zonnepanelen.js', array(), '1.0.0', true);  
            }

            if(strpos(get_page_template(), "page-partner.php")){
                wp_enqueue_style( 'partner', get_template_directory_uri() . '/css/partner.css' );
                wp_enqueue_script( 'partnerjs', get_template_directory_uri() . '/js/partner.js', array(), '1.0.0', true);  
            }
            
            wp_enqueue_script( 'lightslider', get_template_directory_uri() . '/js/lib/lightslider.min.js', array(), '1.0.0', true);
            wp_enqueue_script( 'landingjs', get_template_directory_uri() . '/js/landing.js', array(), '1.0.0', true);  
        }

        wp_enqueue_script( 'cookiejs', get_template_directory_uri() . '/js/cookie.js', array(), '1.0.0', true);  

}
add_action( 'wp_enqueue_scripts', 'wpse_enqueue_page_template_styles' );


function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

add_action( 'admin_head', 'hide_editor' );
function hide_editor() {
    $template_file = $template_file = basename( get_page_template() );
    if($template_file == 'page-faq.php'){ // template
        remove_post_type_support('page', 'editor');
    }
}

add_action('after_setup_theme', 'remove_admin_bar');
 function remove_admin_bar() {
  show_admin_bar(false);
}

//add richsnippet for landingspage
add_filter( 'language_attributes', 'wpse140730_add_manifest_to_language_attributes' );
function wpse140730_add_manifest_to_language_attributes($output) {
    global $wp_query;
    $tpl = get_page_template($wp_query->queried_object->ID);
    if(strpos($tpl, "page-landingspage.php")) {
        return $output. ' itemscope itemtype="https://schema.org/FAQPage"';
    }
    else return $output;
}

function productGallery($gallery,$product_id)
{   
    $out = null;

    global $product;
    

    if($gallery)
    {
        foreach( $gallery as $attachment_id ) {
            $out .= '
            <li data-thumb="'.wp_get_attachment_url( $attachment_id ).'" class="noID">
                <img src="'.wp_get_attachment_url( $attachment_id ).'" alt="" />
            </li>';
        }
    }


    $product = new WC_Product_Variable( $product_id );
    $variations = $product->get_available_variations();

    foreach ( $variations as $variation ) {
      // echo "<img src=" . $variation['image']['thumb_src'] .">";
      $out .= '
            <li data-thumb="'.$variation['image']['thumb_src'].'" data-id="'.$variation['variation_id'].'">
                <img src="'.$variation['image']['thumb_src'].'" alt="" />
            </li>';
    }

    if ( $product->is_type('variable') ) {
       
      ?>
      <script>
      // jQuery(document).ready(function() {
        jQuery(window).load(function(){
            var proceed = false;
            
            setTimeout(function(){
                changePictures();
            }, 800);
      });

        function changePictures()
        {
            jQuery('input.variation_id').change( function(){
                if( '' != jQuery('input.variation_id').val() ) {
                   var var_id = $('input.variation_id').val();
                   jQuery(".product-slider li[data-id='"+var_id+"']").trigger('click');
                }
            });
        };
      </script>
      <?php
   }
    return $out;
}

function simpleProductGallery($gallery) {
    $out = '';
    if($gallery)
    {
        foreach( $gallery as $attachment_id ) {
            $out .= '
            <li data-thumb="'.wp_get_attachment_url( $attachment_id ).'" class="simpleProduct">
                <img src="'.wp_get_attachment_url( $attachment_id ).'" alt="" />
            </li>';
        }
    }
    return $out;
}


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );

function productPlusMin($plusMin)
{
    $found = '<ul class="plusmin">';

    $f = preg_match_all('#<li>.+?</li>#is', $plusMin, $matches);
    foreach($matches[0] as $match)
    {
        $icon = null;
        if(preg_match('#<li><strong>(.*?)</strong>(.*?)</li>#i', $match) === 1)
        {
            $icon = 'min';
        }
        else $icon = 'plus'; 

        $match = str_replace(array("<li>", "</li>", "<strong>", "</strong>"), "",$match);

        $found .= '<li>
                        <span class="icon"><img src="'.get_template_directory_uri().'/images/'.$icon.'.svg" alt=""></span>
                        <span class="text">'.$match.'</span>
                    </li>';
    }

    $found .= '</ul>';

    return $found;
}

function productCategoryPlusMin($plusMin, $max=false)
{
    $found = '<ul class="plusmin">';

    $f = preg_match_all('#<li>.+?</li>#is', $plusMin, $matches);
    $counter = 0;
    $maxItems = $max? $max : 4;
    foreach($matches[0] as $match)
    {
        $icon = null;
        if(preg_match('#<li><strong>(.*?)</strong>(.*?)</li>#i', $match) === 1)
        {
            $icon = '-';
        }
        else $icon = '+'; 

        $match = str_replace(array("<li>", "</li>", "<strong>", "</strong>"), "",$match);

        if($counter < $maxItems)
        {
            $found .= '<li><span'.(($icon == '-')? ' class="minpunt"' : '').'>'.$icon.'</span> '.$match.'</li>';
        }

        $counter += 1;
        
    }

    $found .= '</ul>';

    return $found;
}

function getAttributeDescription($attr_name)
{   
    $out = '';
    $INFO_QUESTIONS = $GLOBALS['INFO_QUESTIONS'];
    if(isset($INFO_QUESTIONS[$attr_name]) && $INFO_QUESTIONS[$attr_name] != '')
    {
        $out = $INFO_QUESTIONS[$attr_name];
    } else {
        $out = '';
    }

    return $out;
}

function parseAttributesToTable($attributes, $ids)
{
    global $product; 
    $out = '';
    foreach($attributes as $attribute)
    {
        $attribute_label_name = wc_attribute_label($attribute['name']);

        if(in_array($attribute['name'], $ids))
        {
            $out .= '
            <li>
                <span class="attribute">
                    <span class="text">'.$attribute_label_name.'</span>
                    <a class="questionPopup" href="javascript: void(0);" data-title="'.$attribute_label_name.'" data-txt="'.getAttributeDescription($attribute['name']).'"><img src="'.get_template_directory_uri().'/images/question-2.svg"></a>
                </span>
                <span class="value">
                    '.$product->get_attribute($attribute['name']).'
                </span>
                <div class="clearfix"></div>
            </li>
            ';
        }
    }
    return $out;
}


//override widget class
add_action( 'widgets_init', 'err_override_woocommerce_widgets', 15 );

function err_override_woocommerce_widgets() {
  // Ensure our parent class exists to avoid fatal error (thanks Wilgert!)

  if ( class_exists( 'WC_Widget_Layered_Nav' ) ) {

        unregister_widget( 'WC_Widget_Layered_Nav' );

        include_once( 'woocommerce/includes/widgets/custom-wc-widget-layered-nav.php' );

        register_widget( 'Custom_WC_Widget_Layered_Nav' );
  }


  if ( class_exists( 'WC_Widget_Price_Filter' ) ) {

        unregister_widget( 'WC_Widget_Price_Filter' );

        include_once( 'woocommerce/includes/widgets/custom-wc-widget-price-filter.php' );

        register_widget( 'Custom_WC_Widget_Price_Filter' );
  }

}

function my_widget_title($title, $instance, $id_base) {
    $activeIds = array("merk", "");

    return $title.'<span class="filter-arrow'.(in_array(@$instance['attribute'], $activeIds)? ' active' : '').'"></span>';
}
add_filter ( 'widget_title' , 'my_widget_title', 10, 3); //we use the default priority and 3 arguments in the callback function




remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');

add_filter( 'change_title_to_link_category', 'change_title_to_link_category_func' );


remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);


/* Email tpls */

function wc_display_item_meta_custom_email( $item, $args = array() ) { 
    $strings = array(); 
    $html = ''; 
    $args = wp_parse_args( $args, array( 
        // 'before' => '<ul class="wc-item-meta"><li>',  
        // 'after' => '</li></ul>',  
        // 'separator' => '</li><li>',  
        'echo' => true,  
        'autop' => false,  
 ) ); 
 
    foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) { 
        $price = 0;
        $value = $args['autop'] ? wp_kses_post( $meta->display_value ) : wp_kses_post( make_clickable( trim( strip_tags( $meta->display_value ) ) ) ); 
        // echo wp_kses_post( $data['key'] );
        
        // $identifier = $meta->key;
        $term = get_term_by( 'slug', $meta->value, str_replace("attribute_pa_","", $meta->key));
        $fieldname = 'post_tag_'.$term->term_id;

        if(get_field('product_optie_prijs',$fieldname))
        {
            $price = get_field('product_optie_prijs',$fieldname);
        }
        
        $strings[] = '
        <tr>
          <td style="padding-bottom: 6px;">
             <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                   <td style="font-family: Arial, sans-serif; font-size: 14px; color: #6e706f;" align="left">
                      '.$value.'
                   </td>
                   <td style="font-family: Arial, sans-serif; font-size: 14px; color: '.(($price == 0)? '#9faba5' : '#000000').';" align="right">
                      <b>'.(($price == 0)? '&euro; 0,00' : '&euro;'.number_format($price,2,',','.')).'</b>
                   </td>
                </tr>
             </table>
          </td>
       </tr>'; 
    } 
 
    if ( $strings ) { 
        $html = @$args['before'] . implode( @$args['separator'], $strings ) . @$args['after']; 
    } 
 
    $html = apply_filters( 'woocommerce_display_item_meta', $html, $item, $args ); 
 
    if ( $args['echo'] ) { 
        echo $html; 
    } else { 
        return $html; 
    } 
} 

function change_title_to_link_category_func( $args ) {
  return '<a href="'.$args[1].'" title="'.$args[0].'">'.$args[0].'</a>';
}

/* Edit thumbnails in product-category */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
    function woocommerce_template_loop_product_thumbnail() {
        echo woocommerce_get_product_thumbnail();
    } 
}
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {   
    function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0  ) {
        global $post, $woocommerce;
        $output = '<div class="picture">';

        if ( has_post_thumbnail() ) {               
            $output .= '<a href="'.get_the_permalink($post->ID).'">'.get_the_post_thumbnail( $post->ID, $size ).'</a>';
        } else {
             $output .= wc_placeholder_img( $size );
        }                       
        //
        // $output .= '<div class="num"><img src="'.get_template_directory_uri().'/images/1@2x.png" alt="Nummer 1" /></div>';
        $output .= '</div>';
        return $output;
    }
}

function wpb_widgets_init() {
 
    register_sidebar( array(
        'name'          => 'Woocommerce filters',
        'id'            => 'wc-filters'
    ) );
 
}
add_action( 'widgets_init', 'wpb_widgets_init' );


function myTruncate($string, $limit, $break=".", $pad="...")
{
  // return with no change if string is shorter than $limit
  if(strlen($string) <= $limit) return $string;

  // is $break present between $limit and the end of the string?
  if(false !== ($breakpoint = strpos($string, $break, $limit))) {
    if($breakpoint < strlen($string) - 1) {
      $string = substr($string, 0, $breakpoint) . $pad;
    }
  }

  return $string;
}

//TODO Fix prices for inc/ex btw toggle
// function edit_price_display() {
//     global $product;

//     return '<span class="price" data-inc="'.wc_get_price_including_tax( $product ).'" data-exc="'.wc_get_price_excluding_tax( $product ).'">&euro; <span class="price_digit">'.@number_format(wc_get_price_excluding_tax( $product ),2,',','.').'</span></span>';
// }
// add_filter('woocommerce_get_price_html', 'edit_price_display', 10, 2);



add_filter( 'loop_shop_per_page', 'bbloomer_redefine_products_per_page', 9999 );
 
function bbloomer_redefine_products_per_page( $per_page ) {
  // $per_page = 12;
  return $per_page;
}


/* Add LKP as extra reciptient for processing_order email */
add_filter( 'woocommerce_email_recipient_customer_processing_order', 'bbloomer_order_processing_email_add_to', 9999, 3 );
 
function bbloomer_order_processing_email_add_to( $email_recipient, $email_object, $email ) {
    // if ( $email->id == 'customer_processing_order' ) {
        $email_recipient .= ', info@laadkompas.nl';
    // }
    return $email_recipient;
}

function wc_dropdown_variation_attribute_options( $args = array() ) {
        $args = wp_parse_args(
            apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ),
            array(
                'options'          => false,
                'attribute'        => false,
                'product'          => false,
                'selected'         => false,
                'name'             => '',
                'id'               => '',
                'class'            => '',
                'show_option_none' => __( 'Choose an option', 'woocommerce' ),
            )
        );

        // Get selected value.
        if ( false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product ) {
            $selected_key     = 'attribute_' . sanitize_title( $args['attribute'] );
            $args['selected'] = isset( $_REQUEST[ $selected_key ] ) ? wc_clean( wp_unslash( $_REQUEST[ $selected_key ] ) ) : $args['product']->get_variation_default_attribute( $args['attribute'] ); // WPCS: input var ok, CSRF ok, sanitization ok.
        }

        $options               = $args['options'];
        $product               = $args['product'];
        $attribute             = $args['attribute'];
        $name                  = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
        $id                    = $args['id'] ? $args['id'] : sanitize_title( $attribute );
        $class                 = $args['class'];
        $show_option_none      = (bool) $args['show_option_none'];
        $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

        if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
            $attributes = $product->get_variation_attributes();
            $options    = $attributes[ $attribute ];
        }

        $html  = '<select id="' . esc_attr( $id ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '">';
        $html .= '<option value="">' . esc_html( $show_option_none_text ) . '</option>';

        if ( ! empty( $options ) ) {
            if ( $product && taxonomy_exists( $attribute ) ) {
                // Get terms if this is a taxonomy - ordered. We need the names too.
                $terms = wc_get_product_terms(
                    $product->get_id(),
                    $attribute,
                    array(
                        'fields' => 'all',
                    )
                );

                foreach ( $terms as $term ) {
                    if ( in_array( $term->slug, $options, true ) ) {

                        $optionPrice = null;
                        $identifier = "post_tag_".$term->term_id;
                        $description = get_field('product_optie_prijs',$identifier);

                        if($description != "" && $description != null)
                        {
                            $optPrice = number_format($description,2, ',', '.');
                            $optionPrice = ' +&euro; '.$optPrice;
                        }



                        $html .= '<option data-optPrice="'.$optionPrice.'" value="' . esc_attr( $term->slug ) . '" ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name, $term, $attribute, $product ) ) .$optionPrice.'</option>';
                    }
                }
            } else {
                foreach ( $options as $option ) {
                    // This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
                    $selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false );
                    $html    .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option, null, $attribute, $product ) ) . '</option>';
                }
            }
        }

        $html .= '</select>';

        echo apply_filters( 'woocommerce_dropdown_variation_attribute_options_html', $html, $args ); // WPCS: XSS ok.
    }

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'my_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function my_override_checkout_fields( $fields ) {
    // var_dump($fields);exit;
     unset($fields['order']['order_comments']);

     return $fields;
}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );

// add payment methods
add_action( 'woocommerce_checkout_shipping_custom', 'woocommerce_checkout_payment', 20 );


remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );


add_filter('init', 'add_query_vars');

function add_query_vars() {
    global $wp;
    $wp->add_query_var('overview');
}

function fetchCarBrands()
{
    $out = null;
    global $wpdb;
    
    $out = $wpdb->get_results( "SELECT * FROM lkp_car_brands ORDER BY brand_name ASC", OBJECT );
    
    return $out;
}

function fetchCarTypes($sessionItem = null, $selected = null, $selectedType = null)
{
    $out = null;
    global $wpdb;
    
    $all = $wpdb->get_results( "SELECT * FROM lkp_car_brands ORDER BY id ASC", OBJECT );
    foreach($all as $brand)
    {
       $types = $wpdb->get_results( "SELECT * FROM lkp_car_types where car_brand_id='".$brand->id."' ORDER BY type_name ASC", OBJECT ); 
       
        foreach($types as $type)
        {
            $out .= '<option data-id="'.$brand->id.'"'.((isset($sessionItem) && $sessionItem == $type->type_name)? ' selected="selected"' : '').' '.((isset($selected) && $selected == $brand->id)? ' data-selected="yes"' : '').' '.((isset($selectedType) && $selectedType == $type->id)? ' selected="selected"' : '').' value="'.$type->type_name.'">'.$type->type_name.'</option>';
        }
    }
    
    return $out;
}

function shapeSpace_popular_posts($post_id) {
    $count_key = 'popular_posts';
    $count = get_post_meta($post_id, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($post_id, $count_key);
        add_post_meta($post_id, $count_key, '0');
    } else {
        $count++;
        update_post_meta($post_id, $count_key, $count);
    }
}
function shapeSpace_track_posts($post_id) {
    if (!is_single()) return;
    if (empty($post_id)) {
        global $post;
        $post_id = $post->ID;
    }
    shapeSpace_popular_posts($post_id);
}
add_action('wp_head', 'shapeSpace_track_posts');

function kennisbank_populair()
{
    $return = [];
    $popular = new WP_Query(array('posts_per_page'=>3, 'meta_key'=>'popular_posts', 'orderby'=>'meta_value_num', 'order'=>'DESC'));
    while ($popular->have_posts()) : $popular->the_post(); 
    $return[] = [
            'title' => get_the_title(),
            'url' => get_the_permalink(),
            'image' => get_the_post_thumbnail_url(),
            'cat' => get_the_category()[0]->name,
            'txt' => get_the_excerpt()
        ];
    endwhile; wp_reset_postdata(); 
    
    return $return;
}

$collected_ids = []; //to exclude from all other news

function kennisbank_news($num=false,$ids=false)
{   
    $collected_ids = [];
    $return = [];
    $first = [];
    $args = array(
        'post_type' => 'post',
        'category_name' => 'nieuws',
        'posts_per_page'         => '3',
        'order'                  => 'DESC'
    );

    $post_query = new WP_Query($args);

    $i=0;
    if($post_query->have_posts() ) {
    while($post_query->have_posts() ) {
        $post_query->the_post();

            if($i==0)
            {
                $first = [
                    'title' => get_the_title(),
                    'url' => get_the_permalink(),
                    'image' => get_the_post_thumbnail_url(),
                    'cat' => get_the_category()[0]->name
                ];
            }
            elseif($i==1) {
                $second = [
                    'title' => get_the_title(),
                    'url' => get_the_permalink(),
                    'image' => get_the_post_thumbnail_url(),
                    'cat' => get_the_category()[0]->name,
                    'txt' => (get_field('introductie_tekst') != null && get_field('introductie_tekst') != "")? get_field('introductie_tekst') : get_the_excerpt()
                ];
            }
            elseif($i==2) {
                $third = [
                    'title' => get_the_title(),
                    'url' => get_the_permalink(),
                    'image' => get_the_post_thumbnail_url(),
                    'cat' => get_the_category()[0]->name,
                    'txt' => (get_field('introductie_tekst') != null && get_field('introductie_tekst') != "")? get_field('introductie_tekst') : get_the_excerpt()
                ];
            }

            $collected_ids[] = get_the_ID();
            
            $i+=1;
        }
    }

    if($num == 1)
        $out = $first;
    if($num == 2)
        $out = $second;
    if($num == 3)
        $out = $third;

    return ($ids == false)? $out : $collected_ids;
}

function kennisbank_tips($ids=false)
{
    $collected_ids = [];
    $return = [];
    $args = array(
        'post_type' => 'post',
        'category_name' => 'tips',
        'posts_per_page'         => '3',
        'order'                  => 'DESC'
    );

    $post_query = new WP_Query($args);

    if($post_query->have_posts() ) {
    while($post_query->have_posts() ) {
        $post_query->the_post();

         $return[] = [
            'title' => get_the_title(),
            'url' => get_the_permalink(),
            'image' => get_the_post_thumbnail_url(),
            'cat' => get_the_category()[0]->name,
            'txt' => (get_field('introductie_tekst') != null && get_field('introductie_tekst') != "")? get_field('introductie_tekst') : get_the_excerpt()
        ];

            $collected_ids[] = get_the_ID();
        }
    }

    return ($ids)? $collected_ids : $return;
}

function kennisbank_technical($ids=false)
{
    $collected_ids = [];
    $return = [];
    $args = array(
        'post_type' => 'post',
        'category_name' => 'techniek'
    );

    $post_query = new WP_Query($args);

    if($post_query->have_posts() ) {
    while($post_query->have_posts() ) {
        $post_query->the_post();

         $return[] = [
            'title' => get_the_title(),
            'url' => get_the_permalink(),
            'image' => get_the_post_thumbnail_url(),
            'cat' => get_the_category()[0]->name,
            'txt' => (get_field('introductie_tekst') != null && get_field('introductie_tekst') != "")? get_field('introductie_tekst') : get_the_excerpt()
        ];
   
        }
    }
    return ($ids)? $collected_ids : $return;
}

function kennisbank_all()
{
    $return = [];
    $first = [];
    $args = array(
        'post_type' => 'post'
    );

    $post_query = new WP_Query($args);

    $i=0;
    if($post_query->have_posts() ) {
        while($post_query->have_posts() ) {
            $post_query->the_post();

             $return[] = [
                'title' => get_the_title(),
                'url' => get_the_permalink(),
                'image' => get_the_post_thumbnail_url(),
                'cat' => get_the_category()[0]->name,
                'txt' => (get_field('introductie_tekst') != null && get_field('introductie_tekst') != "")? get_field('introductie_tekst') : get_the_excerpt()
            ];
        }
    }

    return $return;
}


//Settings page
function dbi_add_settings_page() {
    add_options_page( 'Laadkompas instellingen', 'Laadkompas instellingen', 'manage_options', 'dbi-example-plugin', 'dbi_render_plugin_settings_page' );
}
add_action( 'admin_menu', 'dbi_add_settings_page' );


function dbi_render_plugin_settings_page() {
    ?>
    <h2>Laadkompas instellingen</h2>
    <form action="options.php" method="post">
        <?php 
        settings_fields( 'lkp_plugin_options' );
        do_settings_sections( 'dbi_example_plugin' ); ?>
        <input name="submit" class="button button-primary" type="submit" value="<?php esc_attr_e( 'Save' ); ?>" />
    </form>
    <?php
}


function dbi_register_settings() {
    register_setting( 'lkp_plugin_options', 'lkp_plugin_options', 'dbi_example_plugin_options_validate' );
    add_settings_section( 'cat_settings', 'Categorie instellingen', 'dbi_plugin_section_text4', 'dbi_example_plugin' );
    add_settings_field( 'lkp_plugin_setting_sort', 'Categorieen sorteren op:', 'lkp_plugin_setting_sort', 'dbi_example_plugin', 'cat_settings' );


    add_settings_section( 'api_settings', 'Top 3 laadpalen homepage', 'dbi_plugin_section_text', 'dbi_example_plugin' );

    add_settings_field( 'lkp_plugin_setting_laadpaal1', 'Top product 1:', 'lkp_plugin_setting_laadpaal1', 'dbi_example_plugin', 'api_settings' );
    add_settings_field( 'lkp_plugin_setting_laadpaal2', 'Top product 2:', 'lkp_plugin_setting_laadpaal2', 'dbi_example_plugin', 'api_settings' );
    add_settings_field( 'lkp_plugin_setting_laadpaal3', 'Top product 3:', 'lkp_plugin_setting_laadpaal3', 'dbi_example_plugin', 'api_settings' );


    add_settings_section( 'advice_banner', 'Advies aanvragen banner', 'dbi_plugin_section_text2', 'dbi_example_plugin' );

    add_settings_field( 'lkp_plugin_setting_bannerTitle', 'Advies banner titel:', 'lkp_plugin_setting_bannerTitle', 'dbi_example_plugin', 'advice_banner' );
    add_settings_field( 'lkp_plugin_setting_bannerText', 'Advies banner text:', 'lkp_plugin_setting_bannerText', 'dbi_example_plugin', 'advice_banner' );
    add_settings_field( 'lkp_plugin_setting_bannerButton', 'Advies banner button pagina:', 'lkp_plugin_setting_bannerButton', 'dbi_example_plugin', 'advice_banner' );


    add_settings_section( 'advice_banner_product', 'Advies aanvragen banner Product', 'dbi_plugin_section_text3', 'dbi_example_plugin' );
    add_settings_field( 'lkp_plugin_setting_bannerTitleProduct', 'Advies banner titel:', 'lkp_plugin_setting_bannerTitleProduct', 'dbi_example_plugin', 'advice_banner_product' );
    add_settings_field( 'lkp_plugin_setting_bannerTextProduct', 'Advies banner text:', 'lkp_plugin_setting_bannerTextProduct', 'dbi_example_plugin', 'advice_banner_product' );
    add_settings_field( 'lkp_plugin_setting_bannerButtonProduct', 'Advies banner button pagina:', 'lkp_plugin_setting_bannerButtonProduct', 'dbi_example_plugin', 'advice_banner_product' );



    add_settings_section( 'cat_banner', 'Category banner', 'dbi_plugin_section_text5', 'dbi_example_plugin' );

    add_settings_field( 'lkp_plugin_setting_catTitle', 'Categorie banner titel:', 'lkp_plugin_setting_catTitle', 'dbi_example_plugin', 'cat_banner' );
    add_settings_field( 'media_selector_settings_page_callback', 'Categorie banner afbeelding:', 'media_selector_settings_page_callback', 'dbi_example_plugin', 'cat_banner' );
    add_settings_field( 'lkp_plugin_setting_catButton', 'Categorie banner pagina:', 'lkp_plugin_setting_catButton', 'dbi_example_plugin', 'cat_banner' );
}
add_action( 'admin_init', 'dbi_register_settings' );


function dbi_example_plugin_options_validate( $input ) {
    return $input;
}

function dbi_plugin_section_text() {
    echo '<p>Hier zijn de algemene instellingen voor Laadkompas.</p>';
}
function dbi_plugin_section_text2() {
    echo '<p>Deze instellingen zijn voor de shop home / categoriepagina</p>';
}
function dbi_plugin_section_text3() {
    echo '<p>Deze instellingen zijn voor de productpagina</p>';
}

function dbi_plugin_section_text4() {
    echo '<p>Hier zijn de categorie instellingen voor Laadkompas.</p>';
}

function dbi_plugin_section_text5() {
    echo '<p>Hier zijn de banner instellingen voor de categorie pagina.</p>';
}



function lkp_plugin_setting_sort() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<select name="lkp_plugin_options[lkp_cat_sort]">'.loadAllSorting(@$options['lkp_cat_sort']).'</select>';

}

function lkp_plugin_setting_laadpaal1() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<select name="lkp_plugin_options[toplaadpaal1]">'.loadAllProducts(@$options['toplaadpaal1']).'</select>';

}

function lkp_plugin_setting_laadpaal2() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<select name="lkp_plugin_options[toplaadpaal2]">'.loadAllProducts(@$options['toplaadpaal2']).'</select>';
}

function lkp_plugin_setting_laadpaal3() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<select name="lkp_plugin_options[toplaadpaal3]">'.loadAllProducts(@$options['toplaadpaal3']).'</select>';
}


function lkp_plugin_setting_bannerTitle() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<input style="width: 290px;" type="text" name="lkp_plugin_options[banner_title]" value="'.@$options['banner_title'].'" />';
}

function lkp_plugin_setting_bannerText() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<input style="width: 290px;" type="text" name="lkp_plugin_options[banner_text]" value="'.@$options['banner_text'].'" />';
}


function lkp_plugin_setting_bannerButton() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<select style="width: 290px;" name="lkp_plugin_options[banner_btn]">'.loadAllPages(@$options['banner_btn']).'</select>';
}



function lkp_plugin_setting_bannerTitleProduct() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<input style="width: 290px;" type="text" name="lkp_plugin_options[banner_title_product]" value="'.@$options['banner_title_product'].'" />';
}

function lkp_plugin_setting_bannerTextProduct() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<input style="width: 290px;" type="text" name="lkp_plugin_options[banner_text_product]" value="'.@$options['banner_text_product'].'" />';
}


function lkp_plugin_setting_bannerButtonProduct() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<select style="width: 290px;" name="lkp_plugin_options[banner_btn_product]">'.loadAllPages(@$options['banner_btn_product']).'</select>';
}

/* Categorie banner */
function lkp_plugin_setting_catTitle() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<input style="width: 290px;" type="text" name="lkp_plugin_options[cat_banner_title]" value="'.@$options['cat_banner_title'].'" />';
}

function lkp_plugin_setting_catImg() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<input style="width: 290px;" type="text" name="lkp_plugin_options[cat_banner_img]" value="'.@$options['cat_banner_img'].'" />';
}


function lkp_plugin_setting_catButton() {
    $options = get_option( 'lkp_plugin_options' );

    echo '<select style="width: 290px;" name="lkp_plugin_options[cat_banner_btn]">'.loadAllPages(@$options['cat_banner_btn']).'</select>';
}

function media_selector_settings_page_callback() {
    $options = get_option( 'lkp_plugin_options' );
    wp_enqueue_media();

    ?><form method='post'>
        <div class='image-preview-wrapper'>
            <img id='image-preview' src='<?php echo wp_get_attachment_url( $options['image_attachment_id'] ); ?>' height='100'>
        </div>
        <input id="upload_image_button" type="button" class="button" value="<?php _e( 'Kies afbeelding' ); ?>" />
        
        <input<?php echo (($options['image_attachment_id'] != "") ? '' : ' style="display: none;"'); ?> class="button btn-error" style="border-color: red; color: red;" type="button" name="reset" value="Verwijder afbeelding" />

        <input type='hidden' name='lkp_plugin_options[image_attachment_id]' id='image_attachment_id' value='<?php echo $options['image_attachment_id']; ?>'>
        
    </form><?php

}

add_action( 'admin_footer', 'media_selector_print_scripts' );

function media_selector_print_scripts() {

    $my_saved_attachment_post_id = get_option( 'media_selector_attachment_id', 0 );

    ?><script type='text/javascript'>

        jQuery( document ).ready( function( $ ) {

            jQuery("input[name=reset]").on('click', function(){
                $( '#image-preview' ).attr( 'src', '' ).css( 'width', 'auto' );
                $( '#image_attachment_id' ).val( '' );

            });

            // Uploading files
            var file_frame;
            var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
            var set_to_post_id = <?php echo $my_saved_attachment_post_id; ?>; // Set this

            jQuery('#upload_image_button').on('click', function( event ){

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if ( file_frame ) {
                    // Set the post ID to what we want
                    file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
                    // Open frame
                    file_frame.open();
                    return;
                } else {
                    // Set the wp.media post id so the uploader grabs the ID we want when initialised
                    wp.media.model.settings.post.id = set_to_post_id;
                }

                // Create the media frame.
                file_frame = wp.media.frames.file_frame = wp.media({
                    title: 'Select a image to upload',
                    button: {
                        text: 'Use this image',
                    },
                    multiple: false // Set to true to allow multiple files to be selected
                });

                // When an image is selected, run a callback.
                file_frame.on( 'select', function() {
                    // We set multiple to false so only get one image from the uploader
                    attachment = file_frame.state().get('selection').first().toJSON();

                    // Do something with attachment.id and/or attachment.url here
                    $( '#image-preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
                    $( '#image_attachment_id' ).val( attachment.id );

                    // Restore the main post ID
                    wp.media.model.settings.post.id = wp_media_post_id;
                });

                    // Finally, open the modal
                    file_frame.open();
            });

            // Restore the main ID when the add media button is pressed
            jQuery( 'a.add_media' ).on( 'click', function() {
                wp.media.model.settings.post.id = wp_media_post_id;
            });
        });

    </script><?php

}



function loadAllSorting($selected = null)
{
    $out = '';
    $opts = array(
        'menu_order' => 'Standaard sortering', 
        'popularity' => 'Sorteer op populariteit', 
        'date' => 'Sorteren op nieuwste',
        'price' => 'Sorteer op prijs: laag naar hoog',
        'price-desc' => 'Sorteer op prijs: hoog naar laag');
    foreach($opts as $key => $val)
    {
        $out .= '<option value="'.$key.'" '.(($selected && $selected == $key)? 'selected="selected"' : '').'>'.$val.'</option>';
    }
    return $out;
}

function loadAllProducts($selected = null)
{
    $out = '<option value="" '.(($selected == null)? 'selected="selected"' : '').'>Geen</option>';
    $posts = get_posts(array(
        'numberposts'   => -1,
        'post_type'     => 'product',
        'order_by' => 'ID'
    ));
    foreach($posts as $product)
    {
        $out .= '<option value="'.$product->ID.'" '.(($selected && $selected == $product->ID)? 'selected="selected"' : '').'>'.$product->post_title.'</option>';
    }
    return $out;
}


function loadAllPages($selected = null)
{
    $out = '<option value="" '.(($selected == null)? 'selected="selected"' : '').'>Geen</option>';
    $posts = get_pages(array(
        'numberposts'   => -1,
        'order_by' => 'ID'
    ));
    foreach($posts as $product)
    {
        $out .= '<option value="'.$product->ID.'" '.(($selected && $selected == $product->ID)? 'selected="selected"' : '').'>'.$product->post_title.'</option>';
    }
    return $out;
}

function modify_shop_product_image ( $img, $product, $size, $attr, $placeholder ) {
    $alt_tag = 'alt=';
    $pos = stripos( $img, 'alt=' ) + strlen( $alt_tag ) + 1;
    return substr_replace($img, $product->get_name(), $pos, 0);
}

add_action( 'woocommerce_product_get_image', 'modify_shop_product_image', 10, 5 );

// function dbi_plugin_setting_results_limit() {
//     $options = get_option( 'dbi_example_plugin_options' );
//     echo "<input id='dbi_plugin_setting_results_limit' name='dbi_example_plugin_options[results_limit]' type='text' value='{esc_attr( $options['results_limit'] )}' />";
// }

// function dbi_plugin_setting_start_date() {
//     $options = get_option( 'dbi_example_plugin_options' );
//     echo "<input id='dbi_plugin_setting_start_date' name='dbi_example_plugin_options[start_date]' type='text' value='{esc_attr( $options['start_date'] )}' />";
// }

// function importCars()
// {
//     global $wpdb;

//     require_once( __DIR__ . '/funnel.class.php');
//     $funnel = new Funnel;

//     $cars = array(
//         // 'Universeel, verschillende elektrische auto\'s',
//         // 'Overig, mijn auto staat er niet tussen',
//         'Audi A3 E-Tron',
//         'Audi Q4 e-tron',
//         'Audi Q7 e-tron',
//         'Audi e-Tron',
//         'BMW 330e',
//         'BMW 225xe',
//         'BMW 530e',
//         'BMW 740e',
//         'BMW i3',
//         'BMW iX3',
//         'BMW i8',
//         'BMW X5 PHEV xDrive40',
//         'Honda e',
//         'Hyundai IONIQ',
//         'Hyundai Kona',
//         'Hyundai Kona (modeljaar 2020)',
//         'Jaguar I-Pace',
//         'Kia e-Niro',
//         'Kia e-Niro (modeljaar 2020)',
//         'Kia e-Soul',
//         'Lightyear One',
//         'Mercedes EQA',
//         'Mercedes EQC',
//         'Mercedes C 350e Estate',
//         'Mercedes E 350e Plug-In',
//         'Mercedes S500 Plug-In',
//         'Mercedes GLE 500e Plug-In',
//         'Mercedes Vito E-Cell',
//         'MG ZS EV',
//         'Mini Electric',
//         'Mitsubishi Outlander PHEV',
//         'Nissan Leaf',
//         'Nissan Leaf e+',
//         'Opel Ampera-e',
//         'Opel Corsa-e',
//         'Peugeot e-208',
//         'Peugeot e-2008 SUV',
//         'Polestar 2',
//         'Porsche Panamera 4 E-Hybrid',
//         'Porsche Taycan',
//         'Renault DS 3 Crossback E-Tense',
//         'Renault Zoe',
//         'SEAT el-Born',
//         'SEAT Mii Electric',
//         'Sono Sion',
//         'Skoda CITIGOe',
//         'Skoda Vision IV',
//         'Smart EQ forfour',
//         'Smart EQ fortwo',
//         'Tesla Model S',
//         'Tesla Model 3',
//         'Tesla Model X',
//         'Volkswagen E-Golf',
//         'Volkswagen e-Up',
//         'Volkswagen Golf GTE',
//         'Volkswagen ID.3',
//         'Volkswagen Passat GTE',
//         'Volvo XC40 Electric',
//         'Volvo XC60 T8 Twin-Engine',
//         'Volvo V60 Plug-In Hybride',
//         'Volvo XC90 PHEV T8 Twin Engine'
//     );

//     foreach($cars as $car)
//     {
//         $exp = explode(" ", $car);
//         if($exp[0])
//         {
//             $brand = $exp[0];
//             unset($exp[0]);
//             $type = '';
//             foreach($exp as $str)
//             {
//                 $type .= $str.' ';
//             }
//             $type = trim($type);

//             $brand_id = null;

//             //check if exists
//             $rowcount = (int)$wpdb->get_var("SELECT COUNT(*) FROM lkp_car_brands WHERE brand_name = '".$brand."'");
            
//             if($rowcount == 0)
//             {
//                 $brand_id = $funnel->insertCarValue('lkp_car_brands', array(
//                     'brand_name' => $brand
//                 ));
//             }
//             else {
//                 $brand_id = (int)$wpdb->get_var("SELECT id FROM lkp_car_brands WHERE brand_name = '".$brand."'");
//             }


//             if($brand_id)
//             {
//                 $funnel->insertCarValue('lkp_car_types', array(
//                     'car_brand_id' => $brand_id,
//                     'type_name' => $type
//                 ));
//             }
//         }
//     }

    
// }
// importCars();

/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));
    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('html5blankscripts'); // Enqueue it!
    }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}
*/

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}


//product category wysiwyg field

add_action( 'init', 'wpm_product_cat_register_meta' );
/**
 * Register details product_cat meta.
 *
 * Register the details metabox for WooCommerce product categories.
 *
 */
function wpm_product_cat_register_meta() {

    register_meta( 'term', 'prod_cat_details', 'wpm_sanitize_details' );

}

/**
 * Sanitize the details custom meta field.
 *
 * @param  string $details The existing details field.
 * @return string          The sanitized details field
 */
function wpm_sanitize_details( $details ) {

    return wp_kses_post( $details );

}

add_action( 'product_cat_add_form_fields', 'wpm_product_cat_add_details_meta' );
/**
 * Add a details metabox to the Add New Product Category page.
 *
 * For adding a details metabox to the WordPress admin when
 * creating new product categories in WooCommerce.
 *
 */
function wpm_product_cat_add_details_meta() {

    wp_nonce_field( basename( __FILE__ ), 'wpm_product_cat_details_nonce' );

    ?>
    <div class="form-field">
        <label for="wpm-product-cat-details"><?php esc_html_e( 'Categorie tekst onder producten', 'wpm' ); ?></label>
        <textarea name="wpm-product-cat-details" id="wpm-product-cat-details" rows="5" cols="40"></textarea>
        <p class="description"><?php esc_html_e( 'Detailed category info to appear below the product list', 'wpm' ); ?></p>
    </div>
    <?php

}

add_action( 'product_cat_edit_form_fields', 'wpm_product_cat_edit_details_meta' );
/**
 * Add a details metabox to the Edit Product Category page.
 *
 * For adding a details metabox to the WordPress admin when
 * editing an existing product category in WooCommerce.
 *
 * @param  object $term The existing term object.
 */
function wpm_product_cat_edit_details_meta( $term ) {

    $product_cat_details = get_term_meta( $term->term_id, 'details', true );

    if ( ! $product_cat_details ) {
        $product_cat_details = '';
    }

    $settings = array( 'textarea_name' => 'wpm-product-cat-details' );
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="wpm-product-cat-details"><?php esc_html_e( 'Categorie tekst onder producten', 'wpm' ); ?></label></th>
        <td>
            <?php wp_nonce_field( basename( __FILE__ ), 'wpm_product_cat_details_nonce' ); ?>
            <?php wp_editor( wpm_sanitize_details( $product_cat_details ), 'product_cat_details', $settings ); ?>
            <p class="description"><?php esc_html_e( 'Detailed category info to appear below the product list','wpm' ); ?></p>
        </td>
    </tr>
    <?php

}

add_action( 'create_product_cat', 'wpm_product_cat_details_meta_save' );
add_action( 'edit_product_cat', 'wpm_product_cat_details_meta_save' );
/**
 * Save Product Category details meta.
 *
 * Save the product_cat details meta POSTed from the
 * edit product_cat page or the add product_cat page.
 *
 * @param  int $term_id The term ID of the term to update.
 */
function wpm_product_cat_details_meta_save( $term_id ) {

    if ( ! isset( $_POST['wpm_product_cat_details_nonce'] ) || ! wp_verify_nonce( $_POST['wpm_product_cat_details_nonce'], basename( __FILE__ ) ) ) {
        return;
    }

    $old_details = get_term_meta( $term_id, 'details', true );
    $new_details = isset( $_POST['wpm-product-cat-details'] ) ? $_POST['wpm-product-cat-details'] : '';

    if ( $old_details && '' === $new_details ) {
        delete_term_meta( $term_id, 'details' );
    } else if ( $old_details !== $new_details ) {
        update_term_meta(
            $term_id,
            'details',
            wpm_sanitize_details( $new_details )
        );
    }
}

function wpm_product_cat_display_details_meta() {

    if ( ! is_tax( 'product_cat' ) ) {
        return;
    }

    $t_id = get_queried_object()->term_id;
    $details = get_term_meta( $t_id, 'details', true );

    if ( '' !== $details ) {
        ?>
        <div class="product-cat-details">
            <?php echo apply_filters( 'the_content', wp_kses_post( $details ) ); ?>
        </div>
        <?php
    }

}

/* zonnepanelen!! */
// -----------------------------------------
// 1. Add custom field input @ Product Data > Variations > Single Variation
 
add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations', 10, 3 );
 
function bbloomer_add_custom_field_to_variations( $loop, $variation_data, $variation ) {
   woocommerce_wp_text_input( array(
'id' => 'custom_field[' . $loop . ']',
'class' => 'short',
'label' => __( 'Aantal panelen', 'woocommerce' ),
'value' => get_post_meta( $variation->ID, 'custom_field', true )
   ) );
}
 
// -----------------------------------------
// 2. Save custom field on product variation save
 
add_action( 'woocommerce_save_product_variation', 'bbloomer_save_custom_field_variations', 10, 2 );
 
function bbloomer_save_custom_field_variations( $variation_id, $i ) {
   $custom_field = $_POST['custom_field'][$i];
   if ( isset( $custom_field ) ) update_post_meta( $variation_id, 'custom_field', esc_attr( $custom_field ) );
}
 
// -----------------------------------------
// 3. Store custom field value into variation data
 
add_filter( 'woocommerce_available_variation', 'bbloomer_add_custom_field_variation_data' );
 
function bbloomer_add_custom_field_variation_data( $variations ) {
   $variations['custom_field'] = get_post_meta( $variations[ 'variation_id' ], 'custom_field', true );//'<div class="woocommerce_custom_field">Aantal panelen: <span>' .  . '</span></div>';
   return $variations;
}

//Field 2

add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations2', 10, 3 );
 
function bbloomer_add_custom_field_to_variations2( $loop, $variation_data, $variation ) {
   woocommerce_wp_text_input( array(
'id' => 'stroomproductie[' . $loop . ']',
'class' => 'short',
'label' => __( 'Stroomproductie', 'woocommerce' ),
'value' => get_post_meta( $variation->ID, 'stroomproductie', true )
   ) );
}
 
// -----------------------------------------
// 2. Save custom field on product variation save
 
add_action( 'woocommerce_save_product_variation', 'bbloomer_save_custom_field_variations2', 10, 2 );
 
function bbloomer_save_custom_field_variations2( $variation_id, $i ) {
   $custom_field = $_POST['stroomproductie'][$i];
   if ( isset( $custom_field ) ) update_post_meta( $variation_id, 'stroomproductie', esc_attr( $custom_field ) );
}
 
// -----------------------------------------
// 3. Store custom field value into variation data
 
add_filter( 'woocommerce_available_variation', 'bbloomer_add_custom_field_variation_data2' );
 
function bbloomer_add_custom_field_variation_data2( $variations ) {
   $variations['stroomproductie'] = get_post_meta( $variations[ 'variation_id' ], 'stroomproductie', true );//'<div class="woocommerce_custom_field">Aantal panelen: <span>' .  . '</span></div>';
   return $variations;
}

//Field 3

add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations3', 10, 3 );
 
function bbloomer_add_custom_field_to_variations3( $loop, $variation_data, $variation ) {
   woocommerce_wp_text_input( array(
'id' => 'vermogen_paneel[' . $loop . ']',
'class' => 'short',
'label' => __( 'Vermogen p. paneel', 'woocommerce' ),
'value' => get_post_meta( $variation->ID, 'vermogen_paneel', true )
   ) );
}
 
// -----------------------------------------
// 2. Save custom field on product variation save
 
add_action( 'woocommerce_save_product_variation', 'bbloomer_save_custom_field_variations3', 10, 2 );
 
function bbloomer_save_custom_field_variations3( $variation_id, $i ) {
   $custom_field = $_POST['vermogen_paneel'][$i];
   if ( isset( $custom_field ) ) update_post_meta( $variation_id, 'vermogen_paneel', esc_attr( $custom_field ) );
}
 
// -----------------------------------------
// 3. Store custom field value into variation data
 
add_filter( 'woocommerce_available_variation', 'bbloomer_add_custom_field_variation_data3' );
 
function bbloomer_add_custom_field_variation_data3( $variations ) {
   $variations['vermogen_paneel'] = get_post_meta( $variations[ 'variation_id' ], 'vermogen_paneel', true );//'<div class="woocommerce_custom_field">Aantal panelen: <span>' .  . '</span></div>';
   return $variations;
}


//Field 4

add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations4', 10, 3 );
 
function bbloomer_add_custom_field_to_variations4( $loop, $variation_data, $variation ) {
   woocommerce_wp_text_input( array(
'id' => 'opbrengst_jaar[' . $loop . ']',
'class' => 'short',
'label' => __( 'Opbrengst p. jaar', 'woocommerce' ),
'value' => get_post_meta( $variation->ID, 'opbrengst_jaar', true )
   ) );
}
 
// -----------------------------------------
// 2. Save custom field on product variation save
 
add_action( 'woocommerce_save_product_variation', 'bbloomer_save_custom_field_variations4', 10, 2 );
 
function bbloomer_save_custom_field_variations4( $variation_id, $i ) {
   $custom_field = $_POST['opbrengst_jaar'][$i];
   if ( isset( $custom_field ) ) update_post_meta( $variation_id, 'opbrengst_jaar', esc_attr( $custom_field ) );
}
 
// -----------------------------------------
// 3. Store custom field value into variation data
 
add_filter( 'woocommerce_available_variation', 'bbloomer_add_custom_field_variation_data4' );
 
function bbloomer_add_custom_field_variation_data4( $variations ) {
   $variations['opbrengst_jaar'] = get_post_meta( $variations[ 'variation_id' ], 'opbrengst_jaar', true );//'<div class="woocommerce_custom_field">Aantal panelen: <span>' .  . '</span></div>';
   return $variations;
}

//Field 5

add_action( 'woocommerce_variation_options_pricing', 'bbloomer_add_custom_field_to_variations5', 10, 3 );
 
function bbloomer_add_custom_field_to_variations5( $loop, $variation_data, $variation ) {
   woocommerce_wp_text_input( array(
'id' => 'garantie[' . $loop . ']',
'class' => 'short',
'label' => __( 'Garantie op panelen', 'woocommerce' ),
'value' => get_post_meta( $variation->ID, 'garantie', true )
   ) );
}
 
// -----------------------------------------
// 2. Save custom field on product variation save
 
add_action( 'woocommerce_save_product_variation', 'bbloomer_save_custom_field_variations5', 10, 2 );
 
function bbloomer_save_custom_field_variations5( $variation_id, $i ) {
   $custom_field = $_POST['garantie'][$i];
   if ( isset( $custom_field ) ) update_post_meta( $variation_id, 'garantie', esc_attr( $custom_field ) );
}
 
// -----------------------------------------
// 3. Store custom field value into variation data
 
add_filter( 'woocommerce_available_variation', 'bbloomer_add_custom_field_variation_data5' );
 
function bbloomer_add_custom_field_variation_data5( $variations ) {
   $variations['garantie'] = get_post_meta( $variations[ 'variation_id' ], 'garantie', true );//'<div class="woocommerce_custom_field">Aantal panelen: <span>' .  . '</span></div>';
   return $variations;
}


/*
// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}
/*
// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }
*/
/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
            'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

?>
*/