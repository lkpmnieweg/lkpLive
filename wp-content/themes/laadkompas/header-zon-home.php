<?php

@session_start();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Muli:400,700,800,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Google Tag Manager -->
	<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M9P25B6');
	</script>
	<!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-98392756-1"></script>
	<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-98392756-1');
	</script>

	<!-- Hotjar Tracking Code for www.laadkompas.nl -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1358613,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
</head>
<body <?php body_class(); ?>>
<div class="woocommerce-notices-wrapper vergelijking">
	<div class="woocommerce-message" role="alert">
		<span></span>
		<a class="vergelijk-link" href="<?php the_permalink(223); ?>">Vergelijk <span class="count">2</span> laadpalen</a>
	</div>
</div>
	<header>
		<nav>
		<div id="menu_overlay"></div>
			<div class="container">
				<a class="menu_btn" href="javascript:toggleMenu();" title="Menu"><img src="<?php echo get_template_directory_uri(); ?>/images/hamburger-menu.svg" alt="Menu" /></a>
				<a class="logo" href="<?php echo get_the_permalink(78); ?>">laadkompas<span>|</span><span>Experts in opladen</span></a>
				<div class="float-right">
					<ul class="top-menu">
						<li class="first">
							<?php /* <a class="btn" href="<?php echo get_site_url(); ?>">Offerte aanvragen</a> */ ?>
						</li>
						<li class="last" style="margin-right: 20px;">
							<a href="tel:<?php echo str_replace(" ", "", get_option('woocommerce_store_phone_zon'));?>" title="Bel ons">
								<img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon-white.svg" alt="Bel ons" /> 
								<?php echo get_option('woocommerce_store_phone_zon');?>
							</a>
						</li>
						
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="header_menu">
				<div class="hidden mobile-title">
					Menu
					<a class="closeMenu"><i class="fa fa-times"></i></a>
					<div class="clearfix"></div>
				</div>
				<div class="container">
					<?php
					wp_nav_menu(
					  array(
					    'theme_location' => 'header-menu-zon'
					  )
					);
					?>
					<div class="clearfix"></div>
				</div>
			</div>
		</nav>
		<?php
		if(strpos(get_page_template(), "page-zon-homepage.php")):

		if(isset($_SESSION['lkp_current_funnel']))
		{
			$current_funnel = unserialize(decryptData($_SESSION['lkp_current_funnel']));

		}
		?>
		<div class="hero zonnepaneel">
			<div class="container">
				<h1><?php echo get_field('banner_titel'); ?></h1>
				<h2><?php echo get_field('banner_ondertitel'); ?></h2>
				<div class="action-form">
					<div class="title">Benieuwd hoeveel u kunt besparen?<br/>Wij helpen u graag.</div>
					<form action="" onsubmit="return validateFields();" method="POST">
						<label for="sun_zipcode">Uw adres</label>
						<div class="row">
							<div class="column-7">
								<div class="form-group">
									<input type="text" id="sun_zipcode" name="sun_zipcode" class="form-control" placeholder="Postcode" value="" />
								</div>
							</div>
							<div class="column-5">
									<input type="text" maxlength="10" name="sun_nr" class="form-control" placeholder="Huisnr." value="" />
							</div>
							<div class="clearfix"></div>
						</div>
						
						<input class="btn" type="submit" name="calculateSun" value="Nu berekenen">
					</form>
				</div>
			</div>
		</div>
		<div class="usp-bar <?php echo (isset($current_funnel->car_type))? 'active' : ''; ?>">
			<div class="container">
				<ul>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="100% afhankelijk" /> 100% onafhankelijk</li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="BTW teruggave" /> BTW teruggave inbegrepen</li>
					<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="snelle doorlooptijd" /> Snelle doorlooptijd</li>
				</ul>
			</div>
		</div>
		<?php
		endif;
		?>
	</header>
<?php
