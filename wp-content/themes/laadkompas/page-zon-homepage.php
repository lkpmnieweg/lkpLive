<?php
/* Template name: Laadkompas - Zonnepanelen Homepage */
get_header('zon-home');

?>

<div class="container ---Style">

	<?php
	//Text block 1
	$img1 = get_field('content_afbeelding_links_1');
	$img2 = get_field('content_afbeelding_links_2');
	$titel1 = get_field('content_titel_1');
	$txt1 = get_field('content_tekst_1');
	$url1 = get_field('content_link_1');
	$url2 = get_field('content_link_2');

	if($titel1 || $txt1 || $img1 || $img2):
	?>
	<div class="info-block">
		<?php
			if($img1 || $img2):
		?>
			<div class="images">
				<?php
					if($img1) print formatImgObjToLink($img1,$titel1);
					if($img2) print formatImgObjToLink($img2,$titel1);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<?php
				if($titel1):
			?>
				<h1><?php echo $titel1;  ?></h1>
			<?php
				endif;
			?>
			<?php  
				if($txt1) print $txt1;
				
				if($url1 || $url2):
			?> 
				<ul class="content-links">
					<?php
						if($url1) print formatLinkObjToListLink($url1);
						if($url2) print formatLinkObjToListLink($url2);
					?>
				</ul>
			<?php
				endif; 
			?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php
	endif;


	//Text block 2
	$img3 = get_field('content_afbeelding_rechts_1');
	$img4 = get_field('content_afbeelding_rechts_2');
	$titel2 = get_field('content_titel_2');
	$txt2 = get_field('content_tekst_2');
	$url3 = get_field('content_link_3');
	$url4 = get_field('content_link_4');

	if($titel2 || $txt2 || $img3 || $img4):
	?>
	<div class="info-block right first">
		<?php
			if($img3 || $img4):
		?>
			<div class="images">
				<?php
					if($img3) print formatImgObjToLink($img3, $titel2);
					if($img4) print formatImgObjToLink($img4, $titel2);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<?php
				if($titel2):
			?>
				<h1><?php echo $titel2;  ?></h1>
			<?php
				endif;
				if($txt2) print $txt2;
				
				if($url3 || $url4):
			?> 
				<ul class="content-links">
					<?php
						if($url3) print formatLinkObjToListLink($url3);
						if($url4) print formatLinkObjToListLink($url4);
					?>
				</ul>
			<?php
				endif; 
			?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php
	endif;
	?>
</div>

<?php


//zonnepanelen

//Text block 2
	$zon_img1 = get_field('zonnepanelen_afbeelding_1');
	$zon_img2 = get_field('zonnepanelen_afbeelding_2');
	$zon_titel = get_field('zonnepanelen_titel');
	$zon_tekst = get_field('zonnepanelen_tekst');
	$zon_link = get_field('zonnepanelen_link');

	if($zon_titel || $zon_tekst || $zon_img1 || $zon_img2):
?>
<div class="container ---Style noTop">
	<div class="info-block last">
		<?php
			if($zon_img1 || $zon_img2):
		?>
			<div class="images">
				<?php
					if($zon_img1) print formatImgObjToLink($zon_img1, $zon_titel);
					if($zon_img2) print formatImgObjToLink($zon_img2, $zon_titel);
				?>
			</div>
		<?php
			endif;
		?>
		
		<div class="content">
			<?php
				if($zon_titel):
			?>
				<h1><?php echo $zon_titel;  ?></h1>
			<?php
				endif;
				if($zon_tekst) print $zon_tekst;
				
				if($zon_link):
			?> 
				<ul class="content-links">
					<?php
						if($zon_link) print formatLinkObjToListLink($zon_link);
					?>
				</ul>
			<?php
				endif; 
			?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php
endif;

echo get_template_part('partials/zonReviews', 'page');
?>

<?php
//Text block 2
	$img5 = get_field('content_afbeelding_rechts_4');
	$img6 = get_field('content_afbeelding_rechts_3');
	$titel3 = get_field('content_titel_3');
	$txt3 = get_field('content_tekst_3');
	$url5 = get_field('content_link_5');
	$url6 = get_field('content_link_6');

	if($titel3 || $txt3 || $img5 || $img6):
?>
<div class="container ---Style noTop">		
	<div class="info-block right second">
		<?php
			if($img5 || $img6):
		?>
			<div class="images">
				<?php
					if($img5) print formatImgObjToLink($img5, $titel3);
					if($img6) print formatImgObjToLink($img6, $titel3);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<?php
				if($titel3):
			?>
				<h1><?php echo $titel3;  ?></h1>
			<?php
				endif;
				if($txt3) print $txt3;
				
				if($url5 || $url6):
			?> 
				<ul class="content-links">
					<?php
						if($url5) print formatLinkObjToListLink($url5);
						if($url6) print formatLinkObjToListLink($url6);
					?>
				</ul>
			<?php
				endif; 
			?>
		</div>
		
		<div class="clearfix"></div>
	</div>
</div>
<?php
endif;


echo get_template_part('partials/companyUsps', 'page');
?>

<?php
//Text block 2
	$img7 = get_field('content_afbeelding_links_3');
	$img8 = get_field('content_afbeelding_links_4');
	$titel4 = get_field('content_titel_4');
	$txt4 = get_field('content_tekst_4');
	$url7 = get_field('content_link_7');
	$url8 = get_field('content_link_8');

	if($titel4 || $txt4 || $img7 || $img8):
?>
<div class="container ---Style last">
	<div class="info-block last">
		<?php
			if($img7 || $img8):
		?>
			<div class="images">
				<?php
					if($img7) print formatImgObjToLink($img7, $titel4);
					if($img8) print formatImgObjToLink($img8, $titel4);
				?>
			</div>
		<?php
			endif;
		?>
		
		<div class="content">
			<?php
				if($titel4):
			?>
				<h1><?php echo $titel4;  ?></h1>
			<?php
				endif;
				if($txt4) print $txt4;
				
				if($url7 || $url8):
			?> 
				<ul class="content-links">
					<?php
						if($url7) print formatLinkObjToListLink($url7);
						if($url8) print formatLinkObjToListLink($url8);
					?>
				</ul>
			<?php
				endif; 
			?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php
endif;
?>

<section>
	<div class="container">
		<?php
		$contact_title = get_field('contactformulier_titel');
		if($contact_title) print '<h1>'.$contact_title.'</h1>';
		?>
		<div class="row">
			<div class="column-5">
				<?php
				$contact_txt = get_field('contactformulier_tekst');
				if($contact_txt) print $contact_txt;
				?>
				<a class="call-link" href="tel:<?php echo str_replace(" ", "", get_option('woocommerce_store_phone'));?>" title="Bel ons">
					<img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon-dark.svg" alt="Bel ons" /> 
					<?php echo get_option('woocommerce_store_phone');?>
				</a>
			</div>
			<div class="column-7">
				<?php
				echo do_shortcode('[contact-form-7 id="717" title="Laadkompas - Homepage formulier"]');
				?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<noscript>
	<style type="text/css">
		/*header .hero .action-form {
			height: 346px !important;
		}

		header .hero .action-form .form-group.car_type {
			display: block !important;
		}*/
	</style>
</noscript>
<?php
get_footer('zon');
?>