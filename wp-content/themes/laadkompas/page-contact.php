<?php
/* Template name: Laadkompas - Contact */

get_header();
?>
<div class="container">
	<div class="title">
		<h1>Neem contact op</h1>
		<h2>Heeft u nog vragen, neem dan gerust contact op.<br/> Wij helpen u graag verder.</h2>
	</div>
	<div class="boxes">
		<div class="title">Contactgegevens</div>
		<div class="row">
			<div class="column-3">
				<span class="icon">
					<img src="<?php echo get_template_directory_uri(); ?>/images/contact_address.svg" alt="Adres" />
				</span>
				<span class="text">
					Microfoonstraat 5<br/>1322 BN Almere
				</span>
			</div>
			<div class="column-3">
				<span class="icon">
					<img src="<?php echo get_template_directory_uri(); ?>/images/contact_phone.svg" alt="Telefoonnummer" />
				</span>
				<span class="text">
					<a href="tel:<?php echo str_replace(" ", "", get_option('woocommerce_store_phone'));?>" title="Bel ons">
						<?php echo get_option('woocommerce_store_phone');?>
					</a><br/>
					Maandag t/m Vrijdag<br/>
					09:00 - 17:30 
				</span>
			</div>
			<div class="column-3">
				<span class="icon">
					<img src="<?php echo get_template_directory_uri(); ?>/images/contact_mail.svg" alt="Emailadres" />
				</span>
				<span class="text">
					<a href="mailto:info@laadkompas.nl">info@laadkompas.nl</a>
				</span>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<?php

	echo do_shortcode('[contact-form-7 id="716" title="Laadkompas - Contactpagina"]');
	//
	?>
	<div class="map-wrapper">
		<section id="map_block" style="position: relative; overflow: hidden;">
			<div class="marker" data-lat="52.3472391" data-lng="5.1798848"></div>
		</section>
	</div>
</div>
<?php
get_footer();
?>