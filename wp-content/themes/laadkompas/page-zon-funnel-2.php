<?php
/* Template name: Laadkompas - Zonnepaneel Funnel stap 2 */
session_start();


if(isset($_SESSION['lkp_current_funnel_zon']))
{
	$current_funnel = $zonfunnel->decryptSession();


	$address = getAddressByZipHouseNr($current_funnel->zipcode, $current_funnel->housenr);
}
else wp_redirect(get_site_url());
/* https://go.pardot.com/l/851933/2020-07-15/21blj */
get_header('zon-funnel');
?>
<div class="usp-bar active">
	<div class="container">
		<ul>
			<li><img src="https://www.laadkompas.nl/wp-content/themes/laadkompas/images/icon-checkmark.svg" alt="<?php echo get_field('usp_1_tekst'); ?>"> <?php echo get_field('usp_1_tekst'); ?></li>
			<li><img src="https://www.laadkompas.nl/wp-content/themes/laadkompas/images/icon-checkmark.svg" alt="<?php echo get_field('usp_2_tekst'); ?>"> <?php echo get_field('usp_2_tekst'); ?></li>
			<li><img src="https://www.laadkompas.nl/wp-content/themes/laadkompas/images/icon-checkmark.svg" alt="<?php echo get_field('usp_3_tekst'); ?>"> <?php echo get_field('usp_3_tekst'); ?></li>
		</ul>
	</div>
</div>
<form id="funnel2" autocomplete="off" action="" method="POST" autocomplete="off">
	<input type="hidden" autocomplete="false">
	<div class="container">
	<section class="---Funnel-form">
		<div class="right">
			<div class="reviews">
				<div class="left-rating">
					<h1>Onze klanten <br>geven ons een</h1>	
					<div class="reviewStars">
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        			</div>
				</div>
				<div class="rating">
					<img src="<?php echo get_template_directory_uri(); ?>/images/kiyoh-9-2-logo@2x.png" alt="Kiyoh" />
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="left">
			
		</div>
		<div class="clearfix"></div>
	</section>
	<section class="---Funnel-form">
		<div class="right">
			<div class="result-box">
				<h1>Uw situatie</h1>
				<div class="form-group">
					<label>Adres</label>
					<?php echo $address; ?>
					<input type="hidden" name="full_address" value="<?php echo sanitize_text_field($address); ?>" />
				</div>
				<div class="form-group">
					<label>Stroomverbruik</label>
					<?php echo ucfirst(str_replace("pers", "pers.", $current_funnel->consumption)); ?>
				</div>
				<div class="form-group">
					<label>Laadpaal</label>
					<?php echo ucfirst($current_funnel->gotchp); ?>
				</div>
				<div class="form-group">
					<label>Voorkeur</label>
					<?php echo ucfirst($current_funnel->preference); ?>
				</div>
			</div>
		</div>
        
		<div class="left">
			<h2>Alleen nog uw gegevens</h2><p>U ontvangt <strong> binnen 10 minuten</strong> advies per mail.</p>
			<div class="row large-row">
				<!-- <div class="column-2">
					<div class="form-group aanhef">
						<select name="name_title" data-validation="required" data-validation-error-msg="Kiest u een aanhef." class="form-control">
							<option value="" selected="selected">Aanhef</option>
							<option value="dhr.">Dhr.</option>
							<option value="mevr.">Mevr.</option>
						</select>
					</div>
				</div> -->
				<div class="column-2">
					<div class="form-group">
						<input class="form-control" autocomplete="off" data-validation="length" data-validation-length="3-100" type="text" name="firstname" value="" placeholder="Voornaam*" data-validation-error-msg="Voer uw voornaam in." />
					</div>
				</div>
				<div class="column-2">
					<div class="form-group">
						<input class="form-control" autocomplete="off" data-validation="length" data-validation-length="3-100" type="text" name="lastname" value="" placeholder="Achternaam*" data-validation-error-msg="Voer uw achternaam in." />
					</div>
				</div>
			</div>
			<div class="row large-row">
				<div class="column-2">
					<div class="form-group">
						<input class="form-control" autocomplete="off" type="tel" name="phone" data-validation="phone_number"  data-validation-error-msg="Voer een geldig telefoonnummer in." value="" placeholder="Telefoonnummer*" />
					</div>
				</div>
				<div class="column-2">
					<div class="form-group">
						<input class="form-control" autocomplete="off" type="text" name="emailadres" data-validation="email" data-validation-error-msg="Voer een geldig e-mailadres in." value="" placeholder="E-mailadres*" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="column-12">
					<div class="form-group">
						<textarea rows="5" name="comments" placeholder="Opmerkingen (optioneel)" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="column-12">
					<div class="form-group label-secure">
						* Deze pagina en uw contactgegevens zijn veilig.
					</div>
				</div>
			</div>
			<!-- <div class="row">
				<div class="column-12">
					<div class="form-group">
						<label>
							<input type="checkbox" name="optin_newsletter" value="yes">
							<span>Ja, ik wil op de hoogte blijven van nieuws in de EV markt.</span>
						</label>
						<div class="description">
							De EV markt verandert continue, met onze maandelijkse nieuwsbrief blijft u op de hoogte.
						</div>
					</div>	
					<div class="form-group last_agreement">
						<label>
							<input type="checkbox" name="optin_commercial" value="yes">
							<span>Ja, ik wil informatie ontvangen omtrent aanbiedingen, kortingen en winacties.</span>
						</label>
					</div>	
					<<p>* Uw gegevens worden alleen gebruikt om u van een persoonlijk laadadvies te voorzien. Wij houden daarnaast </p>
				</div>
			</div> -->
		<section class="---Funnel-buttons">
			<div class="container">
				<div class="button-right">
					<input class="btn" type="submit" name="giveAdviceSolarPanels" value="Persoonlijk advies" />
				</div>
				<div class="button-left">
					<a href="<?php echo get_the_permalink(14204); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="Vorige stap" />Vorige stap</a>
				</div>
				<div class="clearfix"></div>
			</div>
		</section>
		</div>
		<div class="clearfix"></div>
		<div class="right">
			<div class="reviewBox">
				<?php
				echo get_template_part('partials/zonFunnelReviews', 'page');
				?>
			</div>
		</div>
		<div class="left second">
			<div class="previous_orders">
			<?php
			$leadCount = "11.000";
			global $wpdb;
			$result = $wpdb->get_results( "SELECT id,counter FROM lkp_lead_counter WHERE id = 2", OBJECT );
			if($result && $result[0]->counter != null) {
				$leadCount = number_format($result[0]->counter, 0, ',', '.');
			}
			?>
				<h1>Al <span class="request_count"><?php echo $leadCount; ?></span> mensen gingen u voor</h1>
				<p>Bekijk verschillende resultaten van de door <strong>ons geadviseerde en ge&iuml;nstalleerde zonnepanelen.</strong></p>
				<div class="arrow">
					<img src="<?php echo get_template_directory_uri().'/images/arrow-green-funnel.png'; ?>" alt="" />
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
	</div>
</form>

<script type="text/javascript">
	var templateUrl = "<?php echo get_template_directory_uri(); ?>";
</script>
<?php
/*

<!-- <div class="form-group">
<div class="agree_row">
	<input name="product" type="checkbox" id="akkoord_evmarkt" name="akkoord_evmarkt" value="yes" />
	<label class="custom-label-wrap" for="akkoord_evmarkt">
		<span class="custom-check newsletter"></span> <span class="custom-label">Ja, ik wil op de hoogte blijven van nieuws in de EV markt.</span>
	</label>
</div>
<div class="description">
	De EV markt verandert continue, met onze maandelijkse nieuwsbrief blijft u op de hoogte. (We sturen u geen aanbiedingen, alleen relevante artikelen of nieuws) 
</div>
</div>
<div class="form-group">
<div class="description last">
	Ik geef Laadkompas toestemming om contact met me op te nemen m.b.t. mijn aanvraag.
	<a href="" title="Laadkompas Privacy Policy">Laadkompas Privacy Policy</a>
</div>
</div> -->
*/
get_footer('funnel');
?>