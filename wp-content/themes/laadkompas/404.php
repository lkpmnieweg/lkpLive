<?php 

wp_redirect(site_url()); exit;

get_header(); ?>

<div class="container">
	<h1>Pagina niet gevonden.</h1>
	<p>De pagina die u probeert te zoeken, kan niet worden gevonden.</p>
	<a class="back-link-404" href="<?php echo get_the_permalink(78); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="" /> <span>Klik hier om naar de homepage te gaan</span></a>
</div>

<?php get_footer(); ?>


<?php

/*

	<main role="main">
		<!-- section -->
		<section>

			<!-- article -->
			<article id="post-404">

				<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
				<h2>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
				</h2>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

*/
?>