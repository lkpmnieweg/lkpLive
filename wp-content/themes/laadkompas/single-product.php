<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();

global $product;
global $post;

$product_gallery = $product->get_gallery_image_ids();
$plusmin = get_field( 'plus_minpunten' );


$isSimpleProduct = false;
$isSimpleProduct = $product instanceof WC_Product_Simple;

while ( have_posts() ) :
	the_post();

?>
<div class="container breadcrumb">
	<div id="breadcrumb">
		<?php
		$category_url = null;
		if(isset($product->category_ids[0]))
		{
			$cat = get_term((int)$product->category_ids[0]);
		}
		?>
		<ul>
			<li><a href="/">Home</a></li>
			<li><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="" /></li>
			<?php if($cat) echo '<li><a href="'.get_site_url().'/'.$cat->slug.'/">'.$cat->name.'</a></li>'; ?>
			<div class="clearfix"></div>
		</ul>
	</div>
		<div class="switch-box">
			<label class="switch">
				<input type="checkbox" data-identifier="product">
				<span class="slider round"></span>
			</label>
			<p>Toon prijzen inclusief BTW</p>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
</div>
<div class="container product-detail">
	<div class="row">
			<?php
			$extra_css = '';
			if(!$isSimpleProduct) {
				$gallery = productGallery($product_gallery,$product->get_ID());
			} else {
				$gallery = simpleProductGallery($product_gallery);	
				$extra_css = 'simple_slider';
			}
			if($gallery):
			?>
			<div class="column-2-product">
				<div class="picture">
					<div class="product-slider <?php echo $extra_css; ?>">
						<ul id="lightSlider">
						<?php
							echo $gallery;
						?>
						</ul>
					</div>
					<?php
					// cashback_banner_text
					if(get_field('cashback_banner_text') !== "" && get_field('cashback_banner_text') !== null):
					?>
						<div class="ribbon ribbon-top-right"><span><?php echo get_field('cashback_banner_text'); ?></span></div>
					<?php
					endif;
					?>
					<?php
					if(count($product_gallery) > 1):
					?>
					<div class="thumb_arrows">
						<a class="prev" href="javascript:slider.goToPrevSlide();"><img src="<?php echo get_template_directory_uri(); ?>/images/green.png"></a>
						<a class="next" href="javascript:slider.goToNextSlide();"><img class="mirror" src="<?php echo get_template_directory_uri(); ?>/images/green.png"></a>
					</div>
					<?php
						endif;
					?>
				</div>
			</div>
			<?php
				endif;
		?>
		<div class="column-5 column-offset-half-product">
			
			<div class="product_details">
				<?php
					echo get_template_part('partials/productDetails', 'page');
				?>
			</div>
			<div class="mobile">
				<div class="scrollContainer">
					<div class="top">
						<div class="label"><span>Option</span><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/question-white.svg" alt="" /></a></div>
						<a class="closeFilters" href="javascript:hideFilterMenu();"><i class="fa fa-times"></i></a>
					</div>
					<div id="product_options"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row row-reversed">
		
		<div class="column-5 column-offset-half">
			<ul class="usps">
				<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="" /> <strong>98%</strong> binnen 1 week geleverd</li>
				<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="" /> Al <strong>5.000+</strong> laadpalen ge&iuml;nstalleerd</li>
				<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="" /> Klanten geven Laadkompas een <strong>9,2</strong></li>
				<li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="" /> <strong>2 jaar</strong> garantie op uw laadpaal</li>
			</ul>
		</div>
		<div id="anchor_pi" class="column-2">
			<h2>Productinformatie</h2>
			<p>
			<?php
				echo nl2br(get_the_content());
			?>
			</p>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="row row-page_links">
		<?php
		if(get_field("plus-_en_minpunten_tonen") == "ja") 
			$showPlus = true;
		else 
			$showPlus = false;	

		if($showPlus || get_field("plus-_en_minpunten_tonen") == null):
		?>
		<div class="column-2">
			<h2>Plus- en minpunten</h2>
			<?php
			echo productPlusMin($plusmin);
			?>
		</div>
		<?php
		endif;
		
		if(get_field("direct_door_naar_tonen") == "ja") 
			$showDirect = true;
		else 
			$showDirect = false;	

		if($showDirect || get_field("direct_door_naar_tonen") == null):
		?>
		<div class="column-5 column-offset-half">
			<div class="page_links">
				<span>Direct door naar</span>
				<a href="#anchor_pi">Product informatie</a>
				<a href="#anchor_speed">Snelheid</a>
				<a href="#anchor_secure">Veiligheid</a>
				<a href="#anchor_about">Over fabrikant</a>
				<a href="#anchor_smart">Slim laden</a>
				<a href="#anchor_install">Installatie</a>
				<a href="#anchor_ts">Technische specificaties</a>
				<a href="#reviews">Reviews</a>
			</div>
		</div>
		<?php
		endif;
		?>
		<div class="clearfix"></div>
	</div>
</div>
<?php
$options = get_option( 'lkp_plugin_options' );
if($options['banner_title_product'] != "" && $options['banner_text_product'] != ""):
?>
<div id="banner">
	<div class="container">
		<div class="logo_content">
			<img src="<?php echo get_template_directory_uri(); ?>/images/logo-content.svg">
		</div>
		<div class="banner_content">
			<div class="title"><?php echo $options['banner_title_product']; ?></div>
			<p><?php echo $options['banner_text_product']; ?></p>
		</div>
		<div class="buttons">
			<a class="btn-phone" href="tel:<?php echo str_replace(" ", "", get_option('woocommerce_store_phone'));?>"><img src="<?php echo get_template_directory_uri(); ?>/images/phone-black.svg" alt="" /><?php echo get_option('woocommerce_store_phone');?></a>
			<?php
			if(isset($options['banner_btn_product']) && $options['banner_btn_product'] != ""):
			?>
			<a class="btn btn-primary" href="<?php echo get_the_permalink($options['banner_btn_product']); ?>">Advies aanvragen</a>
			<?php
			endif;
			?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php
endif;
?>
<div id="anchor_speed" class="container product-detail bottom">
	<?php
	//Text block 1
	$img1 = get_field('snelheid_image');
	$titel1 = get_field('snelheid_titel');
	$txt1 = get_field('snelheid_tekst');

	if($titel1 || $txt1):
	?>
	<div class="info-block first">
		<div class="content">
			<?php
				if($titel1):
			?>
				<h1><?php echo $titel1;  ?></h1>
			<?php
				endif;
			?>
			<?php  
				if($txt1) print $txt1;
			?>
		</div>
		<?php
			if($img1):
		?>
			<div class="images">
				<?php
					if($img1) print formatImgObjToLink($img1);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="clearfix"></div>
	</div>
	<?php
		endif;

	//Text block 2
	$img2 = get_field('veiligheid_image');
	$titel2 = get_field('veiligheid_titel');
	$txt2 = get_field('veiligheid_tekst');

	if($titel2 || $txt2):
	?>
	<div id="anchor_secure" class="info-block second">
		<?php
			if($img2):
		?>
			<div class="images">
				<?php
					if($img2) print formatImgObjToLink($img2);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<?php
				if($titel2):
			?>
				<h1><?php echo $titel2;  ?></h1>
			<?php
				endif;
			?>
			<?php  
				if($txt2) print $txt2;
			?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php
		endif;
	?>
</div>
<?php
if(get_field("usps_tonen") == "ja") 
	$showUSPS = true;
else 
	$showUSPS = false;	

if($showUSPS || get_field("usps_tonen") == null):
?>
<section class="company-usps">
	<div class="container">
		<div class="row">
			<div class="column-3">
				<div class="item">
					<h4><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="">Actief sinds 2015</h4>
			</div>
			</div>
			<div class="column-3">
				<div class="item">
					<h4><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="">100% onafhankelijk</h4>
				</div>
			</div>
			<div class="column-3">
				<div class="item">
					<h4><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="">Tot wel 40% besparen</h4>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<?php
endif;
?>
<div id="anchor_about" class="container product-detail bottom">
	<?php
	//Text block 3
	$img3 = get_field('over_het_merk_image');
	$titel3 = get_field('over_het_merk_titel');
	$txt3 = get_field('over_het_merk_tekst');
	$quote = get_field('over_het_merk_quote');

	if($titel3 || $txt3):
	?>
	<div class="info-block right third">
		<?php
			if($img3):
		?>
			<div class="images">
				<?php
					if($img3) print formatImgObjToLink($img3);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<?php
				if($titel3):
			?>
				<h1><?php echo $titel3;  ?></h1>
			<?php
				endif;
			?>
			<?php  
				if($txt3) print $txt3;

				if($quote):
			?>
			<div class="quote">	
				<?php echo $quote; ?>
			</div>
			<?php
				endif;
			?>
		</div>
		
		<div class="clearfix"></div>
	</div>
	<?php
		endif;
	?>

	<?php
	//Text block 4
	$img4 = get_field('slim_laden_image');
	$titel4 = get_field('slim_laden_titel');
	$txt4 = get_field('slim_laden_tekst');
	$slim_price = get_field('slim_laden_prijs');
	$slim_btn = get_field('slim_laden_knop');

	if($titel4 || $txt4):
	?>
	<div id="anchor_smart" class="info-block second fourth">
		<?php
			if($img4):
		?>
			<div class="images">
				<?php
					if($img4) print formatImgObjToLink($img4);
				?>
			</div>
		<?php
			endif;
		?>
		<div class="content">
			<?php
				if($titel4):
			?>
				<h1><?php echo $titel4;  ?></h1>
			<?php
				endif;
			?>
			<?php  
				if($txt4) print $txt4;

				if($slim_price):
			?>
			<div class="detail_price">
				<?php echo $slim_price; ?><span>excl. BTW per maand</span>
			</div>
			<?php
				endif;
				if($slim_btn):
			?>
				<a class="btn btn-primary" href="<?php echo $slim_btn['url']; ?>"><?php echo $slim_btn['title']; ?></a>
			<?php
				endif;
			?>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php
		endif;
	?>
</div>
<?php
if(get_field("usps_tonen") == "ja") 
	$showUSPS = true;
else 
	$showUSPS = false;	

if($showUSPS || get_field("usps_tonen") == null):
	echo get_template_part('partials/uspBlock', 'page');
endif;

//Text block 5
$img5 = get_field('installatie_image');
$titel5 = get_field('installatie_titel');
$txt5 = get_field('installatie_tekst');
$btn5 = get_field('installatie_knop');
if($titel5 || $txt5):
?>
<div id="anchor_install" class="container product-detail bottom">
	<div class="info-block right fifth">
		<?php
			if($img5):
		?>
			<div class="images">
				<?php
					if($img5) print formatImgObjToLink($img5);
				?>
			</div>
		<?php
			endif;
		?>
			<div class="content">
		<?php
				if($titel5):
			?>
				<h1><?php echo $titel5;  ?></h1>
			<?php
				endif;
			?>
			<?php  
				if($txt5) print $txt5;

				if($btn5):
			?>
			<a class="btn btn-primary" href="<?php echo $btn5['url']; ?>"<?php echo (isset($btn5['target']))? 'target="'.$btn5['target'].'"' : ''; ?>><?php echo $btn5['title']; ?></a>
			<?php
			endif;
			?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php
endif;
?>
<?php
if(get_field("usps_tonen") == "ja") 
	$showUSPS = true;
else 
	$showUSPS = false;	

if($showUSPS || get_field("usps_tonen") == null):
?>

<section class="company-usps">
	<div class="container">
		<div class="row">
			<div class="column-3">
				<div class="item">
					<h4><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="">Actief sinds 2015</h4>
			</div>
			</div>
			<div class="column-3">
				<div class="item">
					<h4><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="">100% onafhankelijk</h4>
				</div>
			</div>
			<div class="column-3">
				<div class="item">
					<h4><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="">Tot wel 40% besparen</h4>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<?php
endif;

if(get_field("technische_specificaties_tonen") == "ja") 
	$showTech = true;
else 
	$showTech = false;	

if($showTech || get_field("technische_specificaties_tonen") == null):
?>
<div id="anchor_ts" class="container product-detail specs">
	<h2>Technische specificaties</h2>
	<div class="row">
		<?php
			get_template_part('partials/productSpecs', 'page');
		?>
	</div>
</div>
<?php
endif;

echo get_template_part('partials/productReviews', 'page');

echo get_template_part('partials/productRelated', 'page');
?>

<?php
endwhile;

get_footer();
