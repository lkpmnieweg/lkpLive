<?php



// $logData = "";


// $logData .= print_r(apache_request_headers(), true)."\n";//$headers['Authorization_key']."\n";
// if(isset($_POST)){
	// $logData .= $_POST['sf_id']."\n";
// }

// file_put_contents('./log.txt', $logData, FILE_APPEND | LOCK_EX);
//a9af4898c86e5b8d9b1c657735e4c0ec8eb2c74b

function makeRequest($url, $post_string) {
	$soap_do = curl_init(); 
	curl_setopt($soap_do, CURLOPT_URL,            $url );   
	curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10); 
	curl_setopt($soap_do, CURLOPT_TIMEOUT,        10); 
	curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);  
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false); 
	curl_setopt($soap_do, CURLOPT_POST,           true ); 
	curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $post_string); 
	curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array(
		'Content-Type: text/xml; charset=utf-8', 
		'Content-Length: '.strlen($post_string),
		'SOAPAction: Post'
				)
	); 

	$result = curl_exec($soap_do);
	$err = curl_error($soap_do);  

	return $result;
}

function parseResponse($XMLResponse) {
	$doc = new DOMDocument();
    $doc->loadXML($XMLResponse);
    return $doc;
    
}

function soap_login() {
	$wsdl = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Header>
   </soapenv:Header>
   <soapenv:Body>
      <urn:login>
         <urn:username>t.epskamp-txok@force.com</urn:username>
         <urn:password>QeQ{q5TnVnt3?.JVLIj6CeANWfwcU6JHBAfcqv1P</urn:password>
      </urn:login>
   </soapenv:Body>
</soapenv:Envelope>';
	$login_url = "https://login.salesforce.com/services/Soap/u/48.0";
	$request = makeRequest($login_url, $wsdl);
	$response = parseResponse($request);

	$resultData = array(
		"serverUrl" => $response->getElementsByTagName('serverUrl')->item(0)->nodeValue,
		"sessionId" => $response->getElementsByTagName('sessionId')->item(0)->nodeValue,
	);
	return $resultData;
}

function soap_addEvent($loginData=false) {
	$year = date("Y");
	$month = date("m");
	$day = date("d");
	$hour = date("H", strtotime("-2 hour"));
	$minute = date("i");
	$seconds = date("s");

	$whatStartElement = ($loginData['whatAmI'] == true)? '<WhatId>' : '<WhoId>';
	$whatEndElement = ($loginData['whatAmI'] == true)? '</WhatId>' : '</WhoId>';

	$soap_text = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">
	   <soapenv:Header>
	      <urn:SessionHeader>
	         <urn:sessionId>'.$loginData['sessionId'].'</urn:sessionId>
	      </urn:SessionHeader>
	   </soapenv:Header>
	   <soapenv:Body>
	      <urn:create>
	         <!--Zero or more repetitions:-->
	         <urn:sObjects xsi:type="urn1:Event" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	            <!--Zero or more repetitions:-->
	            '.$whatStartElement.$loginData['sf_id'].$whatEndElement.'
	            <Subject>'.(($loginData['subject'] != null)? $loginData['subject'] : 'Clang: laadpaal vergelijking verstuurd').'</Subject>
	            <Description>'.(($loginData['description'] != null)? $loginData['description'] : 'Emailadres: '.$loginData['emailaddr'].'
VergelijkUrl: '.$loginData['verg_url']).'</Description>
	            <DurationInMinutes>1</DurationInMinutes>
	            <StartDateTime>'.$year.'-'.$month.'-'.$day.'T'.$hour.':'.$minute.':'.$seconds.'</StartDateTime>
	            <EndDateTime>'.$year.'-'.$month.'-'.$day.'T'.$hour.':'.date("i", strtotime("+1 minute")).':'.$seconds.'</EndDateTime>
	            <!--You may enter ANY elements at this point-->
	         </urn:sObjects>
	      </urn:create>
	   </soapenv:Body>
	</soapenv:Envelope>';
	if(isset($loginData['sessionId']) && isset($loginData['serverUrl']))
	{
		$request = makeRequest($loginData['serverUrl'], $soap_text);
		$response = parseResponse($request);
		var_dump($response);
	}
	//<Type>Email</Type>
}

function soap_updateLead($loginData=false) {
	$soap_text = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">
   <soapenv:Header>
      <urn:SessionHeader>
         <urn:sessionId>'.$loginData['sessionId'].'</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:update>
         <!--Zero or more repetitions:-->
         <urn:sObjects xsi:type="urn1:Lead" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <!--Zero or more repetitions:-->
            <Id>'.$loginData['sf_id'].'</Id>
            <Status>Vergelijking verstuurd</Status>
            <!--You may enter ANY elements at this point-->
         </urn:sObjects>
      </urn:update>
   </soapenv:Body>
</soapenv:Envelope>';
	if(isset($loginData['sessionId']) && isset($loginData['serverUrl']))
	{
		$request = makeRequest($loginData['serverUrl'], $soap_text);
		$response = parseResponse($request);
	}
}

$headers = apache_request_headers();
//only allow Clang
if(isset($headers['Authorization'])) {
	if($headers['Authorization'] == "a9af4898c86e5b8d9b1c657735e4c0ec8eb2c74b") {
		if(isset($_POST['sf_id'])) {
			$salesforce_id = str_replace(array("lead_", "account_"), "", $_POST['sf_id']);
			if($salesforce_id) {
				$subject = null;
				$description = null;
				$whatAmI = false;
				if(isset($_POST['subject'])) {
					$subject = $_POST['subject'];
				}
				if(isset($_POST['description'])) {
					$description = $_POST['description'];
				}
				if(isset($_POST['type']) && $_POST['type'] == 'Opportunity') {
					$whatAmI = true;
				}

				$loginData = soap_login();
				$loginData['sf_id'] = $salesforce_id;
				$loginData['verg_url'] = $_POST['vergelijkUrl'];
				$loginData['emailaddr'] = $_POST['emailaddress'];
				$loginData['subject'] = $subject;
				$loginData['description'] = $description;			
				$loginData['whatAmI'] = $whatAmI;	
				soap_addEvent($loginData);

				if($subject == 'Clang: Laadpaal vergelijking verstuurd' || $subject == 'Clang: Zonnenpaneel vergelijking verstuurd')
					soap_updateLead($loginData);
			}
		}
	}
}

?>