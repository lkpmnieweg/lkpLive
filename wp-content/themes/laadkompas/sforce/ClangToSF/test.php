<?php
error_reporting(E_ALL);

function makeRequest($url, $post_string) {
	$soap_do = curl_init(); 
	curl_setopt($soap_do, CURLOPT_URL,            $url );   
	curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10); 
	curl_setopt($soap_do, CURLOPT_TIMEOUT,        10); 
	curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);  
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false); 
	curl_setopt($soap_do, CURLOPT_POST,           true ); 
	curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $post_string); 
	curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array(
		'Content-Type: text/xml; charset=utf-8', 
		'Content-Length: '.strlen($post_string),
		'SOAPAction: Post'
				)
	); 

	$result = curl_exec($soap_do);
	$err = curl_error($soap_do);  

	return $result;
}

function parseResponse($XMLResponse) {
	$doc = new DOMDocument();
    $doc->loadXML($XMLResponse);
    return $doc;
    
}


function soap_login() {
	$wsdl = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Header>
   </soapenv:Header>
   <soapenv:Body>
      <urn:login>
         <urn:username>t.epskamp-txok@force.com</urn:username>
         <urn:password>QeQ{q5TnVnt3?.JVLIj6CeANWfwcU6JHBAfcqv1P</urn:password>
      </urn:login>
   </soapenv:Body>
</soapenv:Envelope>';
	$login_url = "https://login.salesforce.com/services/Soap/u/48.0";
	$request = makeRequest($login_url, $wsdl);
	$response = parseResponse($request);

	$resultData = array(
		"serverUrl" => $response->getElementsByTagName('serverUrl')->item(0)->nodeValue,
		"sessionId" => $response->getElementsByTagName('sessionId')->item(0)->nodeValue,
	);
	return $resultData;
}

function soap_addEvent($loginData=false) {
	$year = date("Y");
	$month = date("m");
	$day = date("d");
	$hour = date("H", strtotime("-1 hour"));
	$minute = date("i");
	$seconds = date("s");
	$soap_text = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">
	   <soapenv:Header>
	      <urn:SessionHeader>
	         <urn:sessionId>'.$loginData['sessionId'].'</urn:sessionId>
	      </urn:SessionHeader>
	   </soapenv:Header>
	   <soapenv:Body>
	      <urn:create>
	         <!--Zero or more repetitions:-->
	         <urn:sObjects xsi:type="urn1:Event" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	            <!--Zero or more repetitions:-->
	            <WhoId>'.$loginData['sf_id'].'</WhoId>
	            <Subject>Clang: laadpaal vergelijking verstuurd</Subject>
	            <Description>Emailadres: '.$loginData['emailaddr'].'
VergelijkUrl: '.$loginData['verg_url'].'</Description>
	            <DurationInMinutes>1</DurationInMinutes>
	            <StartDateTime>'.$year.'-'.$month.'-'.$day.'T'.$hour.':'.$minute.':'.$seconds.'</StartDateTime>
	            <EndDateTime>'.$year.'-'.$month.'-'.$day.'T'.$hour.':'.date("i", strtotime("+1 minute")).':'.$seconds.'</EndDateTime>
	            <!--You may enter ANY elements at this point-->
	         </urn:sObjects>
	      </urn:create>
	   </soapenv:Body>
	</soapenv:Envelope>';
	if(isset($loginData['sessionId']) && isset($loginData['serverUrl']))
	{
		$request = makeRequest($loginData['serverUrl'], $soap_text);
		$response = parseResponse($request);
		var_dump($response);
	}
}

$salesforce_id = str_replace(array("lead_", "account_"), "", "lead_00Q5J000003s5CyUAI");
if($salesforce_id) {
	$loginData = soap_login();
	$loginData['sf_id'] = $salesforce_id;
	$loginData['verg_url'] = 'test';
	$loginData['emailaddr'] = urlencode('benvanklinken+tests@gmail.com');
	soap_addEvent($loginData);
	
	// soap_updateLead($loginData);
}

?>