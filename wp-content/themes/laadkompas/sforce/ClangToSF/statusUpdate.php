<?php
error_reporting(E_ALL);
ini_set('display_error',1);


// $logData = "";


// $logData .= print_r(apache_request_headers(), true)."\n";//$headers['Authorization_key']."\n";
// if(isset($_POST)){
	// $logData .= $_POST['sf_id']."\n";
// }

// file_put_contents('./log.txt', $logData, FILE_APPEND | LOCK_EX);
//a9af4898c86e5b8d9b1c657735e4c0ec8eb2c74b

function makeRequest($url, $post_string) {
	$soap_do = curl_init(); 
	curl_setopt($soap_do, CURLOPT_URL,            $url );   
	curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10); 
	curl_setopt($soap_do, CURLOPT_TIMEOUT,        10); 
	curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);  
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false); 
	curl_setopt($soap_do, CURLOPT_POST,           true ); 
	curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $post_string); 
	curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array(
		'Content-Type: text/xml; charset=utf-8', 
		'Content-Length: '.strlen($post_string),
		'SOAPAction: Post'
				)
	); 

	$result = curl_exec($soap_do);
	$err = curl_error($soap_do);  

	return $result;
}

function parseResponse($XMLResponse) {
	$doc = new DOMDocument();
    $doc->loadXML($XMLResponse);
    return $doc;
    
}

function soap_login() {
	$wsdl = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Header>
   </soapenv:Header>
   <soapenv:Body>
      <urn:login>
         <urn:username>t.epskamp-txok@force.com</urn:username>
         <urn:password>QeQ{q5TnVnt3?.JVLIj6CeANWfwcU6JHBAfcqv1P</urn:password>
      </urn:login>
   </soapenv:Body>
</soapenv:Envelope>';
	$login_url = "https://login.salesforce.com/services/Soap/u/48.0";
	$request = makeRequest($login_url, $wsdl);
	$response = parseResponse($request);

	$resultData = array(
		"serverUrl" => $response->getElementsByTagName('serverUrl')->item(0)->nodeValue,
		"sessionId" => $response->getElementsByTagName('sessionId')->item(0)->nodeValue,
	);
	return $resultData;
}

function soap_updateLead($loginData=false) {
	$soap_text = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">
   <soapenv:Header>
      <urn:SessionHeader>
         <urn:sessionId>'.$loginData['sessionId'].'</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:update>
         <!--Zero or more repetitions:-->
         <urn:sObjects xsi:type="urn1:Lead" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <!--Zero or more repetitions:-->
            <Id>'.$loginData['sf_id'].'</Id>
            <Status>'.$loginData['status'].'</Status>
            <!--You may enter ANY elements at this point-->
         </urn:sObjects>
      </urn:update>
   </soapenv:Body>
</soapenv:Envelope>';
	if(isset($loginData['sessionId']) && isset($loginData['serverUrl']))
	{
		$request = makeRequest($loginData['serverUrl'], $soap_text);
		$response = parseResponse($request);
		var_dump($response);
	}
}

$headers = apache_request_headers();
//only allow Clang
if(isset($headers['Authorization'])) {
	if($headers['Authorization'] == "a9af4898c86e5b8d9b1c657735e4c0ec8eb2c74b") {
		if(isset($_POST['sf_id'])) {
			$salesforce_id = str_replace(array("lead_", "account_"), "", $_POST['sf_id']);
			if($salesforce_id) {
				$subject = null;
				$description = null;
				$status = false;
				if(isset($_POST['subject'])) {
					$subject = $_POST['subject'];
				}
				if(isset($_POST['status'])) {
					$status = $_POST['status'];
				}

				$loginData = soap_login();
				$loginData['sf_id'] = $salesforce_id;
				$loginData['status'] = $status;
				soap_updateLead($loginData);
			}
		}
	}
}

?>