<?php


//soap call login 
class SalesForceLaadKompas {

	private $api_user = "t.epskamp-txok@force.com";
	private $api_pass = "QeQ{q5TnVnt3?.JVLIj6CeANWfwcU6JHBAfcqv1P";
	private $login_url = "https://login.salesforce.com/services/Soap/u/48.0";

	public function _construct() {

	}

	public function _WSDLLogin() {
		$soapCall = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Header>
   </soapenv:Header>
   <soapenv:Body>
      <urn:login>
         <urn:username>'.$this->api_user.'</urn:username>
         <urn:password>'.$this->api_pass.'</urn:password>
      </urn:login>
   </soapenv:Body>
</soapenv:Envelope>';


		//make soap call & fetch result
		$request = $this->makeRequest($this->login_url, $soapCall);
		$response = $this->parseResponse($request);

		$resultData = array(
			"serverUrl" => $response->getElementsByTagName('serverUrl')->item(0)->nodeValue,
			"sessionId" => $response->getElementsByTagName('sessionId')->item(0)->nodeValue,
		);
		return $resultData;
	}

	public function _WSDLLead($loginData, $nawData) { //serverUrl / sessionId
		if(isset($loginData['sessionId']) && isset($loginData['serverUrl']))
		{
			$zakelijkPrive = '';
			switch($nawData['SlimmeLaadpaal__c']) {
				case 'Ja':
					$zakelijkPrive = 'Zakelijk';
				break;
				case 'Nee':
					$zakelijkPrive = 'Prive';
				break;
				case 'Weet niet / Adviseer mij':
					$zakelijkPrive = 'Nog niet bekend';
				break;
			}

		$soapCall = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">
   <soapenv:Header>
      <urn:SessionHeader>
         <urn:sessionId>'.$loginData['sessionId'].'</urn:sessionId>
      </urn:SessionHeader>
	  <urn:AssignmentRuleHeader>
		 <urn:useDefaultRule>true</urn:useDefaultRule>
	  </urn:AssignmentRuleHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:create>
         <!--Zero or more repetitions:-->
         <urn:sObjects xsi:type="urn1:Lead" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <!--Zero or more repetitions:-->
            <Company></Company>
            <LeadSource>Laadkompas Nieuw</LeadSource>
            
			  <LocatieLaadpaal__c>'.$nawData['locatielaadpaal'].'</LocatieLaadpaal__c>
			  <Auto__c>'.$nawData['auto'].'</Auto__c>
			  <LaadstationInstallatie__c>'.$nawData['LaadstationInstallatie__c'].'</LaadstationInstallatie__c>
			  <GewensteLaadpaal__c>'.$nawData['GewensteLaadpaal__c'].'</GewensteLaadpaal__c>
			  <SlimmeLaadpaal__c>'.$nawData['SlimmeLaadpaal__c'].'</SlimmeLaadpaal__c>
			  <zonnepanelen__c>'.$nawData['zonnepanelen__c'].'</zonnepanelen__c>
			  <Zakelijk_of_prive__c>'.$zakelijkPrive.'</Zakelijk_of_prive__c>
			  
            <Firstname>'.$nawData['firstname'].'</Firstname>
            <Lastname>'.$nawData['lastname'].'</Lastname>
            <MobilePhone>'.$nawData['tel'].'</MobilePhone>
            <Email>'.$nawData['email'].'</Email>
            <ExtraOpmerkingen__c>'.$nawData['extra'].'</ExtraOpmerkingen__c>
            <vergelijkUrlSalesforce__c>'.$nawData['vergelijk_url'].'</vergelijkUrlSalesforce__c>
         </urn:sObjects>
      </urn:create>
   </soapenv:Body>
</soapenv:Envelope>
		';
		}

		//<Title></Title>		  
		$request = $this->makeRequest($loginData['serverUrl'], $soapCall);
		$response = $this->parseResponse($request);

		// $resultData = array(
		// 	"serverUrl" => $response->getElementsByTagName('serverUrl')->item(0)->nodeValue,
		// 	"sessionId" => $response->getElementsByTagName('sessionId')->item(0)->nodeValue,
		// );
		return $response;
	}

	private function makeRequest($url, $post_string) {
		$soap_do = curl_init(); 
		curl_setopt($soap_do, CURLOPT_URL,            $url );   
		curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10); 
		curl_setopt($soap_do, CURLOPT_TIMEOUT,        10); 
		curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);  
		curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($soap_do, CURLOPT_POST,           true ); 
		curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $post_string); 
		curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array(
			'Content-Type: text/xml; charset=utf-8', 
			'Content-Length: '.strlen($post_string),
			'SOAPAction: Post'
					)
		); 

		$result = curl_exec($soap_do);
		$err = curl_error($soap_do);  

		return $result;
	}

	private function parseResponse($XMLResponse) {
		$doc = new DOMDocument();
        $doc->loadXML($XMLResponse);
        return $doc;
        
	}

	/* Zonnepanelen */

	//assignmentRuleId = assignment for leads zonnepanelen
	public function _WSDLLeadSolarPanel($loginData, $nawData) { //serverUrl / sessionId
		if(isset($loginData['sessionId']) && isset($loginData['serverUrl']))
		{
		$soapCall = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">
   <soapenv:Header>
      <urn:SessionHeader>
         <urn:sessionId>'.$loginData['sessionId'].'</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:create>
         <!--Zero or more repetitions:-->
         <urn:sObjects xsi:type="urn1:Lead" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <!--Zero or more repetitions:-->
            <Company></Company>
            <LeadSource>Laadkompas Zonnepanelen</LeadSource>
            
            <Firstname>'.$nawData['firstname'].'</Firstname>
            <Lastname>'.$nawData['lastname'].'</Lastname>
            <MobilePhone>'.$nawData['tel'].'</MobilePhone>
            <Email>'.$nawData['email'].'</Email>
            <ExtraOpmerkingen__c>'.$nawData['extra'].'</ExtraOpmerkingen__c>
            <vergelijkUrlSalesforce__c>'.$nawData['vergelijk_url'].'</vergelijkUrlSalesforce__c>
            <Stroomverbruik_p_jaar__c>'.$nawData['consumption'].'</Stroomverbruik_p_jaar__c>
            <Voorkeur_zonnepaneel__c>'.$nawData['preference'].'</Voorkeur_zonnepaneel__c>
            <In_bezit_van_laadpaal__c>'.$nawData['gotchp'].'</In_bezit_van_laadpaal__c>
            <Volledig_adres__c>'.$nawData['full_address'].'</Volledig_adres__c>
            <PV_klant__c>true</PV_klant__c>
         </urn:sObjects>
      </urn:create>
   </soapenv:Body>
</soapenv:Envelope>
		';
		}

		//<Title></Title>		  
		$request = $this->makeRequest($loginData['serverUrl'], $soapCall);
		$response = $this->parseResponse($request);

		// $resultData = array(
		// 	"serverUrl" => $response->getElementsByTagName('serverUrl')->item(0)->nodeValue,
		// 	"sessionId" => $response->getElementsByTagName('sessionId')->item(0)->nodeValue,
		// );
		return $response;
	}

	public function _WSDLRetrieveLead($loginData, $leadID=false) {
		if($leadID) {
			$soapCall = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
			<soapenv:Header>
			<urn:SessionHeader>
				<urn:sessionId>'.$loginData['sessionId'].'</urn:sessionId>
			</urn:SessionHeader>
			</soapenv:Header>
			<soapenv:Body>
			<urn:retrieve>
				<urn:fieldList> Owner.FirstName </urn:fieldList>
				<urn:sObjectType>Lead</urn:sObjectType>
				<urn:ids>'.$leadID.'</urn:ids>
				<!--Zero or more repetitions:-->
				
			</urn:retrieve>
			</soapenv:Body>
		</soapenv:Envelope>';


			//<Title></Title>		  
			$request = $this->makeRequest($loginData['serverUrl'], $soapCall);
			$response = $this->parseResponse($request);

			// $resultData = array(
			// 	"serverUrl" => $response->getElementsByTagName('serverUrl')->item(0)->nodeValue,
			// 	"sessionId" => $response->getElementsByTagName('sessionId')->item(0)->nodeValue,
			// );
			return $response;
		}
	}
}
?>