<?php

class SalesForce {

	public function postSForceSolarPanel($funnelObj)
	{
		$funnel = $funnelObj;
		$firstname = $funnel->firstname;
		$lastname = $funnel->name;
		

    	if($lastname == '')
    		$lastname = 'LEEG';

		$mobile = $funnel->phone;
		$emailadres = $funnel->email;
		$textarea = $funnel->comments;

		include_once(__DIR__."/api.php");

		$soap = new SalesForceLaadKompas();

		var_dump($funnelObj);


		//init api
		$loginData = $soap->_WSDLLogin();

		// print __DIR__; exit;

		// //add lead
		$nawData = array(
			'firstname' => ucfirst($firstname),
			'lastname' => ucfirst($lastname),
			'tel' => $funnel->phone,
			'email' => $funnel->email,
			'extra' => $funnel->comments,
			'vergelijk_url' => $funnel->quoteUrl,
			'consumption' => $funnel->consumption,
			'preference' => $funnel->preference,
			'gotchp' => $funnel->gotchp,
			'full_address' => $funnel->fullAddress,
		);


		var_dump('solar lead:');
		$lead = $soap->_WSDLLeadSolarPanel($loginData, $nawData);
		var_dump($lead);
		$server_output = $lead;

		//Send to Clang
		if($lead->textContent && $lead->textContent != "") {
				$answer = explode(" ", $lead->textContent);
				if(count($answer) > 0) {
					$sf_lead_ID = str_replace(array("REQUESTS", "true"), "", $answer[1]);
					var_dump('sf_lead_ID');
					var_dump($sf_lead_ID);
					if($sf_lead_ID != "") {
						/*** CLANG ****/
						//make call to Clang to save form data + sf_lead_ID
						$json_data = array(
							"firstname" => ucfirst($firstname), 
							"lastname" => ucfirst($lastname), 
							"phone_nr" => $funnel->phone,
							"emailaddress" => $funnel->email,
							"comments" => $funnel->comments,
							"consumption" => $funnel->consumption,
							"preffered_type" => $funnel->preference,
							"has_chargingpoint" => $funnel->gotchp,
							"full_address" => $funnel->fullAddress,
							"vergelijkUrl" => str_replace("/geen-categorie/", "/zonnepanelen-quote/", $funnel->quoteUrl),
						    "salesforce_id" => 'lead_'.$sf_lead_ID,
						);                                                                                   
						                                                                                                                     
						$ch4 = curl_init('https://webhook.myclang.com/app/api/rest/public/v2/project/datahook/data-receiver/6-zonnepaneel_vergelijking');
						curl_setopt($ch4, CURLOPT_CUSTOMREQUEST, "POST");  
						curl_setopt($ch4, CURLOPT_POST, 1);
						curl_setopt($ch4, CURLOPT_POSTFIELDS, json_encode($json_data));
						curl_setopt($ch4, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, 1);                                                                 
						curl_setopt($ch4, CURLOPT_HTTPHEADER, array(                                                                          
						    'Content-Type: application/json',                                                                                
						    'Content-Length: ' . strlen(json_encode($json_data)),
							'Authorization: Bearer e525a4af-0d1e-4fea-8641-9698d3fd4bcb'
						),              
						);                                                                                                                   
						                                                                                                                     
						$result4 = curl_exec($ch4);
						curl_close($ch4);
						var_dump($result4);
					}
				}
			}


		return ($server_output != "")? true : false;
	}
	
	public function postToAPI($funnelObj)
	{
		
		//add lead
		/*
		$nawData = array(
			'heer' => 'dhr.',
			'firstname' => 'Ben',
			'name' => 'Mick2505',
			'mobile' => '0631366919',
			'emailadres' => 'mick2505@micktest2505.nl',
			'textarea' => 'LOREM',
			'charging_station_location' => 'Thuislocatie',
			'step_num' => '2',
			'action' => 'quote_request_action',
			'locatie' => 'Thuislocatie',
			'car_type' => 'Overig, mijn auto staat er niet tussen',
			'model' => 'Enkele laadpaal',
			'laadstation' => 'Ja',
			'accessoires' => 'Selecteer optie',
			'installatie' => 'Ja',
			'extra_info' => 'TEST',
			'vergelijk_url' => 'http://bmdigital.nl/laadkompas/quote/lkt-316-1-2/'
		);


		$heer = $nawData['heer'];
		$firstname = $nawData['firstname'];
		$name = $nawData['name'];
		$mobile = $nawData['mobile'];
		$emailadres = $nawData['emailadres'];
		$textarea = $nawData['textarea'];
		$charging_station_location = $nawData['charging_station_location'];
		$step_num = $nawData['step_num'];
		$action = $nawData['action'];
		$locatie = $nawData['locatie'];
		$car_type = $nawData['car_type'];
		$model = $nawData['model'];
		$laadstation = $nawData['laadstation'];
		$accessoires = $nawData['accessoires'];
		$installatie = $nawData['installatie'];
		$extra_info = $nawData['extra_info'];
		$vergelijk_url = urlencode($nawData['vergelijk_url']);*/

		$funnel = $funnelObj;
		// if($exp_name = explode(" ", $funnel->name)){
		// 	$firstname = array_shift($exp_name);
		// 	$lastname = implode(" ",$exp_name);
		// } else {
		// }
		$firstname = $funnel->firstname;
		$lastname = $funnel->name;
		

    	if($lastname == '')
    		$lastname = 'LEEG';

		$heer = $funnel->title;
		$firstname = $firstname;
		$name = $lastname;
		$mobile = $funnel->phone;
		$emailadres = $funnel->email;
		$textarea = $funnel->comments;
		$charging_station_location = urlencode($funnel->location);
		$step_num = '2';
		$action = 'quote_request_action';
		$locatie = urlencode($funnel->location);
		$car_type = $funnel->car_brand.' '.urlencode($funnel->car_type);
		$model = ucfirst($funnel->singleDouble);
		$laadstation = ucfirst($funnel->smart);
		$accessoires = '';
		$installatie = ucfirst($funnel->install);
		$extra_info = urlencode($funnel->comments);
		$vergelijk_url = urlencode(str_replace("/geen-categorie/", "/quote/", $funnel->quoteUrl));
		$zonnepanelen = ucfirst($funnel->zonnepanelen);


		$server_output = "";
		$ch = curl_init();
		// $firstname = "Mick";
		// $name = "Digit";
		// $email = "youneedtoget@live.nl";
		// $aanhef = "dhr.";

		$fields = "heer=$heer&firstname=$firstname&name=$name&mobile=$mobile&emailadres=$emailadres&textarea=$textarea&charging_station_location=$charging_station_location&step_num=$step_num&action=$action&locatie=$locatie&car_type=$car_type&model=$model&laadstation=$laadstation&accessoires=$accessoires&installatie=$installatie&extra_info=$extra_info&vergelijk_url=$vergelijk_url";

		$fieldsArray = array(
			'heer' => $funnel->title,
			'firstname' => $firstname,
			'name' => $lastname,
			'mobile' => $funnel->phone,
			'emailadres' => $funnel->email,
			'textarea' => $funnel->comments,
			'charging_station_location' => ucfirst($funnel->location),
			'step_num' => '2',
			'action' => 'quote_request_action',
			'location' => ucfirst($funnel->location),
			'car_type' => $funnel->car_brand.' '.$funnel->car_type,
			'model' => $funnel->singleDouble,
			'laadstation' => ucfirst($funnel->smart),
			'accessoires' => '',
			'installatie' => ucfirst($funnel->install),
			'extra_info' => $funnel->comments,
			'vergelijk_url' => str_replace("/geen-categorie/", "/quote/", $funnel->quoteUrl)
		);

		// var_dump($fieldsArray);

		// if($_SERVER['REMOTE_ADDR'] == '217.121.83.181')
		// {
			include_once(__DIR__."/api.php");

			$soap = new SalesForceLaadKompas();

			//init api
			$loginData = $soap->_WSDLLogin();

			// print __DIR__; exit;

			// //add lead
			$nawData = array(
				'locatielaadpaal' => ucfirst($funnel->location),
				'title' => $funnel->title,
				'auto' => $funnel->car_brand.' '.$funnel->car_type,
				'LaadstationInstallatie__c' => ucfirst($funnel->install),
				'GewensteLaadpaal__c' => $funnel->singleDouble,
				'SlimmeLaadpaal__c' => ucfirst($funnel->smart),
				'zonnepanelen__c' => $zonnepanelen,
				'firstname' => ucfirst($firstname),
				'lastname' => ucfirst($lastname),
				'tel' => $funnel->phone,
				'email' => $funnel->email,
				'extra' => $funnel->comments,
				'vergelijk_url' => str_replace("/geen-categorie/", "/quote/", $funnel->quoteUrl)
			);


			var_dump('lead:');
			$lead = $soap->_WSDLLead($loginData, $nawData);
			var_dump($lead);
			$server_output = $lead;

			if($lead->textContent && $lead->textContent != "") {
				$answer = explode(" ", $lead->textContent);
				if(count($answer) > 0) {
					$sf_lead_ID = str_replace(array("REQUESTS", "true"), "", $answer[1]);
					var_dump('sf_lead_ID');
					var_dump($sf_lead_ID);
					if($sf_lead_ID != "") {
						/** RETRIEVE ACCOUNT MANAGER */
						$am_firstname = '';
						$am_email = '';
						$am_email_clang = '';
						$am_full_name = '';
						$newCreatedLead = $soap->_WSDLRetrieveLead($loginData, $sf_lead_ID);
						var_dump($newCreatedLead);
						//00Q5J000005OGHLUA4
						if($newCreatedLead->textContent && $newCreatedLead->textContent != "") {
							$answer = explode(" ", $newCreatedLead->textContent);
							if(count($answer) > 0) {
								$account_manager = str_replace(array("REQUESTS", "true", "Lead", "Name", $sf_lead_ID), "", $answer[1]);
								var_dump($account_manager);
								if($account_manager != '') {
									$am_firstname = $account_manager;
									$am_email = '';
									$am_email_clang = '';
									$am_full_name = '';

									switch($account_manager) {
										case 'Mick':
											$am_email = 'm.bakker@laadkompas.nl';
											$am_email_clang = 'm.bakker@e.laadkompas.nl';
											$am_full_name = 'Mick Bakker';
											break;								
										case 'Kishen':
											$am_email = 'k.thakoersingh@laadkompas.nl';
											$am_email_clang = 'k.thakoersingh@e.laadkompas.nl';
											$am_full_name = 'Kishen Thakoersingh';
											break;
										case 'Oscar':
											$am_email = 'o.santana@laadkompas.nl';
											$am_email_clang = 'o.santana@e.laadkompas.nl';
											$am_full_name = 'Oscar Santana';
											break;
										case 'Jordy':
											$am_email = 'j.klerk@laadkompas.nl';
											$am_email_clang = 'j.klerk@e.laadkompas.nl';
											$am_full_name = 'Jordy Klerk';
											break;
										case 'Sharona':
											$am_email = 's.balker@laadkompas.nl';
											$am_email_clang = 's.balker@e.laadkompas.nl';
											$am_full_name = 'Sharona Balker';
											break;
									}
								}
							}
						}

						$lead_counter = '17.221';
						
						//Load Lead Counter
						require_once __DIR__.'/../../../../wp-config.php';
						global $wpdb;
						$result = $wpdb->get_results("SELECT counter FROM lkp_lead_counter WHERE id=1");
						if($result[0]){
							$lead_counter = number_format((int)$result[0]->counter,0,'.','.');
						}

						
						//make call to Clang to save form data + sf_lead_ID
						$json_data = array(
							"firstname" => ucfirst($firstname), 
							"lastname" => ucfirst($lastname), 
							"phone_nr" => $funnel->phone,
							"emailaddress" => $funnel->email,
							"comments" => $funnel->comments,
							"car_brand" => $funnel->car_brand,
							"car_type" => $funnel->car_type,
							"type_charging_point" => $funnel->singleDouble,
							"location" => ucfirst($funnel->location),
							"installation" => ucfirst($funnel->install),
							"smart" => ucfirst($funnel->smart),
							"solar_panels" => $zonnepanelen,
							"vergelijkUrl" => str_replace("/geen-categorie/", "/quote/", $funnel->quoteUrl),
						    "sforce_user" => 'lead_'.$sf_lead_ID,
						    "optin_newsletter" => $funnel->optin_newsletter,
						    "optin_commercial" => $funnel->optin_commercial, 
						    "optin_newsletter_txt" => $funnel->newsletter_txt,
						    "optin_commercial_txt" => $funnel->commercial_txt,
							"acc_manager_firstname" => $am_firstname,
							"acc_manager_fullname" => $am_full_name,
							"acc_manager_email" => $am_email,
							"acc_manager_email_clang" => $am_email_clang,
							"lead_counter" => $lead_counter
						);                                                                                   
						                                                                                                                     
						$ch4 = curl_init('https://webhook.myclang.com/app/api/rest/public/v2/project/datahook/data-receiver/1-vergelijking');
						curl_setopt($ch4, CURLOPT_CUSTOMREQUEST, "POST");  
						curl_setopt($ch4, CURLOPT_POST, 1);
						curl_setopt($ch4, CURLOPT_POSTFIELDS, json_encode($json_data));
						curl_setopt($ch4, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch4, CURLOPT_FOLLOWLOCATION, 1);                                                                 
						curl_setopt($ch4, CURLOPT_HTTPHEADER, array(                                                                          
						    'Content-Type: application/json',                                                                                
						    'Content-Length: ' . strlen(json_encode($json_data)),
							'Authorization: Bearer 03c736d7-0df1-41fd-a426-d7272c8d20fa'
						),              
						);                                                                                                                   
						                                                                                                                     
						$result4 = curl_exec($ch4);
						curl_close($ch4);
						var_dump($result4);
					}
				}
			}

		// }
		// else {

		// 	$fields = http_build_query($fieldsArray);

		// 	curl_setopt($ch, CURLOPT_URL,"https://laadkompas.nl/wp-admin/admin-sf.php");
		// 	curl_setopt($ch, CURLOPT_POST, 1);
		// 	curl_setopt($ch, CURLOPT_POSTFIELDS,
		// 	            $fields);

		// 	// In real life you should use something like:
		// 	// curl_setopt($ch, CURLOPT_POSTFIELDS, 
		// 	//          http_build_query(array('postvar1' => 'value1')));

		// 	// Receive server response ...
		// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// 	$server_output = curl_exec($ch);

		// 	curl_close ($ch);
		// }

		return ($server_output != "")? true : false;
	}
}

?>