<?php
/* Template name: Laadkompas - Funnel bedankt */
get_header(); 

require_once( __DIR__ . '/funnel.class.php');
require_once( __DIR__ . '/Cryptor.class.php');

if($_SERVER['REMOTE_ADDR'] == '85.145.147.66')
{
ini_set('display_errors',1);
error_reporting(E_ALL);

$funnel = new Funnel();


$current_funnel = $funnel->decryptSession();
print_r($current_funnel);
}
?>
<div class="container single_page">
<?php
if (have_posts()): while (have_posts()) : the_post();
	the_content();
endwhile;
endif;
?>
</div>
<?php

if(is_cart())
	get_footer('funnel');
else get_footer();
?>