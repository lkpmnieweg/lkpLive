<?php get_header(); 

$category = get_queried_object();

?>
<div class="container title">
	<div class="title_bar">
		<h1><?php echo single_cat_title(); ?></h1>
		<div class="categories">
			<?php
			foreach(get_categories() as $cat)
			{
				print '<a'.(($category->term_id == $cat->term_id)? ' class="active"' : '').' href="'.get_the_permalink(10008).''.$cat->slug.'/">'.$cat->name.'</a>';
			}
			?>
			
		</div>
	</div>
</div>
<div class="other_news">
	<div class="container">
		<div class="row">
			<?php
			$return = [];
			$currentPage = get_query_var('paged');
			$taxonomy = get_queried_object();

		    $args = array(
		        'post_type' => 'post',
		        'category_name' => $taxonomy->slug,
		        'posts_per_page'         => -99,
		        'order'                  => 'DESC',
    			'paged' => $currentPage
		    );
		    $items = new WP_Query($args);
			if ( have_posts() ) :

		    while ($items->have_posts()) : $items->the_post(); 
		    ?>
		    <div class="column-3">
				<a href="<?php echo get_the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
					<span class="image"><?php echo get_the_post_thumbnail(); ?></span>
					<span class="details">
						<span class="category"><?php echo $category->name; ?></span>
						<span class="title"><?php echo get_the_title(); ?></span>
						<span class="text">
							<?php echo (get_field('introductie_tekst') != null && get_field('introductie_tekst') != "")? get_field('introductie_tekst') : get_the_excerpt() ?>
						</span>
					</span>
				</a>
			</div>
		    <?php
			endwhile; 
		endif;
			?>
		</div>
		<div class="button_bar pager">
			<?php 

echo "<div class='page-nav-container'>" . paginate_links(array(
    'total' => $posts->max_num_pages,
    'prev_text' => __('<'),
    'next_text' => __('>')
)) . "</div>";
 ?>
		</div>
	</div>
</div>
<div class="usp-bar">
	<div class="container">
	  <ul class="usps">
		  <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Check"> 100% onafhankelijk</li>
		  <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Check"> Besparen tot 40%</li>
		  <li><img src="<?php echo get_template_directory_uri(); ?>/images/icon-checkmark.svg" alt="Check"> 5.000+ laadpalen geïnstalleerd</li>
		</ul>
		<a class="btn" href="/shop/" title="Bezoek onze webshop">Bezoek onze webshop</a>
	</div>
</div>
<div class="container interesting technical">
	<h2>Ook interessant</h2>
	<div class="row">
		<div class="column-2">
			<a href="">
				<span class="image"><img src="<?php echo get_template_directory_uri(); ?>/images/techniek-1-img@2x.jpg" alt="" /></span>
				<span class="category">Techniek</span>
				<span class="title">Hoe lang duurt het om een auto op te laden?</span>
				<span class="text">
					Elke elektrische auto heeft een maximumlaadsnelheid. Een Tesla kan bijvoorbeeld drie keer sneller laden dan een standaard elektrische auto. Daarnaast speelt de laadpaal een belangrijke rol in het behalen..
				</span>
				<span class="more-link"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="" /> <span>Lees meer</span></span>
			</a>
		</div>
		<div class="column-2">
			<a href="">
				<span class="image"><img src="<?php echo get_template_directory_uri(); ?>/images/techniek-2-img@2x.jpg" alt="" /></span>
				<span class="category">Techniek</span>
				<span class="title">Hoe lang duurt het om een auto op te laden?</span>
				<span class="text">
					Elke elektrische auto heeft een maximumlaadsnelheid. Een Tesla kan bijvoorbeeld drie keer sneller laden dan een standaard elektrische auto. Daarnaast speelt de laadpaal een belangrijke rol in het behalen..
				</span>
				<span class="more-link"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="" /> <span>Lees meer</span></span>
			</a>
		</div>
	</div>
	<div class="button_bar">
		<a class="btn" href="">Bekijk meer nieuws</a>	
	</div>
</div>
<?php get_footer(); ?>
