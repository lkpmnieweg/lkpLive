<?php
/* Template name: Laadkompas - Funnel stap 2 */
session_start();


if(isset($_SESSION['lkp_current_funnel']))
{
	$current_funnel = $funnel->decryptSession();

}
else wp_redirect(get_site_url());
/* https://go.pardot.com/l/851933/2020-07-15/21blj */
get_header('funnel');
?>
<div class="usp-bar active">
	<div class="container">
		<ul>
			<li><img src="https://www.laadkompas.nl/wp-content/themes/laadkompas/images/icon-checkmark.svg" alt="<?php echo get_field('usp_1_tekst'); ?>"> <?php echo get_field('usp_1_tekst'); ?></li>
			<li><img src="https://www.laadkompas.nl/wp-content/themes/laadkompas/images/icon-checkmark.svg" alt="<?php echo get_field('usp_2_tekst'); ?>"> <?php echo get_field('usp_2_tekst'); ?></li>
			<li><img src="https://www.laadkompas.nl/wp-content/themes/laadkompas/images/icon-checkmark.svg" alt="<?php echo get_field('usp_3_tekst'); ?>"> <?php echo get_field('usp_3_tekst'); ?></li>
		</ul>
	</div>
</div>
<form id="funnel2" autocomplete="off" action="" method="POST">
	<div class="container">
	<section class="---Funnel-form">
		<div class="right">
			<div class="reviews">
				<div class="left-rating">
					<h1>Onze klanten <br>geven ons een</h1>	
					<div class="reviewStars">
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        				<img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="Ster" />
        			</div>
				</div>
				<div class="rating">
					<img src="<?php echo get_template_directory_uri(); ?>/images/kiyoh-9-2-logo@2x.png" alt="Kiyoh" />
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="left">
			
		</div>
		<div class="clearfix"></div>
	</section>
	<section class="---Funnel-form">
		<div class="right">
			<div class="result-box">
				<h1>Uw situatie</h1>
				<div class="form-group">
					<label>Auto merk</label>
					<?php echo ucfirst($current_funnel->car_brand); ?>
				</div>
				<?php
				if($current_funnel->car_type != ""):
				?>
				<div class="form-group">
					<label>Auto type</label>
					<?php echo ucfirst($current_funnel->car_type); ?>
				</div>
				<?php endif; ?>
				<div class="form-group">
					<label>Enkel / dubbele laadpaal</label>
					<?php echo ucfirst($current_funnel->singleDouble); ?>
				</div>
				<div class="form-group">
					<label>Locatie</label>
					<?php echo ucfirst($current_funnel->location); ?>
				</div>
				<div class="form-group">
					<label>Installatie</label>
					<?php echo ucfirst($current_funnel->install); ?>
				</div>
				<div class="form-group">
					<label>Zakelijk of priv&eacute;</label>
					<?php 
					$zakelijkPrive = '';
					switch(ucfirst($current_funnel->smart)) {
						case 'Ja':
							$zakelijkPrive = 'Zakelijk';
						break;
						case 'Nee':
							$zakelijkPrive = 'Priv&eacute;';
						break;
						case 'Weet niet / Adviseer mij':
							$zakelijkPrive = 'Nog niet bekend';
						break;
					}

						echo ucfirst($zakelijkPrive); 
					?>
				</div>
				<div class="form-group">
					<label>Zonnepanelen</label>
					<?php echo ucfirst($current_funnel->zonnepanelen); ?>
				</div>
				<?php
				if($current_funnel->extra_info_laadtijden == 1 ||
					$current_funnel->extra_info_personalisatie == 1 ||
					$current_funnel->extra_info_smartcharging == 1 ||
					$current_funnel->extra_info_facturatie):
				?>
				<div class="form-group last">
					<label>Extra informatie over</label>
					<ul>
						<?php
						if($current_funnel->extra_info_laadtijden == 1 ):
						?>
						<li><span>+</span> Laadtijden</li>
						<?php
						endif;
						?>

						<?php
						if($current_funnel->extra_info_personalisatie == 1 ):
						?>
						<li><span>+</span> Personalisatie</li>
						<?php
						endif;
						?>

						<?php
						if($current_funnel->extra_info_smartcharging == 1 ):
						?>
						<li><span>+</span> Smart charging</li>
						<?php
						endif;
						?>

						<?php
						if($current_funnel->extra_info_facturatie == 1 ):
						?>
						<li><span>+</span> Facturatie & Vergoedingen</li>
						<?php
						endif;
						?>
						
					</ul>
				</div>
				<?php
					endif;
				?>
			</div>
		</div>
        
		<div class="left">
			<h2>Alleen nog uw gegevens</h2><p>U ontvangt <strong> binnen 10 minuten</strong> advies per mail.</p>
			<div class="row large-row">
				<!-- <div class="column-2">
					<div class="form-group aanhef">
						<select name="name_title" data-validation="required" data-validation-error-msg="Kiest u een aanhef." class="form-control">
							<option value="" selected="selected">Aanhef</option>
							<option value="dhr.">Dhr.</option>
							<option value="mevr.">Mevr.</option>
						</select>
					</div>
				</div> -->
				<div class="column-2">
					<div class="form-group">
						<input class="form-control" autocomplete="off" data-validation="length" data-validation-length="3-100" type="text" name="firstname" value="" placeholder="Voornaam*" data-validation-error-msg="Voer uw voornaam in." />
					</div>
				</div>
				<div class="column-2">
					<div class="form-group">
						<input class="form-control" autocomplete="off" data-validation="length" data-validation-length="3-100" type="text" name="lastname" value="" placeholder="Achternaam*" data-validation-error-msg="Voer uw achternaam in." />
					</div>
				</div>
			</div>
			<div class="row large-row">
				<div class="column-2">
					<div class="form-group">
						<input class="form-control" autocomplete="off" type="tel" name="phone" data-validation="phone_number"  data-validation-error-msg="Voer een geldig telefoonnummer in." value="" placeholder="Telefoonnummer*" />
					</div>
				</div>
				<div class="column-2">
					<div class="form-group">
						<input class="form-control" autocomplete="off" type="text" name="emailadres" data-validation="email" data-validation-error-msg="Voer een geldig e-mailadres in." value="" placeholder="E-mailadres*" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="column-12">
					<div class="form-group">
						<textarea rows="5" name="comments" placeholder="Opmerkingen (optioneel)" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="column-12">
					<div class="form-group label-secure">
						* Deze pagina en uw contactgegevens zijn veilig.
					</div>
				</div>
			</div>
			<!-- <div class="row">
				<div class="column-12">
					<div class="form-group">
						<label>
							<input type="checkbox" name="optin_newsletter" value="yes">
							<span>Ja, ik wil op de hoogte blijven van nieuws in de EV markt.</span>
						</label>
						<div class="description">
							De EV markt verandert continue, met onze maandelijkse nieuwsbrief blijft u op de hoogte.
						</div>
					</div>	
					<div class="form-group last_agreement">
						<label>
							<input type="checkbox" name="optin_commercial" value="yes">
							<span>Ja, ik wil informatie ontvangen omtrent aanbiedingen, kortingen en winacties.</span>
						</label>
					</div>	
					<<p>* Uw gegevens worden alleen gebruikt om u van een persoonlijk laadadvies te voorzien. Wij houden daarnaast </p>
				</div>
			</div> -->
		<section class="---Funnel-buttons">
			<div class="container">
				<div class="button-right">
					<input class="btn" type="submit" name="giveAdvice" value="Top 3 laadpalen" />
				</div>
				<div class="button-left">
					<a href="<?php echo get_the_permalink(229); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/link-groen.svg" alt="Vorige stap" />Vorige stap</a>
				</div>
				<div class="clearfix"></div>
			</div>
		</section>
		</div>
		<div class="clearfix"></div>
		<div class="right">
			<div class="reviewBox">
				<?php
				echo get_template_part('partials/funnelReviews', 'page');
				?>
			</div>
		</div>
		<div class="left second">
			<div class="previous_orders">
			<?php
			$leadCount = "11.000";
			global $wpdb;
			$result = $wpdb->get_results( "SELECT id,counter FROM lkp_lead_counter WHERE id = 1", OBJECT );
			if($result && $result[0]->counter != null) {
				$leadCount = number_format($result[0]->counter, 0, ',', '.');
			}
			?>
				<h1>Al <span class="request_count"><?php echo $leadCount; ?></span> mensen gingen u voor</h1>
				<p>Bekijk de verschillende laadpaalmerken van onze klanten<br>die door ons is <strong>geadviseerd, geleverd en geïnstalleerd.</strong></p>
				<div class="arrow">
					<img src="<?php echo get_template_directory_uri().'/images/arrow-green-funnel.png'; ?>" alt="" />
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</section>
	</div>
</form>

<script type="text/javascript">
	var templateUrl = "<?php echo get_template_directory_uri(); ?>";
</script>
<?php
/*

<!-- <div class="form-group">
<div class="agree_row">
	<input name="product" type="checkbox" id="akkoord_evmarkt" name="akkoord_evmarkt" value="yes" />
	<label class="custom-label-wrap" for="akkoord_evmarkt">
		<span class="custom-check newsletter"></span> <span class="custom-label">Ja, ik wil op de hoogte blijven van nieuws in de EV markt.</span>
	</label>
</div>
<div class="description">
	De EV markt verandert continue, met onze maandelijkse nieuwsbrief blijft u op de hoogte. (We sturen u geen aanbiedingen, alleen relevante artikelen of nieuws) 
</div>
</div>
<div class="form-group">
<div class="description last">
	Ik geef Laadkompas toestemming om contact met me op te nemen m.b.t. mijn aanvraag.
	<a href="" title="Laadkompas Privacy Policy">Laadkompas Privacy Policy</a>
</div>
</div> -->
*/
get_footer('funnel');
?>