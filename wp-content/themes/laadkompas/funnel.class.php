<?php

class Funnel {
	public $car_brand;
	public $car_type;

	public $location;
	public $install;
	public $smart;
	public $singleDouble;
	public $zonnepanelen;

	public $extra_info_laadtijden;
	public $extra_info_personalisatie;
	public $extra_info_smartcharging;
	public $extra_info_facturatie;

	public $title;
	public $firstname;
	public $name;
	public $phone;
	public $email;
	public $comments;
	public $agree_ev;

	public $quoteUrl;

	public $optin_newsletter;
	public $optin_commercial;

	public $newsletter_txt;
	public $commercial_txt;

	public function __construct(
		$car_brand = null,
		$car_type = null,
		$location = null,
		$install = null,
		$smart = null,
		$singleDouble = null,
		$extra_info_laadtijden = null,
		$extra_info_personalisatie = null,
		$extra_info_smartcharging = null,
		$extra_info_facturatie = null,
		$title = null,
		$name = null,
		$phone = null,
		$email = null,
		$comments = null,
		$agree_ev = null,
		$quoteUrl = null,
		$firstname = null,
		$zonnepanelen = null,
		$optin_newsletter = null,
		$optin_commercial = null,
		$newsletter_txt = null,
		$commercial_txt = null
	)
	{
		if($car_brand != null)
			$this->car_brand = $car_brand;
		if($car_type != null)
			$this->car_type = $car_type;

		if($location != null)
			$this->location = $location;
		if($install != null)
			$this->install = $install;
		if($smart != null)
			$this->smart = $smart;
		if($singleDouble != null)
			$this->singleDouble = $singleDouble;

		if($extra_info_laadtijden != null)
			$this->extra_info_laadtijden = $extra_info_laadtijden;
		if($extra_info_personalisatie != null)
			$this->extra_info_personalisatie = $extra_info_personalisatie;
		if($extra_info_smartcharging != null)
			$this->extra_info_smartcharging = $extra_info_smartcharging;
		if($extra_info_facturatie != null)
			$this->extra_info_facturatie = $extra_info_facturatie;

		$this->title = '';

		if($name != null)
			$this->name = $name;
		if($phone != null)
			$this->phone = $phone;
		if($email != null)
			$this->email = $email;
		if($comments != null)
			$this->comments = $comments;
		if($agree_ev != null)
			$this->agree_ev = $agree_ev;
		if($quoteUrl != null)
			$this->quoteUrl = $quoteUrl;


		if($firstname != null)
			$this->firstname = $firstname;

		if($zonnepanelen != null)
			$this->zonnepanelen = $zonnepanelen;

		if($optin_newsletter != null)
			$this->optin_newsletter = $optin_newsletter;
		if($optin_commercial != null)
			$this->optin_commercial = $optin_commercial;

		
		if($newsletter_txt != null)
			$this->newsletter_txt = $newsletter_txt;
		if($commercial_txt != null)
			$this->commercial_txt = $commercial_txt;
	}

	public function decryptSession()
	{
		@session_start();

		$return = unserialize(decryptData($_SESSION['lkp_current_funnel']));

		return $return;
	}

	public function encryptSession()
	{
		@session_start();

		$encrypt = encryptData(serialize($this));

		$_SESSION['lkp_current_funnel'] = $encrypt;
		return true;
	}

	public function clearSession()
	{
		unset($_SESSION['lkp_current_funnel']);
	}

	public function getCarBrand()
	{
		return $this->car_brand;
	}

	public function getCarType()
	{
		return $this->car_type;
	}



	// public function insertCarValue($table='', $fields=array())
	// {
	// 	global $wpdb;
	// 	$out = null;
	// 	if($table != '' && count($fields) > 0)
	// 	{
	// 		$wpdb->insert($table, $fields);
	// 		$out = $wpdb->insert_id;
	// 	}
	// 	return $out;
	// }
}

?>