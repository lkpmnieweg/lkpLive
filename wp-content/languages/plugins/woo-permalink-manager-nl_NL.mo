��          t      �              
        $  �   6     �     �  .   �     
  ]   !  	     D  �     �     �     �  �      	   �     �  -   �     �  ]     	   b                
                       	                     Account Contact Us Permalink Manager Premmerce Permalink Manager for WooCommerce allows you to change WooCommerce permalink and remove product and product_category slugs from the URL. Products Settings The %s plugin requires %s plugin to be active! https://premmerce.com/ https://premmerce.com/woocommerce-permalink-manager-remove-shop-product-product-category-url/ premmerce PO-Revision-Date: 2020-11-02 10:03:32+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - Premmerce Permalink Manager for WooCommerce - Stable (latest release)
 Account Neem contact met ons op Permalink manager Premmerce  Permalink Manager voor WooCommerce staat je toe om de WooCommerce permalink aan te passen en de product en product_category slugs te verwijderen uit de URL. Producten Instellingen De %s plugin vereist dat %s plugin actief is! https://premmerce.com/ https://premmerce.com/woocommerce-permalink-manager-remove-shop-product-product-category-url/ premmerce 